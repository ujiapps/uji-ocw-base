CREATE OR REPLACE FORCE VIEW UJI_OCW.OCW_EXT_PERSONAS (ID, NOMBRE, APELLIDO1, APELLIDO2, EMAIL) AS
   select to_number (to_char (id)) id, trim (rpad (nombre, 1000)) nombre, trim (rpad (apellido1, 1000)) apellido1,
          trim (rpad (apellido2, 1000)) apellido2, trim (rpad (substr (cuenta, 1, instr (cuenta, '@') - 1), 1000))
                                                                                                                  email
   from   (select id, nombre, apellido1, apellido2, busca_cuenta (id) cuenta
           from   per_personas p
           where  exists (select 1
                          from   per_personas_subvinculos
                          where  svi_vin_id in (4, 9, 3, 11)
                          and    per_id = p.id)
           or     exists (select 1
                          from   per_personas_subvinculos
                          where  svi_vin_id = 5
                          and    svi_id = 4
                          and    per_id = p.id)
           or     exists (select 1
                          from   ocw_autores
                          where  persona_id = p.id
                          and    persona_id is not null)) x
   where  id <> 0
   and    cuenta is not null;
   
   
   
   