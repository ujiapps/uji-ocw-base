

alter table uji_ocw.ocw_cursos_materiales add (descripcion varchar2(1000));

ALTER TABLE UJI_OCW.OCW_GRUPOS MODIFY(DESCRIPCION  NULL);

ALTER TABLE UJI_OCW.OCW_GRUPOS ADD (nombre  VARCHAR2(100) NULL);

update uji_ocw.ocw_grupos
set nombre = descripcion;

update uji_ocw.ocw_grupos
set descripcion = null;

commit;

ALTER TABLE UJI_OCW.OCW_GRUPOS MODIFY(NOMBRE NOT NULL);



CREATE OR REPLACE FORCE VIEW UJI_OCW.OCW_VW_MATERIALES_POR_CURSO (CURSO_ID,
                                                                  CURSO_MATERIAL_ID,
                                                                  GRUPO_ID,
                                                                  TIPO,
                                                                  TITULO,
                                                                  ORDEN,
                                                                  NIVEL,
                                                                  DESCRIPCION,
                                                                  CONTENT_TYPE
                                                                 ) AS
   select cm.curso_id, cm.id curso_material_id, 0 grupo_id, 'MAT' tipo, m.titulo, cm.orden * 100000 orden,
                  1 nivel, cm.descripcion, content_type
           from   ocw_cursos_materiales cm,
                  ocw_materiales m
           where  cm.material_id = m.id
           and    (cm.grupo_id is null)
           union all
           select g.curso_id, 0 curso_material_id, g.id grupo_id, 'GRUPO' tipo, g.nombre titulo,
                  g.orden * 100000 orden, 1 nivel, descripcion, null content_type
           from   ocw_grupos g
           union all
           select cm.curso_id, cm.id curso_material_id, cm.grupo_id, 'MAT' tipo, m.titulo,
                  g.orden * 100000 + cm.orden orden, 2 nivel, cm.descripcion, content_type
           from   ocw_cursos_materiales cm,
                  ocw_materiales m,
                  ocw_grupos g
           where  cm.material_id = m.id
           and    cm.grupo_id = g.id
           and    cm.curso_id = g.curso_id;
		   
   
CREATE OR REPLACE FORCE VIEW UJI_OCW.OCW_VW_MATERIALES (ID,
                                                        TITULO,
                                                        NOMBRE_FICHERO,
                                                        URL,
                                                        DESCRIPCION,
                                                        FECHA,
                                                        PERSONA_ID,
                                                        NOMBRE_PERSONA,
                                                        IDIOMA_ID,
                                                        NOMBRE_IDIOMA,
                                                        LICENCIA_ID,
                                                        NOMBRE_LICENCIA
                                                       ) AS
   SELECT m.id, m.titulo, m.nombre_fichero, m.url, m.descripcion, m.fecha, m.persona_id, p.nombre_persona, m.idioma_id,
          i.nombre nombre_idioma, m.licencia_id, l.nombre_licencia
   FROM   uji_ocw.ocw_materiales m,
          (SELECT uji_ocw.ocw_ext_personas.id,
                  uji_ocw.ocw_ext_personas.nombre || ' ' || uji_ocw.ocw_ext_personas.apellido1 || ' '
                  || uji_ocw.ocw_ext_personas.apellido2 nombre_persona
           FROM   uji_ocw.ocw_ext_personas) p,
          uji_ocw.ocw_idiomas i,
          (SELECT l.id, li.titulo nombre_licencia
           FROM   uji_ocw.ocw_licencias l,
                  uji_ocw.ocw_licencias_idiomas li
            --,(SELECT * FROM uji_ocw.ocw_idiomas WHERE uji_ocw.ocw_idiomas.orden = 1
           -- ) i
           WHERE  l.id = li.licencia_id
                                       --AND li.idioma_id = i.id
          ) l
   WHERE  m.persona_id = p.id(+)
   AND    m.idioma_id = i.id(+)
   AND    m.licencia_id = l.id(+);   
   
   
CREATE OR REPLACE FORCE VIEW UJI_OCW.OCW_VW_CURSOS (ID,
                                                    NOMBRE,
                                                    ESTADO,
                                                    VISIBLE,
                                                    FECHA_CREACION,
                                                    FECHA_BAJA,
                                                    PERSONA_ID,
                                                    DESCRIPCION
                                                   ) AS
   SELECT c.id, ci.nombre, c.estado, c.visible, c.fecha_creacion, c.fecha_baja, c.persona_id, ci.descripcion
   FROM   ocw_cursos c,
          ocw_cursos_idiomas ci,
          ocw_idiomas i
   WHERE  c.id = ci.curso_id
   AND    ci.idioma_id = i.id
   AND    i.orden = (SELECT MIN (ocw_idiomas.orden)
                     FROM   ocw_idiomas
                     WHERE  ocw_idiomas.edicion = 'S');


CREATE OR REPLACE FORCE VIEW UJI_OCW.OCW_VW_ACCESOS_CURSOS (CURSO_ID, NUMERO_ACCESOS) AS
   select   curso_id, count (*) numero_accesos
   from     ocw_accesos
   where    curso_id is not null
   group by curso_id;








  
  
   