CREATE OR REPLACE VIEW uji_ocw.ocw_vw_nube_tags AS
SELECT id,
  nombre,
  SUM(cuantos) total,
  SUM(cursos) cursos,
  SUM(materiales) materiales
FROM
  (SELECT t.id,
    t.nombre,
    COUNT(*) cuantos,
    0 materiales,
    COUNT(*) cursos
  FROM ocw_tags t,
    ocw_cursos_tags ct
  WHERE t.id = ct.tag_id
  GROUP BY t.id,
    t.nombre
  UNION ALL
  SELECT t.id,
    t.nombre,
    COUNT(*) cuantos,
    COUNT(*) materiales,
    0 cursos
  FROM ocw_tags t,
    ocw_materiales_tags ct
  WHERE t.id = ct.tag_id
  GROUP BY t.id,
    t.nombre
  )
GROUP BY id,
  nombre ;




CREATE OR REPLACE FORCE VIEW UJI_OCW.OCW_EXT_PERSONAS (ID, NOMBRE, APELLIDO1, APELLIDO2, EMAIL) AS
select to_number (to_char (id)) id, trim (rpad (nombre, 1000)) nombre, trim (rpad (apellido1, 1000)) apellido1,
       trim (rpad (apellido2, 1000)) apellido2, trim (rpad (substr (cuenta, 1, instr (cuenta, '@') - 1), 1000)) email
from   (select id, nombre, apellido1, apellido2, busca_cuenta (id) cuenta
        from   per_personas p
        where  exists (select 1
                       from   per_personas_subvinculos
                       where  svi_vin_id in (4, 9, 3, 11)
                       and    per_id = p.id)
        or     exists (select 1
                       from   ocw_autores
                       where  persona_id = p.id
                       and    persona_id is not null)) x
where  id <> 0
and    cuenta is not null;



CREATE OR REPLACE VIEW uji_ocw.ocw_vw_cursos_detalle AS
SELECT c.id,
  'CA' iso,
  c.fecha_creacion,
  c.fecha_baja,
  c.estado,
  c.visible,
  NVL(ca.nombre, NVL(es.nombre, en.nombre)) nombre,
  NVL(ca.periodo, NVL(es.periodo, en.periodo)) periodo,
  NVL(ca.descripcion, NVL(es.descripcion, en.descripcion)) descripcion,
  DECODE(DBMS_LOB.getlength(ca.programa), 0, DECODE(DBMS_LOB.getlength(es.programa), 0, en.programa, es.programa), ca.programa) programa,
  DECODE(DBMS_LOB.getlength(ca.guia_aprendizaje), 0, DECODE(DBMS_LOB.getlength(es.guia_aprendizaje), 0, en.guia_aprendizaje, es.guia_aprendizaje), ca.guia_aprendizaje) guia_aprendizaje,
  p.nombre
  || ' '
  || p.apellido1
  || ' '
  || p.apellido2 nombre_creador,
  im.id id_imagen,
  im.content_type,
  tag.tag_id,
  tag.nombre tag,
  cat.nombre categoria,
  cat.categoria_id,
  cat.acronimo,
  (SELECT COUNT(*) numero_accesos
  FROM ocw_accesos a
  WHERE a.curso_id = c.id
  AND a.curso_id  IS NOT NULL
  ) numero_accesos
FROM ocw_cursos c,
  ocw_cursos_idiomas ci,
  ocw_idiomas i,
  ocw_ext_personas p,
  (SELECT ci1.*
  FROM ocw_cursos_idiomas ci1,
    ocw_idiomas i
  WHERE ci1.idioma_id = i.id
  AND (i.acronimo     = 'CA')
  ) ca,
  (SELECT ci2.*
  FROM ocw_cursos_idiomas ci2,
    ocw_idiomas i
  WHERE ci2.idioma_id = i.id
  AND (i.acronimo     = 'ES')
  ) es,
  (SELECT ci2.*
  FROM ocw_cursos_idiomas ci2,
    ocw_idiomas i
  WHERE ci2.idioma_id = i.id
  AND (i.acronimo     = 'EN')
  ) en,
  (SELECT *
  FROM ocw_cursos_imagenes im2
  WHERE im2.id IN
    (SELECT MIN(im3.id)
    FROM ocw_cursos_imagenes im3
    WHERE im2.curso_id = im3.curso_id
    )
  ) im,
  (SELECT ct.curso_id,
    ct.tag_id,
    ta.nombre
  FROM ocw_cursos_tags ct,
    ocw_tags ta
  WHERE ct.tag_id = ta.id
  ) tag,
  (SELECT cidi.nombre,
    cc.curso_id,
    cc.categoria_id,
    'CA' acronimo
  FROM ocw_cursos_categorias cc,
    ocw_categorias_idiomas cidi,
    ocw_idiomas idi
  WHERE cc.categoria_id = cidi.categoria_id
  AND cidi.idioma_id    = idi.id
  AND (idi.acronimo     = 'CA')
  ) cat
WHERE c.id       = ci.curso_id
AND ci.idioma_id = i.id
AND c.id         = ca.curso_id(+)
AND c.id         = es.curso_id(+)
AND c.id         = en.curso_id(+)
AND c.persona_id = p.id
AND c.id         = im.curso_id(+)
AND c.id         = tag.curso_id(+)
AND c.id         = cat.curso_id(+)
AND (i.orden     =
  (SELECT MIN(ocw_idiomas.orden)
  FROM ocw_idiomas
  WHERE ocw_idiomas.edicion = 'S'
  ))
UNION ALL
SELECT c.id,
  'ES' iso,
  c.fecha_creacion,
  c.fecha_baja,
  c.estado,
  c.visible,
  NVL(es.nombre, NVL(ca.nombre, en.nombre)) nombre,
  NVL(es.periodo, NVL(ca.periodo, en.periodo)) periodo,
  NVL(es.descripcion, NVL(ca.descripcion, en.descripcion)) descripcion,
  DECODE(DBMS_LOB.getlength(es.programa), 0, DECODE(DBMS_LOB.getlength(ca.programa), 0, en.programa, ca.programa), es.programa) programa,
  DECODE(DBMS_LOB.getlength(es.guia_aprendizaje), 0, DECODE(DBMS_LOB.getlength(ca.guia_aprendizaje), 0, en.guia_aprendizaje, ca.guia_aprendizaje), es.guia_aprendizaje) guia_aprendizaje,
  p.nombre
  || ' '
  || p.apellido1
  || ' '
  || p.apellido2 nombre_creador,
  im.id id_imagen,
  im.content_type,
  tag.tag_id,
  tag.nombre tag,
  cat.nombre categoria,
  cat.categoria_id,
  cat.acronimo,
  (SELECT COUNT(*) numero_accesos
  FROM ocw_accesos a
  WHERE a.curso_id = c.id
  AND a.curso_id  IS NOT NULL
  ) numero_accesos
FROM ocw_cursos c,
  ocw_cursos_idiomas ci,
  ocw_idiomas i,
  ocw_ext_personas p,
  (SELECT ci1.*
  FROM ocw_cursos_idiomas ci1,
    ocw_idiomas i
  WHERE ci1.idioma_id = i.id
  AND (i.acronimo     = 'CA')
  ) ca,
  (SELECT ci2.*
  FROM ocw_cursos_idiomas ci2,
    ocw_idiomas i
  WHERE ci2.idioma_id = i.id
  AND (i.acronimo     = 'ES')
  ) es,
  (SELECT ci2.*
  FROM ocw_cursos_idiomas ci2,
    ocw_idiomas i
  WHERE ci2.idioma_id = i.id
  AND (i.acronimo     = 'EN')
  ) en,
  (SELECT *
  FROM ocw_cursos_imagenes im2
  WHERE im2.id IN
    (SELECT MIN(im3.id)
    FROM ocw_cursos_imagenes im3
    WHERE im2.curso_id = im3.curso_id
    )
  ) im,
  (SELECT ct.curso_id,
    ct.tag_id,
    ta.nombre
  FROM ocw_cursos_tags ct,
    ocw_tags ta
  WHERE ct.tag_id = ta.id
  ) tag,
  (SELECT cidi.nombre,
    cc.curso_id,
    cc.categoria_id,
    'ES' acronimo
  FROM ocw_cursos_categorias cc,
    ocw_categorias_idiomas cidi,
    ocw_idiomas idi
  WHERE cc.categoria_id = cidi.categoria_id
  AND cidi.idioma_id    = idi.id
  AND (idi.acronimo     = 'ES')
  ) cat
WHERE c.id       = ci.curso_id
AND ci.idioma_id = i.id
AND c.id         = ca.curso_id(+)
AND c.id         = es.curso_id(+)
AND c.id         = en.curso_id(+)
AND c.persona_id = p.id
AND c.id         = im.curso_id(+)
AND c.id         = tag.curso_id(+)
AND c.id         = cat.curso_id(+)
AND (i.orden     =
  (SELECT MIN(ocw_idiomas.orden)
  FROM ocw_idiomas
  WHERE ocw_idiomas.edicion = 'S'
  ))
UNION ALL
SELECT c.id,
  'EN' iso,
  c.fecha_creacion,
  c.fecha_baja,
  c.estado,
  c.visible,
  NVL(en.nombre, NVL(es.nombre, ca.nombre)) nombre,
  NVL(en.periodo, NVL(es.periodo, ca.periodo)) periodo,
  NVL(en.descripcion, NVL(es.descripcion, ca.descripcion)) descripcion,
  DECODE(DBMS_LOB.getlength(en.programa), 0, DECODE(DBMS_LOB.getlength(es.programa), 0, ca.programa, es.programa), en.programa) programa,
  DECODE(DBMS_LOB.getlength(en.guia_aprendizaje), 0, DECODE(DBMS_LOB.getlength(es.guia_aprendizaje), 0, ca.guia_aprendizaje, es.guia_aprendizaje), en.guia_aprendizaje) guia_aprendizaje,
  p.nombre
  || ' '
  || p.apellido1
  || ' '
  || p.apellido2 nombre_creador,
  im.id id_imagen,
  im.content_type,
  tag.tag_id,
  tag.nombre tag,
  cat.nombre categoria,
  cat.categoria_id,
  cat.acronimo,
  (SELECT COUNT(*) numero_accesos
  FROM ocw_accesos a
  WHERE a.curso_id = c.id
  AND a.curso_id  IS NOT NULL
  ) numero_accesos
FROM ocw_cursos c,
  ocw_cursos_idiomas ci,
  ocw_idiomas i,
  ocw_ext_personas p,
  (SELECT ci1.*
  FROM ocw_cursos_idiomas ci1,
    ocw_idiomas i
  WHERE ci1.idioma_id = i.id
  AND (i.acronimo     = 'CA')
  ) ca,
  (SELECT ci2.*
  FROM ocw_cursos_idiomas ci2,
    ocw_idiomas i
  WHERE ci2.idioma_id = i.id
  AND (i.acronimo     = 'ES')
  ) es,
  (SELECT ci2.*
  FROM ocw_cursos_idiomas ci2,
    ocw_idiomas i
  WHERE ci2.idioma_id = i.id
  AND (i.acronimo     = 'EN')
  ) en,
  (SELECT *
  FROM ocw_cursos_imagenes im2
  WHERE im2.id IN
    (SELECT MIN(im3.id)
    FROM ocw_cursos_imagenes im3
    WHERE im2.curso_id = im3.curso_id
    )
  ) im,
  (SELECT ct.curso_id,
    ct.tag_id,
    ta.nombre
  FROM ocw_cursos_tags ct,
    ocw_tags ta
  WHERE ct.tag_id = ta.id
  ) tag,
  (SELECT cidi.nombre,
    cc.curso_id,
    cc.categoria_id,
    'EN' acronimo
  FROM ocw_cursos_categorias cc,
    ocw_categorias_idiomas cidi,
    ocw_idiomas idi
  WHERE cc.categoria_id = cidi.categoria_id
  AND cidi.idioma_id    = idi.id
  AND (idi.acronimo     = 'EN')
  ) cat
WHERE c.id       = ci.curso_id
AND ci.idioma_id = i.id
AND c.id         = ca.curso_id(+)
AND c.id         = es.curso_id(+)
AND c.id         = en.curso_id(+)
AND c.persona_id = p.id
AND c.id         = im.curso_id(+)
AND c.id         = tag.curso_id(+)
AND c.id         = cat.curso_id(+)
AND (i.orden     =
  (SELECT MIN(ocw_idiomas.orden)
  FROM ocw_idiomas
  WHERE ocw_idiomas.edicion = 'S'
  )) ;




