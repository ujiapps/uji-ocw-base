-- Generado por Oracle SQL Developer Data Modeler 3.1.0.687
--   en:        2012-03-27 17:05:54 CEST
--   sitio:      Oracle Database 11g
--   tipo:      Oracle Database 11g



DROP VIEW uji_ocw.ocw_vw_autores 
;
DROP VIEW uji_ocw.ocw_vw_categorias 
;
DROP VIEW uji_ocw.ocw_vw_cursos 
;
DROP VIEW uji_ocw.ocw_vw_licencias 
;
DROP VIEW ocw_vw_materiales 
;
DROP VIEW uji_ocw.ocw_vw_metadatos 
;
DROP TABLE uji_ocw.ocw_accesos CASCADE CONSTRAINTS 
;
DROP TABLE uji_ocw.ocw_autores CASCADE CONSTRAINTS 
;
DROP TABLE uji_ocw.ocw_categorias CASCADE CONSTRAINTS 
;
DROP TABLE uji_ocw.ocw_categorias_idiomas CASCADE CONSTRAINTS 
;
DROP TABLE uji_ocw.ocw_cursos CASCADE CONSTRAINTS 
;
DROP TABLE uji_ocw.ocw_cursos_asignaturas CASCADE CONSTRAINTS 
;
DROP TABLE uji_ocw.ocw_cursos_autorizados CASCADE CONSTRAINTS 
;
DROP TABLE uji_ocw.ocw_cursos_idiomas CASCADE CONSTRAINTS 
;
DROP TABLE uji_ocw.ocw_cursos_imagenes CASCADE CONSTRAINTS 
;
DROP TABLE uji_ocw.ocw_cursos_materiales CASCADE CONSTRAINTS 
;
DROP TABLE uji_ocw.ocw_cursos_tags CASCADE CONSTRAINTS 
;
DROP TABLE uji_ocw.ocw_errores_workflow CASCADE CONSTRAINTS 
;
DROP TABLE uji_ocw.ocw_ext_asignaturas CASCADE CONSTRAINTS 
;
DROP TABLE uji_ocw.ocw_ext_personas CASCADE CONSTRAINTS 
;
DROP TABLE uji_ocw.ocw_idiomas CASCADE CONSTRAINTS 
;
DROP TABLE uji_ocw.ocw_licencias CASCADE CONSTRAINTS 
;
DROP TABLE uji_ocw.ocw_licencias_idiomas CASCADE CONSTRAINTS 
;
DROP TABLE uji_ocw.ocw_materiales CASCADE CONSTRAINTS 
;
DROP TABLE uji_ocw.ocw_materiales_autores CASCADE CONSTRAINTS 
;
DROP TABLE uji_ocw.ocw_materiales_estados CASCADE CONSTRAINTS 
;
DROP TABLE uji_ocw.ocw_materiales_metadatos CASCADE CONSTRAINTS 
;
DROP TABLE uji_ocw.ocw_materiales_tags CASCADE CONSTRAINTS 
;
DROP TABLE uji_ocw.ocw_metadatos CASCADE CONSTRAINTS 
;
DROP TABLE uji_ocw.ocw_metadatos_idiomas CASCADE CONSTRAINTS 
;
DROP TABLE uji_ocw.ocw_metadatos_valores CASCADE CONSTRAINTS 
;
DROP TABLE uji_ocw.ocw_tags CASCADE CONSTRAINTS 
;
DROP TABLE uji_ocw.ocw_usuarios_bonita CASCADE CONSTRAINTS 
;
DROP TABLE uji_ocw.ocw_usuarios_roles_bonita CASCADE CONSTRAINTS 
;
CREATE TABLE uji_ocw.ocw_accesos 
    ( 
     id NUMBER  NOT NULL , 
     curso_id NUMBER , 
     material_id NUMBER , 
     fecha DATE , 
     ip VARCHAR2 (1000) , 
     Descripcion VARCHAR2 (1000) 
    ) 
;


CREATE INDEX uji_ocw.ocw_accesos_cur_IDX ON uji_ocw.ocw_accesos 
    ( 
     curso_id ASC 
    ) 
;
CREATE INDEX uji_ocw.ocw_accesos_mat_IDX ON uji_ocw.ocw_accesos 
    ( 
     material_id ASC 
    ) 
;

ALTER TABLE uji_ocw.ocw_accesos 
    ADD CONSTRAINT ocw_accesos_cursos_PK PRIMARY KEY ( id ) ;


CREATE TABLE uji_ocw.ocw_autores 
    ( 
     id NUMBER  NOT NULL , 
     nombre VARCHAR2 (1000) , 
     apellido1 VARCHAR2 (1000) , 
     apellido2 VARCHAR2 (1000) , 
     email VARCHAR2 (1000) , 
     persona_id NUMBER 
    ) 
;



ALTER TABLE uji_ocw.ocw_autores 
    ADD CONSTRAINT ocw_autores_PK PRIMARY KEY ( id ) ;


CREATE TABLE uji_ocw.ocw_categorias 
    ( 
     id NUMBER  NOT NULL , 
     orden NUMBER 
    ) 
;



ALTER TABLE uji_ocw.ocw_categorias 
    ADD CONSTRAINT ocw_categorias_PK PRIMARY KEY ( id ) ;


CREATE TABLE uji_ocw.ocw_categorias_idiomas 
    ( 
     id NUMBER  NOT NULL , 
     categoria_id NUMBER  NOT NULL , 
     idioma_id NUMBER  NOT NULL , 
     nombre VARCHAR2 (1000) 
    ) 
;


CREATE INDEX uji_ocw.ocw_categorias_idiomas_cat_IDX ON uji_ocw.ocw_categorias_idiomas 
    ( 
     categoria_id ASC 
    ) 
;
CREATE INDEX uji_ocw.ocw_categorias_idiomas_idi_IDX ON uji_ocw.ocw_categorias_idiomas 
    ( 
     idioma_id ASC 
    ) 
;

ALTER TABLE uji_ocw.ocw_categorias_idiomas 
    ADD CONSTRAINT ocw_categorias_idiomas_PK PRIMARY KEY ( id ) ;


CREATE TABLE uji_ocw.ocw_cursos 
    ( 
     id NUMBER  NOT NULL , 
     categoria_id NUMBER  NOT NULL , 
     estado VARCHAR2 (10) , 
     visible VARCHAR2 (1) DEFAULT 'N' CHECK ( visible IN ('N', 'S')) , 
     fecha_creacion DATE  NOT NULL , 
     fecha_baja DATE , 
     persona_id NUMBER  NOT NULL 
    ) 
;



ALTER TABLE uji_ocw.ocw_cursos 
    ADD CONSTRAINT ocw_cursos_estados_CK 
    CHECK (estado in ('I','CF','F'))
;


ALTER TABLE uji_ocw.ocw_cursos 
    ADD CONSTRAINT ocw_cursos_PK PRIMARY KEY ( id ) ;


CREATE TABLE uji_ocw.ocw_cursos_asignaturas 
    ( 
     id NUMBER  NOT NULL , 
     curso_id NUMBER  NOT NULL , 
     asignatura_id VARCHAR2 (10)  NOT NULL 
    ) 
;


CREATE INDEX uji_ocw.ocw_cursos_asignaturas_cur_IDX ON uji_ocw.ocw_cursos_asignaturas 
    ( 
     curso_id ASC 
    ) 
;
CREATE INDEX uji_ocw.ocw_cursos_asignaturas_asi_IDX ON uji_ocw.ocw_cursos_asignaturas 
    ( 
     asignatura_id ASC 
    ) 
;

ALTER TABLE uji_ocw.ocw_cursos_asignaturas 
    ADD CONSTRAINT ocw_cursos_asignaturas_PK PRIMARY KEY ( id ) ;


CREATE TABLE uji_ocw.ocw_cursos_autorizados 
    ( 
     id NUMBER  NOT NULL , 
     curso_id NUMBER  NOT NULL , 
     persona_id NUMBER 
    ) 
;


CREATE INDEX uji_ocw.ocw_cursos_aut_cur_IDX ON uji_ocw.ocw_cursos_autorizados 
    ( 
     curso_id ASC 
    ) 
;

ALTER TABLE uji_ocw.ocw_cursos_autorizados 
    ADD CONSTRAINT ocw_cursos_autorizados_PK PRIMARY KEY ( id ) ;


CREATE TABLE uji_ocw.ocw_cursos_idiomas 
    ( 
     id NUMBER  NOT NULL , 
     curso_id NUMBER  NOT NULL , 
     idioma_id NUMBER  NOT NULL , 
     nombre VARCHAR2 (1000) , 
     periodo VARCHAR2 (1000) , 
     descripcion VARCHAR2 (4000) , 
     programa CLOB , 
     guia_aprendizaje CLOB , 
     orden NUMBER  NOT NULL 
    ) 
;


CREATE INDEX uji_ocw.ocw_cursos_det_idiomas_cur_IDX ON uji_ocw.ocw_cursos_idiomas 
    ( 
     curso_id ASC 
    ) 
;
CREATE INDEX uji_ocw.ocw_cursos_det_idiomas_idi_IDX ON uji_ocw.ocw_cursos_idiomas 
    ( 
     idioma_id ASC 
    ) 
;

ALTER TABLE uji_ocw.ocw_cursos_idiomas 
    ADD CONSTRAINT ocw_cursos_det_idiomas_PK PRIMARY KEY ( id ) ;


CREATE TABLE uji_ocw.ocw_cursos_imagenes 
    ( 
     id NUMBER  NOT NULL , 
     curso_id NUMBER  NOT NULL , 
     imagen BLOB , 
     content_type VARCHAR2 (100) 
    ) 
;


CREATE INDEX uji_ocw.ocw_cursos_imagenes__IDX ON uji_ocw.ocw_cursos_imagenes 
    ( 
     curso_id ASC 
    ) 
;

ALTER TABLE uji_ocw.ocw_cursos_imagenes 
    ADD CONSTRAINT ocw_cursos_imagenes_PK PRIMARY KEY ( id ) ;


CREATE TABLE uji_ocw.ocw_cursos_materiales 
    ( 
     id NUMBER  NOT NULL , 
     curso_id NUMBER  NOT NULL , 
     material_id NUMBER  NOT NULL 
    ) 
;


CREATE INDEX uji_ocw.ocw_cursos_materiales_cur_IDX ON uji_ocw.ocw_cursos_materiales 
    ( 
     curso_id ASC 
    ) 
;
CREATE INDEX uji_ocw.ocw_cursos_materiales_mat_IDX ON uji_ocw.ocw_cursos_materiales 
    ( 
     material_id ASC 
    ) 
;

ALTER TABLE uji_ocw.ocw_cursos_materiales 
    ADD CONSTRAINT ocw_cursos_materiales_PK PRIMARY KEY ( id ) ;


CREATE TABLE uji_ocw.ocw_cursos_tags 
    ( 
     id NUMBER  NOT NULL , 
     curso_id NUMBER  NOT NULL , 
     tag_id NUMBER  NOT NULL 
    ) 
;


CREATE INDEX uji_ocw.ocw_cursos_tags_cur_IDX ON uji_ocw.ocw_cursos_tags 
    ( 
     curso_id ASC 
    ) 
;
CREATE INDEX uji_ocw.ocw_cursos_tags_tag_IDX ON uji_ocw.ocw_cursos_tags 
    ( 
     tag_id ASC 
    ) 
;

ALTER TABLE uji_ocw.ocw_cursos_tags 
    ADD CONSTRAINT ocw_cursos_tags_PK PRIMARY KEY ( id ) ;


CREATE TABLE uji_ocw.ocw_errores_workflow 
    ( 
     id NUMBER  NOT NULL , 
     id_proceso VARCHAR2 (1000) , 
     id_actividad VARCHAR2 (1000) , 
     error VARCHAR2 (2000) , 
     fecha VARCHAR2 (1000) 
    ) 
;



ALTER TABLE uji_ocw.ocw_errores_workflow 
    ADD CONSTRAINT ocw_errores_workflow_PK PRIMARY KEY ( id ) ;


CREATE TABLE uji_ocw.ocw_ext_asignaturas 
    ( 
     id VARCHAR2 (10)  NOT NULL , 
     nombre_ca VARCHAR2 (1000)  NOT NULL , 
     nombre_es VARCHAR2 (1000)  NOT NULL , 
     nombre_uk VARCHAR2 (1000) 
    ) 
;



ALTER TABLE uji_ocw.ocw_ext_asignaturas 
    ADD CONSTRAINT ocw_asignaturas_PK PRIMARY KEY ( id ) ;


CREATE TABLE uji_ocw.ocw_ext_personas 
    ( 
     id NUMBER  NOT NULL , 
     nombre VARCHAR2 (1000) , 
     apellido1 VARCHAR2 (1000) , 
     apellido2 VARCHAR2 (1000) , 
     email VARCHAR2 (1000) 
    ) 
;



ALTER TABLE uji_ocw.ocw_ext_personas 
    ADD CONSTRAINT ocw_ext_personas_PK PRIMARY KEY ( id ) ;


CREATE TABLE uji_ocw.ocw_idiomas 
    ( 
     id NUMBER  NOT NULL , 
     nombre VARCHAR2 (1000)  NOT NULL , 
     acronimo VARCHAR2 (10)  NOT NULL , 
     orden NUMBER  NOT NULL , 
     edicion VARCHAR2 (1) DEFAULT 'N'  NOT NULL CHECK ( edicion IN ('N', 'S')) 
    ) 
;



ALTER TABLE uji_ocw.ocw_idiomas 
    ADD CONSTRAINT ocw_idiomas_PK PRIMARY KEY ( id ) ;


CREATE TABLE uji_ocw.ocw_licencias 
    ( 
     id NUMBER  NOT NULL , 
     codigo VARCHAR2 (1000) , 
     imagen BLOB , 
     content_type VARCHAR2 (100) 
    ) 
;



ALTER TABLE uji_ocw.ocw_licencias 
    ADD CONSTRAINT ocw_licencias_PK PRIMARY KEY ( id ) ;


CREATE TABLE uji_ocw.ocw_licencias_idiomas 
    ( 
     id NUMBER  NOT NULL , 
     licencia_id NUMBER  NOT NULL , 
     idioma_id NUMBER  NOT NULL , 
     titulo VARCHAR2 (1000)  NOT NULL , 
     contenido CLOB , 
     url VARCHAR2 (2000) 
    ) 
;


CREATE INDEX uji_ocw.ocw_licencias_idiomas_lic_IDX ON uji_ocw.ocw_licencias_idiomas 
    ( 
     licencia_id ASC 
    ) 
;
CREATE INDEX uji_ocw.ocw_licencias_idiomas_idi_IDX ON uji_ocw.ocw_licencias_idiomas 
    ( 
     idioma_id ASC 
    ) 
;

ALTER TABLE uji_ocw.ocw_licencias_idiomas 
    ADD CONSTRAINT ocw_licencias_idiomas_PK PRIMARY KEY ( id ) ;


CREATE TABLE uji_ocw.ocw_materiales 
    ( 
     id NUMBER  NOT NULL , 
     titulo VARCHAR2 (1000)  NOT NULL , 
     nombre_fichero VARCHAR2 (1000) , 
     url VARCHAR2 (1000) , 
     descripcion VARCHAR2 (4000) , 
     content_type VARCHAR2 (1000) , 
     documento BLOB , 
     fecha DATE , 
     handle_id VARCHAR2 (1000) , 
     persona_id NUMBER  NOT NULL , 
     xml CLOB , 
     licencia_id NUMBER  NOT NULL , 
     idioma_id NUMBER  NOT NULL 
    ) 
;



ALTER TABLE uji_ocw.ocw_materiales 
    ADD CONSTRAINT ocw_materiales_url_fich_UK 
    CHECK ((url is null and nombre_fichero is not null) 
or
(url is not null and nombre_fichero is null))
;

CREATE INDEX uji_ocw.ocw_materiales_lic_IDX ON uji_ocw.ocw_materiales 
    ( 
     licencia_id ASC 
    ) 
;
CREATE INDEX uji_ocw.ocw_materiales_idi_IDX ON uji_ocw.ocw_materiales 
    ( 
     idioma_id ASC 
    ) 
;

ALTER TABLE uji_ocw.ocw_materiales 
    ADD CONSTRAINT ocw_materiales_PK PRIMARY KEY ( id ) ;


CREATE TABLE uji_ocw.ocw_materiales_autores 
    ( 
     id NUMBER  NOT NULL , 
     material_id NUMBER  NOT NULL , 
     autor_id NUMBER  NOT NULL , 
     estado VARCHAR2 (10) , 
     hash_si VARCHAR2 (1000) , 
     hash_no VARCHAR2 (1000) , 
     hash VARCHAR2 (1000) 
    ) 
;


CREATE INDEX uji_ocw.ocw_materiales_autores_mat_IDX ON uji_ocw.ocw_materiales_autores 
    ( 
     material_id ASC 
    ) 
;
CREATE INDEX uji_ocw.ocw_materiales_autores_per_IDX ON uji_ocw.ocw_materiales_autores 
    ( 
     autor_id ASC 
    ) 
;

ALTER TABLE uji_ocw.ocw_materiales_autores 
    ADD CONSTRAINT ocw_materiales_autores_PK PRIMARY KEY ( id ) ;


CREATE TABLE uji_ocw.ocw_materiales_estados 
    ( 
     id NUMBER  NOT NULL , 
     material_id NUMBER  NOT NULL , 
     estado VARCHAR2 (10) , 
     inicio_publicacion NUMBER , 
     id_proceso VARCHAR2 (1000) 
    ) 
;


CREATE INDEX uji_ocw.ocw_materiales_estados__IDX ON uji_ocw.ocw_materiales_estados 
    ( 
     material_id ASC 
    ) 
;

ALTER TABLE uji_ocw.ocw_materiales_estados 
    ADD CONSTRAINT ocw_materiales_estados_PK PRIMARY KEY ( id ) ;


CREATE TABLE uji_ocw.ocw_materiales_metadatos 
    ( 
     id NUMBER  NOT NULL , 
     material_id NUMBER  NOT NULL , 
     metadato_id NUMBER  NOT NULL , 
     valor VARCHAR2 (1000)  NOT NULL 
    ) 
;


CREATE INDEX uji_ocw.ocw_materiales_met_mat_IDX ON uji_ocw.ocw_materiales_metadatos 
    ( 
     material_id ASC 
    ) 
;
CREATE INDEX uji_ocw.ocw_materiales_met_met_IDX ON uji_ocw.ocw_materiales_metadatos 
    ( 
     metadato_id ASC 
    ) 
;

ALTER TABLE uji_ocw.ocw_materiales_metadatos 
    ADD CONSTRAINT ocw_materiales_metadatos_PK PRIMARY KEY ( id ) ;


CREATE TABLE uji_ocw.ocw_materiales_tags 
    ( 
     id NUMBER  NOT NULL , 
     material_id NUMBER  NOT NULL , 
     tag_id NUMBER  NOT NULL 
    ) 
;


CREATE INDEX uji_ocw.ocw_materiales_tags_mat_IDX ON uji_ocw.ocw_materiales_tags 
    ( 
     material_id ASC 
    ) 
;
CREATE INDEX uji_ocw.ocw_materiales_tags_tags_IDX ON uji_ocw.ocw_materiales_tags 
    ( 
     tag_id ASC 
    ) 
;

ALTER TABLE uji_ocw.ocw_materiales_tags 
    ADD CONSTRAINT ocw_materiales_tags_PK PRIMARY KEY ( id ) ;


CREATE TABLE uji_ocw.ocw_metadatos 
    ( 
     id NUMBER  NOT NULL , 
     limitado VARCHAR2 (1) DEFAULT 'N'  NOT NULL CHECK ( limitado IN ('N', 'S')) , 
     obligatorio VARCHAR2 (1) DEFAULT 'S'  NOT NULL CHECK ( obligatorio IN ('N', 'S')) , 
     tag VARCHAR2 (1000) 
    ) 
;



ALTER TABLE uji_ocw.ocw_metadatos 
    ADD CONSTRAINT ocw_metadatos_PK PRIMARY KEY ( id ) ;


CREATE TABLE uji_ocw.ocw_metadatos_idiomas 
    ( 
     id NUMBER  NOT NULL , 
     metadato_id NUMBER  NOT NULL , 
     idioma_id NUMBER  NOT NULL , 
     nombre VARCHAR2 (1000) 
    ) 
;


CREATE INDEX uji_ocw.ocw_metadatos_idiomas_met_IDX ON uji_ocw.ocw_metadatos_idiomas 
    ( 
     metadato_id ASC 
    ) 
;
CREATE INDEX uji_ocw.ocw_metadatos_idiomas_idi_IDX ON uji_ocw.ocw_metadatos_idiomas 
    ( 
     idioma_id ASC 
    ) 
;

ALTER TABLE uji_ocw.ocw_metadatos_idiomas 
    ADD CONSTRAINT ocw_metadatos_idiomas_PK PRIMARY KEY ( id ) ;


CREATE TABLE uji_ocw.ocw_metadatos_valores 
    ( 
     id NUMBER  NOT NULL , 
     metadato_id NUMBER  NOT NULL , 
     valor VARCHAR2 (1000) , 
     orden NUMBER 
    ) 
;


CREATE INDEX uji_ocw.ocw_metadatos_valores__IDX ON uji_ocw.ocw_metadatos_valores 
    ( 
     metadato_id ASC 
    ) 
;

ALTER TABLE uji_ocw.ocw_metadatos_valores 
    ADD CONSTRAINT ocw_metadatos_valores_PK PRIMARY KEY ( id ) ;


CREATE TABLE uji_ocw.ocw_tags 
    ( 
     id NUMBER  NOT NULL , 
     nombre VARCHAR2 (1000)  NOT NULL 
    ) 
;



ALTER TABLE uji_ocw.ocw_tags 
    ADD CONSTRAINT ocw_tags_PK PRIMARY KEY ( id ) ;


CREATE TABLE uji_ocw.ocw_usuarios_bonita 
    ( 
     id NUMBER  NOT NULL , 
     nombre VARCHAR2 (1000) , 
     email VARCHAR2 (1000) , 
     nombre_completo VARCHAR2 (1000) 
    ) 
;



ALTER TABLE uji_ocw.ocw_usuarios_bonita 
    ADD CONSTRAINT ocw_usuarios_bonita_PK PRIMARY KEY ( id ) ;


CREATE TABLE uji_ocw.ocw_usuarios_roles_bonita 
    ( 
     id NUMBER  NOT NULL , 
     usuario_id NUMBER  NOT NULL , 
     rol VARCHAR2 (1000)  NOT NULL 
    ) 
;


CREATE INDEX uji_ocw.ocw_usuarios_roles_b_usu_IDX ON uji_ocw.ocw_usuarios_roles_bonita 
    ( 
     usuario_id ASC 
    ) 
;

ALTER TABLE uji_ocw.ocw_usuarios_roles_bonita 
    ADD CONSTRAINT ocw_usuarios_roles_bonita_PK PRIMARY KEY ( id ) ;



ALTER TABLE uji_ocw.ocw_accesos 
    ADD CONSTRAINT ocw_accesos_cur_FK FOREIGN KEY 
    ( 
     curso_id
    ) 
    REFERENCES uji_ocw.ocw_cursos 
    ( 
     id
    ) 
;


ALTER TABLE uji_ocw.ocw_accesos 
    ADD CONSTRAINT ocw_accesos_mat_FK FOREIGN KEY 
    ( 
     material_id
    ) 
    REFERENCES uji_ocw.ocw_materiales 
    ( 
     id
    ) 
;


ALTER TABLE uji_ocw.ocw_categorias_idiomas 
    ADD CONSTRAINT ocw_categorias_idi_cat_FK FOREIGN KEY 
    ( 
     categoria_id
    ) 
    REFERENCES uji_ocw.ocw_categorias 
    ( 
     id
    ) 
;


ALTER TABLE uji_ocw.ocw_categorias_idiomas 
    ADD CONSTRAINT ocw_categorias_idi_idi_FK FOREIGN KEY 
    ( 
     idioma_id
    ) 
    REFERENCES uji_ocw.ocw_idiomas 
    ( 
     id
    ) 
;


ALTER TABLE uji_ocw.ocw_cursos_asignaturas 
    ADD CONSTRAINT ocw_cursos_asignaturas_asi_FK FOREIGN KEY 
    ( 
     asignatura_id
    ) 
    REFERENCES uji_ocw.ocw_ext_asignaturas 
    ( 
     id
    ) 
;


ALTER TABLE uji_ocw.ocw_cursos_asignaturas 
    ADD CONSTRAINT ocw_cursos_asignaturas_cur_FK FOREIGN KEY 
    ( 
     curso_id
    ) 
    REFERENCES uji_ocw.ocw_cursos 
    ( 
     id
    ) 
;


ALTER TABLE uji_ocw.ocw_cursos_autorizados 
    ADD CONSTRAINT ocw_cursos_aut_cur_FK FOREIGN KEY 
    ( 
     curso_id
    ) 
    REFERENCES uji_ocw.ocw_cursos 
    ( 
     id
    ) 
;


ALTER TABLE uji_ocw.ocw_cursos 
    ADD CONSTRAINT ocw_cursos_cat_FK FOREIGN KEY 
    ( 
     categoria_id
    ) 
    REFERENCES uji_ocw.ocw_categorias 
    ( 
     id
    ) 
;


ALTER TABLE uji_ocw.ocw_cursos_idiomas 
    ADD CONSTRAINT ocw_cursos_det_idiomas_cur_FK FOREIGN KEY 
    ( 
     curso_id
    ) 
    REFERENCES uji_ocw.ocw_cursos 
    ( 
     id
    ) 
;


ALTER TABLE uji_ocw.ocw_cursos_idiomas 
    ADD CONSTRAINT ocw_cursos_det_idiomas_idi_FK FOREIGN KEY 
    ( 
     idioma_id
    ) 
    REFERENCES uji_ocw.ocw_idiomas 
    ( 
     id
    ) 
;


ALTER TABLE uji_ocw.ocw_cursos_imagenes 
    ADD CONSTRAINT ocw_cursos_imagenes_FK FOREIGN KEY 
    ( 
     curso_id
    ) 
    REFERENCES uji_ocw.ocw_cursos 
    ( 
     id
    ) 
;


ALTER TABLE uji_ocw.ocw_cursos_materiales 
    ADD CONSTRAINT ocw_cursos_mat_curs_FK FOREIGN KEY 
    ( 
     curso_id
    ) 
    REFERENCES uji_ocw.ocw_cursos 
    ( 
     id
    ) 
;


ALTER TABLE uji_ocw.ocw_cursos_materiales 
    ADD CONSTRAINT ocw_cursos_mat_mat_FK FOREIGN KEY 
    ( 
     material_id
    ) 
    REFERENCES uji_ocw.ocw_materiales 
    ( 
     id
    ) 
;


ALTER TABLE uji_ocw.ocw_cursos 
    ADD CONSTRAINT ocw_cursos_per_FK FOREIGN KEY 
    ( 
     persona_id
    ) 
    REFERENCES uji_ocw.ocw_ext_personas 
    ( 
     id
    ) 
;


ALTER TABLE uji_ocw.ocw_cursos_tags 
    ADD CONSTRAINT ocw_cursos_tags_ocw_cursos_FK FOREIGN KEY 
    ( 
     curso_id
    ) 
    REFERENCES uji_ocw.ocw_cursos 
    ( 
     id
    ) 
;


ALTER TABLE uji_ocw.ocw_cursos_tags 
    ADD CONSTRAINT ocw_cursos_tags_ocw_tags_FK FOREIGN KEY 
    ( 
     tag_id
    ) 
    REFERENCES uji_ocw.ocw_tags 
    ( 
     id
    ) 
;


ALTER TABLE uji_ocw.ocw_licencias_idiomas 
    ADD CONSTRAINT ocw_licencias_idiomas_idi_FK FOREIGN KEY 
    ( 
     idioma_id
    ) 
    REFERENCES uji_ocw.ocw_idiomas 
    ( 
     id
    ) 
;


ALTER TABLE uji_ocw.ocw_licencias_idiomas 
    ADD CONSTRAINT ocw_licencias_idiomas_lic_FK FOREIGN KEY 
    ( 
     licencia_id
    ) 
    REFERENCES uji_ocw.ocw_licencias 
    ( 
     id
    ) 
;


ALTER TABLE uji_ocw.ocw_materiales_autores 
    ADD CONSTRAINT ocw_mat_autores_FK FOREIGN KEY 
    ( 
     autor_id
    ) 
    REFERENCES uji_ocw.ocw_autores 
    ( 
     id
    ) 
;


ALTER TABLE uji_ocw.ocw_materiales_autores 
    ADD CONSTRAINT ocw_mat_materiales_FK FOREIGN KEY 
    ( 
     material_id
    ) 
    REFERENCES uji_ocw.ocw_materiales 
    ( 
     id
    ) 
;


ALTER TABLE uji_ocw.ocw_materiales_estados 
    ADD CONSTRAINT ocw_materiales_estados_mat_FK FOREIGN KEY 
    ( 
     material_id
    ) 
    REFERENCES uji_ocw.ocw_materiales 
    ( 
     id
    ) 
;


ALTER TABLE uji_ocw.ocw_materiales 
    ADD CONSTRAINT ocw_materiales_idi_FK FOREIGN KEY 
    ( 
     idioma_id
    ) 
    REFERENCES uji_ocw.ocw_idiomas 
    ( 
     id
    ) 
;


ALTER TABLE uji_ocw.ocw_materiales 
    ADD CONSTRAINT ocw_materiales_lic_FK FOREIGN KEY 
    ( 
     licencia_id
    ) 
    REFERENCES uji_ocw.ocw_licencias 
    ( 
     id
    ) 
;


ALTER TABLE uji_ocw.ocw_materiales_metadatos 
    ADD CONSTRAINT ocw_materiales_met_mat_FK FOREIGN KEY 
    ( 
     material_id
    ) 
    REFERENCES uji_ocw.ocw_materiales 
    ( 
     id
    ) 
;


ALTER TABLE uji_ocw.ocw_materiales_metadatos 
    ADD CONSTRAINT ocw_materiales_met_met_FK FOREIGN KEY 
    ( 
     metadato_id
    ) 
    REFERENCES uji_ocw.ocw_metadatos 
    ( 
     id
    ) 
;


ALTER TABLE uji_ocw.ocw_materiales_tags 
    ADD CONSTRAINT ocw_materiales_tags_mat_FK FOREIGN KEY 
    ( 
     material_id
    ) 
    REFERENCES uji_ocw.ocw_materiales 
    ( 
     id
    ) 
;


ALTER TABLE uji_ocw.ocw_materiales_tags 
    ADD CONSTRAINT ocw_materiales_tags_tags_FK FOREIGN KEY 
    ( 
     tag_id
    ) 
    REFERENCES uji_ocw.ocw_tags 
    ( 
     id
    ) 
;


ALTER TABLE uji_ocw.ocw_metadatos_idiomas 
    ADD CONSTRAINT ocw_metadatos_idiomas_idi_FK FOREIGN KEY 
    ( 
     idioma_id
    ) 
    REFERENCES uji_ocw.ocw_idiomas 
    ( 
     id
    ) 
;


ALTER TABLE uji_ocw.ocw_metadatos_idiomas 
    ADD CONSTRAINT ocw_metadatos_idiomas_met_FK FOREIGN KEY 
    ( 
     metadato_id
    ) 
    REFERENCES uji_ocw.ocw_metadatos 
    ( 
     id
    ) 
;


ALTER TABLE uji_ocw.ocw_metadatos_valores 
    ADD CONSTRAINT ocw_metadatos_valores_met_FK FOREIGN KEY 
    ( 
     metadato_id
    ) 
    REFERENCES uji_ocw.ocw_metadatos 
    ( 
     id
    ) 
;


ALTER TABLE uji_ocw.ocw_usuarios_roles_bonita 
    ADD CONSTRAINT ocw_usuarios_roles_usu_FK FOREIGN KEY 
    ( 
     usuario_id
    ) 
    REFERENCES uji_ocw.ocw_usuarios_bonita 
    ( 
     id
    ) 
;

CREATE OR REPLACE VIEW uji_ocw.ocw_vw_autores AS
SELECT ocw_autores.* FROM ocw_autores WHERE ocw_autores.persona_id IS NULL
UNION ALL
SELECT ocw_autores.id,
  ocw_ext_personas.nombre,
  ocw_ext_personas.apellido1,
  ocw_ext_personas.apellido2,
  ocw_ext_personas.email,
  ocw_autores.persona_id
FROM ocw_autores ocw_autores,
  ocw_ext_personas ocw_ext_personas
WHERE ocw_autores.persona_id = ocw_ext_personas.id
AND (ocw_autores.persona_id IS NOT NULL) ;



CREATE OR REPLACE VIEW uji_ocw.ocw_vw_categorias AS
SELECT c.id,
  c.orden,
  ci.nombre
FROM uji_ocw.ocw_categorias c,
  uji_ocw.ocw_categorias_idiomas ci,
  uji_ocw.ocw_idiomas i
WHERE c.id       = ci.categoria_id
AND ci.idioma_id = i.id
AND (i.orden     =
  (SELECT MIN(uji_ocw.ocw_idiomas.orden)
  FROM uji_ocw.ocw_idiomas
  WHERE uji_ocw.ocw_idiomas.edicion = 'S'
  )) ;


CREATE OR REPLACE VIEW UJI_OCW.OCW_VW_CURSOS (ID, NOMBRE, ESTADO, VISIBLE, FECHA_CREACION, FECHA_BAJA, PERSONA_ID) AS
   SELECT c.id, ci.nombre, c.estado, c.visible, c.fecha_creacion, c.fecha_baja, c.persona_id
   FROM   ocw_cursos c,
          ocw_cursos_idiomas ci,
          ocw_idiomas i
   WHERE  c.id = ci.curso_id
   AND    ci.idioma_id = i.id
   AND    i.orden = (SELECT MIN (ocw_idiomas.orden)
                     FROM   ocw_idiomas
                     WHERE  ocw_idiomas.edicion = 'S');

  
  

CREATE OR REPLACE VIEW uji_ocw.ocw_vw_licencias AS
SELECT c.id,
  ci.titulo,
  c.codigo,
  c.imagen,
  ci.contenido,
  ci.url
FROM uji_ocw.ocw_licencias c,
  uji_ocw.ocw_licencias_idiomas ci,
  uji_ocw.ocw_idiomas i
WHERE c.id       = ci.licencia_id
AND ci.idioma_id = i.id
AND (i.orden     =
  (SELECT MIN(uji_ocw.ocw_idiomas.orden)
  FROM uji_ocw.ocw_idiomas
  WHERE uji_ocw.ocw_idiomas.edicion = 'S'
  )) ;



CREATE OR REPLACE VIEW ocw_vw_materiales AS
SELECT m.id,
  m.titulo,
  m.nombre_fichero,
  m.url,
  m.descripcion,
  m.fecha,
  m.persona_id,
  p.nombre_persona,
  m.idioma_id,
  i.nombre nombre_idioma,
  m.licencia_id,
  l.nombre_licencia
FROM uji_ocw.ocw_materiales m,
  (SELECT uji_ocw.ocw_ext_personas.id,
    uji_ocw.ocw_ext_personas.nombre
    || ' '
    || uji_ocw.ocw_ext_personas.apellido1
    || ' '
    || uji_ocw.ocw_ext_personas.apellido2 nombre_persona
  FROM uji_ocw.ocw_ext_personas
  ) p,
  uji_ocw.ocw_idiomas i,
  (SELECT l.id,
    li.titulo nombre_licencia
  FROM uji_ocw.ocw_licencias l,
    uji_ocw.ocw_licencias_idiomas li,
    (SELECT * FROM uji_ocw.ocw_idiomas WHERE uji_ocw.ocw_idiomas.orden = 1
    ) i
  WHERE l.id       = li.licencia_id
  AND li.idioma_id = i.id
  ) l
WHERE m.persona_id = p.id
AND m.idioma_id    = i.id
AND m.licencia_id  = l.id ;



CREATE OR REPLACE VIEW uji_ocw.ocw_vw_metadatos AS
SELECT c.id,
  ci.nombre,
  c.limitado,
  c.obligatorio,
  c.tag
FROM uji_ocw.ocw_metadatos c,
  uji_ocw.ocw_metadatos_idiomas ci,
  uji_ocw.ocw_idiomas i
WHERE c.id       = ci.metadato_id
AND ci.idioma_id = i.id
AND (i.orden     =
  (SELECT MIN(uji_ocw.ocw_idiomas.orden)
  FROM uji_ocw.ocw_idiomas
  WHERE uji_ocw.ocw_idiomas.edicion = 'S'
  )) ;



CREATE OR REPLACE TRIGGER FKNTO_ocw_accesos 
BEFORE UPDATE OF curso_id, material_id 
ON uji_ocw.ocw_accesos 
FOR EACH ROW 
BEGIN 
 IF :old.curso_id IS NOT NULL THEN 
  raise_application_error(-20225,'Non Transferable FK constraint ocw_accesos_cur_FK on table uji_ocw.ocw_accesos is violated'); 
 END IF; 
 IF :old.material_id IS NOT NULL THEN 
  raise_application_error(-20225,'Non Transferable FK constraint ocw_accesos_mat_FK on table uji_ocw.ocw_accesos is violated'); 
 END IF; 
END; 
/





































-- Informe de Resumen de Oracle SQL Developer Data Modeler: 
-- 
-- CREATE TABLE                            28
-- CREATE INDEX                            29
-- ALTER TABLE                             61
-- CREATE VIEW                              6
-- CREATE PACKAGE                           0
-- CREATE PACKAGE BODY                      0
-- CREATE PROCEDURE                         0
-- CREATE FUNCTION                          0
-- CREATE TRIGGER                           1
-- ALTER TRIGGER                            0
-- CREATE STRUCTURED TYPE                   0
-- CREATE COLLECTION TYPE                   0
-- CREATE CLUSTER                           0
-- CREATE CONTEXT                           0
-- CREATE DATABASE                          0
-- CREATE DIMENSION                         0
-- CREATE DIRECTORY                         0
-- CREATE DISK GROUP                        0
-- CREATE ROLE                              0
-- CREATE ROLLBACK SEGMENT                  0
-- CREATE SEQUENCE                          0
-- CREATE MATERIALIZED VIEW                 0
-- CREATE SYNONYM                           0
-- CREATE TABLESPACE                        0
-- CREATE USER                              0
-- 
-- DROP TABLESPACE                          0
-- DROP DATABASE                            0
-- 
-- ERRORS                                   0
-- WARNINGS                                 0
