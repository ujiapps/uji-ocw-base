

CREATE TABLE uji_ocw.ocw_cursos_enlaces 
    ( 
     id NUMBER  NOT NULL , 
     curso_id NUMBER  NOT NULL , 
     url VARCHAR2 (1000)  NOT NULL , 
     descripcion VARCHAR2 (1000) , 
     orden NUMBER 
    ) 
;


CREATE INDEX uji_ocw.ocw_cursos_enlaces__IDX ON uji_ocw.ocw_cursos_enlaces 
    ( 
     curso_id ASC 
    ) 
;

ALTER TABLE uji_ocw.ocw_cursos_enlaces 
    ADD CONSTRAINT ocw_cursos_enlaces_PK PRIMARY KEY ( id ) ;




ALTER TABLE uji_ocw.ocw_cursos_enlaces 
    ADD CONSTRAINT ocw_cursos_enl_curso_FK FOREIGN KEY 
    ( 
     curso_id
    ) 
    REFERENCES uji_ocw.ocw_cursos 
    ( 
     id
    ) 
;




alter table uji_ocw.ocw_cursos_materiales add (orden number);




CREATE TABLE uji_ocw.ocw_grupos 
    ( 
     id NUMBER  NOT NULL , 
     curso_id NUMBER  NOT NULL , 
     descripcion VARCHAR2 (1000)  NOT NULL , 
     orden NUMBER 
    ) 
;


CREATE INDEX uji_ocw.ocw_grupos__IDX ON uji_ocw.ocw_grupos 
    ( 
     curso_id ASC 
    ) 
;

ALTER TABLE uji_ocw.ocw_grupos 
    ADD CONSTRAINT ocw_cursos_grupos_PK PRIMARY KEY ( id ) ;





ALTER TABLE uji_ocw.ocw_grupos 
    ADD CONSTRAINT ocw_grupos_curso_FK FOREIGN KEY 
    ( 
     curso_id
    ) 
    REFERENCES uji_ocw.ocw_cursos 
    ( 
     id
    ) 
;


ALTER TABLE UJI_OCW.OCW_CURSOS_MATERIALES ADD (grupo_id  NUMBER);
 

CREATE INDEX uji_ocw.ocw_cursos_materiales_grup_IDX ON uji_ocw.ocw_cursos_materiales 
    ( 
     grupo_id ASC 
    ) 
;


ALTER TABLE uji_ocw.ocw_cursos_materiales 
    ADD CONSTRAINT ocw_cursos_mat_grup_FK FOREIGN KEY 
    ( 
     grupo_id
    ) 
    REFERENCES uji_ocw.ocw_grupos 
    ( 
     id
    ) 
;






CREATE TABLE uji_ocw.ocw_cursos_categorias 
    ( 
     id NUMBER  NOT NULL , 
     curso_id NUMBER  NOT NULL , 
     categoria_id NUMBER  NOT NULL 
    ) 
;


CREATE INDEX uji_ocw.ocw_cursos_categorias_cat_IDX ON uji_ocw.ocw_cursos_categorias 
    ( 
     categoria_id ASC 
    ) 
;
CREATE INDEX uji_ocw.ocw_cursos_categorias_cur_IDX ON uji_ocw.ocw_cursos_categorias 
    ( 
     curso_id ASC 
    ) 
;

ALTER TABLE uji_ocw.ocw_cursos_categorias 
    ADD CONSTRAINT ocw_cursos_categorias_PK PRIMARY KEY ( id ) ;

ALTER TABLE uji_ocw.ocw_cursos_categorias 
    ADD CONSTRAINT ocw_cursos_categorias__UN UNIQUE ( curso_id , categoria_id ) ;




ALTER TABLE uji_ocw.ocw_cursos_categorias 
    ADD CONSTRAINT ocw_cursos_cat_cat_FK FOREIGN KEY 
    ( 
     categoria_id
    ) 
    REFERENCES uji_ocw.ocw_categorias 
    ( 
     id
    ) 
;


ALTER TABLE uji_ocw.ocw_cursos_categorias 
    ADD CONSTRAINT ocw_cursos_cat_cur_FK FOREIGN KEY 
    ( 
     curso_id
    ) 
    REFERENCES uji_ocw.ocw_cursos 
    ( 
     id
    ) 
;

insert into uji_ocw.ocw_cursos_categorias (id, curso_id, categoria_id)
select uji_ocw.hibernate_sequence.nextval, id, categoria_id
from uji_ocw.ocw_cursos;

commit;

ALTER TABLE UJI_OCW.OCW_CURSOS DROP COLUMN CATEGORIA_ID;


CREATE OR REPLACE VIEW uji_ocw.ocw_vw_materiales_por_curso AS
SELECT cm.curso_id,
  m.id material_id,
  0 grupo_id,
  'MAT' tipo,
  m.titulo,
  cm.orden,
  1 nivel
FROM ocw_cursos_materiales cm,
  ocw_materiales m
WHERE cm.material_id = m.id
AND (cm.grupo_id    IS NULL)
UNION ALL
SELECT g.curso_id,
  0 material_id,
  g.id grupo_id,
  'GRUPO' tipo,
  g.descripcion titulo,
  g.orden,
  1 nivel
FROM ocw_grupos g
UNION ALL
SELECT cm.curso_id,
  m.id,
  cm.grupo_id,
  'MAT' tipo,
  m.titulo,
  cm.orden,
  1 nivel
FROM ocw_cursos_materiales cm,
  ocw_materiales m
WHERE cm.material_id = m.id
AND (cm.grupo_id    IS NOT NULL) ;





CREATE OR REPLACE FORCE VIEW UJI_OCW.OCW_VW_CURSOS (ID, NOMBRE, ESTADO, VISIBLE, FECHA_CREACION, FECHA_BAJA, PERSONA_ID) AS
   SELECT c.id, ci.nombre, c.estado, c.visible, c.fecha_creacion, c.fecha_baja, c.persona_id
   FROM   ocw_cursos c,
          ocw_cursos_idiomas ci,
          ocw_idiomas i
   WHERE  c.id = ci.curso_id
   AND    ci.idioma_id = i.id
   AND    i.orden = (SELECT MIN (ocw_idiomas.orden)
                     FROM   ocw_idiomas
                     WHERE  ocw_idiomas.edicion = 'S');

					 
					 

