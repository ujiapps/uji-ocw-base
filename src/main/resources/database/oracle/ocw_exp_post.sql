

drop table uji_ocw.ocw_ext_personas cascade constraints;

create or replace force view uji_ocw.ocw_ext_personas (id, nombre, apellido1, apellido2, email) as
   select to_number(to_char(id)) id, trim (rpad (nombre, 1000)) nombre, trim (rpad (apellido1, 1000)) apellido1,
          trim (rpad (apellido2, 1000)) apellido2, trim (rpad (substr (cuenta, 1, instr (cuenta, '@') - 1), 1000)) email
   from   (select id, nombre, apellido1, apellido2, busca_cuenta (id) cuenta
           from   per_personas)
   where  id <> 0
   and    cuenta is not null;


   
grant select on gra_exp.exp_v_asi_todas to uji_ocw;

drop table uji_ocw.ocw_ext_asignaturas cascade constraints;

create or replace force view uji_ocw.ocw_ext_asignaturas (id, nombre_ca, nombre_es, nombre_uk) as
   select trim (rpad (id, 10)) id, trim (rpad (nombre_ca, 1000)) nombre_ca, trim (rpad (nombre_es, 1000)) nombre_es,
          trim (rpad (nombre_uk, 1000)) nombre_uk
   from   gra_exp.exp_v_asi_todas;

