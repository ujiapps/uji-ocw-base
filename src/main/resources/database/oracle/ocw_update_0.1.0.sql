

CREATE OR REPLACE VIEW ocw_vw_materiales AS
SELECT m.id,
  m.titulo,
  m.nombre_fichero,
  m.url,
  m.descripcion,
  m.fecha,
  m.persona_id,
  p.nombre_persona,
  m.idioma_id,
  i.nombre nombre_idioma,
  m.licencia_id,
  l.nombre_licencia,
  m.content_type
FROM uji_ocw.ocw_materiales m,
  (SELECT uji_ocw.ocw_ext_personas.id,
    uji_ocw.ocw_ext_personas.nombre
    || ' '
    || uji_ocw.ocw_ext_personas.apellido1
    || ' '
    || uji_ocw.ocw_ext_personas.apellido2 nombre_persona
  FROM uji_ocw.ocw_ext_personas
  ) p,
  uji_ocw.ocw_idiomas i,
  (SELECT l.id,
    li.titulo nombre_licencia
  FROM uji_ocw.ocw_licencias l,
    uji_ocw.ocw_licencias_idiomas li,
    (SELECT * FROM uji_ocw.ocw_idiomas WHERE uji_ocw.ocw_idiomas.orden = 1
    ) i
  WHERE l.id       = li.licencia_id
  AND li.idioma_id = i.id
  ) l
WHERE m.persona_id = p.id(+)
AND m.idioma_id    = i.id(+)
AND m.licencia_id  = l.id(+) ;


CREATE OR REPLACE VIEW uji_ocw.ocw_vw_cursos AS
SELECT c.id,
  ci.nombre,
  c.estado,
  c.visible,
  c.fecha_creacion,
  c.fecha_baja,
  c.persona_id,
  ci.descripcion,
  (SELECT COUNT(*)
  FROM ocw_accesos
  WHERE ocw_accesos.curso_id IS NOT NULL
  AND ocw_accesos.curso_id    = c.id
  ) numero_accesos
FROM ocw_cursos c,
  ocw_cursos_idiomas ci,
  ocw_idiomas i
WHERE c.id       = ci.curso_id
AND ci.idioma_id = i.id
AND (i.orden     =
  (SELECT MIN(ocw_idiomas.orden)
  FROM ocw_idiomas
  WHERE ocw_idiomas.edicion = 'S'
  )) ;




CREATE OR REPLACE FORCE VIEW UJI_OCW.OCW_VW_MATERIALES_POR_CURSO AS
select cm.curso_id, cm.id curso_material_id, m.id material_id, 0 grupo_id, 'MAT' tipo, m.titulo,
       cm.orden * 100000 orden, 1 nivel, cm.descripcion, m.descripcion descripcion_material, m.content_type, m.fecha,
       m.nombre_fichero, m.url, i.acronimo, i.nombre idioma
from   ocw_cursos_materiales cm,
       ocw_materiales m,
       ocw_idiomas i
where  cm.material_id = m.id
and    (cm.grupo_id is null)
and    i.id = m.idioma_id
union all
select g.curso_id, 0 curso_material_id, 0 material_id, g.id grupo_id, 'GRUPO' tipo, g.nombre titulo,
       g.orden * 100000 orden, 1 nivel, descripcion, null descripcion_material, null content_type, null fecha,
       null nombre_fichero, null url, null acronimo, null idioma
from   ocw_grupos g
union all
select cm.curso_id, cm.id curso_material_id, m.id material_id, cm.grupo_id, 'MAT' tipo, m.titulo,
       g.orden * 100000 + cm.orden orden, 2 nivel, cm.descripcion, m.descripcion descripcion_material, content_type,
       m.fecha, m.nombre_fichero, m.url, i.acronimo, i.nombre idioma
from   ocw_cursos_materiales cm,
       ocw_materiales m,
       ocw_grupos g,
       ocw_idiomas i
where  cm.material_id = m.id
and    cm.grupo_id = g.id
and    cm.curso_id = g.curso_id
and    i.id = m.idioma_id;




CREATE OR REPLACE FORCE VIEW UJI_OCW.OCW_VW_CURSOS_DETALLE AS
   SELECT c.id, 'CA' iso, c.fecha_creacion, c.fecha_baja, c.estado, c.visible,
          NVL (ca.nombre, NVL (es.nombre, en.nombre)) nombre, NVL (ca.periodo, NVL (es.periodo, en.periodo)) periodo,
          NVL (ca.descripcion, NVL (es.descripcion, en.descripcion)) descripcion,
          DECODE (DBMS_LOB.getlength (ca.programa),
                  0, DECODE (DBMS_LOB.getlength (es.programa), 0, en.programa, es.programa),
                  ca.programa
                 ) programa,
          DECODE (DBMS_LOB.getlength (ca.guia_aprendizaje),
                  0, DECODE (DBMS_LOB.getlength (es.guia_aprendizaje), 0, en.guia_aprendizaje, es.guia_aprendizaje),
                  ca.guia_aprendizaje
                 ) guia_aprendizaje,
          p.nombre || ' ' || p.apellido1 || ' ' || p.apellido2 nombre_creador, im.id id_imagen, tag.tag_id,
          tag.nombre tag, cat.nombre categoria, cat.categoria_id, cat.acronimo
   FROM   ocw_cursos c,
          ocw_cursos_idiomas ci,
          ocw_idiomas i,
          ocw_ext_personas p,
          (SELECT ci1.*
           FROM   ocw_cursos_idiomas ci1,
                  ocw_idiomas i
           WHERE  ci1.idioma_id = i.id
           AND    (i.acronimo = 'CA')) ca,
          (SELECT ci2.*
           FROM   ocw_cursos_idiomas ci2,
                  ocw_idiomas i
           WHERE  ci2.idioma_id = i.id
           AND    (i.acronimo = 'ES')) es,
          (SELECT ci2.*
           FROM   ocw_cursos_idiomas ci2,
                  ocw_idiomas i
           WHERE  ci2.idioma_id = i.id
           AND    (i.acronimo = 'EN')) en,
          (select *
           from   ocw_cursos_imagenes im2
           where  id in (select min (id)
                         from   ocw_cursos_imagenes im3
                         where  im2.curso_id = im3.curso_id)) im,
          (select curso_id, tag_id, nombre
           from   ocw_cursos_tags ct,
                  ocw_tags ta
           where  tag_id = ta.id) tag,
          (select cidi.nombre, cc.curso_id, cc.categoria_id, 'CA' acronimo
           from   ocw_cursos_categorias cc,
                  ocw_categorias_idiomas cidi,
                  ocw_idiomas idi
           where  cc.categoria_id = cidi.categoria_id
           and    cidi.idioma_id = idi.id
           and    acronimo = 'CA') cat
   WHERE  c.id = ci.curso_id
   AND    ci.idioma_id = i.id
   AND    c.id = ca.curso_id(+)
   AND    c.id = es.curso_id(+)
   AND    c.id = en.curso_id(+)
   AND    c.persona_id = p.id
   AND    (i.orden = (SELECT MIN (ocw_idiomas.orden)
                      FROM   ocw_idiomas
                      WHERE  ocw_idiomas.edicion = 'S'))
   and    c.id = im.curso_id(+)
   and    c.id = tag.curso_id(+)
   and    c.id = cat.curso_id(+)
   UNION ALL
   SELECT c.id, 'ES' iso, c.fecha_creacion, c.fecha_baja, c.estado, c.visible,
          NVL (es.nombre, NVL (ca.nombre, en.nombre)) nombre, NVL (es.periodo, NVL (ca.periodo, en.periodo)) periodo,
          NVL (es.descripcion, NVL (ca.descripcion, en.descripcion)) descripcion,
          DECODE (DBMS_LOB.getlength (es.programa),
                  0, DECODE (DBMS_LOB.getlength (ca.programa), 0, en.programa, ca.programa),
                  es.programa
                 ) programa,
          DECODE (DBMS_LOB.getlength (es.guia_aprendizaje),
                  0, DECODE (DBMS_LOB.getlength (ca.guia_aprendizaje), 0, en.guia_aprendizaje, ca.guia_aprendizaje),
                  es.guia_aprendizaje
                 ) guia_aprendizaje,
          p.nombre || ' ' || p.apellido1 || ' ' || p.apellido2 nombre_creador, im.id id_imagen, tag.tag_id,
          tag.nombre tag, cat.nombre categoria, cat.categoria_id, cat.acronimo
   FROM   ocw_cursos c,
          ocw_cursos_idiomas ci,
          ocw_idiomas i,
          ocw_ext_personas p,
          (SELECT ci1.*
           FROM   ocw_cursos_idiomas ci1,
                  ocw_idiomas i
           WHERE  ci1.idioma_id = i.id
           AND    (i.acronimo = 'CA')) ca,
          (SELECT ci2.*
           FROM   ocw_cursos_idiomas ci2,
                  ocw_idiomas i
           WHERE  ci2.idioma_id = i.id
           AND    (i.acronimo = 'ES')) es,
          (SELECT ci2.*
           FROM   ocw_cursos_idiomas ci2,
                  ocw_idiomas i
           WHERE  ci2.idioma_id = i.id
           AND    (i.acronimo = 'EN')) en,
          (select *
           from   ocw_cursos_imagenes im2
           where  id in (select min (id)
                         from   ocw_cursos_imagenes im3
                         where  im2.curso_id = im3.curso_id)) im,
          (select curso_id, tag_id, nombre
           from   ocw_cursos_tags ct,
                  ocw_tags ta
           where  tag_id = ta.id) tag,
          (select cidi.nombre, cc.curso_id, cc.categoria_id, 'ES' acronimo
           from   ocw_cursos_categorias cc,
                  ocw_categorias_idiomas cidi,
                  ocw_idiomas idi
           where  cc.categoria_id = cidi.categoria_id
           and    cidi.idioma_id = idi.id
           and    acronimo = 'ES') cat
   WHERE  c.id = ci.curso_id
   AND    ci.idioma_id = i.id
   AND    c.id = ca.curso_id(+)
   AND    c.id = es.curso_id(+)
   AND    c.id = en.curso_id(+)
   AND    c.persona_id = p.id
   AND    (i.orden = (SELECT MIN (ocw_idiomas.orden)
                      FROM   ocw_idiomas
                      WHERE  ocw_idiomas.edicion = 'S'))
   and    c.id = im.curso_id(+)
   and    c.id = tag.curso_id(+)
   and    c.id = cat.curso_id(+)
   UNION ALL
   SELECT c.id, 'EN' iso, c.fecha_creacion, c.fecha_baja, c.estado, c.visible,
          NVL (en.nombre, NVL (es.nombre, ca.nombre)) nombre, NVL (en.periodo, NVL (es.periodo, ca.periodo)) periodo,
          NVL (en.descripcion, NVL (es.descripcion, ca.descripcion)) descripcion,
          DECODE (DBMS_LOB.getlength (en.programa),
                  0, DECODE (DBMS_LOB.getlength (es.programa), 0, ca.programa, es.programa),
                  en.programa
                 ) programa,
          DECODE (DBMS_LOB.getlength (en.guia_aprendizaje),
                  0, DECODE (DBMS_LOB.getlength (es.guia_aprendizaje), 0, ca.guia_aprendizaje, es.guia_aprendizaje),
                  en.guia_aprendizaje
                 ) guia_aprendizaje,
          p.nombre || ' ' || p.apellido1 || ' ' || p.apellido2 nombre_creador, im.id id_imagen, tag.tag_id,
          tag.nombre tag, cat.nombre categoria, cat.categoria_id, cat.acronimo
   FROM   ocw_cursos c,
          ocw_cursos_idiomas ci,
          ocw_idiomas i,
          ocw_ext_personas p,
          (SELECT ci1.*
           FROM   ocw_cursos_idiomas ci1,
                  ocw_idiomas i
           WHERE  ci1.idioma_id = i.id
           AND    (i.acronimo = 'CA')) ca,
          (SELECT ci2.*
           FROM   ocw_cursos_idiomas ci2,
                  ocw_idiomas i
           WHERE  ci2.idioma_id = i.id
           AND    (i.acronimo = 'ES')) es,
          (SELECT ci2.*
           FROM   ocw_cursos_idiomas ci2,
                  ocw_idiomas i
           WHERE  ci2.idioma_id = i.id
           AND    (i.acronimo = 'EN')) en,
          (select *
           from   ocw_cursos_imagenes im2
           where  id in (select min (id)
                         from   ocw_cursos_imagenes im3
                         where  im2.curso_id = im3.curso_id)) im,
          (select curso_id, tag_id, nombre
           from   ocw_cursos_tags ct,
                  ocw_tags ta
           where  tag_id = ta.id) tag,
          (select cidi.nombre, cc.curso_id, cc.categoria_id, 'EN' acronimo
           from   ocw_cursos_categorias cc,
                  ocw_categorias_idiomas cidi,
                  ocw_idiomas idi
           where  cc.categoria_id = cidi.categoria_id
           and    cidi.idioma_id = idi.id
           and    acronimo = 'EN') cat
   WHERE  c.id = ci.curso_id
   AND    ci.idioma_id = i.id
   AND    c.id = ca.curso_id(+)
   AND    c.id = es.curso_id(+)
   AND    c.id = en.curso_id(+)
   AND    c.persona_id = p.id
   AND    (i.orden = (SELECT MIN (ocw_idiomas.orden)
                      FROM   ocw_idiomas
                      WHERE  ocw_idiomas.edicion = 'S'))
   and    c.id = im.curso_id(+)
   and    c.id = tag.curso_id(+)
   and    c.id = cat.curso_id(+);

