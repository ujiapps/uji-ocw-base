<%@page import="es.uji.commons.sso.User" %>
<%@page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta content="text/html; charset=utf-8">

    <link type="image/x-icon" rel="shortcut icon"
          href="http://e-ujier.uji.es/img/portal2/favicon.ico"/>
    <link rel="stylesheet" type="text/css"
          href="http://static.uji.es/js/extjs/ext-3.4.0/resources/css/ext-all.css"/>
    <link rel="stylesheet"
          href="http://static.uji.es/js/extjs/ext-3.4.0/examples/ux/css/RowEditor.css"/>
    <link rel="stylesheet" type="text/css"
          href="http://static.uji.es/js/extjs/uji-commons-extjs/css/icons.css"/>
    <link rel="stylesheet" type="text/css" href="css/dataviewer.css"/>

    <script type="text/javascript"
            src="http://static.uji.es/js/extjs/ext-3.4.0/adapter/ext/ext-base.js"></script>
    <script type="text/javascript"
            src="http://static.uji.es/js/extjs/ext-3.4.0/ext-all-debug.js"></script>
    <script type="text/javascript"
            src="http://static.uji.es/js/ckeditor/ckeditor-3.6.1/ckeditor.js"></script>
    <script type="text/javascript"
            src="http://static.uji.es/js/extjs/uji-commons-extjs/Ext/form/CKEditor/0.0.2/CKEditor.js"></script>
    <script type="text/javascript"
            src="http://static.uji.es/js/extjs/uji-commons-extjs/Ext/ux/uji/ApplicationViewport/0.0.1/ApplicationViewport.js"></script>
    <script type="text/javascript"
            src="http://static.uji.es/js/extjs/uji-commons-extjs/Ext/ux/tree/XmlTreeLoader/0.0.1/XmlTreeLoader.js"></script>
    <script type="text/javascript"
            src="http://static.uji.es/js/extjs/uji-commons-extjs/Ext/ux/TabCloseMenu/0.0.1/TabCloseMenu.js"></script>
    <script type="text/javascript"
            src="http://static.uji.es/js/extjs/uji-commons-extjs/Ext/ux/uji/TabPanel/0.0.1/TabPanel.js"></script>
    <script type="text/javascript"
            src="http://static.uji.es/js/extjs/uji-commons-extjs/Ext/ux/uji/Util.js"></script>
    <script type="text/javascript"
            src="http://static.uji.es/js/extjs/ext-3.4.0/examples/ux/RowEditor.js"></script>
    <script type="text/javascript"
            src="http://static.uji.es/js/extjs/uji-commons-extjs/Ext/ux/uji/grid/RowEditor/0.0.1/RowEditor.js"></script>
    <script type="text/javascript"
            src="http://static.uji.es/js/extjs/uji-commons-extjs/Ext/ux/uji/form/MultiLanguageField/0.0.1/MultiLanguageField.js"></script>
    <script type="text/javascript"
            src="http://static.uji.es/js/extjs/uji-commons-extjs/Ext/ux/uji/form/MultiLanguageTextField/0.0.1/MultiLanguageTextField.js"></script>
    <script type="text/javascript"
            src="http://static.uji.es/js/extjs/uji-commons-extjs/Ext/ux/uji/form/MultiLanguageTextArea/0.0.1/MultiLanguageTextArea.js"></script>
    <script type="text/javascript"
            src="http://static.uji.es/js/extjs/uji-commons-extjs/Ext/ux/uji/grid/GridPanel/0.0.1/GridPanel.js"></script>
    <script type="text/javascript"
            src="http://static.uji.es/js/extjs/uji-commons-extjs/Ext/ux/uji/form/LookupWindow/0.0.1/LookupWindow.js"></script>
    <script type="text/javascript"
            src="http://static.uji.es/js/extjs/uji-commons-extjs/Ext/ux/uji/grid/GridPanel/0.0.1/GridPanel.js"></script>
    <script type="text/javascript"
            src="http://static.uji.es/js/extjs/uji-commons-extjs/UJI/ContentType/0.0.1/ContentType.js"></script>
    <script type="text/javascript"
            src="http://static.uji.es/js/extjs/uji-commons-extjs/Ext/ux/tree/XmlTreeLoader/0.0.1/XmlTreeLoader.js"></script>
    <script type="text/javascript" src="http://static.uji.es/js/extjs/ext-3.4.0/src/locale/ext-lang-ca.js"></script>
    <script type="text/javascript" src="js/ext-lang-ca-custom.js"></script>
    <script type="text/javascript" src="js/tabCursoImagenes.js"></script>
    <script type="text/javascript" src="js/formCursoDatosGenerales.js"></script>
    <script type="text/javascript" src="js/tabCursoMateriales.js"></script>
    <script type="text/javascript" src="js/tabCursoAsignaturas.js"></script>
    <script type="text/javascript" src="js/tabCursoAutorizados.js"></script>
    <script type="text/javascript" src="js/tabCursoCategorias.js"></script>
    <script type="text/javascript" src="js/cursos.js"></script>
    <script type="text/javascript" src="js/cursoGrid.js"></script>
    <script type="text/javascript" src="js/cursoForm.js"></script>
    <script type="text/javascript" src="js/dashboard.js"></script>
    <script type="text/javascript" src="js/tabMaterialAutores.js"></script>
    <script type="text/javascript" src="js/tabMaterialCursos.js"></script>
    <script type="text/javascript" src="js/tabMaterialMetadatos.js"></script>
    <script type="text/javascript" src="js/materialGrid.js"></script>
    <script type="text/javascript" src="js/materialForm.js"></script>
    <script type="text/javascript" src="js/materiales.js"></script>
    <script type="text/javascript" src="js/autores.js"></script>
    <script type="text/javascript" src="js/categorias.js"></script>
    <script type="text/javascript" src="js/idiomas.js"></script>
    <script type="text/javascript" src="js/licencias.js"></script>
    <script type="text/javascript" src="js/metadatos.js"></script>
    <script type="text/javascript" src="js/tags.js"></script>
    <script type="text/javascript" src="js/enlaces.js"></script>
    <script type="text/javascript" src="js/vtypes.js"></script>
    <title>Open CourseWare</title>

    <%
        String login = ((User) session.getAttribute(User.SESSION_USER))
                .getName();
    %>

    <script type="text/javascript">
        Ext.BLANK_IMAGE_URL = 'http://static.uji.es/js/extjs/ext-3.4.0/resources/images/default/s.gif';

        var login = '<%=login%>';

        Ext.Ajax.defaultHeaders = {
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'
        };

        Ext.onReady(function () {
            Ext.override(Ext.data.Connection,
                    {
                        timeout: 120000
                    });

            Ext.QuickTips.init();

            initOCWVtypes();

            new Ext.data.Store(
                    {
                        restful: true,
                        url: '/ocw/rest/idioma/edicion',
                        autoLoad: true,
                        reader: new Ext.data.XmlReader(
                                {
                                    record: 'Idioma',
                                    id: 'id'
                                }, [ 'id', 'nombre', 'acronimo', 'orden', 'edicion' ]),
                        listeners: {
                            load: function (store) {
                                var idiomasAux = new Array();
                                for (var i = 0; i < store.getCount(); i++) {
                                    var idioma = store.getAt(i);
                                    if (idioma.get("edicion") == "S") {
                                        idiomasAux[i] = idioma.get("acronimo").toLowerCase();
                                    }
                                }
                                window.idiomas = idiomasAux;

                                new Ext.ux.uji.ApplicationViewport(
                                        {
                                            codigoAplicacion: 'OCW',
                                            tituloAplicacion: 'OpenCourseware'
                                        });
                            }
                        }
                    });
        });
    </script>
</head>
<body>
</body>
</html>