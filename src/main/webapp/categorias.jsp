<%@page contentType="text/html; charset=UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta content="text/html; charset=utf-8">

<link rel="stylesheet" type="text/css"
	href="http://devel.uji.es/resources/css/icons.css" />
<link rel="stylesheet" type="text/css"
	href="http://devel.uji.es/resources/js/ext-3.4.0/resources/css/ext-all.css" />
<link rel="stylesheet"
	href="http://devel.uji.es/resources/js/ext-3.4.0/examples/ux/css/RowEditor.css" />

<script type="text/javascript"
	src="http://devel.uji.es/resources/js/ext-3.4.0/adapter/ext/ext-base.js"></script>
<script type="text/javascript"
	src="http://devel.uji.es/resources/js/ext-3.4.0/ext-all-debug.js"></script>
<script type="text/javascript"
	src="http://devel.uji.es/resources/js/uji-commons-extjs/Ext/ux/uji/Util.js"></script>

<script type="text/javascript"
	src="http://devel.uji.es/resources/js/ext-3.4.0/examples/ux/RowEditor.js"></script>
<script type="text/javascript"
	src="http://devel.uji.es/resources/js/uji-commons-extjs/Ext/ux/uji/grid/RowEditor/0.0.1/RowEditor.js"></script>
<script type="text/javascript"
	src="http://devel.uji.es/resources/js/uji-commons-extjs/Ext/ux/uji/form/MultiLanguageField/0.0.1/MultiLanguageField.js"></script>
<script type="text/javascript"
	src="http://devel.uji.es/resources/js/uji-commons-extjs/Ext/ux/uji/form/MultiLanguageTextField/0.0.1/MultiLanguageTextField.js"></script>
<script type="text/javascript" src="js/vtypes.js"></script>
<script type="text/javascript" src="js/categorias.js"></script>

<title>Categorias OCW</title>

<script type="text/javascript">
    Ext.BLANK_IMAGE_URL = 'http://devel.uji.es/resources/js/ext-3.4.0.0/resources/images/default/s.gif';

    Ext.onReady(function()
    {
        Ext.QuickTips.init();
        Ext.Ajax.on('requestcomplete', function(conn, response, options)
        {
            var contentType = response.getResponseHeader("Content-Type");

            if (contentType && contentType.substring(0, 9) == "text/html")
            {
                window.location = "index.jsp?";
            }
        });

        Ext.Ajax.on('requestexception', function(conn, response, options)
        {
            if (response.responseXML)
            {
                var msgList = response.responseXML.getElementsByTagName("msg");

                if (msgList && msgList[0] && msgList[0].firstChild)
                {
                    alert(msgList[0].firstChild.nodeValue);
                }
            }

            if (response.status == 0)
            {
                window.location = "index.jsp?";
            }
        });

        initOCWVtypes();

        var panelEdicion = new UJI.OCW.Categorias();

        new Ext.Viewport(
        {
            layout : 'fit',
            items : [ panelEdicion ]
        });

    });
</script>

</head>

<body>
</body>
</html>