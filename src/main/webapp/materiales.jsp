<%@page contentType="text/html; charset=UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta content="text/html; charset=utf-8">

<link rel="stylesheet" type="text/css"
	href="http://devel.uji.es/resources/css/icons.css" />
<link rel="stylesheet" type="text/css"
	href="http://devel.uji.es/resources/js/ext-3.4.0/resources/css/ext-all.css" />
<link rel="stylesheet" type="text/css"
	href="http://devel.uji.es/resources/js/ext-3.4.0/examples/ux/css/RowEditor.css" />

<script type="text/javascript"
	src="http://devel.uji.es/resources/js/ckeditor-3.6.1/ckeditor.js"></script>

<script type="text/javascript"
	src="http://devel.uji.es/resources/js/ext-3.4.0/adapter/ext/ext-base.js"></script>
<script type="text/javascript"
	src="http://devel.uji.es/resources/js/ext-3.4.0/ext-all-debug.js"></script>

<script type="text/javascript"
	src="http://devel.uji.es/resources/js/uji-commons-extjs/Ext/ux/uji/Util.js"></script>
<script type="text/javascript"
	src="http://devel.uji.es/resources/js/ext-3.4.0/examples/ux/RowEditor.js"></script>
<script type="text/javascript"
	src="http://devel.uji.es/resources/js/uji-commons-extjs/Ext/form/CKEditor/0.0.2/CKEditor.js"></script>
<script type="text/javascript"
	src="http://devel.uji.es/resources/js/uji-commons-extjs/Ext/ux/uji/form/LookupWindow/0.0.1/LookupWindow.js"></script>

<script type="text/javascript" src="js/tabMaterialMetadatos.js"></script>
<script type="text/javascript" src="js/tabMaterialAutores.js"></script>
<script type="text/javascript" src="js/tabMaterialCursos.js"></script>
<script type="text/javascript" src="js/materialGrid.js"></script>
<script type="text/javascript" src="js/materialForm.js"></script>
<script type="text/javascript" src="js/materiales.js"></script>

<title>Materiales OCW</title>

<script type="text/javascript">
    Ext.BLANK_IMAGE_URL = 'http://devel.uji.es/resources/js/ext-3.4.0/resources/images/default/s.gif';

    Ext.onReady(function()
    {
        Ext.QuickTips.init();

        new Ext.data.Store(
        {
            restful : true,
            url : '/ocw/rest/idioma/edicion',
            autoLoad : true,
            reader : new Ext.data.XmlReader(
            {
                record : 'Idioma',
                id : 'id'
            }, [ 'id', 'nombre', 'acronimo', 'orden', 'edicion' ]),
            listeners :
            {
                load : function(store)
                {
                    var idiomasAux = new Array();
                    for ( var i = 0; i < store.getCount(); i++)
                    {
                        var idioma = store.getAt(i);
                        if (idioma.get("edicion") == "S")
                        {
                            idiomasAux[i] = idioma.get("acronimo").toLowerCase();
                        }
                    }
                    window.idiomas = idiomasAux;

                    var panel = new UJI.OCW.Materiales();

                    new Ext.Viewport(
                    {
                        layout : 'fit',
                        items : [ panel ]
                    });

                }
            }
        });
    });
</script>

</head>

<body>
</body>
</html>