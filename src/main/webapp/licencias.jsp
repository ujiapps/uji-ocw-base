<%@page contentType="text/html; charset=UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta content="text/html; charset=utf-8">

<link rel="stylesheet" type="text/css" href="http://devel.uji.es/resources/css/icons.css" />
<link rel="stylesheet" type="text/css"
	href="http://devel.uji.es/resources/js/ext-3.4.0/resources/css/ext-all.css" />

<script type="text/javascript"
	src="http://devel.uji.es/resources/js/ext-3.4.0/adapter/ext/ext-base-debug.js"></script>
<script type="text/javascript"
	src="http://devel.uji.es/resources/js/ext-3.4.0/ext-all-debug.js"></script>

	
<script type="text/javascript"
	src="http://devel.uji.es/resources/js/uji-commons-extjs/Ext/ux/uji/Util.js"></script>
<script type="text/javascript"
	src="http://devel.uji.es/resources/js/uji-commons-extjs/Ext/ux/uji/LanguagePanel/0.0.1/LanguagePanel.js"></script>
<script type="text/javascript"
	src="http://devel.uji.es/resources/js/ext-3.4.0/examples/ux/RowEditor.js"></script>
<script type="text/javascript"
	src="http://localhost/resources/js/uji-commons-extjs/Ext/ux/uji/form/MultiLanguageField/0.0.1/MultiLanguageField.js"></script>
<script type="text/javascript"
	src="http://localhost/resources/js/uji-commons-extjs/Ext/ux/uji/form/MultiLanguageTextField/0.0.1/MultiLanguageTextField.js"></script>
<script type="text/javascript"
	src="http://devel.uji.es/resources/js/uji-commons-extjs/Ext/ux/uji/grid/GridPanel/0.0.1/GridPanel.js"></script>	

<script type="text/javascript" src="js/licencias.js"></script>

<title>Licencias OCW</title>

<script type="text/javascript">
    Ext.BLANK_IMAGE_URL = '/ocw/img/blank.png';

    Ext.onReady(function()
    {
        Ext.QuickTips.init();

        var panelEdicion = new UJI.OCW.Licencias();

        new Ext.Viewport(
        {
            layout : 'fit',
            items : [ panelEdicion ]
        });

    });
</script>

</head>

<body>
</body>
</html>