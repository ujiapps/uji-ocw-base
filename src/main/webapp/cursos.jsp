<%@page contentType="text/html; charset=UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta content="text/html; charset=utf-8">

<link rel="stylesheet" type="text/css"
	href="http://devel.uji.es/resources/css/icons.css" />
<link rel="stylesheet" type="text/css"
	href="http://devel.uji.es/resources/js/ext-3.4.0/resources/css/ext-all.css" />
<link rel="stylesheet" type="text/css"
	href="http://devel.uji.es/resources/js/ext-3.4.0/examples/ux/css/RowEditor.css" />
<link rel="stylesheet" type="text/css" href="css/dataviewer.css" />
<script type="text/javascript"
	src="http://devel.uji.es/resources/js/ckeditor-3.6.1/ckeditor.js"></script>

<script type="text/javascript"
	src="http://devel.uji.es/resources/js/ext-3.4.0/adapter/ext/ext-base.js"></script>
<script type="text/javascript"
	src="http://devel.uji.es/resources/js/ext-3.4.0/ext-all-debug.js"></script>

<script type="text/javascript"
	src="http://devel.uji.es/resources/js/uji-commons-extjs/Ext/ux/uji/Util.js"></script>
<script type="text/javascript"
	src="http://devel.uji.es/resources/js/ext-3.4.0/examples/ux/RowEditor.js"></script>
<script type="text/javascript"
	src="http://devel.uji.es/resources/js/uji-commons-extjs/Ext/form/CKEditor/0.0.1/CKEditor.js"></script>
<script type="text/javascript"
	src="http://devel.uji.es/resources/js/uji-commons-extjs/Ext/ux/uji/form/MultiLanguageField/0.0.1/MultiLanguageField.js"></script>
<script type="text/javascript"
	src="http://devel.uji.es/resources/js/uji-commons-extjs/Ext/ux/uji/form/MultiLanguageTextField/0.0.1/MultiLanguageTextField.js"></script>
<script type="text/javascript"
	src="http://devel.uji.es/resources/js/uji-commons-extjs/Ext/ux/uji/form/MultiLanguageTextArea/0.0.1/MultiLanguageTextArea.js"></script>
<script type="text/javascript"
	src="http://devel.uji.es/resources/js/uji-commons-extjs/Ext/ux/uji/form/LookupWindow/0.0.1/LookupWindow.js"></script>
<script type="text/javascript"
	src="http://devel.uji.es/resources/js/uji-commons-extjs/UJI/ContentType/0.0.1/ContentType.js"></script>
<script type="text/javascript"
        src="http://devel.uji.es/resources/js/uji-commons-extjs/Ext/ux/tree/XmlTreeLoader/0.0.1/XmlTreeLoader.js"></script>
<script type="text/javascript" src="js/tabCursoCategorias.js"></script>
<script type="text/javascript" src="js/tabCursoImagenes.js"></script>
<script type="text/javascript" src="js/formCursoDatosGenerales.js"></script>
<script type="text/javascript" src="js/tabCursoMateriales.js"></script>
<script type="text/javascript" src="js/tabCursoAsignaturas.js"></script>
<script type="text/javascript" src="js/tabCursoAutorizados.js"></script>
<script type="text/javascript" src="js/cursoGrid.js"></script>
<script type="text/javascript" src="js/cursoForm.js"></script>
<script type="text/javascript" src="js/cursos.js"></script>

<title>Cursos OCW</title>

<script type="text/javascript">
    Ext.BLANK_IMAGE_URL = 'http://devel.uji.es/resources/js/ext-3.4.0/resources/images/default/s.gif';

    Ext.onReady(function()
    {
        Ext.QuickTips.init();



        new Ext.data.Store(
        {
            restful : true,
            url : '/ocw/rest/idioma/edicion',
            autoLoad : true,
            reader : new Ext.data.XmlReader(
            {
                record : 'Idioma',
                id : 'id'
            }, [ 'id', 'nombre', 'acronimo', 'orden', 'edicion' ]),
            listeners :
            {
                load : function(store)
                {
                    var idiomasAux = new Array();
                    for ( var i = 0; i < store.getCount(); i++)
                    {
                        var idioma = store.getAt(i);
                        if (idioma.get("edicion") == "S")
                        {
                            idiomasAux[i] = idioma.get("acronimo").toLowerCase();
                        }
                    }
                    window.idiomas = idiomasAux;
                    
                    var panel = new UJI.OCW.Cursos();

                    new Ext.Viewport(
                    {
                        layout : 'fit',
                        items : [ panel ]
                    });
                }
            }
        });
    });
</script>

</head>

<body>
</body>
</html>