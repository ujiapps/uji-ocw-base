<%@page contentType="text/html; charset=UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta content="text/html; charset=utf-8">

<link rel="stylesheet" type="text/css" href="http://devel.uji.es/resources/css/icons.css" />
<link rel="stylesheet" type="text/css"
	href="http://devel.uji.es/resources/js/ext-3.4.0/resources/css/ext-all.css" />
<link rel="stylesheet"
	href="http://devel.uji.es/resources/js/ext-3.4.0/examples/ux/css/RowEditor.css" />

<script type="text/javascript"
	src="http://devel.uji.es/resources/js/ext-3.4.0/adapter/ext/ext-base.js"></script>
<script type="text/javascript"
	src="http://devel.uji.es/resources/js/ext-3.4.0/ext-all-debug.js"></script>

<script type="text/javascript"
	src="http://devel.uji.es/resources/js/uji-commons-extjs/Ext/ux/uji/grid/GridPanel/0.0.1/GridPanel.js"></script>

<script type="text/javascript"
	src="http://devel.uji.es/resources/js/ext-3.4.0/examples/ux/RowEditor.js"></script>
<script type="text/javascript" src="js/tags.js"></script>

<title>Tags OCW</title>

<script type="text/javascript">
    Ext.BLANK_IMAGE_URL = 'http://devel.uji.es/resources/js/ext-3.4.0/resources/images/default/s.gif';

    Ext.onReady(function()
    {
        Ext.QuickTips.init();

        var panelEdicion = new UJI.OCW.Tags();

        new Ext.Viewport(
        {
            layout : 'fit',
            items : [ panelEdicion ]
        });

    });
</script>

</head>

<body>
</body>
</html>