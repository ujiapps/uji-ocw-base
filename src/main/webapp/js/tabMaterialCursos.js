Ext.ns('UJI.OCW');

UJI.OCW.tabMaterialCursos = Ext.extend(Ext.Panel,
{
    title : 'Cursos',
    layout : 'vbox',
    layoutConfig :
    {
        align : 'stretch'
    },
    storeMaterialCursos : {},
    gridMaterialCursos : {},

    initComponent : function()
    {
        var config = {};
        Ext.apply(this, Ext.apply(this.initialConfig, config));
        UJI.OCW.tabMaterialCursos.superclass.initComponent.call(this);
        this.initUI();
        this.add(this.gridMaterialCursos);
    },

    initUI : function()
    {
        this.buildStoreMaterialCursos();
        this.buildGridMaterialCursos();
    },

    buildStoreMaterialCursos : function()
    {
        ref = this;

        this.storeMaterialCursos = new Ext.data.Store(
        {
            restful : true,
            url : '/ocw/rest/material',
            autoLoad : false,
            proxy : new Ext.data.HttpProxy(
            {

                url : '/ocw/rest/material/' + ref.materialId + '/cursos'
            }),
            reader : new Ext.data.XmlReader(
            {
                record : 'CursoIdiomaPrincipal',
                id : 'id'
            }, [ 'id', 'nombre', 'persona', 'estado', 'visible', 'fechaCreacion']),
            listeners :
            {
                'beforeload' : function(editor, index)
                {
                    this.proxy.setUrl('/ocw/rest/material/' + ref.materialId + '/cursos');
                }
            }

        });
    },

    buildGridMaterialCursos : function()
    {
        var ref = this;

        var colModel = new Ext.grid.ColumnModel(
        {
            defaults :
            {
                sortable : true
            },
            columns : [
            {
                header : "id",
                width : 25,
                hidden : true,
                dataIndex : 'id'
            },
            {
                header : 'Nom',
                dataIndex : 'nombre'
            },
            {
                header : 'Creat per',
                dataIndex : 'persona'
            },
            {
                header : 'Estat',
                dataIndex : 'estado'
            },
            {
                header : 'Visible',
                dataIndex : 'visible',
                renderer : function(value, metaData, record, rowIndex, colIndex, store)
                {
                    if (value == 'S')
                    {
                        return 'Sí';
                    }
                    else
                    {
                        return 'No';
                    }
                }

            },
            {
                header : 'Data Creació',
                dataIndex : 'fechaCreacion',
                renderer : function(value, metadata)
                {
                    return value.split(" ")[0];
                }               
            } ]
        });

        this.gridMaterialCursos = new Ext.grid.GridPanel(
        {
            frame : true,
            flex : 1,
            loadMask : true,
            viewConfig :
            {
                forceFit : true
            },
            store : ref.storeMaterialCursos,
            sm : new Ext.grid.RowSelectionModel(
            {
                singleSelect : true
            }),
            colModel : colModel
        });

    },
    setMaterialActivo : function(materialId)
    {
        this.materialId = materialId;
        this.storeMaterialCursos.reload();
    }
});