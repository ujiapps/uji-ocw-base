Ext.ns('UJI.OCW');

UJI.OCW.MaterialGrid = Ext.extend(Ext.Panel,
{
    id : 'materialGrid',
    layout : 'vbox',
    layoutConfig :
    {
        align : 'stretch'
    },
    botonBorrar : {},
    botonAnadir : {},
    botonEditar : {},
    storeMateriales : {},
    storeIdiomas : {},
    gridMateriales : {},
    panelFiltro : {},
    filtroCadena : {},
    filtroIdioma : {},
    botonFiltrar : {},
    botonLimpiarFiltros: {},
    bubbleEvents : [ 'materialChangeCard' ],

    initComponent : function()
    {
        var config = {};
        Ext.apply(this, Ext.apply(this.initialConfig, config));
        UJI.OCW.MaterialGrid.superclass.initComponent.call(this);
        this.initUI();
        this.addEvents("materialChangeCard");
        this.add(this.panelFiltro);
        this.add(this.gridMateriales);
    },

    initUI : function()
    {
        this.buildBotonLimpiarFiltros();
        this.buildStoreMateriales();
        this.buildStoreIdiomas();
        this.buildBotonBorrar();
        this.buildBotonAnadir();
        this.buildBotonEditar();
        this.buildFiltroCadena();
        this.buildFiltroIdioma();
        this.buildBotonFiltrar();
        this.buildPanelFiltro();
        this.buildGridMateriales();
    },

    buildStoreIdiomas : function()
    {

        this.storeIdiomas = new Ext.data.Store(
        {
            restful : true,
            url : '/ocw/rest/idioma',
            autoLoad : false,
            reader : new Ext.data.XmlReader(
            {
                record : 'Idioma',
                id : 'id'
            }, [ 'id', 'nombre' ]),
            writer : new Ext.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            })
        });
    },

    buildStoreMateriales : function()
    {

        this.storeMateriales = new Ext.data.Store(
        {
            restful : true,
            url : '/ocw/rest/material',
            autoLoad : true,
            reader : new Ext.data.XmlReader(
            {
                record : 'MaterialCompleto',
                id : 'id'
            }, [ 'id', 'titulo', 'fecha', 'nombrePersona', 'nombreIdioma', 'nombreLicencia', 'contentType' ]),
            writer : new Ext.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            })
        });
    },

    buildBotonAnadir : function()
    {
        var ref = this;

        this.botonAnadir = new Ext.Button(
        {
            text : 'Afegir',
            iconCls : 'application-add',
            handler : function(button, event)
            {
                ref.fireEvent('materialChangeCard', 'materialGrid', 'add');
            }
        });
    },

    buildBotonEditar : function()
    {
        var ref = this;

        this.botonEditar = new Ext.Button(
        {
            text : 'Editar',
            iconCls : 'application-edit',
            bubbleEvents : [ 'materialChangeCard' ],
            handler : function(btn, evt)
            {
                    ref.fireEvent('materialChangeCard', 'materialGrid', 'edit');
            }
        });

    },

    buildBotonLimpiarFiltros : function()
    {
        var ref = this;
        this.botonLimpiarFiltros = new Ext.Button(
        {
            text : 'Netejar filtres',
            iconCls : '',
            margins: "0 0 0 10",
            width : 100,
            handler : function()
            {
                ref.filtroIdioma.setValue(null);
                ref.filtroCadena.setValue("");
            }
        });
    },
    
    buildBotonBorrar : function()
    {
        var ref = this;

        this.botonBorrar = new Ext.Button(
        {
            text : 'Esborrar',
            iconCls : 'application-delete',
            handler : function(boton, evento)
            {

                var record = ref.getMaterialSeleccionado();
                if (!record)
                {
                    return false;
                }
                else
                {
                    Ext.Msg.confirm("Corfirmació de la operació", "Realment dessitga esborrar aquest registre?", function(button, text)
                    {
                        if (button == "yes")
                        {
                            ref.storeMateriales.remove(record);
                        }
                    });
                }
            }
        });
    },

    buildFiltroIdioma : function()
    {
        this.filtroIdioma = new Ext.form.ComboBox(
        {
            store : this.storeIdiomas,
            displayField : 'nombre',
            margins: "0 0 0 10",
            emptyText : 'Selecciona un Idioma',
            fieldLabel : 'Filtrar per idioma',
            editable : false,
            triggerAction : 'all',
            valueField : 'id',
            name : 'filtroIdioma'
        });
    },

    buildFiltroCadena : function()
    {
        this.filtroCadena = new Ext.form.TextField(
        {
            displayField : 'Filtrar',
            name : 'titulo',
            width : 300
        });
    },

    buildBotonFiltrar : function()
    {
        var ref = this;
        this.botonFiltrar = new Ext.Button(
        {
            text : 'Cerca',
            iconCls : 'magnifier',
            margins: "0 0 0 10",
            handler : function()
            {
                ref.reload();
            }
        });
    },

    buildPanelFiltro : function()
    {
        var ref = this;
        this.panelFiltro = new Ext.form.FieldSet(
        {
            items : [
            {
                title : 'Búsqueda de materials',
                xtype : 'panel',
                layout : 'hbox',
                flex : 0,
                padding : 10,
                region : 'north',
                frame : true,
                items : [ this.filtroCadena, this.filtroIdioma, this.botonFiltrar, this.botonLimpiarFiltros ],
                keys : [
                {
                    key : [ Ext.EventObject.ENTER ],
                    handler : function()
                    {
                        ref.botonFiltrar.handler();
                    }
                } ]
            } ]
        });
    },

    reload : function()
    {

        var cadena = this.filtroCadena.getValue();
        var idiomaId = this.filtroIdioma.getValue();

        this.gridMateriales.store.setBaseParam("cadena", cadena);
        this.gridMateriales.store.setBaseParam("idiomaId", idiomaId);
        this.storeMateriales.load();

    },

    getMaterialSeleccionado : function()
    {
        return this.gridMateriales.getSelectionModel().getSelected();
    },

    buildGridMateriales : function()
    {
        var ref = this;

        var colModel = new Ext.grid.ColumnModel(
        {
            defaults :
            {
                sortable : true
            },
            columns : [
            {
                header : "id",
                width : 25,
                hidden : false,
                dataIndex : 'id'
            },
            {
                header : 'Títol',
                dataIndex : 'titulo'

            },
            {
                header : 'Data',
                dataIndex : 'fecha',
                renderer : function(value, metadata)
                {
                    return value.split(" ")[0];
                }
            },
            {
                header : 'Persona',
                dataIndex : 'nombrePersona'
            },

            {
                header : 'Idioma',
                dataIndex : 'nombreIdioma'
            },
            {
                header : 'Licencia',
                dataIndex : 'nombreLicencia'
            },
            {
            	header : 'Tipus',
            	dataIndex : 'contentType',
            	renderer : function(value)
            	{
            		if (value == "") {
            			return "URL";
            		}
            		else
            		{
            			return value;
            		}
            	}
            }]
        });

        this.gridMateriales = new Ext.grid.GridPanel(
        {
            frame : true,
            flex : 1,
            loadMask : true,
            bubbleEvents : [ 'materialChangeCard' ],
            tbar :
            {
                items : [ ref.botonAnadir, ref.botonEditar, ref.botonBorrar ]
            },
            viewConfig :
            {
                forceFit : true
            },
            store : ref.storeMateriales,
            sm : new Ext.grid.RowSelectionModel(
            {
                singleSelect : true
            }),
            colModel : colModel,
            listeners :
            {
                dblclick : function(event)
                {
                    ref.botonEditar.handler.call(ref.botonEditar.scope);
                }
            }
        });
    }
});
