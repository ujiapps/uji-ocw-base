Ext.ns('UJI.OCW');

UJI.OCW.tabCursoCategorias = Ext.extend(Ext.Panel,
{
    title : 'Categories',
    layout : 'vbox',
    cursoId : null,
    layoutConfig :
    {
        align : 'stretch'
    },

    initComponent : function()
    {
        var config = {};
        Ext.apply(this, Ext.apply(this.initialConfig, config));
        UJI.OCW.tabCursoCategorias.superclass.initComponent.call(this);
        this.initUI();
        this.add(this.gridCategorias);
    },

    initUI : function()
    {
        this.buildStoreCategorias();
        this.buildStoreCursoCategorias();
        this.buildBotonBorrarCategoria();
        this.buildBotonAnadirCategoria();
        this.buildGridCategorias();
    },

    buildStoreCategorias : function()
    {

        this.storeCategorias = new Ext.data.Store(
        {
            restful : true,
            url : '/ocw/rest/categoria',
            autoLoad : false,
            reader : new Ext.data.XmlReader(
            {
                record : 'Categoria',
                id : 'id'
            }, [ 'id', 'nombre' ]),
            writer : new Ext.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            })
        });
    },

    buildStoreCursoCategorias : function()
    {

        var ref = this;
        this.storeCursoCategorias = new Ext.data.Store(
        {
            restful : true,
            proxy : new Ext.data.HttpProxy(
            {
                url : '/ocw/rest/curso/' + ref.cursoId + '/categoria'

            }),
            autoLoad : false,
            reader : new Ext.data.XmlReader(
            {
                record : 'CursoCategoria',
                id : 'id'
            }, [ 'id', 'categoriaId', 'orden', 'nombre', 'cursoId' ]),
            writer : new Ext.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            }),
            listeners :
            {
                'beforeload' : function(editor, index)
                {
                    this.proxy.setUrl('/ocw/rest/curso/' + ref.cursoId + '/categoria');
                },
                'beforewrite' : function(editor, index)
                {
                    this.proxy.setUrl('/ocw/rest/curso/' + ref.cursoId + '/categoria');
                }
            }

        });
    },

    buildBotonAnadirCategoria : function()
    {
        var ref = this;

        this.addEvents('LookoupWindowClickSeleccion');
        var window = new Ext.ux.uji.form.LookupWindow(
        {
            appPrefix : 'ocw',
            bean : 'categoria'
        });

        window.on('LookoupWindowClickSeleccion', function(id)
        {
            if (ref.storeCursoCategorias.find('categoriaId', id) == -1)
            {
                var rec = new ref.storeCursoCategorias.recordType(
                {
                    id : '',
                    cursoId : ref.cursoId,
                    categoriaId : id

                });
                ref.storeCursoCategorias.insert(0, rec);

                ref.storeCursoCategorias.commitChanges();
            }
            else
            {
                Ext.Msg.alert('Error', "La categoria ja estaba inserida");
            }
        });

        this.botonAnadirCategoria = new Ext.Button(
        {
            text : 'Afegir',
            iconCls : 'application-add',
            handler : function(button)
            {
                window.executeSearch('%');
                window.show();

            }
        });
    },

    buildBotonBorrarCategoria : function()
    {
        var ref = this;

        this.botonBorrarCategoria = new Ext.Button(
        {
            text : 'Esborrar',
            iconCls : 'application-delete',
            handler : function(boton, evento)
            {
                if (ref.storeCursoCategorias.getCount() > 1)
                {

                    Ext.Msg.confirm("Corfirmació de la operació",
                            "Realment dessitga esborrar aquest registre?", function(button, text)
                            {
                                if (button == "yes")
                                {
                                    var registroSeleccionado = ref.gridCategorias
                                            .getSelectionModel().getSelected();
                                    if (!registroSeleccionado)
                                    {
                                        return false;
                                    }
                                    ref.storeCursoCategorias.remove(registroSeleccionado);
                                }
                            });
                }
                else
                {
                    Ext.Msg.alert('Error', "No es poden esborrar totes les categories.");

                }
            }
        });
    },

    buildGridCategorias : function()
    {
        var ref = this;

        var colModel = new Ext.grid.ColumnModel(
        {
            defaults :
            {
                sortable : true
            },
            columns : [
            {
                header : "id",
                hidden : true,
                dataIndex : 'id'
            },
            {
                header : 'Id',
                dataIndex : 'categoriaId',
                width : 25
            },
            {
                header : 'Nom',
                dataIndex : 'nombre'
            },
            {
                header : 'Ordre',
                dataIndex : 'orden'

            } ]
        });

        this.gridCategorias = new Ext.grid.GridPanel(
        {
            frame : true,
            flex : 1,
            loadMask : true,
            tbar :
            {
                items : [ ref.botonAnadirCategoria, ref.botonBorrarCategoria ]
            },
            viewConfig :
            {
                forceFit : true
            },
            store : ref.storeCursoCategorias,
            sm : new Ext.grid.RowSelectionModel(
            {
                singleSelect : true
            }),
            colModel : colModel
        });

    },

    setCursoActivo : function(cursoId)
    {
        this.cursoId = cursoId;
        this.storeCursoCategorias.reload();
    }
});