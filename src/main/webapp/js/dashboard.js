function initOCWDashboard()
{
    var dashboardPanel = new Ext.Panel(
    {
        xtype : 'panel',
        frame : true,
        padding : '10',
        title : 'Open CourseWare',
        layout :
        {
            type : 'vbox',
            align : 'stretch'
        },
        items : [  ]
    });

    return dashboardPanel;
}