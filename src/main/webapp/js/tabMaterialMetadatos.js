Ext.ns('UJI.OCW');

UJI.OCW.tabMaterialMetadatos = Ext.extend(Ext.Panel,
{
    id : 'materialMetadatos',
    title : 'Metadades',
    closable : true,
    layout : 'vbox',
    layoutConfig :
    {
        align : 'stretch'
    },
    ventanaEmergente : {},
    botonBorrar : {},
    botonAnadir : {},
    botonGuardar : {},
    botonCancelar : {},
    storeMetadatos : {},
    comboMetadatos : {},
    botonEditar : {},
    storeMaterialMetadatos : {},
    gridMaterialMetadatos : {},
    formMetadatosValores : {},
    storeValoresMetadatos : {},
    comboValoresMetadatos : {},
    textValor : {},
    materialId : 0,
    clean : {},

    initComponent : function()
    {
        var config = {};
        Ext.apply(this, Ext.apply(this.initialConfig, config));
        UJI.OCW.tabMaterialMetadatos.superclass.initComponent.call(this);
        Ext.util.Format.comboRenderer = function(combo)
        {
            return function(value)
            {
                var record = combo.findRecord(combo.valueField, value);
                return record ? record.get(combo.displayField) : combo.valueNotFoundText;
            };
        };

        this.initUI();
        this.add(this.gridMaterialMetadatos);
    },

    initUI : function()
    {
        this.buildBotonEditar();
        this.buildStoreMetadatos();
        this.buildStoreMaterialMetadatos();
        this.buildStoreValoresMetadatos();
        this.buildBotonGuardar();
        this.buildBotonCancelar();
        this.buildBotonBorrar();
        this.buildBotonAnadir();
        this.buildComboMetadatos();
        this.buildgridMaterialMetadatos();
        this.buildComboValoresMetadatos();
        this.buildTextValor();
        this.buildFormMetadatosValores();
        this.buildVentanaEmergente();
    },

    buildStoreMetadatos : function()
    {

        this.storeMetadatos = new Ext.data.Store(
        {
            restful : true,
            url : '/ocw/rest/metadato',
            autoLoad : false,
            reader : new Ext.data.XmlReader(
            {
                record : 'Metadato',
                id : 'id'
            }, [ 'id', 'limitado', 'obligatorio', 'tag' ]),
            writer : new Ext.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            })
        });
    },

    buildStoreMaterialMetadatos : function()
    {
        var ref = this;

        this.storeMaterialMetadatos = new Ext.data.Store(
        {
            restful : true,
            url : '/ocw/rest/materialMetadato',
            autoLoad : false,
            baseParams :
            {
                materialId : ref.materialId
            },
            reader : new Ext.data.XmlReader(
            {
                record : 'MaterialMetadato',
                id : 'id'
            }, [ 'id', 'tag', 'valor', 'materialId', 'metadatoId', 'limitado', 'obligatorio' ]),
            writer : new Ext.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            }),
            listeners :
            {
                'beforeload' : function(editor, index)
                {
                    this.setBaseParam('materialId', ref.materialId);
                }
            }
        });
    },

    buildStoreValoresMetadatos : function()
    {

        this.storeValoresMetadatos = new Ext.data.Store(
        {
            restful : true,
            url : '/ocw/rest/metadatoValor',
            autoLoad : false,
            baseParams :
            {
                metadatoId : 0
            },
            reader : new Ext.data.XmlReader(
            {
                record : 'MetadatoValor',
                id : 'id'
            }, [ 'id', 'metadatoId', 'valor', 'orden' ])

        });
    },

    buildVentanaEmergente : function()
    {
        var ref = this;

        this.ventanaEmergente = new Ext.Window(
        {
            title : 'Metadada',
            layout : 'fit',
            modoEdicionVentana : false,
            modal : true,
            record : false,
            width : 350,
            height : 200,
            closeAction : 'hide',
            closable : true,
            items : [ ref.formMetadatosValores ],
            buttons : [ ref.botonGuardar, ref.botonCancelar ]

        });
    },

    buildFormMetadatosValores : function()
    {

        var ref = this;
        this.formMetadatosValores = new Ext.FormPanel(
        {
            labelWidth : 60,
            frame : true,
            bodyStyle : 'padding:5px 5px 0',
            width : 350,
            defaultType : 'textfield',

            plain : true,
            items : [ ref.comboMetadatos, ref.comboValoresMetadatos, ref.textValor ]
        });

    },

    buildComboMetadatos : function()
    {
        var ref = this;

        this.comboMetadatos = new Ext.form.ComboBox(
        {
            emptyText : 'Selecciona una metadada',
            fieldLabel : 'Metadada',
            forceSelection : true,
            editable : false,
            triggerAction : 'all',
            store : ref.storeMetadatos,
            valueField : 'id',
            displayField : 'tag',
            allowBlank : false,
            listeners :
            {
                select : function(combo, record, index)
                {
                    if (record.get('limitado') == 'N')
                    {
                        ref.comboValoresMetadatos.setValue(null);
                        // ref.comboValoresMetadatos.setDisabled(true);
                        ref.comboValoresMetadatos.setVisible(false);
                        ref.textValor.setVisible(true);
                        ref.textValor.setDisabled(false);
                    }
                    else
                    {
                        ref.textValor.setValue(null);
                        ref.textValor.setVisible(false);
                        ref.comboValoresMetadatos.setVisible(true);
                        ref.comboValoresMetadatos.setDisabled(false);
                        ref.storeValoresMetadatos.setBaseParam('metadatoId', record.get('id'));
                        ref.storeValoresMetadatos.load();
                    }
                }
            }
        });
    },

    buildComboValoresMetadatos : function()
    {
        var ref = this;
        this.comboValoresMetadatos = new Ext.form.ComboBox(
        {
            emptyText : 'Selecciona un valor de la metadada',
            fieldLabel : 'Valor',
            forceSelection : true,
            editable : false,
            triggerAction : 'all',
            disabled : true,
            store : ref.storeValoresMetadatos,
            valueField : 'valor',
            displayField : 'valor',
            allowBlank : false
        });
        ref.comboValoresMetadatos.setVisible(true);
    },

    buildTextValor : function()
    {
        var ref = this;
        this.textValor = new Ext.form.TextField(
        {
            xtype : 'textfield',
            fieldLabel : 'Valor',
            allowBlank : false,
            name : 'Valor',
            autoWidth : true
        });
        ref.textValor.setVisible(true);
    },

    buildBotonEditar : function()
    {
        var ref = this;

        this.botonEditar = new Ext.Button(
        {
            text : 'Editar',
            iconCls : 'application-edit',
            handler : function(btn, evt)
            {
                var record = ref.gridMaterialMetadatos.getSelectionModel().getSelections()[0];

                if (record)
                {
                    ref.comboMetadatos.setValue(record.get('metadatoId'));
                    ref.comboMetadatos.setDisabled(true);

                    if (record.get('limitado') == 'S')
                    {
                        ref.storeValoresMetadatos.setBaseParam('metadatoId', record.get('metadatoId'));
                        ref.comboValoresMetadatos.setValue(record.get('valor'));
                        ref.comboValoresMetadatos.setDisabled(false);
                        ref.comboValoresMetadatos.setVisible(true);
                        ref.textValor.setValue(null);
                        ref.textValor.setVisible(false);
                        ref.storeValoresMetadatos.load();
                    }
                    else
                    {
                        ref.comboValoresMetadatos.setValue(null);
                        // ref.comboValoresMetadatos.setDisabled(true);
                        ref.comboValoresMetadatos.setVisible(false);
                        ref.textValor.setValue(record.get('valor'));
                        ref.textValor.setVisible(true);
                        ref.textValor.setDisabled(false);
                    }
                    ref.ventanaEmergente.modoEdicionVentana = true;
                    ref.ventanaEmergente.show();
                }
            }
        });

    },

    buildBotonAnadir : function()
    {

        var ref = this;

        this.botonAnadir = new Ext.Button(
        {
            text : 'Afegir',
            iconCls : 'application-add',
            handler : function(btn, evt)
            {
                ref.comboMetadatos.setDisabled(false);
                ref.comboMetadatos.setValue(null);
                ref.comboValoresMetadatos.setValue(null);
                ref.comboValoresMetadatos.setDisabled(true);
                ref.textValor.setValue(null);
                ref.textValor.setVisible(false);
                ref.ventanaEmergente.modoEdicionVentana = false;
                ref.ventanaEmergente.show();
            }
        });

    },
    buildBotonGuardar : function()
    {

        var ref = this;
        var valorsel = null;
        this.botonGuardar = new Ext.Button(
        {
            text : 'Guardar',
            handler : function(btn, evt)
            {
                if (ref.comboValoresMetadatos.value == null)
                {
                    valorsel = ref.textValor.getValue();
                }
                else
                {
                    valorsel = ref.comboValoresMetadatos.getValue();
                }

                if (valorsel != null && valorsel != "")
                {
                    if (ref.ventanaEmergente.modoEdicionVentana)
                    {
                        var record = ref.gridMaterialMetadatos.getSelectionModel().getSelections()[0];
                        record.set('valor', valorsel);
                    }
                    else
                    {
                        var rec = new ref.storeMaterialMetadatos.recordType(
                        {
                            id : '',
                            materialId : ref.materialId,
                            metadatoId : ref.comboMetadatos.value,
                            valor : valorsel
                        });

                        ref.storeMaterialMetadatos.insert(0, rec);
                    }
                    ref.ventanaEmergente.hide();
                }
            }
        });
    },

    buildBotonCancelar : function()
    {

        var ref = this;

        this.botonCancelar = new Ext.Button(
        {
            text : 'Cancelar',
            handler : function(btn, evt)
            {
                ref.ventanaEmergente.hide();
            }
        });

    },
    buildBotonBorrar : function()
    {
        var ref = this;

        this.botonBorrar = new Ext.Button(
        {
            text : 'Esborrar',
            iconCls : 'application-delete',
            handler : function(boton, evento)
            {

                Ext.Msg.confirm("Corfirmació de la operació", "Realment dessitga esborrar aquest registre?", function(button, text)
                {
                    if (button == "yes")
                    {
                        var registroSeleccionado = ref.gridMaterialMetadatos.getSelectionModel().getSelected();
                        if (!registroSeleccionado)
                        {
                            return false;
                        }
                        ref.storeMaterialMetadatos.remove(registroSeleccionado);
                    }
                });
            }
        });
    },

    buildgridMaterialMetadatos : function()
    {
        var ref = this;

        var colModel = new Ext.grid.ColumnModel(
        {
            defaults :
            {
                sortable : true
            },
            columns : [
            {
                header : "id",
                width : 25,
                hidden : true,
                dataIndex : 'id'
            },
            {
                header : 'Metadada',
                dataIndex : 'metadatoId',
                renderer : Ext.util.Format.comboRenderer(ref.comboMetadatos)
            },
            {
                header : 'Valor',
                dataIndex : 'valor'
            } ]
        });

        this.gridMaterialMetadatos = new Ext.grid.GridPanel(
        {
            frame : true,
            flex : 1,
            loadMask : true,
            tbar :
            {
                items : [ ref.botonAnadir, ref.botonEditar, ref.botonBorrar ]
            },
            viewConfig :
            {
                forceFit : true
            },
            store : ref.storeMaterialMetadatos,
            sm : new Ext.grid.RowSelectionModel(
            {
                singleSelect : true
            }),
            colModel : colModel,
            listeners :
            {
                'rowdblclick' : function(grid, rowIndex, event)
                {
                    ref.botonEditar.handler.call(ref.botonEditar.scope, ref.botonEditar, Ext.EventObject);
                }

            }
        });

    },
    setMaterialActivo : function(materialId)
    {
        this.materialId = materialId;
        syncStoreLoad([ this.storeMetadatos, this.storeMaterialMetadatos ]);
    }
});