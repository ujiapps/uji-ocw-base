Ext.ns('UJI.OCW');

UJI.OCW.tabCursoAsignaturas = Ext.extend(Ext.Panel,
{
    title : 'Assignatures',
    closable : true,
    layout : 'vbox',
    autoScroll : true,
    layoutConfig :
    {
        align : 'stretch'
    },
    cursoId : null,
    storeAsignaturas : {},
    storeCursoAsignaturas : {},
    botonAnadir : {},
    botonBorrar : {},
    gridAsignaturas : {},
    // editor : {},
    initComponent : function()
    {
        var config = {};
        Ext.apply(this, Ext.apply(this.initialConfig, config));
        UJI.OCW.tabCursoAsignaturas.superclass.initComponent.call(this);
        this.initUI();
        this.add(this.gridAsignaturas);
    },

    initUI : function()
    {
        this.buildStoreAsignaturas();
        this.buildStoreCursoAsignaturas();
        this.buildBotonAnadir();
        this.buildBotonBorrar();
        this.buildGridAsignaturas();

    },

    setCursoActivo : function(cursoId)
    {
        this.cursoId = cursoId;
        this.storeCursoAsignaturas.reload();
        this.gridAsignaturas.store.reload();

    },

    buildStoreAsignaturas : function()
    {
        this.storeAsignaturas = new Ext.data.Store(
        {
            restful : true,
            url : '/ocw/rest/asignaturas',
            autoLoad : false,
            reader : new Ext.data.XmlReader(
            {
                record : 'Asignatura',
                id : 'id'
            }, [ 'id', 'nombreCa' ]),
            writer : new Ext.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            })
        });
    },
    buildStoreCursoAsignaturas : function()
    {

        var ref = this;
        this.storeCursoAsignaturas = new Ext.data.Store(
        {
            restful : true,
            proxy : new Ext.data.HttpProxy(
            {
                url : '/ocw/rest/curso/' + ref.cursoId + '/asignatura'

            }),
            autoLoad : false,
            reader : new Ext.data.XmlReader(
            {
                record : 'CursoAsignatura',
                id : 'id'
            }, [ 'id', 'asignaturaId', 'asignatura', 'cursoId' ]),
            writer : new Ext.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            }),
            listeners :
            {
                'beforeload' : function(editor, index)
                {
                    this.proxy.setUrl('/ocw/rest/curso/' + ref.cursoId + '/asignatura');
                },
                'beforewrite' : function(editor, index)
                {
                    this.proxy.setUrl('/ocw/rest/curso/' + ref.cursoId + '/asignatura');
                }
            }

        });
    },

    buildBotonAnadir : function()
    {
        var ref = this;

        this.addEvents('LookoupWindowClickSeleccion');
        var window = new Ext.ux.uji.form.LookupWindow(
        {
            appPrefix : 'ocw',
            bean : 'asignatura'
        });

        window.on('LookoupWindowClickSeleccion', function(id)
        {
            if (ref.storeCursoAsignaturas.find('asignaturaId', id) == -1)
            {
                var rec = new ref.storeCursoAsignaturas.recordType(
                {
                    id : '',
                    asignaturaId : id,
                    cursoId : ref.cursoId
                });
                ref.storeCursoAsignaturas.insert(0, rec);
                ref.storeCursoAsignaturas.commitChanges();
            }
            else
            {
                Ext.Msg.alert('Error', "L'assignatura ja estaba inserida");
            }
        });

        this.botonAnadir = new Ext.Button(
        {
            text : 'Afegir assignatura',
            iconCls : 'application-add',
            handler : function(button)
            {
                window.show();
            }
        });
    },

    buildBotonBorrar : function()
    {
        var ref = this;

        this.botonBorrar = new Ext.Button(
        {
            text : 'Esborrar',
            iconCls : 'application-delete',
            handler : function(boton, evento)
            {

                var registroSeleccionado = ref.gridAsignaturas.getSelectionModel().getSelected();
                if (!registroSeleccionado)
                {
                    return false;
                }
                else
                {
                    Ext.Msg.confirm("Corfirmació de la operació",
                            "Realment dessitga esborrar aquest registre?", function(button, text)
                            {
                                if (button == "yes")
                                {
                                    ref.storeCursoAsignaturas.remove(registroSeleccionado);
                                }
                            });
                }
            }
        });
    },
    buildGridAsignaturas : function()
    {
        var ref = this;

        var colModel = new Ext.grid.ColumnModel(
        {
            defaults :
            {
                sortable : true
            },
            columns : [
            {
                header : "id",
                hidden : true,
                dataIndex : 'id'
            },
            {
                header : 'Codi',
                width : 25,
                dataIndex : 'asignaturaId'

            },
            {
                header : 'Assignatura',
                dataIndex : 'asignatura'
            } ]
        });

        this.gridAsignaturas = new Ext.grid.GridPanel(
        {
            frame : true,
            flex : 1,
            loadMask : true,
            tbar :
            {
                items : [ ref.botonAnadir, ref.botonBorrar ]
            },
            viewConfig :
            {
                forceFit : true
            },
            store : ref.storeCursoAsignaturas,
            sm : new Ext.grid.RowSelectionModel(
            {
                singleSelect : true
            }),
            colModel : colModel
        });
    }
});