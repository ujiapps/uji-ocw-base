Ext.ns('UJI.OCW');

UJI.OCW.tabCursoImagenes = Ext.extend(Ext.Panel,
{
    title : 'Imatges',
    layout : 'form',
    cursoId : null,
    storeCursoImagenes : {},
    formGestionImagenes : {},
    dataviewImagenes : {},
    botonEliminar : {},
    campoImagen : {},
    botonSubirImagen : {},
    panelImagenes : {},
    layoutConfig :
    {
        align : 'stretch'
    },

    initComponent : function()
    {
        var config = {};
        Ext.apply(this, Ext.apply(this.initialConfig, config));
        UJI.OCW.tabCursoImagenes.superclass.initComponent.call(this);
        this.initUI();
        this.add(this.formGestionImagenes);
        this.add(this.panelImagenes);

    },

    initUI : function()
    {
        this.buildCampoImagen();
        this.buildBotonEliminar();
        this.buildBotonSubirImagen();
        this.buildStoreCursoImagenes();
        this.buildFormGestionImagenes();
        this.buildDataviewImagenes();
        this.buildPanelImagenes();
    },

    buildStoreCursoImagenes : function()
    {

        var ref = this;
        this.storeCursoImagenes = new Ext.data.Store(
        {
            restful : true,
            proxy : new Ext.data.HttpProxy(
            {
                url : '/ocw/rest/curso/' + ref.cursoId + '/imagen'

            }),
            autoLoad : false,
            reader : new Ext.data.XmlReader(
            {
                record : 'CursoImagen',
                id : 'id'
            }, [ 'id', 'cursoId', 'url' ]),
            writer : new Ext.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            }),
            listeners :
            {
                'beforeload' : function(editor, index)
                {
                    this.proxy.setUrl('/ocw/rest/curso/' + ref.cursoId + '/imagen');
                }
            }

        });
    },

    buildBotonEliminar : function()
    {
        var ref = this;
        this.botonEliminar = new Ext.Button(
        {
            text : 'Esborrar',
            iconCls : 'cancel',
            handler : function()
            {
                if (ref.dataviewImagenes.getSelectionCount() > 0)
                {
                    var record = ref.dataviewImagenes.getSelectedRecords()[0];
                    ref.storeCursoImagenes.remove(record);
                }
            }
        });
    },

    buildBotonSubirImagen : function()
    {
        var ref = this;

        this.botonSubirImagen = new Ext.Button(
        {
            iconCls : 'arrow-up',
            text : 'Pujar imatge',
            height : 22,
            handler : function()
            {
                var form = ref.formGestionImagenes.getForm();

                if (form.isValid())
                {
                    form.submit(
                    {
                        method : 'post',
                        waitMsg : 'Pujant imatge...',
                        clientValidation : true,
                        url : '/ocw/rest/curso/' + ref.cursoId + '/imagen',
                        success : function()
                        {
                            ref.storeCursoImagenes.reload();
                            ref.campoImagen.reset();
                        }
                    });
                }
            }
        });
    },
    buildCampoImagen : function()
    {
        this.campoImagen = new Ext.form.TextField(
        {
            fieldLabel : 'Seleccionar imatge',
            name : 'imagen',
            allowBlank: false,
            inputType : 'file',
            handler : function()
            {
                if (this.campoNombreDocumento.getValue())
                {
                    window.open('/ocw/rest/curso/' + ref.materialId + '/raw');
                }
            }
        });
    },
    buildFormGestionImagenes : function()
    {
        var ref = this;
        this.formGestionImagenes = new Ext.FormPanel(
        {
            frame : true,
            url : '/ocw/rest/' + ref.cursoId + '/imagen',
            fileUpload : true,
            errorReader : new Ext.data.XmlReader(
            {
                record : 'responseMessage',
                success : 'success'
            }, [ 'success', 'msg' ]),
            items : [
            {
                'xtype' : 'panel',
                layout : 'hbox',
                items : [
                {
                    'xtype' : 'fieldset',
                    'title' : 'Pujar una nova imatge',
                    layout : 'hbox',
                    height : 70,
                    labelWidth : 150,
                    flex : 1,
                    items : [ this.campoImagen, this.botonSubirImagen ]
                } ]
            } ]
        });
    },

    buildDataviewImagenes : function()
    {
        var tpl = new Ext.XTemplate('<tpl for=".">', '<div class="thumb-wrap">', '<div class="thumb"><img src="{url}"></div>', '</div>', '</tpl>', '<div class="x-clear"></div>');

        this.dataviewImagenes = new Ext.DataView(
        {
            store : this.storeCursoImagenes,
            singleSelect : true,
            tpl : tpl,
            autoHeight : true,
            overClass : 'x-view-over',
            itemSelector : 'div.thumb-wrap',
            emptyText : 'No n\'hi han imatges.'

        });
    },

    buildPanelImagenes : function()
    {
        var ref = this;

        this.panelImagenes = new Ext.Panel(
        {
            frame : true,
            autoHeight : true,
            height : 300,
            collapsible : false,
            layout : 'fit',
            tbar : [ ref.botonEliminar ],
            flex : 1,
            title : 'Gestió d\'imatges',
            items : [ ref.dataviewImagenes ]
        });
    },

    setCursoActivo : function(cursoId)
    {
        this.cursoId = cursoId;
        this.storeCursoImagenes.reload();
    }
});