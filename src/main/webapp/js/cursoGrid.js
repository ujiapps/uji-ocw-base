Ext.ns('UJI.OCW');

UJI.OCW.CursoGrid = Ext.extend(Ext.Panel,
{
    layout : 'vbox',
    layoutConfig :
    {
        align : 'stretch'
    },
    botonBorrar : {},
    botonAnadir : {},
    botonEditar : {},
    storeCursos : {},
    botonGuardar : {},
    botonCancelar : {},
    storeCategorias : {},
    gridCursos : {},
    panelFiltro : {},
    formDatosGenerales : {},
    ventanaCursoDatosGenerales : {},
    filtroCadena : {},
    filtroCategoria : {},
    botonFiltrar : {},
    botonLimpiarFiltros : {},

    bubbleEvents : [ 'cursoChangeCard' ],

    initComponent : function()
    {
        var config = {};
        Ext.apply(this, Ext.apply(this.initialConfig, config));
        UJI.OCW.CursoGrid.superclass.initComponent.call(this);
        this.initUI();
        this.add(this.panelFiltro);
        this.add(this.gridCursos);
    },

    initUI : function()
    {
        this.buildBotonLimpiarFiltros();
        this.buildBotonGuardar();
        this.buildBotonCancelar();
        this.buildFormDatosGenerales();
        this.buildVentanaCursoDatosGenerales();
        this.buildStoreCursos();
        this.buildStoreCategorias();
        this.buildBotonBorrar();
        this.buildBotonAnadir();
        this.buildBotonEditar();
        this.buildFiltroCadena();
        this.buildFiltroCategoria();
        this.buildBotonFiltrar();
        this.buildPanelFiltro();
        this.buildGridCursos();
    },

    buildStoreCategorias : function()
    {

        this.storeCategorias = new Ext.data.Store(
        {
            restful : true,
            url : '/ocw/rest/categoria/idiomaPrincipal',
            autoLoad : false,
            reader : new Ext.data.XmlReader(
            {
                record : 'CategoriaIdiomaPrincipal',
                id : 'id'
            }, [ 'id', 'orden', 'nombre' ]),
            writer : new Ext.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            })
        });
    },

    buildStoreCursos : function()
    {

        this.storeCursos = new Ext.data.Store(
        {
            restful : true,
            url : '/ocw/rest/curso',
            autoLoad : true,
            reader : new Ext.data.XmlReader(
            {
                record : 'CursoIdiomaPrincipal',
                id : 'id'
            }, [
            {
                name : 'id',
                sortType : 'asInt',
                type : 'int'
            }, 'personaId', 'nombre', 'categorias', 'visible', 'fechaCreacion', 'fechaBaja',
                    'estado' ]),
            writer : new Ext.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            })
        });
    },

    buildBotonGuardar : function()
    {

        var ref = this;

        this.botonGuardar = new Ext.Button(
        {
            text : 'Guardar',
            handler : function(btn, evt)
            {
                var form = ref.formDatosGenerales.getForm();

                if (!ref.formDatosGenerales.campoNombre.editor.getValue())
                {
                    Ext.Msg.alert('Error', 'El camp Nom es obligatori');
                }
                else
                {

                    if (form.isValid())
                    {
                        ref.formDatosGenerales.submitForm();
                    }
                    else
                    {
                        Ext.Msg.alert('Error', 'Revisa els camps del formulari');
                    }
                }
            }
        });
    },

    buildBotonCancelar : function()
    {

        var ref = this;

        this.botonCancelar = new Ext.Button(
        {
            text : 'Cancelar',
            handler : function(btn, evt)
            {
                ref.ventanaCursoDatosGenerales.hide();
            }
        });

    },
    buildFormDatosGenerales : function()
    {        
        this.formDatosGenerales = new UJI.OCW.formCursoDatosGenerales(
        {
            extendido : false            
        });
    },

    buildVentanaCursoDatosGenerales : function()
    {
        var ref = this;

        this.ventanaCursoDatosGenerales = new Ext.Window(
        {
            title : 'Afegir nou curs',
            layout : 'fit',
            modoEdicionVentana : false,
            modal : true,
            record : false,
            width : 640,
            height : 240,
            closeAction : 'hide',
            closable : true,
            items : [ ref.formDatosGenerales ],
            buttons : [ ref.botonGuardar, ref.botonCancelar ],
            onEsc : function()
            {
                ref.botonCancelar.handler.call(ref.botonCancelar.scope);
            },

            listeners :
            {
                'cursoInsertadoCorrectamente' : function(id)
                {
                    this.hide();
                    ref.fireEvent('cursoChangeCard', 'cursoGrid', 'edit', id);

                }
            }

        });
    },

    buildBotonAnadir : function()
    {
        var ref = this;

        this.botonAnadir = new Ext.Button(
        {
            text : 'Afegir',
            iconCls : 'application-add',
            handler : function(button, event)
            {
                ref.formDatosGenerales.cleanForm();
                 if (ref.filtroCategoria.getValue()){
                     ref.formDatosGenerales.setComboCategorias(ref.filtroCategoria.getValue());
                 }
                ref.ventanaCursoDatosGenerales.show();
            }
        });
    },

    buildBotonEditar : function()
    {
        var ref = this;

        this.botonEditar = new Ext.Button(
        {
            text : 'Editar',
            iconCls : 'application-edit',
            handler : function(btn, evt)
            {
                var record = ref.getCursoSeleccionado();

                if (record)
                {
                    ref.fireEvent('cursoChangeCard', 'cursoGrid', 'edit');
                }
            }
        });

    },

    buildBotonBorrar : function()
    {
        var ref = this;

        this.botonBorrar = new Ext.Button(
        {
            text : 'Esborrar',
            iconCls : 'application-delete',
            handler : function(boton, evento)
            {

                var registroSeleccionado = ref.gridCursos.getSelectionModel().getSelected();
                if (!registroSeleccionado)
                {
                    return false;
                }
                else
                {
                    Ext.Msg.confirm("Corfirmació de la operació",
                            "Realment dessitga esborrar aquest registre?", function(button, text)
                            {
                                if (button == "yes")
                                {
                                    ref.storeCursos.remove(registroSeleccionado);
                                }
                            });
                }
            }
        });
    },

    buildFiltroCategoria : function()
    {
        this.filtroCategoria = new Ext.form.ComboBox(
        {
            store : this.storeCategorias,
            displayField : 'nombre',
            emptyText : 'Selecciona una Categoria',
            fieldLabel : 'Filtrar per Categoria',
            forceSelection : false,
            margins : "0 0 0 10",
            editable : false,
            triggerAction : 'all',
            valueField : 'id',
            name : 'filtroCategoria'
        });
    },

    buildFiltroCadena : function()
    {
        this.filtroCadena = new Ext.form.TextField(
        {
            displayField : 'Filtrar',
            name : 'titulo',
            width : 300
        });
    },

    buildBotonFiltrar : function()
    {
        var ref = this;
        this.botonFiltrar = new Ext.Button(
                {
                    text : 'Cerca',
                    iconCls : 'magnifier',
                    margins : "0 0 0 10",
                    width : 100,
                    handler : function()
                    {
                        ref.reload();
                    }
                });
    },

    buildBotonLimpiarFiltros : function()
    {
        var ref = this;
        this.botonLimpiarFiltros = new Ext.Button(
        {
            text : 'Netejar filtres',
            iconCls : '',
            margins : "0 0 0 10",
            width : 100,
            handler : function()
            {
                ref.filtroCategoria.setValue(null);
                ref.filtroCadena.setValue("");
            }
        });
    },

    buildPanelFiltro : function()
    {
        var ref = this;
        this.panelFiltro =
        {
            title : 'Búsqueda de cursos',
            xtype : 'panel',
            layout : 'hbox',
            padding : 10,
            flex : 1,
            height : 100,
            frame : true,
            items : [ this.filtroCadena, this.filtroCategoria, this.botonFiltrar,
                    this.botonLimpiarFiltros ],
            keys : [
            {
                key : [ Ext.EventObject.ENTER ],
                handler : function()
                {
                    ref.botonFiltrar.handler();
                }
            } ]
        };
    },

    reload : function()
    {
        var cadena = this.filtroCadena.getValue();
        var categoriaId = this.filtroCategoria.getValue();

        this.gridCursos.store.setBaseParam("cadena", cadena);
        this.gridCursos.store.setBaseParam("categoriaId", categoriaId);
        this.storeCursos.load();
    },

    getCursoSeleccionado : function()
    {
        return this.gridCursos.getSelectionModel().getSelected();
    },

    buildGridCursos : function()
    {
        var ref = this;

        var colModel = new Ext.grid.ColumnModel(
        {
            defaults :
            {
                sortable : true
            },
            columns : [
            {
                header : "id",
                width : 25,
                hidden : false,
                dataIndex : 'id'
            },
            {
                header : 'Nom',
                dataIndex : 'nombre'
            },
            {
                header : 'Visible',
                dataIndex : 'visible',
                renderer : function(val, meta, record)
                {
                    if (!val)
                    {
                        return "N";
                    }
                    else
                    {
                        return val;
                    }
                }
            },
            {
                header : 'F. Creació',
                dataIndex : 'fechaCreacion',
                renderer : function(value, metadata)
                {
                    return value.split(" ")[0];
                }
            },
            {
                header : 'Categorias',
                dataIndex : 'categorias'

            } ]
        });

        this.gridCursos = new Ext.grid.GridPanel(
        {
            frame : true,
            flex : 1,
            loadMask : true,
            bubbleEvents : [ 'cursoChangeCard' ],
            tbar :
            {
                items : [ ref.botonAnadir, ref.botonEditar, ref.botonBorrar ]
            },
            viewConfig :
            {
                forceFit : true
            },
            store : ref.storeCursos,
            sm : new Ext.grid.RowSelectionModel(
            {
                singleSelect : true
            }),
            colModel : colModel,
            listeners :
            {
                dblclick : function(event)
                {
                    ref.botonEditar.handler.call(ref.botonEditar.scope);
                }
            }
        });
    }
});
