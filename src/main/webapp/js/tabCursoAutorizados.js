Ext.ns('UJI.OCW');

UJI.OCW.tabCursoAutorizados = Ext.extend(Ext.Panel,
{
    title : 'Autoritzats',
    closable : true,

    layout : 'vbox',
    autoScroll : true,
    layoutConfig :
    {
        align : 'stretch'
    },
    cursoId : null,
    storePersonas : {},
    storeCursoAutorizados : {},
    botonAnadir : {},
    botonBorrar : {},
    gridAutorizados : {},
    initComponent : function()
    {
        var config = {};
        Ext.apply(this, Ext.apply(this.initialConfig, config));
        UJI.OCW.tabCursoAutorizados.superclass.initComponent.call(this);
        this.initUI();
        this.add(this.gridAutorizados);
    },

    initUI : function()
    {
        this.buildStorePersonas();
        this.buildStoreCursoAutorizados();
        this.buildBotonAnadir();
        this.buildBotonBorrar();
        this.buildGridAutorizados();

    },

    setCursoActivo : function(cursoId)
    {
        this.cursoId = cursoId;
        this.storeCursoAutorizados.reload();
        this.gridAutorizados.store.reload();

    },
    buildStorePersonas : function()
    {
        this.storePersonas = new Ext.data.Store(
        {
            restful : true,
            url : '/ocw/rest/persona',
            autoLoad : false,
            reader : new Ext.data.XmlReader(
            {
                record : 'Persona',
                id : 'id'
            }, [ 'id', 'nombre', 'apellido1', 'apellido2', 'email' ]),
            writer : new Ext.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            })
        });
    },

    buildStoreCursoAutorizados : function()
    {

        var ref = this;
        this.storeCursoAutorizados = new Ext.data.Store(
        {
            restful : true,
            proxy : new Ext.data.HttpProxy(
            {
                url : '/ocw/rest/curso/' + ref.cursoId + '/autorizado'
            }),
            autoLoad : false,
            reader : new Ext.data.XmlReader(
            {
                record : 'CursoAutorizado',
                id : 'id'
            }, [ 'id', 'personaId', 'nombre', 'cursoId' ]),
            writer : new Ext.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            }),
            listeners :
            {
                'beforeload' : function(editor, index)
                {
                    this.proxy.setUrl('/ocw/rest/curso/' + ref.cursoId + '/autorizado');
                },
                'beforewrite' : function(editor, index)
                {
                    this.proxy.setUrl('/ocw/rest/curso/' + ref.cursoId + '/autorizado');
                }
            }

        });
    },
    buildBotonAnadir : function()
    {
        var ref = this;

        this.addEvents('LookoupWindowClickSeleccion');
        var window = new Ext.ux.uji.form.LookupWindow(
        {
            appPrefix : 'ocw',
            bean : 'persona'
        });

        window.on('LookoupWindowClickSeleccion', function(id)
        {
            if (ref.storeCursoAutorizados.find('personaId', id) == -1)
            {
                var rec = new ref.storeCursoAutorizados.recordType(
                {
                    id : '',
                    personaId : id,
                    cursoId : ref.cursoId
                });
                ref.storeCursoAutorizados.insert(0, rec);
                ref.storeCursoAutorizados.commitChanges();
            }
            else
            {
                Ext.Msg.alert('Error', "La persona autoritzada ja estaba inserida");
            }
        });

        this.botonAnadir = new Ext.Button(
        {
            text : 'Afegir autoritzat',
            iconCls : 'application-add',
            handler : function(button)
            {
                window.show();
            }
        });
    },

    buildBotonBorrar : function()
    {
        var ref = this;

        this.botonBorrar = new Ext.Button(
        {
            text : 'Esborrar',
            iconCls : 'application-delete',
            handler : function(boton, evento)
            {
                var registroSeleccionado = ref.gridAutorizados.getSelectionModel().getSelected();
                if (!registroSeleccionado)
                {
                    return false;
                }
                else
                {
                    Ext.Msg.confirm("Corfirmació de la operació",
                            "Realment dessitga esborrar aquest registre?", function(button, text)
                            {
                                if (button == "yes")
                                {
                                    ref.storeCursoAutorizados.remove(registroSeleccionado);
                                }
                            });
                }
            }
        });
    },

    buildGridAutorizados : function()
    {
        var ref = this;

        var colModel = new Ext.grid.ColumnModel(
        {
            defaults :
            {
                sortable : true
            },
            columns : [
            {
                header : "id",           
                hidden : true,
                dataIndex : 'id'
            },
            {
                header : 'Codi',
                width : 25,
                dataIndex : 'personaId'

            },
            {
                header : 'Nom',
                dataIndex : 'nombre'
            } ]
        });

        this.gridAutorizados = new Ext.grid.GridPanel(
        {
            frame : true,
            flex : 1,
            loadMask : true,
            tbar :
            {
                items : [ ref.botonAnadir, ref.botonBorrar ]
            },
            viewConfig :
            {
                forceFit : true
            },
            store : ref.storeCursoAutorizados,
            sm : new Ext.grid.RowSelectionModel(
            {
                singleSelect : true
            }),
            colModel : colModel
        });
    }
});