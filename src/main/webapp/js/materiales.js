Ext.ns('UJI.OCW');

UJI.OCW.Materiales = Ext.extend(Ext.Panel,
{
    title : 'Materials',
    layout : 'card',
    autoScroll: true,
    frame : true,
    closable : true,
    activeItem : 0,
    materialForm : {},
    materialGrid : {},

    initComponent : function()
    {
        var config = {};

        Ext.apply(this, Ext.apply(this.initialConfig, config));
        UJI.OCW.Materiales.superclass.initComponent.call(this);

        this.initUI();
        this.add(this.materialGrid);
        this.add(this.materialForm);
    },

    initUI : function()
    {
        this.buildMaterialGrid();
        this.buildMaterialForm();
    },

    buildMaterialGrid : function()
    {
        this.materialGrid = new UJI.OCW.MaterialGrid();
    },

    buildMaterialForm : function()
    {
        this.materialForm = new UJI.OCW.MaterialForm();
    },

    reloadGrid : function()
    {
        this.materialGrid.reload();
    },
    listeners :
    {
        'materialChangeCard' : function(card, action)
        {
            if (card == "materialForm")
            {
                this.getLayout().setActiveItem(0);
            }
            else
            {
                this.getLayout().setActiveItem(1);
                if (action == "edit")
                {
                    var record = this.materialGrid.getMaterialSeleccionado();
                    if (record)
                    {
                        this.materialForm.loadMaterial(record.get('id'));
                    }
                } else {
                    this.materialForm.cleanForm();
                }
            }
        }
    }

});