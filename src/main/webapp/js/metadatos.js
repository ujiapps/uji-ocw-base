Ext.ns('UJI.OCW');

UJI.OCW.Metadatos = Ext.extend(Ext.Panel,
{
    title : 'Metadades',
    closable : true,
    layout : 'vbox',
    layoutConfig :
    {
        align : 'stretch'
    },

    storeMetadatos : {},
    gridMetadatos : {},
    storeValores : {},
    gridValores : {},
    botonInsert : {},
    botonDelete : {},
    editor : {},
    botonInsertValor : {},
    botonDeleteValor : {},
    editorValor : {},
    metadatoId : 0,
    storeIdiomasEdicion : {},
    initComponent : function()
    {
        var config = {};
        Ext.apply(this, Ext.apply(this.initialConfig, config));

        UJI.OCW.Metadatos.superclass.initComponent.call(this);

        this.storeIdiomasEdicion = this.getStoreIdiomasEdicion();
        this.storeMetadatos = this.getStoreMetadatos();
        this.editor = this.getEditor();
        this.botonInsert = this.getBotonInsert();
        this.botonDelete = this.getBotonDelete();
        this.gridMetadatos = this.getGridMetadatos();

        this.storeValores = this.getStoreValores();
        this.editorValor = this.getEditorValor();
        this.botonInsertValor = this.getBotonInsertValor();
        this.botonDeleteValor = this.getBotonDeleteValor();
        this.gridValores = this.getGridValores();
        this.gridValores.disable();

        this.add(this.gridMetadatos, this.gridValores);

    },
    getStoreMetadatos : function()
    {
        var ref = this;
        var store = new Ext.data.Store(
        {
            restful : true,
            url : '/ocw/rest/metadato',
            autoLoad : true,
            reader : new Ext.data.XmlReader(
            {
                record : 'Metadato',
                id : 'id'
            }, buildMultilangStoreColumns('id', multilang('nombre'),
            {
                name : 'limitado',
                convert : function(value, record)
                {
                    return (value == 'S') ? true : false;
                }
            },
            {
                name : 'obligatorio',
                convert : function(value, record)
                {
                    return (value == 'S') ? true : false;
                }
            }, 'tag')),
            writer : new Ext.data.XmlWriter(
            {
                record : 'Metadato',
                id : 'id',
                writeAllFields : true
            }, buildMultilangStoreColumns('id', multilang('nombre'), 'limitado', 'obligatorio', 'tag'))

        });

        return store;
    },
    getEditor : function()
    {
        var ref = this;

        var editor = new Ext.ux.uji.grid.RowEditor(
        {
            saveText : 'Actualitzar',
            cancelText : 'Cancelar',
            errorSummary : false,
            listeners :
            {
                canceledit : function(rowEditor)
                {
                    var record = rowEditor.record;

                    if (record.phantom)
                    {
                        ref.storeMetadatos.remove(rowEditor.record);
                    }
                }
            }
        });
        return editor;
    },

    getGridMetadatos : function()
    {
        var ref = this;
        var userColumns = [
        {
            header : 'Id',
            sortable : true,
            dataIndex : 'id',
            width : 35
        },
        {
            header : 'Nom',
            sortable : true,
            width : 120,
            dataIndex : 'nombre__' + idiomas[0],
            editor :
            {
                xtype : 'multilanguagetextfield',
                name : 'nombre',
                language : window.idiomas,
                roweditor : ref.editor,
                allowBlank : false,
                blankText : 'El camp "Nom" es requerit'
            }
        },
        {
            header : 'Llimitat',
            dataIndex : 'limitado',
            width : 20,
            editor : new Ext.form.Checkbox({}),
            renderer : function(value, metaData, record, rowIndex, colIndex, store)
            {
                if (value)
                {
                    return 'Sí';
                }
                else
                {
                    return 'No';
                }
            }

        },
        {
            header : 'Obligatori',
            dataIndex : 'obligatorio',
            width : 20,
            editor : new Ext.form.Checkbox({}),
            renderer : function(value, metaData, record, rowIndex, colIndex, store)
            {
                if (value)
                {
                    return 'Sí';
                }
                else
                {
                    return 'No';
                }
            }
        },
        {
            header : 'Tag',
            sortable : true,
            dataIndex : 'tag',
            editor : new Ext.form.TextField(
            {
                allowBlank : false,
                blankText : 'El camp "Tag" es requerit'
            })
        } ];

        var grid = new Ext.grid.GridPanel(
        {

            frame : true,
            flex : 1,
            loadMask : true,
            viewConfig :
            {
                forceFit : true
            },
            height : 300,
            store : this.storeMetadatos,
            sm : new Ext.grid.RowSelectionModel(
            {
                singleSelect : true
            }),
            plugins : [ this.editor ],
            columns : userColumns,
            tbar :
            {
                items : [ this.botonInsert, '-', this.botonDelete ]
            },
            listeners :
            {
                rowclick : function(grid, rowIndex, evt)
                {
                    if (grid.selModel.selections.keys.length > 0)
                    {
                        var id = grid.selModel.getSelected().id;
                        var limitado = grid.selModel.getSelected().get('limitado');

                        if (!grid.selModel.getSelected().dirty)
                        {
                            ref.botonDelete.enable();

                            if (limitado)
                            {
                                ref.gridValores.enable();
                                ref.setMetadatoActivo(id);
                            }
                        }
                        else
                        {
                            ref.botonDelete.disable();
                            ref.storeMetadatos.reload();
                        }

                        if (grid.selModel.getSelected().dirty || !limitado)
                        {
                            ref.gridValores.disable();
                            ref.setMetadatoActivo(0);
                        }
                    }

                    ref.botonDeleteValor.disable();
                }
            }
        });

        return grid;
    },
    getBotonDelete : function()
    {
        var ref = this;

        var botonDelete = new Ext.Button(
        {
            text : 'Esborrar',
            iconCls : 'application-delete',
            disabled : true,
            handler : function(btn, evt)
            {
                var rec = ref.gridMetadatos.getSelectionModel().getSelected();

                if (!rec)
                {
                    return false;
                }

                Ext.Msg.confirm("Esborrar Llicència", "Desitja esborrar la metadata?", function(btn)
                {
                    if (btn == 'yes')
                    {
                        ref.storeMetadatos.remove(rec);
                        ref.botonDelete.disable();

                        ref.setMetadatoActivo(0);
                        ref.gridValores.disable();
                    }
                });
            }
        });

        return botonDelete;
    },

    getBotonInsert : function()
    {
        var ref = this;

        var botonInsert = new Ext.Button(
        {
            text : 'Afegir',
            iconCls : 'application-add',
            handler : function(btn, evt)
            {
                var rec = new ref.storeMetadatos.recordType(
                {
                    id : ''
                });

                ref.editor.stopEditing();
                ref.storeMetadatos.insert(0, rec);
                ref.editor.startEditing(0);
            }
        });

        return botonInsert;
    },

    getStoreValores : function()
    {
        var ref = this;

        var store = new Ext.data.Store(
        {
            restful : true,
            url : '/ocw/rest/metadatoValor',
            baseParamas :
            {
                metadatoId : ref.metadatoId
            },
            autoload : true,
            reader : new Ext.data.XmlReader(
            {
                record : 'MetadatoValor',
                id : 'id'
            }, [ 'id', 'metadatoId', 'valor', 'orden' ]),
            writer : new Ext.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            }),
            listeners :
            {
                'beforeload' : function(editor, index)
                {
                    this.setBaseParam('metadatoId', ref.metadatoId);
                }
            }
        });

        return store;
    },

    getEditorValor : function()
    {
        var editor = new Ext.ux.uji.grid.RowEditor(
        {
            saveText : 'Actualitzar',
            cancelText : 'Cancelar',
            errorSummary : false
        });

        return editor;
    },

    getGridValores : function()
    {
        var ref = this;

        var columns = [
        {
            header : 'Valor',
            dataIndex : 'valor',
            sortable : 'true',
            editor : new Ext.form.TextField(
            {
                allowBlank : false,
                blankText : 'El camp "Valor" es requerit'
            })
        },
        {
            header : 'Ordre',
            sortable : true,
            dataIndex : 'orden',
            editor : new Ext.form.TextField(
            {
                vtype : 'orden',
                allowBlank : false,
                blankText : 'El camp "Ordre" es requerit'
            })
        } ];

        var grid = new Ext.grid.GridPanel(
        {
            frame : true,
            flex : 1,
            loadMask : true,
            viewConfig :
            {
                forceFit : true
            },
            store : this.storeValores,
            sm : new Ext.grid.RowSelectionModel(
            {
                singleSelect : true
            }),
            plugins : [ this.editorValor ],
            columns : columns,
            tbar :
            {
                items : [ this.botonInsertValor, '-', this.botonDeleteValor ]
            },
            listeners :
            {
                rowclick : function(grid, rowIndex, evt)
                {
                    if (grid.selModel.selections.keys.length > 0)
                    {

                        if (!grid.selModel.getSelected().dirty)
                        {
                            ref.botonDeleteValor.enable();
                        }
                        else
                        {
                            ref.botonDeleteValor.disable();
                        }
                    }
                }
            }
        });

        return grid;
    },

    getBotonDeleteValor : function()
    {
        var ref = this;

        var botonDelete = new Ext.Button(
        {
            text : 'Esborrar',
            iconCls : 'application-delete',
            disabled : true,
            handler : function(btn, evt)
            {
                var rec = ref.gridValores.getSelectionModel().getSelected();

                if (!rec)
                {
                    return false;
                }

                Ext.Msg.confirm("Esborrar Valor Metadata", "Desitja esborrar el valor de la metadata?", function(btn)
                {
                    if (btn == 'yes')
                    {
                        ref.storeValores.remove(rec);
                    }
                });

            }
        });

        return botonDelete;
    },

    getBotonInsertValor : function()
    {
        var ref = this;

        var botonInsert = new Ext.Button(
        {
            text : 'Afegir',
            iconCls : 'application-add',
            handler : function(btn, evt)
            {
                var rec = new ref.storeValores.recordType(
                {
                    id : '',
                    valor : '',
                    orden : '',
                    metadatoId : ref.metadatoId
                });

                ref.editorValor.stopEditing();
                ref.storeValores.insert(0, rec);
                ref.editorValor.startEditing(0);
            }
        });

        return botonInsert;
    },

    setMetadatoActivo : function(metadatoId)
    {
        this.metadatoId = metadatoId;
        this.storeValores.reload();
    },

    getStoreIdiomasEdicion : function()
    {
        var store = new Ext.data.Store(
        {
            restful : true,
            url : '/ocw/rest/idioma/edicion',
            autoLoad : true,
            reader : new Ext.data.XmlReader(
            {
                record : 'Idioma',
                id : 'id'
            }, [ 'id', 'nombre', 'acronimo', 'orden', 'edicion' ]),
            listeners :
            {
                load : function(store)
                {
                    // Cargamos los idiomas
                    var idiomasAux = new Array();
                    for ( var i = 0; i < store.getCount(); i++)
                    {
                        var idioma = store.getAt(i);
                        idiomasAux[i] = idioma.get("acronimo").toLowerCase();
                    }
                    idiomas = idiomasAux;
                }
            }
        });

        return store;
    }

});