Ext.ns('UJI.OCW');

UJI.OCW.Cursos = Ext.extend(Ext.Panel,
{
    title : 'Cursos',
    layout : 'card',
    frame : true,
    closable : true,
    activeItem : 0,
    cursoForm : {},
    cursoGrid : {},

    initComponent : function()
    {
        var config = {};

        Ext.apply(this, Ext.apply(this.initialConfig, config));
        UJI.OCW.Cursos.superclass.initComponent.call(this);
        this.initUI();

        this.add(this.cursoGrid);
        this.add(this.cursoForm);

    },

    initUI : function()
    {
        this.buildCursoGrid();
        this.buildCursoForm();
    },

    buildCursoGrid : function()
    {
        this.cursoGrid = new UJI.OCW.CursoGrid();
        this.cursoGrid.cardPanel = this;
    },

    buildCursoForm : function()
    {
        this.cursoForm = new UJI.OCW.CursoForm();
        this.cursoGrid.cardPanel = this;
    },

    reloadGrid : function()
    {
        this.cursoGrid.reload();
    },
    listeners :
    {
        'cursoChangeCard' : function(card, action, cursoId)
        {
            if (card == "cursoForm")
            {
                this.getLayout().setActiveItem(0);
            }
            else
            {
                this.getLayout().setActiveItem(1);

                if (action == "edit")
                {
                    if (!cursoId) {
                        var record = this.cursoGrid.getCursoSeleccionado();
                        cursoId = record.get("id");
                    }
                    
                    if (cursoId)
                    {
                        this.cursoForm.loadCurso(cursoId);
                    }
                }
            }
        }
    }
});
