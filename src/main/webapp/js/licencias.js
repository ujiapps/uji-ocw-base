Ext.ns('UJI.OCW');

UJI.OCW.Licencias = Ext.extend(Ext.Panel,
{
    title : 'Llicències',
    closable : true,
    layout : 'vbox',
    layoutConfig :
    {
        align : 'stretch'
    },

    storeLicencias : {},
    gridLicencias : {},
    selectionModel : {},
    botonInsert : {},
    botonEdit : {},
    botonDelete : {},
    botonGuardar : {},
    botonCerrar : {},
    botonDelImagen : {},
    ventanaEmergente : {},
    formLicencias : {},
    conImagen : false,
    urlImagen : '/ocw/rest/licencia/0/img',

    initComponent : function()
    {
        var config = {};
        Ext.apply(this, Ext.apply(this.initialConfig, config));

        UJI.OCW.Licencias.superclass.initComponent.call(this);

        this.storeLicencias = this.getStoreLicencias();
        this.ventanaEmergente = this.getVentanaEmergente();
        this.botonInsert = this.getBotonInsert();
        this.botonEdit = this.getBotonEdit();
        this.botonDelete = this.getBotonDelete();

        this.selectionModel = new Ext.grid.RowSelectionModel(
        {
            singleSelect : true
        });

        this.gridLicencias = this.getGridLicencias();

        this.add(this.gridLicencias);
    },

    getStoreLicencias : function()
    {
        var store = new Ext.data.Store(
        {
            restful : true,
            url : '/ocw/rest/licencia',
            autoLoad : true,
            reader : new Ext.data.XmlReader(
            {
                record : 'Licencia',
                id : 'id'
            }, buildMultilangStoreColumns('id', 'codigo', multilang('titulo'), 'imagen', multilang('url'))),
            writer : new Ext.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            }),
            listeners :
            {
                save : function(store, batch, data)
                {
                    if (data.destroy !== null)
                    {
                        this.reload();
                    }
                }
            }
        });

        return store;
    },

    getVentanaEmergente : function()
    {
        this.botonGuardar = this.getBotonGuardar();
        this.botonCerrar = this.getBotonCerrar();
        this.formLicencias = this.getFormLicencias();
        this.formLicencias.addButton(this.botonGuardar);
        this.formLicencias.addButton(this.botonCerrar);

        var ventanaEmergente = new Ext.Window(
        {
            title : 'Llicència',
            layout : 'fit',
            modal : true,
            width : 510,
            height : 300,
            closable : true,
            items : new Ext.Panel(
            {
                layout : 'vbox',
                layoutConfig :
                {
                    align : 'stretch'
                },
                items : [ this.formLicencias ]
            })
        });

        return ventanaEmergente;
    },

    getGridLicencias : function()
    {

        var ref = this;

        var userColumns = [
        {
            header : 'Id',
            dataIndex : 'id',
            sortable : true,
            width : 50
        },
        {
            header : 'Codi',
            dataIndex : 'codigo'
        },
        {
            header : 'Títol',
            dataIndex : 'titulo__' + idiomas[0]
        },
        {
            header : 'Url',
            dataIndex : 'url__' + idiomas[0]
        },
        {
            header : 'Imatge',
            dataIndex : 'imagen'
        } ];

        var grid = new Ext.grid.GridPanel(
        {
            frame : true,
            flex : 1,
            loadMask : true,
            viewConfig :
            {
                forceFit : true
            },
            store : this.storeLicencias,
            sm : new Ext.grid.RowSelectionModel(
            {
                singleSelect : true
            }),
            columns : userColumns,
            tbar :
            {
                items : [ this.botonInsert, '-', this.botonEdit, '-', this.botonDelete ]
            },
            listeners :
            {
                rowclick : function(grid, rowIndex, evt)
                {
                    if (grid.selModel.selections.keys.length > 0)
                    {
                        if (!grid.selModel.getSelected().dirty)
                        {
                            ref.botonEdit.enable();
                            ref.botonDelete.enable();
                        }
                    }
                },
                rowdblclick : function(grid, rowIndex, event)
                {
                    ref.abreVentanaEmergente();
                }
            }
        });

        return grid;
    },

    getBotonDelete : function()
    {
        var ref = this;

        var botonDelete = new Ext.Button(
        {
            text : 'Esborrar',
            iconCls : 'application-delete',
            disabled : true,
            handler : function(btn, evt)
            {
                var rec = ref.gridLicencias.getSelectionModel().getSelected();

                if (!rec)
                {
                    return false;
                }

                Ext.Msg.confirm("Esborrar Llicència", "Desitja esborrar la llicència?", function(btn)
                {
                    if (btn == 'yes')
                    {
                        ref.storeLicencias.remove(rec);
                    }
                });

            }
        });

        return botonDelete;
    },

    getBotonInsert : function()
    {
        var ref = this;

        var botonInsert = new Ext.Button(
        {
            text : 'Afegir',
            iconCls : 'application-add',
            listeners :
            {
                click : function(btn, evt)
                {
                    ref.ventanaEmergente = ref.getVentanaEmergente();

                    ref.botonGuardar.enable();

                    ref.formLicencias.getForm().setValues(
                    {
                        id : '',
                        codigo : ''
                    });

                    ref.ventanaEmergente.show();
                }
            }
        });

        this.conImagen = false;

        return botonInsert;
    },

    getBotonEdit : function()
    {
        var ref = this;

        var botonEdit = new Ext.Button(
        {
            text : 'Editar',
            iconCls : 'application-edit',
            disabled : true,
            handler : function()
            {
                if (ref.gridLicencias.getSelectionModel().getSelections()[0])
                {
                    ref.abreVentanaEmergente();

                }
                else
                {
                    Ext.Msg.show(
                    {
                        title : "Triar llicència",
                        icon : Ext.MessageBox.WARNING,
                        msg : "Deus triar una llicència"
                    });
                    return false;
                }
            }
        });

        return botonEdit;
    },

    getBotonCerrar : function()
    {
        var ref = this;

        var boton = new Ext.Button(
        {
            text : 'Tancar',
            name : 'cerrar',
            width : '50',
            height : '20',
            pressed : false,
            listeners :
            {
                click : function(btn, evt)
                {
                    ref.ventanaEmergente.close();
                }
            }
        });

        return boton;
    },

    getBotonGuardar : function()
    {
        var ref = this;

        var boton = new Ext.Button(
        {
            text : 'Guardar',
            name : 'guardar',
            width : '50',
            height : '20',
            pressed : false,
            disabled : true,
            listeners :
            {
                click : function(btn, evt)
                {
                    if (ref.formLicencias.getForm().isValid())
                    {
                        if ((ref.formLicencias.getForm().getValues().id))
                        {
                            ref.formLicencias.getForm().submit(
                            {
                                method : 'PUT',
                                url : '/ocw/rest/licencia/' + ref.formLicencias.getForm().getValues().id,
                                success : function(form, action)
                                {
                                    ref.gridLicencias.store.proxy.conn.url = '/' + 'ocw/rest/licencia';
                                    ref.gridLicencias.store.reload();
                                    ref.ventanaEmergente.close();
                                },
                                failure : function(form, action)
                                {
                                    if (action.response.responseXML)
                                    {
                                        var msgList = action.response.responseXML.getElementsByTagName("msg");

                                        if (msgList && msgList[0] && msgList[0].firstChild)
                                        {
                                            Ext.Msg.alert("Error", msgList[0].firstChild.nodeValue);
                                        }
                                    }
                                    else
                                    {
                                        Ext.Msg.alert('Error', 'Error en l\'actualització');
                                    }
                                }
                            });
                        }
                        else
                        {
                            ref.formLicencias.getForm().submit(
                            {
                                method : 'POST',
                                url : '/ocw/rest/licencia',
                                success : function(form, action)
                                {
                                    ref.gridLicencias.store.proxy.conn.url = '/ocw/rest/licencia';
                                    ref.gridLicencias.store.reload();
                                    ref.ventanaEmergente.close();
                                },
                                failure : function(form, action)
                                {
                                    if (action.response.responseXML)
                                    {
                                        var msgList = action.response.responseXML.getElementsByTagName("msg");

                                        if (msgList && msgList[0] && msgList[0].firstChild)
                                        {
                                            Ext.Msg.alert("Error", msgList[0].firstChild.nodeValue);
                                        }
                                    }
                                    else
                                    {
                                        Ext.Msg.alert('Error', 'Error en la inserció');
                                    }
                                }
                            });
                        }
                    }
                    else
                    {
                        Ext.Msg.alert("Error", "Revise els valors del camps.");
                    }
                }
            }
        });

        return boton;
    },

    getBotonDelImagen : function()
    {
        var ref = this;

        var boton = new Ext.Button(
        {
            text : 'Esborrar imatge',
            name : 'del_img',
            iconCls : 'cancel',
            height : '20',
            pressed : false,
            disabled : true,
            hidden : true,
            listeners :
            {
                click : function(btn, evt)
                {
                    Ext.Ajax.request(
                    {
                        url : '/ocw/rest/licencia/' + ref.formLicencias.getForm().getValues().id + '/img',
                        method : 'DELETE',
                        success : function()
                        {
                            ref.conImagen = false;
                            ref.botonDelImagen.hide();
                            ref.botonDelImagen.disable();
                            ref.formLicencias.getForm().findField('imagen').setValue('');

                            Ext.getDom('imagen-pic').src = ref.urlImagen;
                            ref.gridLicencias.store.reload();
                        }
                    });
                }
            }
        });

        return boton;
    },

    getFormLicencias : function()
    {
        this.botonDelImagen = this.getBotonDelImagen();

        var ref = this;

        var formLicencias = new Ext.FormPanel(
        {
            fileUpload : true,
            autoScroll : true,
            frame : true,
            flex : 1,
            name : 'licencias',
            url : '/ocw/rest/licencia',
            reader : new Ext.data.XmlReader(
            {
                record : 'Licencia',
                id : 'id'
            }, buildMultilangStoreColumns('id', 'codigo', multilang('titulo'), 'imagen', multilang('url'))),
            writer : new Ext.data.XmlWriter(
            {
                record : 'Licencia',
                writeAllFields : true,
                id : 'id'
            }, buildMultilangStoreColumns('id', 'codigo', multilang('titulo'), 'imagen', multilang('url'))),
            errorReader : new Ext.data.XmlReader(
            {
                record : 'responseMessage',
                success : 'success'
            }, [ 'success', 'msg' ]),

            items : [
            {
                xtype : 'hidden',
                name : 'id'
            },
            {
                xtype : 'textfield',
                fieldLabel : 'Codi',
                name : 'codigo',
                width : 300,
                allowBlank : false,
                value : '',
                blankText : 'El camp "Codi" es requerit'
            },
            {
                xtype : 'multilanguagetextfield',
                fieldLabel : 'Títol',
                width : 350,
                name : 'titulo',
                language : idiomas
            },
            {
                xtype : 'multilanguagetextfield',
                fieldLabel : 'Url',
                width : 350,
                name : 'url',
                language : idiomas
            },
            {
                xtype : 'textfield',
                fieldLabel : 'Imatge:',
                labelSeparator : '',
                name : 'imagen',
                inputType : 'file'
            },
            {
                xtype : 'box',
                autoEl :
                {
                    tag : 'div',
                    html : '<img id="imagen-pic" src="' + ref.urlImagen + '" style="border : solid 1px #BDBDBD; max-height: 50px; max-width: 200px;"  />'
                },
                style :
                {
                    height : '60px',
                    width : '300px',
                    marginTop : '15px'
                }
            }, ref.botonDelImagen ],
            buttonAlign : 'right',
            style :
            {
                overflow : 'auto'
            }
        });

        return formLicencias;
    },

    abreVentanaEmergente : function()
    {
        this.ventanaEmergente = this.getVentanaEmergente();
        this.ventanaEmergente.show();
        this.cargaLicencias();
    },

    cargaLicencias : function()
    {
        var ref = this;

        var id = this.gridLicencias.selModel.getSelected().id;

        if (!this.gridLicencias.selModel.getSelected().dirty)
        {
            var f = this.formLicencias.getForm();
            f.load(
            {
                url : '/ocw/rest/licencia/' + id,
                method : 'get',
                success : function(form, action)
                {
                    ref.botonGuardar.enable();
                    Ext.getDom('imagen-pic').src = '/ocw/rest/licencia/' + action.result.data.id + '/img?' + (new Date()).getTime();

                    // Comprobamos si hay que hay añadir el botón de eliminar imagen

                    if (ref.gridLicencias.getSelectionModel().getSelected().data.imagen == "Si")
                    {
                        ref.conImagen = true;
                        ref.botonDelImagen.show();
                        ref.botonDelImagen.enable();
                    }
                },
                failure : function(form, action)
                {
                    Ext.Msg.alert("Error", "Error al carregar les dades.");
                }
            });
        }

    },

    licenciaConImagen : function(licenciaId)
    {
        var ref = this;

        Ext.Ajax.request(
        {
            url : '/ocw/rest/licencia/' + licenciaId + '/hasImg',
            method : 'GET',
            success : function(response, options)
            {
                var res = response.responseText;
                if (res == 'Si')
                {
                    ref.conImagen = true;
                    ref.botonDelImagen.show();
                    ref.botonDelImagen.enable();
                }
                else
                {
                    ref.conImagen = false;
                    ref.botonDelImagen.hide();
                    ref.botonDelImagen.disable();
                }
            },
            error : function()
            {
                ref.conImagen = false;
                ref.botonDelImagen.hide();
                ref.botonDelImagen.disable();
            }
        });
    }
});