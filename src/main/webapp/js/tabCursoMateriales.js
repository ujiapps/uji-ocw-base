Ext.ns('UJI.OCW');

UJI.OCW.tabCursoMateriales = Ext.extend(Ext.Panel,
{
    title : 'Materials',
    layout : 'border',
    cursoId : null,
    layoutConfig :
    {
        align : 'stretch'
    },
    treeGruposMateriales : null,
    storeCursoMateriales : null,
    storeGrupos : null,
    botonAnadirMaterial : null,
    botonBorrar : null,
    botonAnadirGrupo : null,
    formEdicion : null,
    edicionNombre : null,
    edicionDescripcion : null,
    nombreGrupo : null,
    ventanaGrupo : null,
    formGrupo : null,
    botonCancelar : null,
    botonGuardarGrupo : null,
    botonGuardarEdicion : null,

    initComponent : function()
    {
        var config = {};
        Ext.apply(this, Ext.apply(this.initialConfig, config));
        UJI.OCW.tabCursoMateriales.superclass.initComponent.call(this);
        this.initUI();
        this.add(this.treeGruposMateriales);
        this.add(this.formEdicion);
    },

    initUI : function()
    {
        this.buildEdicionNombre();
        this.buildEdicionDescripcion();
        this.buildStoreCursoMateriales();
        this.buildStoreGrupos();
        this.buildNombreGrupo();
        this.buildBotonCancelar();
        this.buildBotonGuardarGrupo();
        this.buildBotonGuardarEdicion();
        this.buildFormEdicion();
        this.buildFormGrupo();
        this.buildVentanaGrupo();
        this.buildBotonAnadirMaterial();
        this.buildBotonBorrar();
        this.buildBotonAnadirGrupo();
        this.buildTreeGruposMateriales();
        this.buildEnlazaStoreCursoMaterialesConTreePanel();
        this.buildEnlazaStoreGruposConTreePanel();
    },

    buildTreeGruposMateriales : function()
    {
        var ref = this;

        this.treeGruposMateriales = new Ext.tree.TreePanel(
        {
            title : 'Agrupació i ordre de materials del curs',
            collapsible : false,
            region : 'west',
            margins : '2 2 0 2',
            autoScroll : true,
            rootVisible : true,
            draggable : true,
            enableDD : true,
            width : 420,
            split : true,
            collapsible : false,
            tbar : [ this.botonAnadirGrupo, this.botonAnadirMaterial, this.botonBorrar ],
            root : new Ext.tree.AsyncTreeNode(
            {
                'text' : "Materials d'aquest curs"
            }),
            frame : false,
            style : 'background-color: #fff;',
            loader : new Ext.ux.tree.XmlTreeLoader(
            {
                baseParams :
                {
                    cursoId : ref.cursoId
                },
                dataUrl : '/ocw/rest/cursoMaterial',
                processAttributes : function(attr)
                {
                    attr.loaded = true;
                    attr.expanded = true;
                    if (attr.leaf === "false")
                    {
                        attr.icon = "http://devel.uji.es/resources/js/ext-3.3.1/resources/images/default/tree/folder-open.gif";
                        attr.leaf = false;
                    }
                    else
                    {
                        attr.leaf = true;
                        var tipo = attr.tipo;
                        if (tipo)
                        {
                            var tipoIcono = UJI.ContentType.getIconoPorContentType(tipo);
                            attr.icon = tipoIcono.icono;
                        }
                    }
                }
            }),

            reload : function()
            {
                ref.treeGruposMateriales.getLoader().baseParams =
                {
                    cursoId : ref.cursoId
                };
                ref.treeGruposMateriales.getRootNode().reload();
            },

            listeners :
            {
                click : function(node, event)
                {
                    if (node.isRoot === true)
                    {
                        ref.formEdicion.setDisabled(true);
                        ref.edicionDescripcion.setValue("");
                        ref.edicionNombre.setValue("");

                    }
                    else
                    {
                        ref.formEdicion.setDisabled(false);
                        ref.edicionDescripcion.setValue(node.attributes.descripcion);
                        ref.edicionNombre.setValue(node.attributes.title);
                        if (node.leaf === true)
                        {
                            ref.edicionNombre.setDisabled(true);
                        }
                        else
                        {
                            ref.edicionNombre.setDisabled(false);
                        }
                    }
                },
                beforeappend : function(tree, parent, node)
                {
                    if (parent.isRoot !== true && node.leaf === false)
                    {
                        Ext.Msg.alert("Error", "Un grupo de materials no pot anidar-se dins d'un altre grup.");
                        return false;

                    }
                    else
                    {
                        return true;
                    }
                },

                dragdrop : function(panel, node, dragdrop, event)
                {
                    if (node && node.leaf === false)
                    {
                        var grupoId = node.id;
                        var anteriorId = (node.previousSibling != null) ? node.previousSibling.id : -1;
                        var tipoAnterior = "GRUPO";
                        if (node.previousSibling && node.previousSibling.leaf === true)
                        {
                            tipoAnterior = "MAT";
                        }
                        Ext.Ajax.request(
                        {
                            url : '/ocw/rest/grupo/mover/' + grupoId,
                            method : 'PUT',
                            params :
                            {
                                anteriorId : anteriorId,
                                tipoAnterior : tipoAnterior
                            }
                        });
                    }
                    else
                    {
                        var nodoActual = node.id;
                        var nodoPadre = node.parentNode.id;
                        if (node.parentNode.isRoot === true)
                        {
                            nodoPadre = -1;
                        }

                        var anteriorId = (node.previousSibling != null) ? node.previousSibling.id : -1;
                        var tipoAnterior = "GRUPO";
                        if (node.previousSibling && node.previousSibling.leaf === true)
                        {
                            tipoAnterior = "MAT";
                        }

                        Ext.Ajax.request(
                        {
                            url : '/ocw/rest/cursoMaterial/mover/' + nodoActual,
                            method : 'PUT',
                            params :
                            {
                                grupoPadre : nodoPadre,
                                anteriorId : anteriorId,
                                tipoAnterior : tipoAnterior
                            }
                        });
                    }
                }
            }
        });
    },

    buildEnlazaStoreCursoMaterialesConTreePanel : function()
    {
        var ref = this;

        this.storeCursoMateriales.on("save", function(store, number, data)
        {
            if (data.create)
            {
                var record = data.create[0];
                var icon = "http://static.uji.es/js/extjs/ext-3.3.1/resources/images/default/tree/leaf.gif";
                var tipo = record.tipo;
                if (tipo)
                {
                    var tipoIcono = UJI.ContentType.getIconoPorContentType(tipo);
                    icon = tipoIcono.icono;
                }
                var node = new Ext.tree.TreeNode(
                {
                    id : record.id,
                    text : record.titulo,
                    title : record.titulo,
                    grupoId : record.grupoId,
                    icon : icon,
                    leaf : true,
                    orden : 10
                });

                node.setId(record.id);
                var parent = ref.treeGruposMateriales.root;

                if (node.attributes.grupoId !== "" && node.attributes.grupoId != -1)
                {
                    parent = parent.findChild("id", node.attributes.grupoId);
                }
                parent.appendChild(node);
            }
        });
    },

    buildEnlazaStoreGruposConTreePanel : function()
    {

        this.storeGrupos.on("update", function(store, record)
        {
            var node = ref.treeGruposMateriales.getRootNode().findChild("id", record.get("id"));

            if (!node)
            {
                var node = new Ext.tree.TreeNode(
                {
                    id : record.get("id"),
                    text : record.get("nombre"),
                    title : record.get("nombre"),
                    icon : "http://devel.uji.es/resources/js/ext-3.3.1/resources/images/default/tree/folder-open.gif",
                    leaf : false,
                    orden : 10
                });
                ref.treeGruposMateriales.root.appendChild(node);
            }
        });
    },

    buildStoreGrupos : function()
    {
        var ref = this;
        this.storeGrupos = new Ext.data.Store(
        {
            restful : true,
            url : '/ocw/rest/grupo',
            autoLoad : false,
            reader : new Ext.data.XmlReader(
            {
                record : 'Grupo',
                id : 'id'
            }, [ 'id', 'cursoId', 'orden', 'nombre', 'descripcion' ]),
            writer : new Ext.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            }),
            listeners :
            {
                beforeload : function(editor, index)
                {
                    this.baseParams =
                    {
                        cursoId : ref.cursoId
                    };
                }
            }
        });
    },

    buildStoreCursoMateriales : function()
    {

        var ref = this;
        this.storeCursoMateriales = new Ext.data.Store(
        {
            restful : true,
            proxy : new Ext.data.HttpProxy(
            {
                url : '/ocw/rest/curso/' + ref.cursoId + '/material'

            }),
            autoLoad : false,
            reader : new Ext.data.XmlReader(
            {
                record : 'CursoMaterial',
                id : 'id'
            }, [ 'id', 'titulo', 'tipo', 'materialId', 'cursoId', 'grupoId', 'descripcion', 'fecha', 'creador', 'orden' ]),
            writer : new Ext.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            }),
            listeners :
            {
                'beforeload' : function(editor, index)
                {
                    this.proxy.setUrl('/ocw/rest/curso/' + ref.cursoId + '/material');
                },
                'beforewrite' : function(editor, index)
                {
                    this.proxy.setUrl('/ocw/rest/curso/' + ref.cursoId + '/material');
                }
            }

        });
    },

    buildNombreGrupo : function()
    {

        this.nombreGrupo = new Ext.form.TextField(
        {
            width : 200,
            xtype : 'textfield',
            fieldLabel : 'Nombre',
            allowBlank : false,
            name : 'Valor',
            listeners :
            {
                specialkey : function(f, e)
                {
                    if (e.getKey() == e.ENTER)
                    {
                        ref.botonGuardarGrupo.handler.call(ref.botonGuardarGrupo.scope);
                    }
                }
            }
        });
    },

    buildFormGrupo : function()
    {

        var ref = this;
        this.formGrupo = new Ext.FormPanel(
        {
            url : '/ocw/rest/grupo/',
            method : 'GET',
            waitMsg : 'Carregant ...',

            layout : "form",
            labelWidth : 60,
            frame : true,
            bodyStyle : 'padding:5px 5px 0',
            defaultType : 'textfield',
            plain : true,

            items : [ ref.nombreGrupo ]
        });

    },

    buildBotonGuardarGrupo : function()
    {

        var ref = this;
        this.botonGuardarGrupo = new Ext.Button(
        {
            text : 'Guardar',
            handler : function(btn, evt)
            {
                var form = ref.formGrupo.getForm();

                if (form.isValid())
                {
                    recordGrupo = Ext.data.Record.create([
                    {
                        name : "id",
                        type : 'integer'
                    },
                    {
                        name : "descripcion",
                        type : "string"
                    },
                    {
                        name : "cursoId",
                        type : 'integer'
                    },
                    {
                        name : "orden",
                        type : 'integer'
                    },
                    {
                        name : "nombre",
                        type : 'string'
                    } ]);

                    var rec = new recordGrupo(
                    {
                        id : '',
                        cursoId : ref.cursoId,
                        nombre : ref.nombreGrupo.getValue(),
                        descripcion : '',
                        orden : 10
                    });
                    ref.storeGrupos.add(rec);
                }
                ref.ventanaGrupo.hide();
            }
        });
    },

    buildBotonGuardarEdicion : function()
    {

        var ref = this;
        this.botonGuardarEdicion = new Ext.Button(
        {
            text : 'Guardar',
            iconCls : 'application-add',
            handler : function(btn, evt)
            {
                var form = ref.formEdicion.getForm();

                if (form.isValid())
                {
                    var node = ref.treeGruposMateriales.getSelectionModel().getSelectedNode();
                    if (node.leaf === true)
                    {
                        var record = ref.storeCursoMateriales.getById(node.attributes.id);
                        record.set("descripcion", ref.edicionDescripcion.getValue());
                        node.attributes.descripcion = record.get("descripcion");
                    }
                    else
                    {
                        var record = ref.storeGrupos.getById(node.attributes.id);
                        record.set("nombre", ref.edicionNombre.getValue());
                        record.set("descripcion", ref.edicionDescripcion.getValue());

                        node.setText(record.get("nombre"));
                        node.attributes.title = record.get("nombre");
                        node.attributes.descripcion = record.get("descripcion");

                    }
                }
            }
        });
    },

    buildBotonCancelar : function()
    {

        var ref = this;

        this.botonCancelar = new Ext.Button(
        {
            text : 'Cancelar',
            handler : function(btn, evt)
            {
                ref.ventanaGrupo.hide();
            }
        });

    },

    buildVentanaGrupo : function()
    {
        var ref = this;
        this.ventanaGrupo = new Ext.Window(
        {
            title : 'Afegir un grup',
            layout : 'fit',
            modoEdicionVentana : false,
            modal : true,
            record : false,
            width : 340,
            height : 120,
            closeAction : 'hide',
            closable : true,
            items : [ ref.formGrupo ],
            buttons : [ ref.botonGuardarGrupo, ref.botonCancelar ]
        });

    },

    buildBotonAnadirGrupo : function()
    {
        var ref = this;

        this.botonAnadirGrupo = new Ext.Button(
        {
            text : 'Afegir grup',
            iconCls : 'application-add',
            handler : function(button)
            {
                ref.editionMode = false;
                ref.nombreGrupo.setValue('');
                ref.ventanaGrupo.show();
                ref.nombreGrupo.focus(false, 200);
            }
        });
    },

    buildBotonBorrar : function()
    {
        var ref = this;

        this.botonBorrar = new Ext.Button(
        {
            text : 'Esborrar',
            iconCls : 'application-delete',
            handler : function(boton, evento)
            {
                var node = ref.treeGruposMateriales.getSelectionModel().getSelectedNode();
                if (node && node.isRoot == true)
                {
                    Ext.Msg.alert("Error", "No es pot esborrar el grup arrel.");
                }
                else if (node && (node.leaf === true || node.childNodes.length === 0))
                {
                    if (node.leaf === false)
                    {
                        Ext.Ajax.request(
                        {
                            url : '/ocw/rest/grupo/' + node.attributes.id,
                            method : 'DELETE',
                            success : function()
                            {
                                node.remove(true);
                                ref.formEdicion.setDisabled(true);
                                ref.edicionDescripcion.setValue("");
                                ref.edicionNombre.setValue("");
                            }
                        });

                    }
                    else
                    {
                        Ext.Ajax.request(
                        {
                            url : '/ocw/rest/cursoMaterial/' + node.attributes.id,
                            method : 'DELETE',
                            success : function()
                            {
                                node.remove(true);
                                ref.formEdicion.setDisabled(true);
                                ref.edicionDescripcion.setValue("");
                                ref.edicionNombre.setValue("");
                            }
                        });
                    }
                }
                else
                {
                    Ext.Msg.alert("Error", "Per esborrar un grup tens d'esborrar els seus materials primer.");

                }
            }
        });
    },

    buildBotonAnadirMaterial : function()
    {
        var ref = this;

        this.addEvents('LookoupWindowClickSeleccion');
        var window = new Ext.ux.uji.form.LookupWindow(
        {
            appPrefix : 'ocw',
            bean : 'material'
        });

        window.on('LookoupWindowClickSeleccion', function(id, nombre)
        {
            var grupoId = -1;
            var nodoSeleccionado = ref.treeGruposMateriales.getSelectionModel().getSelectedNode();
            if (nodoSeleccionado && nodoSeleccionado.leaf === false && nodoSeleccionado.isRoot !== true)
            {
                grupoId = nodoSeleccionado.attributes.id;
            }
            var CursoMaterialesRecord = Ext.data.Record.create([
            {
                name : "id",
                type : "integer"
            },
            {
                name : "titulo",
                type : "string"
            },
            {
                name : "tipo",
                type : "string"
            },
            {
                name : 'cursoId',
                type : 'integer'
            },
            {
                name : 'materialId',
                type : 'integer'
            },
            {
                name : 'grupoId',
                type : 'integer'
            },
            {
                name : "descripcion",
                type : "string"
            },
            {
                name : "fecha",
                type : "string"
            },
            {
                name : "creador",
                type : "string"
            },
            {
                name : 'orden',
                type : 'integer'
            }, ]);

            var rec = new CursoMaterialesRecord(
            {
                id : '',
                titulo : '',
                cursoId : ref.cursoId,
                grupoId : grupoId,
                materialId : id,
                descripcion : '',
                fecha : '',
                creador : ''
            });

            ref.storeCursoMateriales.add(rec);
        });

        this.botonAnadirMaterial = new Ext.Button(
        {
            text : 'Afegir material',
            iconCls : 'application-add',
            handler : function(button)
            {
                window.show();
            }
        });
    },

    buildEdicionNombre : function()
    {

        this.edicionNombre = new Ext.form.TextField(
        {
            width : 200,
            xtype : 'textfield',
            fieldLabel : 'Nombre',
            allowBlank : false,
            name : 'Valor'
        });
    },

    buildEdicionDescripcion : function()
    {

        this.edicionDescripcion = new Ext.form.TextArea(
        {
            width : 200,
            xtype : 'textfield',
            fieldLabel : 'Descripción',
            allowBlank : true,
            name : 'Valor'
        });
    },

    buildFormEdicion : function()
    {
        ref = this;

        this.formEdicion = new Ext.FormPanel(
        {
            name : 'edicion',
            title : 'Edició de dades',
            disabled : true,
            padding : '20px 0px',
            margins : '2 2 0 0',
            frame : true,
            region : 'center',
            tbar : [ this.botonGuardarEdicion ],
            items : [ this.edicionNombre, this.edicionDescripcion ]
        });
    },

    setCursoActivo : function(cursoId)
    {
        this.cursoId = cursoId;
        this.storeCursoMateriales.reload();
        this.storeGrupos.reload();
        this.treeGruposMateriales.reload();
        this.formEdicion.setDisabled(true);
        this.edicionDescripcion.setValue("");
        this.edicionNombre.setValue("");
    }
});