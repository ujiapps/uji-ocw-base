function initOCWVtypes() {
	
	Ext.apply(Ext.form.VTypes, {
	    orden: function(val, field) {
	        return (/^[1-9][0-9]*$/i).test(val);
	    },
	    ordenText: 'Valor per a ordre incorrecte, sols nombres que no comencen per 0'
	});
	
}