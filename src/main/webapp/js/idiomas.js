Ext.ns('UJI.OCW');

UJI.OCW.Idiomas = Ext.extend(Ext.Panel,
{
    title : 'Idiomes',
    closable : true,
    layout : 'vbox',
    layoutConfig :
    {
        align : 'stretch'
    },

    storeIdiomas : {},
    gridIdiomas : {},
    initComponent : function()
    {
        var config = {};
        Ext.apply(this, Ext.apply(this.initialConfig, config));

        UJI.OCW.Idiomas.superclass.initComponent.call(this);

        this.storeIdiomas = this.getStoreIdiomas();
        this.gridIdiomas = this.getGridIdiomas();

        this.add(this.gridIdiomas);

    },

    getStoreIdiomas : function()
    {
        var store = new Ext.data.Store(
        {
            restful : true,
            url : '/ocw/rest/idioma',
            autoLoad : true,
            reader : new Ext.data.XmlReader(
            {
                record : 'Idioma',
                id : 'id'
            }, [ 'id', 'nombre', 'acronimo', 'orden', 'edicion' ]),
            writer : new Ext.data.XmlWriter(
            {
                record : 'Idioma',
                id : 'id',
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            })
        });

        return store;
    },

    getGridIdiomas : function()
    {

        var userColumns = [
        {
            header : "id",
            width : 25,
            sortable : true,
            hidden : true,
            dataIndex : 'id'
        },
        {
            header : 'Nom',
            dataIndex : 'nombre',
            editor : new Ext.form.TextField(
            {
                allowBlank : false,
                blankText : 'El camp "Nom" es requerit'
            })

        },
        {
            header : 'Acrònim',
            dataIndex : 'acronimo',
            editor : new Ext.form.TextField(
            {
                allowBlank : false,
                blankText : 'El camp "Acrònim" es requerit'
            })

        },
        {
            header : 'Ordre',
            dataIndex : 'orden',
            editor : new Ext.form.TextField(
            {
                allowBlank : false,
                vtype : 'orden',
                blankText : 'El camp "Ordre" es requerit'
            })
        },
        {
            header : 'Edició',
            dataIndex : 'edicion',
            renderer : function(value, metaData, record, rowIndex, colIndex, store)
            {
                if (value == 'S')
                {
                    return 'Sí';
                }
                else
                {
                    return 'No';
                }
            },
            editor : new Ext.form.Checkbox(
            {
                allowBlank : false,
                blankText : 'El camp "Edició" es requerit',
                inputValue : 'S'

            })
        } ];

        var grid = new Ext.ux.uji.grid.GridPanel(
        {

            frame : true,
            flex : 1,
            loadMask : true,
            viewConfig :
            {
                forceFit : true
            },
            store : this.storeIdiomas,
            sm : new Ext.grid.RowSelectionModel(
            {
                singleSelect : true
            }),
            columns : userColumns
        });

        return grid;
    }
});