Ext.ns('UJI.OCW');

UJI.OCW.tabMaterialAutores = Ext.extend(Ext.Panel,
{
    title : ' Autors',
    closable : true,
    layout : 'vbox',
    layoutConfig :
    {
        align : 'stretch'
    },
    botonBorrarAutor : {},
    botonAnadirAutor : {},
    storeAutores : {},
    storeMaterialAutores : {},
    gridAutores : {},

    initComponent : function()
    {
        var config = {};
        Ext.apply(this, Ext.apply(this.initialConfig, config));
        UJI.OCW.tabMaterialAutores.superclass.initComponent.call(this);
        this.initUI();
        this.add(this.gridAutores);
    },

    initUI : function()
    {
        this.buildStoreAutores();
        this.buildStoreMaterialAutores();
        this.buildBotonBorrarAutor();
        this.buildBotonAnadirAutor();
        this.buildGridAutores();
    },

    buildStoreAutores : function()
    {

        this.storeAutores = new Ext.data.Store(
        {
            restful : true,
            url : '/ocw/rest/autor',
            autoLoad : false,
            reader : new Ext.data.XmlReader(
            {
                record : 'Autor',
                id : 'id'
            }, [ 'id', 'nombre', 'apellido1', 'apellido2', 'email',
            {
                name : 'personaId',
                type : 'number'
            } ]),
            writer : new Ext.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            })
        });
    },

    buildStoreMaterialAutores : function()
    {

        var ref = this;
        this.storeMaterialAutores = new Ext.data.Store(
        {
            restful : true,
            proxy : new Ext.data.HttpProxy(
            {
                url : '/ocw/rest/materialAutor'

            }),
            baseParamas :
            {
                materialId : ref.materialId
            },
            autoLoad : false,
            reader : new Ext.data.XmlReader(
            {
                record : 'MaterialAutor',
                id : 'id'
            }, [ 'id', 'autorId', 'materialId', 'nombre', 'apellido1', 'apellido2', 'email' ]),
            writer : new Ext.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            }),
            listeners :
            {
                'beforeload' : function(editor, index)
                {
                    this.setBaseParam('materialId', ref.materialId);
                }
            }

        });
    },

    buildBotonAnadirAutor : function()
    {
        var ref = this;

        this.addEvents('LookoupWindowClickSeleccion');
        var window = new Ext.ux.uji.form.LookupWindow(
        {
            appPrefix : 'ocw',
            bean : 'autor'
        });

        window.on('LookoupWindowClickSeleccion', function(id)
        {
            var rec = new ref.storeMaterialAutores.recordType(
            {
                id : '',
                materialId : ref.materialId,
                autorId : id
            });
            ref.storeMaterialAutores.insert(0, rec);
        });

        this.botonAnadirAutor = new Ext.Button(
        {
            text : 'Afegir',
            iconCls : 'application-add',
            handler : function(button)
            {
                window.show();
            }
        });
    },

    buildBotonBorrarAutor : function()
    {
        var ref = this;

        this.botonBorrarAutor = new Ext.Button(
        {
            text : 'Esborrar',
            iconCls : 'application-delete',
            handler : function(boton, evento)
            {

                Ext.Msg.confirm("Corfirmació de la operació",
                        "Realment dessitga esborrar aquest registre?", function(button, text)
                        {
                            if (button == "yes")
                            {
                                var registroSeleccionado = ref.gridAutores.getSelectionModel()
                                        .getSelected();
                                if (!registroSeleccionado)
                                {
                                    return false;
                                }
                                ref.storeMaterialAutores.remove(registroSeleccionado);
                            }
                        });
            }
        });
    },

    buildGridAutores : function()
    {
        var ref = this;

        var colModel = new Ext.grid.ColumnModel(
        {
            defaults :
            {
                sortable : true
            },
            columns : [
            {
                header : "id",
                width : 25,
                hidden : true,
                dataIndex : 'id'
            },
            {
                header : 'Nom',
                dataIndex : 'nombre'

            },
            {
                header : 'Cognom 1',
                dataIndex : 'apellido1'
            },
            {
                header : 'Cognom 2',
                dataIndex : 'apellido2'
            },
            {
                header : 'Adreça correu',
                dataIndex : 'email'
            } ]
        });

        this.gridAutores = new Ext.grid.GridPanel(
        {
            frame : true,
            flex : 1,
            loadMask : true,
            tbar :
            {
                items : [ ref.botonAnadirAutor, ref.botonBorrarAutor ]
            },
            viewConfig :
            {
                forceFit : true
            },
            store : ref.storeMaterialAutores,
            sm : new Ext.grid.RowSelectionModel(
            {
                singleSelect : true
            }),
            colModel : colModel
        });

    },
    setMaterialActivo : function(materialId)
    {
        this.materialId = materialId;
        this.storeMaterialAutores.reload();
        this.storeAutores.reload();
    }
});
