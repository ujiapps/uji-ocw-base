Ext.ns('UJI.OCW');

UJI.OCW.formCursoDatosGenerales = Ext.extend(Ext.Panel,
{
    layout : 'anchor',
    flex : 1,
    cursoId : null,
    categoriaPorDefectoId : '',
    botonVolver : {},
    botonGuardar : {},
    comboEstados : {},
    campoEstados : {},
    storeCategorias : {},
    comboCategorias : {},
    campoEtiquetas : {},
    checkboxVisible : {},
    formDatosGenerales : {},
    storeCurso : {},
    campoNombre : {},
    autoScroll : true,
    layoutConfig :
    {
        align : 'stretch'
    },

    bubbleEvents : [ 'cursoChangeCard', 'infoCurso', 'cursoInsertadoCorrectamente' ],

    initComponent : function()
    {
        var config = {};
        Ext.apply(this, Ext.apply(this.initialConfig, config));
        UJI.OCW.formCursoDatosGenerales.superclass.initComponent.call(this);

        this.addEvents('cursoChangeCard', 'infoCurso', 'cursoInsertadoCorrectamente');
        this.initUI();
        this.add(this.formDatosGenerales);
    },

    initUI : function()
    {
        this.buildCampoNombre();
        this.buildCheckboxVisible();
        this.buildCampoEtiquetas();
        this.buildStoreCategorias();
        this.buildStoreCurso();
        this.buildBotonGuardar();
        this.buildComboEstados();
        this.buildCampoEstados();
        this.buildComboCategorias();
        this.buildFormDatosGenerales();
    },

    buildStoreCategorias : function()
    {
        var ref = this;
        this.storeCategorias = new Ext.data.Store(
        {
            restful : true,
            url : '/ocw/rest/categoria',
            autoLoad : false,
            reader : new Ext.data.XmlReader(
            {
                record : 'Categoria',
                id : 'id'
            }, [ 'id', 'nombre__ca' ]),
            writer : new Ext.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            }),
            listeners :
            {
                load : function()
                {
                    ref.comboCategorias.setValue(ref.categoriaPorDefectoId);
                }
            }
        });
    },

    buildStoreCurso : function()
    {
        this.storeCurso = new Ext.data.Store(
        {

            restful : true,
            url : '/ocw/rest/curso/',
            autoLoad : false,
            reader : new Ext.data.XmlReader(
            {
                record : 'Curso',
                id : 'id'
            }, buildMultilangStoreColumns('id', 'tags', 'persona', 'fechaCreacion', 'estado', 'fechaBaja', multilang('nombre'), multilang('periodo'), multilang('programa'),
                    multilang('guiaAprendizaje'), multilang('descripcion'), 'visible')),
            writer : new Ext.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            })
        });
    },

    buildBotonGuardar : function()
    {
        var ref = this;

        this.botonGuardar = new Ext.Button(
        {
            text : 'Guardar',
            iconCls : 'application-add',
            style : 'margin-right: 10px;',
            handler : function(button, event)
            {
                var form = ref.formDatosGenerales.getForm();
                if (!ref.campoNombre.getValue())
                {
                    Ext.Msg.alert('Error', 'El camp Nom es obligatori');
                }
                else
                {
                    if (form.isValid())
                    {
                        ref.submitForm();
                    }
                    else
                    {
                        Ext.Msg.alert('Error', 'Revisa els camps del formulari');
                    }
                }
            }
        });
    },

    buildComboEstados : function()
    {
        this.comboEstados = new Ext.form.ComboBox(
        {
            name : 'estado',
            mode : 'local',
            fieldLabel : 'Estat',
            valueField : 'estado',
            hiddenName : 'estado',
            allowBlank : false,
            triggerAction : 'all',
            width : 150,
            editable : false,
            displayField : 'descripcion',
            store : new Ext.data.ArrayStore(
            {
                fields : [ 'estado', 'descripcion' ],
                data : [ [ 'I', 'Introduïnt dades' ], [ 'CF', 'Casi Finalitzat' ], [ 'F', 'Finalitzat' ] ]
            })
        });
    },

    buildCampoEstados : function()
    {
        this.campoEstados = new Ext.form.CompositeField(
        {
            items : [ this.comboEstados, new Ext.form.Label(
            {
                text : '* El curs no apareix en la web fins que estiga finalitzat',
                style : 'font-style: italic; color: #BBB',
                margins : '7 0 0 10'
            }) ]
        });

    },

    buildComboCategorias : function()
    {
        var ref = this;

        this.comboCategorias = new Ext.form.ComboBox(
        {
            fieldLabel : 'Categoria',
            xtype : 'combo',
            name : 'categoria',
            hiddenName : 'categoriaId',
            emptyText : 'Selecciona una categoria',
            forceSelection : true,
            editable : false,
            allowBlank : false,
            triggerAction : 'all',
            store : this.storeCategorias,
            valueField : 'id',
            displayField : 'nombre__ca'
        });
    },

    buildCampoEtiquetas : function()
    {
        this.campoEtiquetas = new Ext.form.CompositeField(
        {
            items : [ new Ext.form.TextField(
            {
                fieldLabel : 'Tags',
                width : 300,
                xtype : 'textfield',
                name : 'tags'
            }), new Ext.form.Label(
            {
                text : '* Etiquetes separades per comes',
                style : 'font-style: italic; color: #BBB',
                margins : '7 0 0 10'
            }) ]
        });
    },
    buildCheckboxVisible : function()
    {
        this.checkboxVisible = new Ext.form.Checkbox(
        {
            boxLabel : 'Visible',
            xtype : 'checkbox',
            name : 'visible',
            inputValue : 'S',
            value : 'N'
        });
    },
    buildCampoNombre : function()
    {
        this.campoNombre = new Ext.ux.uji.form.MultiLanguageTextField(
        {

            fieldLabel : 'Nom',
            language : window.idiomas,
            width : 450,
            allowBlank : false,
            name : 'nombre'
        });
    },

    buildFormDatosGenerales : function()
    {
        var ref = this;

        var items = [
        {
            xtype : 'textfield',
            hidden : true,
            name : 'id'
        },
        {
            xtype : 'panel',
            layout : 'hbox',
            items : [
            {
                xtype : 'panel',
                layout : 'form',
                items : [ this.campoNombre ]
            }, this.checkboxVisible ]
        }, this.campoEstados, this.comboCategorias,
        {
            xtype : 'panel',
            layout : 'hbox',
            items : [
            {
                xtype : 'panel',
                layout : 'form',
                flex : 1,
                width : 280,
                labelWidth : 90,
                items : [
                {
                    fieldLabel : 'Data creació',
                    flex : 1,
                    format : 'd/m/Y',
                    submitFormat : 'd/m/Y',
                    name : 'fechaCreacion',
                    width : 150,
                    allowBlank : false,
                    xtype : 'datefield'
                } ]
            },
            {
                xtype : 'panel',
                layout : 'form',
                flex : 1,
                labelWidth : 90,
                items : [
                {
                    fieldLabel : 'Data baixa',
                    format : 'd/m/Y',
                    submitFormat : 'd/m/Y',
                    name : 'fechaBaja',
                    width : 150,
                    xtype : 'datefield'
                } ]
            } ]
        },
        {
            fieldLabel : 'Període',
            width : 450,
            xtype : 'multilanguagetextfield',
            language : window.idiomas,
            allowBlank : false,
            name : 'periodo'
        } ];

        if (this.extendido)
        {
            var modalWindowSize = this.getModalWindowSize();

            this.comboCategorias.hide();
            this.comboCategorias.setValue("notneeded");
            this.setTitle('Dades generals');
            items.push(this.campoEtiquetas);

            items.push(
            {
                xtype : 'multilanguagetextarea',
                language : window.idiomas,
                fieldLabel : 'Descripció',
                width : 450,
                height : 75,
                allowBlank : false,
                name : 'descripcion',
                windowWidth : modalWindowSize.width,
                windowHeight : modalWindowSize.height,
                windowMaximizable : true
            });
            items.push(
            {
                fieldLabel : 'Programa',
                width : 450,
                height : 75,
                xtype : 'multilanguagetextarea',
                language : window.idiomas,
                allowBlank : false,
                name : 'programa',
                windowWidth : modalWindowSize.width,
                windowHeight : modalWindowSize.height,
                windowMaximizable : true
            });
            items.push(
            {
                fieldLabel : 'Guia',
                width : 450,
                height : 75,
                xtype : 'multilanguagetextarea',
                language : window.idiomas,
                allowBlank : false,
                name : 'guiaAprendizaje',
                windowWidth : modalWindowSize.width,
                windowHeight : modalWindowSize.height,
                windowMaximizable : true
            });

            this.formDatosGenerales = new Ext.FormPanel(
            {
                flex : 1,
                labelWidth : 90,
                frame : true,
                anchor : 'auto 100%',
                boxMinHeight : 500,
                buttonAlign : 'right',
                buttons : [ this.botonGuardar ],
                layout : 'form',
                padding : 10,
                reader : new Ext.data.XmlReader(
                {
                    record : 'Curso',
                    id : 'id'
                }, buildMultilangStoreColumns('id', 'tags', 'fechaBaja', 'estado', 'fechaCreacion', multilang('nombre'), 'estado', multilang('periodo'), multilang('descripcion'),
                        multilang('programa'), multilang('guiaAprendizaje'), 'visible')),
                errorReader : new Ext.data.XmlReader(
                {
                    record : 'responseMessage',
                    success : 'success'
                }, [ 'success', 'msg' ]),
                items : items
            });

        }
        else
        {
            this.comboEstados.hide();
            this.checkboxVisible.hide();
            this.formDatosGenerales = new Ext.FormPanel(
            {
                flex : 1,
                labelWidth : 90,
                frame : true,
                anchor : 'auto 100%',
                layout : 'form',
                padding : 10,
                bubbleEvents : [ 'cursoInsertadoCorrectamente' ],
                reader : new Ext.data.XmlReader(
                {
                    record : 'Curso',
                    id : 'id'
                }, buildMultilangStoreColumns('id', 'fechaBaja', 'fechaCreacion', multilang('nombre'), multilang('periodo'))),
                errorReader : new Ext.data.XmlReader(
                {
                    record : 'responseMessage',
                    success : 'success'
                }, [ 'success', 'msg' ]),
                items : items,
                listeners :
                {
                    beforeshow : function()
                    {
                        //  ref.storeCategorias.load();
                    }
                }

            });
        }
    },

    setCursoActivo : function(cursoId)
    {
        this.cursoId = cursoId;
        // this.storeMaterialCursos.reload();
    },

    setInfoCurso : function(texto)
    {
        var infoCurso = texto;
        this.fireEvent("infoCurso", infoCurso);
    },
    loadCurso : function(cursoId)
    {
        var ref = this;
        this.cursoId = cursoId;

        this.formDatosGenerales.getForm().load(
        {
            url : '/ocw/rest/curso/' + ref.cursoId,
            method : 'GET',
            waitMsg : 'Carregant ...',
            success : function(form, action)
            {

                var id = form.reader.xmlData.getElementsByTagName("id")[0];
                var nombre__ca = form.reader.xmlData.getElementsByTagName("nombre__ca")[0];
                var persona = form.reader.xmlData.getElementsByTagName("persona")[0];

                if (id !== undefined)
                {
                    id = id.childNodes[0].nodeValue;
                }

                if (nombre__ca !== undefined)
                {
                    nombre__ca = nombre__ca.childNodes[0].nodeValue;
                }

                var infoCurso = id + " - " + nombre__ca;

                if (persona !== undefined)
                {
                    persona = persona.childNodes[0].nodeValue;
                    infoCurso += " - " + persona;
                }

                ref.setInfoCurso(infoCurso);
            }
        });
    },
    submitForm : function()
    {
        var ref = this;
        var form = this.formDatosGenerales.getForm();

        form.submit(
        {
            url : '/ocw/rest/curso/',
            waitMsg : 'Guardant dades...',
            method : 'post',
            success : function(form, action)
            {
                var xmlData = action.response.responseXML;
                var id = Ext.DomQuery.selectValue("id", xmlData);
                ref.fireEvent('cursoInsertadoCorrectamente', id);
            },
            failure : function()
            {
                Ext.MessageBox.show(
                {
                    title : 'Error',
                    msg : 'Error guardant el curs',
                    buttons : Ext.MessageBox.OK,
                    icon : Ext.MessageBox.ERROR
                });
            }
        });
    },
    getForm : function()
    {
        return this.formDatosGenerales.getForm();
    },

    cleanForm : function()
    {
        this.cursoId = null;
        if (this.formDatosGenerales)
        {
            this.formDatosGenerales.getForm().reset();
            this.comboEstados.setValue("I");
        }
    },

    setComboCategorias : function(categoriaId)
    {
        this.categoriaPorDefectoId = categoriaId;
        this.storeCategorias.load();
    },

    getModalWindowSize : function()
    {
        var MAX_WIDTH = 1024;
        var MAX_HEIGHT = 768;

        var size =
        {
            width : window.innerWidth * 0.75,
            height : window.innerHeight * 0.75
        };

        if (size.width > MAX_WIDTH)
        {
            size.width = MAX_WIDTH;
        }

        if (size.height > MAX_HEIGHT)
        {
            size.height = MAX_HEIGHT
        }

        return size;
    }

});