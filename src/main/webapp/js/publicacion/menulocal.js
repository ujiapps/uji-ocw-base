$(document).ready(function() {
	$('.menuLocal .colapsable.expanded ul').css("display", "block");
	// Procesos de cierre y apertura de cada rama de menuLocal
	$('.menuLocal .colapsable > a').not('.leaf').click(function() {
		mychildren = $(this).next();
		var mi = $(this).parent();
		if (mychildren.length > 0) {
			// Escondemos los elementos hijos del menú
			mychildren.animate({
				height : 'toggle'
			}, 500, function() {
				recalcula_alturas();// igualan main y rightCol en altura
			});
			// Cambiamos la apariencia de la flecha del elemento del menú
			mi.toggleClass(function() {
				if (mi.hasClass('.expanded')) {
					return 'colapsed';
				} else {
					mi.toggleClass('colapsed');
					return 'expanded';
				}
			});
		}
		return false;
	});
});
