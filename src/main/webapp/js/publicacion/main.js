function recalcula_alturas() {
  $('#mainContent #padContent').remove();
  var alt_options = $('#rightCol').height();
  var alt_body = $('#main').height();
  if (alt_options > alt_body) {		
  	if ($('#mainContent #padContent').length != 1) {
  		$('#mainContent').append($('<div id="padContent" />').height(alt_options - alt_body + 2));
  	}
  	else {
  		$('#padContent').height(alt_options - alt_body + 2);
  	}
  }
}

$(document).ready(function(){
  //calculamos el margen del título de la cabecera para que quede centrado en altura
  $('#site-title').css('margin-top',(($('#header').height() - $('#site-title').height()) / 2));
	
	//se activa el roller de los recortes de prensa
  $('.newsList').vTicker({
    speed: 1000,
    pause: 8000,
    showItems: 1,
    animation: 'fade',
    mousePause: true
  });
		
  //se igualan las alturas de todas las "units" de cada "line" 
	//incluidas dentro de la zona de contenido
	$('.mainInner .line').each(function(i1) {
		var maxAlt = 0;
		var units = $(this).children('.unit');
		
		if ( ( units.length > 1 ) && ( $(this).hasClass('bloque') ) ){
			units.each(function(i2) {
	      var thisAlt = $(this).height();
	      if (thisAlt > maxAlt) {
	        maxAlt = thisAlt;
	      }
	    });
	    
	    units.each(function(i3) {
	      $(this).height(maxAlt);
	    }); 
		}
  });
	
	recalcula_alturas();
	
	// Comprobamos que en la búsqueda simple, se busque al menos con un caracter
	$('#site-search').submit(function()
	{
		if ($('#buscador-buscar').val().trim() == '')
		{
			return false;
		}
	});
	
});