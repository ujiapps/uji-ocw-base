Ext.ns('UJI.OCW');

UJI.OCW.MaterialForm = Ext.extend(Ext.Panel,
{
    layout : 'vbox',
    region : 'center',
    autoWidth : true,
    formMateriales : {},
    storeLicencias : {},
    storeIdiomas : {},
    botonGuardar : {},
    botonVolver : {},
    boxMinHeight : 700,
    tabMaterialMetadatos : {},
    tabMaterialAutores : {},
    compositeGestionDocumento : {},
    campoEtiquetas : {},
    tabMaterialCursos : {},
    campoNombreDocumento : {},
    panelMateriales : {},
    campoUrl : {},
    radioUrl : {},
    radioContenidos : {},
    campoDocumento : {},
    htmlEditor : {},
    materialId : 0,
    bubbleEvents : [ 'materialChangeCard' ],
    layoutConfig :
    {
        align : 'stretch'
    },
    initComponent : function()
    {
        var config = {};
        Ext.apply(this, Ext.apply(this.initialConfig, config));
        UJI.OCW.MaterialForm.superclass.initComponent.call(this);
        this.initUI();
        this.add(this.formMateriales);
        this.add(this.panelMateriales);
    },

    initUI : function()
    {
        this.buildCampoNombreDocumento();
        this.buildCompositeGestionDocumento();
        this.buildRadioUrl();
        this.buildRadioContenidos();
        this.buildCampoDocumento();
        this.buildCampoEtiquetas();
        this.buildCampoUrl();
        this.buildHtmlEditor();
        this.buildStoreLicencias();
        this.buildStoreIdiomas();
        this.buildBotonVolver();
        this.buildBotonGuardar();
        this.buildFormMateriales();
        this.buildTabMaterialMetadatos();
        this.buildTabMaterialAutores();
        this.buildTabMaterialCursos();
        this.buildPanelMateriales();

    },

    buildBotonVolver : function()
    {
        var ref = this;

        this.botonVolver = new Ext.Button(
        {
            text : 'Tornar',
            iconCls : 'arrow-undo',
            handler : function(button, event)
            {
                ref.fireEvent('materialChangeCard', 'materialForm', 'close');
            }
        });

    },

    submitForm : function()
    {
        var ref = this;
        var form = ref.formMateriales.getForm();

        form.submit(
        {
            url : '/ocw/rest/material/',
            method : 'post',
            waitMsg : 'Guardant dades...',
            success : function(form, action)
            {
                if (action.response.responseXML.title !== '')
                {
                    Ext.MessageBox.show(
                    {
                        title: 'Error',
                        msg: 'Error guardant el document, la mida màxima del fitxer és de 40Mb',
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.ERROR
                    });
                    return;
                }

                if (ref.materialId == 0)
                {
                    ref.materialId = Ext.DomQuery.selectValue('id', action.response.responseXML);
                    ref.formMateriales.getForm().setValues(
                    {
                        id : ref.materialId
                    });
                    ref.panelMateriales.activate('materialMetadatos');
                    ref.panelMateriales.getActiveTab().setMaterialActivo(ref.materialId);
                }

                if (ref.radioUrl.getValue() === true)
                {
                    ref.campoNombreDocumento.setValue(null);
                }
                else
                {
                    var nombreFichero = ref.campoDocumento.getValue();
                    nombreFichero = nombreFichero.replace('C:\\fakepath\\', '');
                    if (nombreFichero)
                    {
                        ref.campoNombreDocumento.setValue(nombreFichero);
                        ref.compositeGestionDocumento.setVisible(true);
                    }
                }
            },
            failure : function()
            {
                Ext.MessageBox.show(
                {
                    title : 'Error',
                    msg : 'Error guardant el document',
                    buttons : Ext.MessageBox.OK,
                    icon : Ext.MessageBox.ERROR
                });
            }
        });
    },
    buildBotonGuardar : function()
    {
        var ref = this;

        this.botonGuardar = new Ext.Button(
        {
            text : 'Guardar',
            iconCls : 'application-add',
            handler : function(button, event)
            {
                var form = ref.formMateriales.getForm();

                if (form.isValid())
                {
                    if ((ref.radioUrl.getValue() === false && ref.radioContenidos.getValue() === false) || (ref.radioUrl.getValue() === true && ref.campoUrl.getValue() === "")
                            || (ref.radioContenidos.getValue() === true && ref.campoDocumento.getValue() === "" && ref.campoNombreDocumento.getValue() === ""))
                    {
                        Ext.MessageBox.show(
                        {
                            title : 'Error',
                            msg : 'Tens que agregar un contingut via URL o un document',
                            buttons : Ext.MessageBox.OK,
                            icon : Ext.MessageBox.ERROR
                        });
                    }
                    else if (ref.radioUrl.getValue() === true && ref.campoNombreDocumento.getValue() !== "")
                    {
                        Ext.Msg.confirm("Corfirmació de la operació", "Aquesta operació eliminarà el document adjunt previament pujat. Està segur/a?", function(button, text)
                        {
                            if (button == "yes")
                            {
                                ref.campoNombreDocumento.setValue(null);
                                ref.campoDocumento.setValue(null);
                                ref.submitForm();
                                ref.panelMateriales.enable();
                            }
                        });
                    }
                    else
                    {
                        if (ref.radioUrl.getValue() === true)
                        {
                            ref.campoNombreDocumento.setValue(null);
                        }
                        else
                        {
                            ref.campoUrl.setValue(null);
                        }
                        ref.submitForm();
                        ref.panelMateriales.enable();
                    }
                }
                else
                {
                    Ext.Msg.alert('Error', 'Revisa els camps del formulari');
                }
            }
        });
    },

    buildStoreLicencias : function()
    {
        this.storeLicencias = new Ext.data.Store(
        {
            restful : true,
            url : '/ocw/rest/licencia',
            autoLoad : false,
            reader : new Ext.data.XmlReader(
            {
                record : 'Licencia',
                id : 'id'
            }, [ 'id', 'titulo__' + idiomas[0] ]),
            writer : new Ext.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            }),
            listeners :
            {
                save : function(store, batch, data)
                {
                    if (data.destroy !== null)
                    {
                        this.reload();
                    }
                }
            }
        });
    },
    buildStoreIdiomas : function()
    {
        this.storeIdiomas = new Ext.data.Store(
        {
            restful : true,
            url : '/ocw/rest/idioma',
            autoLoad : false,
            reader : new Ext.data.XmlReader(
            {
                record : 'Idioma',
                id : 'id'
            }, [ 'id', 'nombre' ]),
            writer : new Ext.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            })
        });
    },

    buildCampoUrl : function()
    {
        var ref = this;

        this.campoUrl = new Ext.form.TextField(
        {
            vtype : 'url',
            width : 250,
            xtype : 'textfield',
            name : 'url',
            listeners :
            {
                focus : function()
                {
                    ref.radioUrl.setValue(true);
                }
            }
        });
    },
    buildCampoEtiquetas : function()
    {
        this.campoEtiquetas = new Ext.form.CompositeField(
        {
            items : [ new Ext.form.TextField(
            {
                fieldLabel : 'Tags',
                width : 400,
                xtype : 'textfield',
                name : 'tags'
            }), new Ext.form.Label(
            {
                text : '* Etiquetes separades per comes',
                style : 'font-style: italic; color: #BBB',
                margins : '7 0 0 10'
            }) ]
        });
    },
    buildCampoDocumento : function()
    {
        this.campoDocumento = new Ext.form.TextField(
        {
            xtype : 'textfield',
            name : 'documento',
            id : 'documento',
            disabled : true,
            inputType : 'file'
        });
    },
    buildCampoNombreDocumento : function()
    {
        this.campoNombreDocumento = new Ext.form.TextField(
        {
            xtype : 'textfield',
            readOnly : true,
            width : 150,
            fieldClass : 'x-item-disabled'
        });
    },
    buildCompositeGestionDocumento : function()
    {
        var ref = this;
        this.compositeGestionDocumento = new Ext.form.CompositeField(
        {
            style : 'margin-left: 55px;',
            items : [ ref.campoNombreDocumento,
            {
                xtype : 'button',
                text : 'Descarregar',
                iconCls : 'arrow-down',
                handler : function()
                {
                    if (this.campoNombreDocumento.getValue())
                    {
                        window.open('/ocw/rest/material/' + ref.materialId + '/raw');
                    }

                },
                scope : this
            } ]
        });

    },
    buildRadioUrl : function()
    {
        var ref = this;
        this.radioUrl = new Ext.form.Radio(
        {
            xtype : 'radio',
            width : 50,
            id : 'radioUrl',
            boxLabel : 'Url',
            name : 'contenidos',
            checked : false,
            listeners :
            {
                check : function(element, checked)
                {
                    if (checked)
                    {
                        ref.campoDocumento.setDisabled(true);
                        ref.compositeGestionDocumento.setVisible(false);
                        ref.campoUrl.setDisabled(false);
                    }
                }
            }
        });
    },
    buildRadioContenidos : function()
    {
        var ref = this;
        this.radioContenidos = new Ext.form.Radio(
        {
            id : 'radioFichero',
            xtype : 'radio',
            boxLabel : 'Fitxer',
            name : 'contenidos',
            checked : false,
            listeners :
            {
                check : function(checkbox, checked)
                {
                    if (checked)
                    {
                        if (checked)
                        {
                            ref.campoDocumento.setDisabled(false);
                            ref.compositeGestionDocumento.setVisible(true);
                            ref.campoUrl.setDisabled(true);
                        }
                    }
                }
            }
        });
    },
    buildFormMateriales : function()
    {
        var ref = this;

        this.formMateriales = new Ext.FormPanel(
        {
            labelWidth : 75,
            frame : true,
            fileUpload : true,
            height : 390,
            buttonAlign : 'right',
            buttons : [ this.botonGuardar, ref.botonVolver ],

            bubbleEvents : [ 'materialChangeCard' ],

            reader : new Ext.data.XmlReader(
            {
                record : 'Material'
            }, [ 'id', 'titulo', 'personaId', 'fecha', 'idiomaId', 'tags', 'descripcion', 'nombreFichero', 'url', 'licenciaId', 'contentType', 'handleId', 'documento' ]),
            errorReader : new Ext.data.XmlReader(
            {
                record : 'responseMessage',
                success : 'success'
            }, [ 'success', 'msg' ]),

            items : [
            {
                xtype : 'textfield',
                hidden : true,
                name : 'id'
            },
            {
                xtype : 'textfield',
                hidden : true,
                name : 'personaId'
            },
            {
                fieldLabel : 'Títol',
                anchor : '50%',
                xtype : 'textfield',
                allowBlank : false,
                name : 'titulo'
            }, ref.htmlEditor, ref.campoEtiquetas,
            {
                xtype : 'panel',
                layout : 'hbox',
                autoHeight : true,
                items : [

                {
                    xtype : 'fieldset',
                    flex : 4,
                    width : 480,
                    height : 110,
                    layout : 'form',
                    labelWidth : 10,
                    title : 'Continguts',
                    items : [
                    {
                        xtype : 'compositefield',
                        items : [ ref.radioUrl, ref.campoUrl ]
                    },
                    {
                        xtype : 'compositefield',
                        items : [ ref.radioContenidos, ref.campoDocumento ]
                    }, ref.compositeGestionDocumento ]
                },
                {
                    xtype : 'fieldset',
                    padding : 5,
                    border : false,
                    flex : 2,
                    labelWidth : 60,
                    layout : 'form',
                    items : [
                    {
                        width : 300,
                        fieldLabel : 'Llicència',
                        xtype : 'combo',
                        name : 'licencia',
                        hiddenName : 'licenciaId',
                        emptyText : 'Selecciona una llicencia',
                        forceSelection : true,
                        editable : false,
                        allowBlank : false,
                        triggerAction : 'all',
                        store : this.storeLicencias,
                        valueField : 'id',
                        displayField : 'titulo__' + idiomas[0]
                    },
                    {
                        xtype : 'combo',
                        emptyText : 'Selecciona un Idioma',
                        fieldLabel : 'Idioma',
                        name : 'idioma',
                        hiddenName : 'idiomaId',
                        forceSelection : true,
                        allowBlank : false,
                        editable : false,
                        triggerAction : 'all',
                        store : this.storeIdiomas,
                        valueField : 'id',
                        displayField : 'nombre',
                        width : 150
                    },
                    {
                        fieldLabel : 'Data',
                        format : 'd/m/Y',
                        submitFormat : 'd/m/Y',
                        name : 'fecha',
                        width : 150,
                        xtype : 'datefield'
                    } ]

                } ]
            } ]
        });
    },

    buildHtmlEditor : function()
    {
        this.htmlEditor = new Ext.form.CKEditor(
        {
            name : 'descripcion',
            fieldLabel : 'Descripció',
            id : 'descripcion',
            hideLabel : false,
            CKConfig :
            {
                height : 100,
                toolbar : 'MyToolbarSet',
                toolbar_MyToolbarSet : [
                {
                    name : 'document',
                    items : [ 'Source' ]
                },
                {
                    name : 'basicstyles',
                    items : [ 'Bold', 'Italic', 'Underline' ]
                },
                {
                    name : 'paragraph',
                    items : [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock' ]
                } ]
            },
            enableColors : false,
            enableAlignments : false
        });

    },

    buildTabMaterialMetadatos : function()
    {
        this.tabMaterialMetadatos = new UJI.OCW.tabMaterialMetadatos();

    },
    buildTabMaterialAutores : function()
    {
        this.tabMaterialAutores = new UJI.OCW.tabMaterialAutores();

    },
    buildTabMaterialCursos : function()
    {
        this.tabMaterialCursos = new UJI.OCW.tabMaterialCursos();

    },
    buildPanelMateriales : function()
    {
        var ref = this;
        this.panelMateriales = new Ext.TabPanel(
        {
            activeTab : 0,
            deferredRender : false,
            flex : 1,
            defaults :
            {
                closable : false
            },
            items : [ this.tabMaterialMetadatos, this.tabMaterialAutores, this.tabMaterialCursos ],
            listeners :
            {
                'tabchange' : function()
                {
                    if (ref.materialId != 0)
                    {
                        this.getActiveTab().setMaterialActivo(ref.materialId);
                    }
                }
            }
        });
    },

    loadMaterial : function(materialId)
    {
        this.clearSelection();
        var ref = this;
        this.materialId = materialId;
        this.panelMateriales.enable();
        this.panelMateriales.activate('materialMetadatos');
        this.panelMateriales.getActiveTab().setMaterialActivo(ref.materialId);
        this.storeIdiomas.load(
        {
            callback : function()
            {
                ref.storeLicencias.load(
                {
                    callback : function()
                    {
                        ref.formMateriales.getForm().load(
                        {
                            url : '/ocw/rest/material/' + ref.materialId,
                            method : 'GET',
                            waitMsg : 'Carregant ...',
                            success : function(form, action)
                            {
                                var url = action.result.data.url;
                                var nombreFichero = action.result.data.nombreFichero;

                                if (url !== '' || (url === '' && nombreFichero === ''))
                                {
                                    ref.radioUrl.setValue(true);
                                    ref.compositeGestionDocumento.setVisible(false);
                                    ref.campoDocumento.setDisabled(true);
                                }
                                else if (nombreFichero !== '')
                                {
                                    ref.radioContenidos.setValue(true);
                                    ref.campoNombreDocumento.setValue(nombreFichero);
                                }
                            }

                        });
                    }
                });
            }
        });
    },

    cleanForm : function()
    {
        this.clearSelection();
        this.materialId = 0;
        this.campoDocumento.setDisabled(true);
        if (this.formMateriales)
        {
            this.formMateriales.getForm().reset();
            this.compositeGestionDocumento.setVisible(false);
        }
        this.panelMateriales.activate('materialMetadatos');
        this.panelMateriales.getActiveTab().setMaterialActivo(this.materialId);
        this.panelMateriales.disable();
    },

    clearSelection : function()
    {
        if (window.getSelection)
        {
            window.getSelection().removeAllRanges();
        }
        else if (document.selection)
        {
            document.selection.empty();
        }
    }
});
