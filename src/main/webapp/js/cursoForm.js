Ext.ns('UJI.OCW');

UJI.OCW.CursoForm = Ext.extend(Ext.Panel,
{
    layout : 'vbox',
    autoScroll : true,
    tabCursoDatosGenerales : {},
    tabCursoMateriales : {},
    tabCursoAsignaturas : {},
    tabCursoAutorizados : {},
    tabCursoImagenes : {},
    tabCursoCategorias : {},
    botonVolver : {},
    botonVistaPrevia : {},
    cursoId : null,
    cardPanel : {},
    zonaSuperior : {},
    layoutConfig :
    {
        align : 'stretch'
    },
    initComponent : function()
    {
        var config = {};
        Ext.apply(this, Ext.apply(this.initialConfig, config));
        UJI.OCW.CursoForm.superclass.initComponent.call(this);

        this.addEvents("cursoChangeCard");

        this.initUI();
        this.add(this.zonaSuperior);
        this.add(this.panelCurso);

    },

    listeners :
    {
        'infoCurso' : function(infoCurso)
        {
            this.infoCurso.setText('<h1 style="font-size:150%">' + infoCurso + '</h1>', false);
        }
    },
    bubbleEvents : [ 'cursoChangeCard' ],

    initUI : function()
    {
        this.buildBotonVolver();
        this.buildBotonVistaPrevia();
        this.buildTabCursoImagenes();
        this.buildTabCursoDatosGenerales();
        this.buildTabCursoMateriales();
        this.buildTabCursoAsignaturas();
        this.buildTabCursoAutorizados();
        this.buildTabCursoCategorias();
        this.buildInfoCurso();
        this.buildZonaSuperior();
        this.buildPanelCurso();
    },

    buildBotonVolver : function()
    {
        var ref = this;

        this.botonVolver = new Ext.Button(
        {
            text : 'Tornar',
            margins : '10 10 10 10',
            iconCls : 'arrow-undo',
            handler : function(button, event)
            {
                ref.cursoId = null;
                ref.fireEvent('cursoChangeCard', 'cursoForm');
            }
        });

    },

    buildBotonVistaPrevia : function()
    {
        var ref = this;

        this.botonVistaPrevia = new Ext.Button(
        {
            text : 'Previsualitzar',
            margins : '10 10 10 10',
            iconCls : 'book-open',
            handler : function(button, event)
            {
                window.open('/ocw/rest/publicacion/curso/' + ref.cursoId + '/preview', 'course_' + ref.cursoId);
            }
        });
    },

    buildInfoCurso : function()
    {
        this.infoCurso = new Ext.form.Label(
        {
            html : '<h1 style="font-size:150%">Nuevo curso</h1>',
            height : 33,
            flex : 1,
            frame : true,
            margins : '10 10 10 10'
        });
    },

    buildZonaSuperior : function()
    {
        var ref = this;
        this.zonaSuperior = new Ext.Panel(
        {
            layout : 'hbox',
            items : [ ref.infoCurso, ref.botonVistaPrevia, ref.botonVolver ]
        });
    },

    buildTabCursoImagenes : function()
    {
        this.tabCursoImagenes = new UJI.OCW.tabCursoImagenes();

    },
    buildTabCursoDatosGenerales : function()
    {
        this.tabCursoDatosGenerales = new UJI.OCW.formCursoDatosGenerales(
        {
            extendido : true
        });

    },
    buildTabCursoMateriales : function()
    {
        this.tabCursoMateriales = new UJI.OCW.tabCursoMateriales();

    },
    buildTabCursoAsignaturas : function()
    {
        this.tabCursoAsignaturas = new UJI.OCW.tabCursoAsignaturas();

    },
    buildTabCursoAutorizados : function()
    {
        this.tabCursoAutorizados = new UJI.OCW.tabCursoAutorizados();

    },
    buildTabCursoCategorias : function()
    {
        this.tabCursoCategorias = new UJI.OCW.tabCursoCategorias();

    },
    buildPanelCurso : function()
    {
        var ref = this;
        this.panelCurso = new Ext.TabPanel(
        {
            activeTab : 0,
            deferredRender : false,
            flex : 1,
            bubbleEvents : [ 'cursoChangeCard', 'infoCurso' ],
            defaults :
            {
                closable : false
            },
            items : [ this.tabCursoDatosGenerales, this.tabCursoCategorias,
                    this.tabCursoMateriales, this.tabCursoAsignaturas, this.tabCursoAutorizados,
                    this.tabCursoImagenes ],
            listeners :
            {
                'tabchange' : function()
                {
                    if (ref.cursoId !== null)
                    {
                        this.getActiveTab().setCursoActivo(ref.cursoId);
                    }
                }
            }
        });
    },
    loadCurso : function(cursoId)
    {
        this.clearSelection();
        var ref = this;
        this.cursoId = cursoId;
        this.panelCurso.activate(0);
        this.tabCursoDatosGenerales.loadCurso(ref.cursoId);
    },

    clearSelection : function()
    {
        if (window.getSelection)
        {
            window.getSelection().removeAllRanges();
        }
        else if (document.selection)
        {
            document.selection.empty();
        }
    }
});
