Ext.ns('UJI.OCW');

UJI.OCW.Autores = Ext.extend(Ext.Panel,
{
    title : 'Autors',
    closable : true,
    layout : 'vbox',
    layoutConfig :
    {
        align : 'stretch'
    },
    botonBorrar : {},
    autoScroll : true,
    botonAnadirExtern : {},
    botonAnadirUJI : {},
    storeAutores : {},
    gridAutores : {},
    editor : {},

    initComponent : function()
    {
        var config = {};
        Ext.apply(this, Ext.apply(this.initialConfig, config));
        UJI.OCW.Autores.superclass.initComponent.call(this);
        this.initUI();
        this.add(this.gridAutores);
    },

    initUI : function()
    {
        this.buildStoreAutores();
        this.buildBotonBorrar();
        this.buildBotonAnadirExtern();
        this.buildBotonAnadirUJI();
        this.buildEditor();
        this.buildGridAutores();
    },

    buildStoreAutores : function()
    {

        this.storeAutores = new Ext.data.Store(
        {
            restful : true,
            url : '/ocw/rest/autor',
            autoLoad : true,
            reader : new Ext.data.XmlReader(
            {
                record : 'Autor',
                id : 'id'
            }, [ 'id', 'nombre', 'apellido1', 'apellido2', 'email', 'personaId' ]),
            writer : new Ext.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            })
        });
    },

    buildEditor : function()
    {
        var ref = this;

        this.editor = new Ext.ux.grid.RowEditor(
        {
            xtype : 'textfield',
            saveText : 'Actualitzar',
            cancelText : 'Cancelar',
            errorSummary : false,
            listeners :
            {
                beforeedit : function(editor, index)
                {
                    if (editor.grid.getStore().getAt(index).get('personaId'))
                    {
                        Ext.MessageBox.show(
                        {
                            title : 'Error',
                            msg : 'Aquest autor no és editable.',
                            buttons : Ext.MessageBox.OK,
                            icon : Ext.MessageBox.ERROR
                        });

                        return false;
                    }
                },
                canceledit : function(rowEditor)
                {
                    // para que funcione, alguno de los campos del Store debe ser obligatorio
                    var record = rowEditor.record;
                    if ((record.phantom) || ((record.nombre == null) && (record.apellido1 == null) && (record.apellido2 == null)))
                    {
                        ref.storeAutores.remove(rowEditor.record);
                    }
                }
            }
        });
    },

    buildBotonAnadirExtern : function()
    {
        var ref = this;

        this.botonAnadirExtern = new Ext.Button(
        {
            text : 'Afegir altres',
            iconCls : 'application-add',
            handler : function(btn, evt)
            {
                var rec = new ref.storeAutores.recordType(
                {
                    nombre : '',
                    apellido1 : '',
                    apellido2 : '',
                    email : '',
                    personaId : null
                });

                ref.editor.stopEditing();
                ref.storeAutores.insert(0, rec);
                ref.storeAutores.commitChanges();
                ref.editor.startEditing(0);
            }
        });

    },

    buildBotonAnadirUJI : function()
    {
        var ref = this;

        this.addEvents('LookoupWindowClickSeleccion');
        var window = new Ext.ux.uji.form.LookupWindow(
        {
            appPrefix : 'ocw',
            bean : 'persona'
        });

        window.on('LookoupWindowClickSeleccion', function(id)
        {

            var rec = new ref.storeAutores.recordType(
            {
                nombre : '',

                apellido1 : '',
                apellido2 : '',
                email : '',
                personaId : id
            });
            ref.storeAutores.insert(0, rec);
            ref.storeAutores.commitChanges();
            ref.gridAutores.store.reload();

        });

        this.botonAnadirUJI = new Ext.Button(
        {
            text : 'Afegir UJI',
            iconCls : 'application-add',
            handler : function(button)
            {
                window.show();
            }
        });
    },

    buildBotonBorrar : function()
    {
        var ref = this;

        this.botonBorrar = new Ext.Button(
        {
            text : 'Esborrar',
            iconCls : 'application-delete',
            handler : function(boton, evento)
            {

                var registroSeleccionado = ref.gridAutores.getSelectionModel().getSelected();
                if (!registroSeleccionado)
                {
                    return false;
                }
                else
                {
                    Ext.Msg.confirm("Corfirmació de la operació", "Realment dessitga esborrar aquest registre?", function(button, text)
                    {
                        if (button == "yes")
                        {
                            ref.storeAutores.remove(registroSeleccionado);
                        }
                    });
                }
            }
        });
    },

    buildGridAutores : function()
    {
        var ref = this;

        var colModel = new Ext.grid.ColumnModel(
        {
            defaults :
            {
                sortable : true
            },
            columns : [
            {
                header : "id",
                width : 25,
                hidden : false,
                dataIndex : 'id'
            },
            {
                header : 'Nom',
                editor : new Ext.form.TextField(
                {
                    allowBlank : false,
                    blankText : 'El nom és obligatori'
                }),
                dataIndex : 'nombre'

            },
            {
                header : 'Cognom 1',
                editor : new Ext.form.TextField(
                {
                    allowBlank : false,
                    blankText : 'El nom és obligatori'
                }),
                dataIndex : 'apellido1'
            },
            {
                header : 'Cognom 2',
                editor : new Ext.form.TextField(
                {
                    allowBlank : true

                }),
                dataIndex : 'apellido2'
            },
            {
                header : 'Adreça correu',
                editor : new Ext.form.TextField(
                {
                    allowBlank : false,
                    blankText : 'El nom és obligatori',
                    vtype : 'email',
                    emailText : 'La direcció de e-mail no és correcta',
                    validator : function(value)
                    {
                        if (value && (value.indexOf("@uji.es") != -1 || value.indexOf(".uji.es") != -1))
                        {
                            return 'Error! L\'e-mail de una persona externa no pot ser del domini uji.';
                        }
                        else
                        {
                            return true;
                        }
                    }
                }),
                dataIndex : 'email'
            },
            {
                header : 'UJI',
                editor : false,
                renderer : function(val, meta, record)
                {
                    if (val !== "")
                    {
                        return 'Sí';
                    }
                    else
                    {
                        return 'No';
                    }
                },
                dataIndex : 'personaId'
            } ]
        });

        this.gridAutores = new Ext.grid.GridPanel(
        {
            frame : true,
            flex : 1,
            loadMask : true,
            plugins : [ this.editor ],
            tbar :
            {
                items : [ ref.botonAnadirUJI, ref.botonAnadirExtern, ref.botonBorrar ]
            },
            viewConfig :
            {
                forceFit : true
            },
            store : ref.storeAutores,
            sm : new Ext.grid.RowSelectionModel(
            {
                singleSelect : true
            }),
            colModel : colModel
        });

    }
});