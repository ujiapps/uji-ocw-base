Ext.ns('UJI.OCW');

UJI.OCW.Enlaces = Ext.extend(Ext.Panel,
{
	title : "Enllaços d'interés",
	closable : true,
	layout : 'vbox',
	layoutConfig :
	{
		align : 'stretch'
	},
	
	storeEnlaces : {},
	gridEnlaces : {},
	selectionModel : {},
    botonInsert : {},
    botonEdit : {},
    botonDelete : {},
    botonGuardar : {},
    botonCerrar : {},
    ventanaEmergente : {},
    formEnlaces : {},
    
    initComponent : function()
    {
    	var config = {};
        Ext.apply(this, Ext.apply(this.initialConfig, config));

        UJI.OCW.Enlaces.superclass.initComponent.call(this);
        
        this.storeEnlaces = this.getStoreEnlaces();
        this.ventanaEmergente = this.getVentanaEmergente();
        this.botonInsert = this.getBotonInsert();
        this.botonEdit = this.getBotonEdit();
        this.botonDelete = this.getBotonDelete();

        this.selectionModel = new Ext.grid.RowSelectionModel(
        {
            singleSelect : true
        });

        this.gridEnlaces = this.getGridEnlaces();

        this.add(this.gridEnlaces);
    },
    
    getStoreEnlaces : function()
    {
    	var store = new Ext.data.Store(
    	{
    		restful : true,
    		url : '/ocw/rest/enlace',
    		autoLoad : true,
    		reader : new Ext.data.XmlReader(
    		{
    			record : 'Enlace',
    			id : 'id'
    		}, buildMultilangStoreColumns('id', 'ocw', 'orden', multilang('descripcion'), multilang('nombre'), multilang('url'))),
    		writer : new Ext.data.XmlWriter(
    		{
    			xmlEncoding : 'UTF-8',
    			writeAllFields : true
    		}),
    		listeners :
    		{
    			save : function(store, batch, data)
                {
                    if (data.destroy !== null)
                    {
                        this.reload();
                    }
                }
    		}
    	});
    	
    	return store;
    },
    
    getVentanaEmergente : function()
    {
        this.botonGuardar = this.getBotonGuardar();
        this.botonCerrar = this.getBotonCerrar();
        this.formEnlaces = this.getFormEnlaces();
        this.formEnlaces.addButton(this.botonGuardar);
        this.formEnlaces.addButton(this.botonCerrar);

        var ventanaEmergente = new Ext.Window(
        {
            title : "Enllaç d'interés",
            layout : 'fit',
            modal : true,
            width : 510,
            height : 290,
            closable : true,
            items : new Ext.Panel(
            {
                layout : 'vbox',
                layoutConfig :
                {
                    align : 'stretch'
                },
                items : [ this.formEnlaces ]
            })
        });

        return ventanaEmergente;
    },
    
    getGridEnlaces : function()
    {
    	var ref = this;
    	
    	var userColumns = [
    	{
    		header : 'Id',
            dataIndex : 'id',
            sortable : true,
            width : 50
    	},
    	{
    		header : 'Ordre',
            dataIndex : 'orden',
            sortable : true,
            width : 50
    	},
    	{
    		header : 'Url',
    		dataIndex : 'url__' + idiomas[0]
    	},
    	{
    		header : 'Nombre',
    		dataIndex : 'nombre__' + idiomas[0]
    	},
    	{
    		header : 'OCW',
    		dataIndex : 'ocw',
    		width : 30
    	} ];
    	
    	var grid = new Ext.grid.GridPanel(
    	{
    		frame : true,
    	    flex : 1,
    	    loadMask : true,
    	    viewConfig :
    	    {
    	    	forceFit : true
    	    },
    	    store : this.storeEnlaces,
    	    sm : new Ext.grid.RowSelectionModel(
    	    {
    	    	singleSelect : true
    	    }),
    	    columns : userColumns,
    	    tbar :
    	    {
    	    	items : [ this.botonInsert, '-', this.botonEdit, '-', this.botonDelete ]
    	    },
    	    listeners :
    	    {
    	    	rowclick : function(grid, rowIndex, evt)
    	    	{
    	    		if (grid.selModel.selections.keys.length > 0)
    	            {
    	    			if (!grid.selModel.getSelected().dirty)
    	                {
    	    				ref.botonEdit.enable();
    	                    ref.botonDelete.enable();
    	                }
    	            }
    	        },
    	        rowdblclick : function(grid, rowIndex, event)
    	        {
    	        	ref.abreVentanaEmergente();
    	        }
    	    }
    	});
    	
    	return grid;
    },
    
    getBotonDelete : function()
    {
        var ref = this;

        var botonDelete = new Ext.Button(
        {
            text : 'Esborrar',
            iconCls : 'application-delete',
            disabled : true,
            handler : function(btn, evt)
            {
                var rec = ref.gridEnlaces.getSelectionModel().getSelected();

                if (!rec)
                {
                    return false;
                }

                Ext.Msg.confirm("Esborrar Enllaç", "Desitja esborrar l'enllaç?", function(btn)
                {
                    if (btn == 'yes')
                    {
                        ref.storeEnlaces.remove(rec);
                    }
                });

            }
        });

        return botonDelete;
    },

    getBotonInsert : function()
    {
        var ref = this;

        var botonInsert = new Ext.Button(
        {
            text : 'Afegir',
            iconCls : 'application-add',
            listeners :
            {
                click : function(btn, evt)
                {
                    ref.ventanaEmergente = ref.getVentanaEmergente();

                    ref.botonGuardar.enable();

                    ref.formEnlaces.getForm().setValues(
                    {
                        id : ''
                    });

                    ref.ventanaEmergente.show();
                }
            }
        });

        return botonInsert;
    },

    getBotonEdit : function()
    {
        var ref = this;

        var botonEdit = new Ext.Button(
        {
            text : 'Editar',
            iconCls : 'application-edit',
            disabled : true,
            handler : function()
            {
                if (ref.gridEnlaces.getSelectionModel().getSelections()[0])
                {
                    ref.abreVentanaEmergente();

                }
                else
                {
                    Ext.Msg.show(
                    {
                        title : "Triar enllaç",
                        icon : Ext.MessageBox.WARNING,
                        msg : "Deus triar un enllaç"
                    });
                    return false;
                }
            }
        });

        return botonEdit;
    },
    
    getBotonCerrar : function()
    {
        var ref = this;

        var boton = new Ext.Button(
        {
            text : 'Tancar',
            name : 'cerrar',
            width : '50',
            height : '20',
            pressed : false,
            listeners :
            {
                click : function(btn, evt)
                {
                    ref.ventanaEmergente.close();
                }
            }
        });

        return boton;
    },
    
    getBotonGuardar : function()
    {
        var ref = this;

        var boton = new Ext.Button(
        {
            text : 'Guardar',
            name : 'guardar',
            width : '50',
            height : '20',
            pressed : false,
            disabled : true,
            listeners :
            {
                click : function(btn, evt)
                {
                    if (ref.formEnlaces.getForm().isValid())
                    {
                        if ((ref.formEnlaces.getForm().getValues().id))
                        {
                            ref.formEnlaces.getForm().submit(
                            {
                                method : 'PUT',
                                url : '/ocw/rest/enlace/' + ref.formEnlaces.getForm().getValues().id,
                                success : function(form, action)
                                {
                                    ref.gridEnlaces.store.proxy.conn.url = '/' + 'ocw/rest/enlace';
                                    ref.gridEnlaces.store.reload();
                                    ref.ventanaEmergente.close();
                                },
                                failure : function(form, action)
                                {
                                    if (action.response.responseXML)
                                    {
                                        var msgList = action.response.responseXML.getElementsByTagName("msg");

                                        if (msgList && msgList[0] && msgList[0].firstChild)
                                        {
                                            Ext.Msg.alert("Error", msgList[0].firstChild.nodeValue);
                                        }
                                    }
                                    else
                                    {
                                        Ext.Msg.alert('Error', 'Error en l\'actualització');
                                    }
                                }
                            });
                        }
                        else
                        {
                            ref.formEnlaces.getForm().submit(
                            {
                                method : 'POST',
                                url : '/ocw/rest/enlace',
                                success : function(form, action)
                                {
                                    ref.gridEnlaces.store.proxy.conn.url = '/ocw/rest/enlace';
                                    ref.gridEnlaces.store.reload();
                                    ref.ventanaEmergente.close();
                                },
                                failure : function(form, action)
                                {
                                    if (action.response.responseXML)
                                    {
                                        var msgList = action.response.responseXML.getElementsByTagName("msg");

                                        if (msgList && msgList[0] && msgList[0].firstChild)
                                        {
                                            Ext.Msg.alert("Error", msgList[0].firstChild.nodeValue);
                                        }
                                    }
                                    else
                                    {
                                        Ext.Msg.alert('Error', 'Error en la inserció');
                                    }
                                }
                            });
                        }
                    }
                    else
                    {
                        Ext.Msg.alert("Error", "Revise els valors del camps.");
                    }
                }
            }
        });

        return boton;
    },
    
    getFormEnlaces : function()
    {
        var ref = this;

        var formEnlaces = new Ext.FormPanel(
        {
            autoScroll : true,
            frame : true,
            flex : 1,
            name : 'enlaces',
            url : '/ocw/rest/enlace',
            reader : new Ext.data.XmlReader(
            {
                record : 'Enlace',
                id : 'id'
            }, buildMultilangStoreColumns('id', 'ocw', 'orden', multilang('descripcion'), multilang('nombre'), multilang('url'))),
            writer : new Ext.data.XmlWriter(
            {
                record : 'Enlace',
                writeAllFields : true,
                id : 'id'
            }, buildMultilangStoreColumns('id', 'ocw', 'orden', multilang('descripcion'), multilang('nombre'), multilang('url'))),
            errorReader : new Ext.data.XmlReader(
            {
                record : 'responseMessage',
                success : 'success'
            }, [ 'success', 'msg' ]),

            items : [
            {
                xtype : 'hidden',
                name : 'id'
            },
            {
                xtype : 'multilanguagetextfield',
                fieldLabel : 'Url',
                width : 375,
                name : 'url',
                allowBlank : false,
                vtype : 'url',
                language : idiomas
            },
            {
                xtype : 'multilanguagetextfield',
                fieldLabel : 'Nom',
                width : 375,
                name : 'nombre',
                allowBlank : false,
                language : idiomas
            },
            {
            	xtype : 'multilanguagetextarea',
                fieldLabel : 'Descripció',
                width : 335,
                height : 100,
                allowBlank : false,
                name : 'descripcion',
                language : idiomas
            },
            {
            	xtype : 'textfield',
            	fieldLabel : 'Ordre',
            	width : 70,
                name : 'orden',
                vtype : 'orden',
                allowBlank : false
            },
            {
            	boxLabel : 'OCW',
                xtype : 'checkbox',
                name : 'ocw'
            } ],
            buttonAlign : 'right',
            style :
            {
                overflow : 'auto'
            }
        });

        return formEnlaces;
    },
    
    abreVentanaEmergente : function()
    {
        this.ventanaEmergente = this.getVentanaEmergente();
        this.ventanaEmergente.show();
        this.cargaEnlaces();
    },
    
    cargaEnlaces : function()
    {
        var ref = this;

        var id = this.gridEnlaces.selModel.getSelected().id;

        if (!this.gridEnlaces.selModel.getSelected().dirty)
        {
            var f = this.formEnlaces.getForm();
            f.load(
            {
                url : '/ocw/rest/enlace/' + id,
                method : 'get',
                success : function(form, action)
                {
                    ref.botonGuardar.enable();
                    
                    if (action.result.data.ocw == 'Si')
                    {
                    	ref.formEnlaces.getForm().setValues(
                    	{
                    		ocw : true
                    	});
                    }
                },
                failure : function(form, action)
                {
                    Ext.Msg.alert("Error", "Error al carregar les dades.");
                }
            });
        }

    }
    
});