Ext.ns('UJI.OCW');

UJI.OCW.Tags = Ext.extend(Ext.Panel,
{
    title : 'Tags',
    closable : true,
    layout : 'vbox',
    layoutConfig :
    {
        align : 'stretch'
    },

    storeTags : {},
    gridTags : {},

    initComponent : function()
    {
        var config = {};
        Ext.apply(this, Ext.apply(this.initialConfig, config));

        UJI.OCW.Tags.superclass.initComponent.call(this);

        this.storeTags = this.getStoreTags();
        this.gridTags = this.getGridTags();

        this.add(this.gridTags);
    },

    getStoreTags : function()
    {
        var store = new Ext.data.Store(
        {
            restful : true,
            url : '/ocw/rest/tag',
            autoLoad : true,
            reader : new Ext.data.XmlReader(
            {
                record : 'Tag',
                id : 'id'
            }, [ 'id', 'nombre' ]),
            writer : new Ext.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            })
        });
        return store;
    },

    getGridTags : function()
    {

        var userColumns = [
        {
            header : "id",
            width : 25,
            hidden : true,
            dataIndex : 'id'
        },
        {
            header : 'Nom',
            sortable : true,
            dataIndex : 'nombre',
            editor : new Ext.form.TextField(
            {
                allowBlank : false,
                blankText : 'El camp "Nom" es requerit'
            })
        } ];

        var grid = new Ext.ux.uji.grid.GridPanel(
        {

            frame : true,
            flex : 1,
            loadMask : true,
            viewConfig :
            {
                forceFit : true
            },
            store : this.storeTags,
            sm : new Ext.grid.RowSelectionModel(
            {
                singleSelect : true
            }),
            columns : userColumns
        });

        return grid;
    }

});