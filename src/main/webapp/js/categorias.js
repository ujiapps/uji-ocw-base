Ext.ns('UJI.OCW');

UJI.OCW.Categorias = Ext.extend(Ext.Panel,
{
    title : 'Categories',
    closable : true,
    layout : 'vbox',
    layoutConfig :
    {
        align : 'stretch'
    },

    storeCategorias : {},
    gridCategorias : {},
    botonInsert : {},
    botonDelete : {},
    editor : {},
    initComponent : function()
    {
        var config = {};
        Ext.apply(this, Ext.apply(this.initialConfig, config));

        UJI.OCW.Categorias.superclass.initComponent.call(this);

        this.storeCategorias = this.getStoreCategorias();
        this.editor = this.getEditor();
        this.botonInsert = this.getBotonInsert();
        this.botonDelete = this.getBotonDelete();
        this.gridCategorias = this.getGridCategorias();

        this.add(this.gridCategorias);

    },

    getStoreCategorias : function()
    {
        var store = new Ext.data.Store(
        {
            restful : true,
            url : '/ocw/rest/categoria',
            autoLoad : true,
            reader : new Ext.data.XmlReader(
            {
                record : 'Categoria',
                id : 'id'
            }, buildMultilangStoreColumns('id', 'orden', multilang('nombre'))),
            writer : new Ext.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            })
        });

        return store;
    },

    getEditor : function()
    {
        var ref = this;
        var editor = new Ext.ux.uji.grid.RowEditor(
        {
            saveText : 'Actualitzar',
            cancelText : 'Cancelar',
            errorSummary : false,
            listeners :
            {
                canceledit : function(rowEditor)
                {
                    var record = rowEditor.record;

                    if (record.phantom)
                    {
                        ref.storeCategorias.remove(rowEditor.record);
                    }
                }
            }
        });
        return editor;
    },

    getGridCategorias : function()
    {
        var userColumns = [
        {
            header : "id",
            width : 25,
            sortable : true,
            dataIndex : 'id'
        },
        {
            header : 'Ordre',
            dataIndex : 'orden',
            width : 25,
            editor : new Ext.form.TextField(
            {
                vtype : 'orden',
                allowBlank : false,
                blankText : 'El camp "Ordre" es requerit'
            })
        },
        {
            header : 'Nom',
            dataIndex : 'nombre__ca',
            editor :
            {
                xtype : 'multilanguagetextfield',
                name : 'nombre',
                language : idiomas,
                roweditor : this.editor
            }
        } ];

        var grid = new Ext.grid.GridPanel(
        {

            frame : true,
            flex : 1,
            loadMask : true,
            viewConfig :
            {
                forceFit : true
            },
            store : this.storeCategorias,
            sm : new Ext.grid.RowSelectionModel(
            {
                singleSelect : true
            }),
            plugins : [ this.editor ],
            columns : userColumns,
            tbar :
            {
                items : [ this.botonInsert, '-', this.botonDelete ]
            }
        });

        return grid;
    },

    getBotonDelete : function()
    {
        var ref = this;

        var botonDelete = new Ext.Button(
        {
            text : 'Esborrar',
            iconCls : 'application-delete',
            handler : function(btn, evt)
            {
                var registroSeleccionado = ref.gridCategorias.getSelectionModel().getSelected();
                if (!registroSeleccionado)
                {
                    return false;
                }
                else
                {
                    Ext.Msg.confirm("Corfirmació de la operació", "Realment dessitga esborrar aquest registre?", function(button, text)
                    {
                        if (button == "yes")
                        {
                            ref.storeCategorias.remove(registroSeleccionado);
                        }
                    });
                }
            }
        });

        return botonDelete;
    },

    getBotonInsert : function()
    {
        var ref = this;

        var botonInsert = new Ext.Button(
        {
            text : 'Afegir',
            iconCls : 'application-add',
            handler : function(btn, evt)
            {
                var rec = new ref.storeCategorias.recordType(
                {
                    id : '',
                    nombreCa : '',
                    nombreEs : '',
                    nombreUk : ''
                });

                ref.editor.stopEditing();
                ref.storeCategorias.insert(0, rec);
                ref.editor.startEditing(0);
            }
        });

        return botonInsert;
    }

});