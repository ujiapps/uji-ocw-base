<%@page contentType="text/html; charset=UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta content="text/html; charset=utf-8">

<link rel="stylesheet" type="text/css" href="http://devel.uji.es/resources/css/icons.css" />
<link rel="stylesheet" type="text/css" href="http://devel.uji.es/resources/js/ext-3.4.0/resources/css/ext-all.css" />
<link rel="stylesheet" type="text/css" href="http://devel.uji.es/resources/js/ext-3.4.0/examples/ux/css/RowEditor.css" />

<script type="text/javascript" src="http://devel.uji.es/resources/js/ext-3.4.0/adapter/ext/ext-base.js"></script>
<script type="text/javascript" src="http://devel.uji.es/resources/js/ext-3.4.0/ext-all-debug.js"></script>
<script type="text/javascript" src="http://devel.uji.es/resources/js/ext-3.4.0/examples/ux/RowEditor.js"></script>
<script type="text/javascript"
	src="http://localhost/resources/js/uji-commons-extjs/Ext/ux/uji/form/LookupWindow/0.0.1/LookupWindow.js"></script>
<script type="text/javascript" src="js/autores.js"></script>

<title>Autores OCW</title>

<script type="text/javascript">
    Ext.BLANK_IMAGE_URL = 'http://devel.uji.es/resources/js/ext-3.4.0/resources/images/default/s.gif';

    Ext.onReady(function()
    {
        Ext.QuickTips.init();

        var panel = new UJI.OCW.Autores();

        new Ext.Viewport(
        {
            layout : 'fit',
            items : [ panel ]
        });
    });
</script>

</head>

<body>
</body>
</html>