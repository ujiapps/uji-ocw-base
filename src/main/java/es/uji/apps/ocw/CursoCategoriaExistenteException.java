package es.uji.apps.ocw;

@SuppressWarnings("serial")
public class CursoCategoriaExistenteException extends GeneralOCWException
{
    public CursoCategoriaExistenteException(String msg)
    {
        super(msg);
    }

    public CursoCategoriaExistenteException()
    {
        super();
    }
}
