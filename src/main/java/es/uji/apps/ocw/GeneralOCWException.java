package es.uji.apps.ocw;

import es.uji.commons.rest.exceptions.CoreBaseException;

@SuppressWarnings("serial")
public class GeneralOCWException extends CoreBaseException
{
    public GeneralOCWException()
    {
        super("S'ha produït un error en l'operació");
    }
    
    public GeneralOCWException(String message)
    {
        super(message);
    }
}