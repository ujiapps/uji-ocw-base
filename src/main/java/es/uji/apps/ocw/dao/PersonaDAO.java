package es.uji.apps.ocw.dao;

import java.util.List;

import es.uji.apps.ocw.model.Persona;
import es.uji.commons.db.BaseDAO;
import es.uji.commons.db.LookupDAO;

public interface PersonaDAO extends BaseDAO, LookupDAO
{
    List<Persona> getPersonaById(Long personaId);

    List<Persona> getPersonas();
}
