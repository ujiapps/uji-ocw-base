package es.uji.apps.ocw.dao;

import java.util.List;

import es.uji.apps.ocw.model.MaterialAutor;
import es.uji.commons.db.BaseDAO;

public interface MaterialAutorDAO extends BaseDAO
{
    public List<MaterialAutor> getMaterialAutor();

    public List<MaterialAutor> getMaterialesAutoresByAutor(Long autorId);
}
