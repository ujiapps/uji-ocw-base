package es.uji.apps.ocw.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.ocw.model.QTagCuenta;
import es.uji.apps.ocw.model.TagCuenta;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class TagCuentaDAODatabaseImpl extends BaseDAODatabaseImpl implements TagCuentaDAO
{
    @Override
    public List<TagCuenta> getTagsCuenta()
    {
        JPAQuery query = new JPAQuery(entityManager);

        QTagCuenta qTagCuenta = QTagCuenta.tagCuenta;
        
        query.from(qTagCuenta).orderBy(qTagCuenta.total.desc());

        return query.list(qTagCuenta);
    }

}
