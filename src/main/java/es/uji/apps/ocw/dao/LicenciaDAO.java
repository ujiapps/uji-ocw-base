package es.uji.apps.ocw.dao;

import java.util.List;

import es.uji.apps.ocw.model.Licencia;
import es.uji.commons.db.BaseDAO;

public interface LicenciaDAO extends BaseDAO
{
    List<Licencia> getLicencias();

    Licencia getLicencia(Long licenciaId);

    public Licencia updateLicencia(Licencia licencia);

    public Long getNumMaterialesConLicencia(Long licenciaId);
}
