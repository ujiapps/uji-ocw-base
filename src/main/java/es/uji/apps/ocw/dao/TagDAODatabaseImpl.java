package es.uji.apps.ocw.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.ocw.model.QCursoTag;
import es.uji.apps.ocw.model.QMaterialTag;
import es.uji.apps.ocw.model.QTag;
import es.uji.apps.ocw.model.Tag;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class TagDAODatabaseImpl extends BaseDAODatabaseImpl implements TagDAO
{
    @Override
    public List<Tag> getTags()
    {
        JPAQuery query = new JPAQuery(entityManager);
        QTag tag = QTag.tag;

        query.from(tag).orderBy(tag.nombre.asc());

        return query.list(tag);

    }

    @Override
    public List<Tag> getTagByName(String tagString)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QTag tag = QTag.tag;

        query.from(tag).where(tag.nombre.eq(tagString)).orderBy(tag.nombre.asc());

        return query.list(tag);
    }

    @Override
    public List<Tag> getTagsByMaterial(Long materialId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QMaterialTag materialTag = QMaterialTag.materialTag;
        QTag tag = QTag.tag;

        query.from(tag, materialTag).join(materialTag.tag, tag)
                .where(materialTag.material.id.eq(materialId)).orderBy(tag.nombre.asc());

        return query.list(tag);
    }

    @Override
    public Long getTagsByNumeroMateriales(Long tagId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QMaterialTag materialTag = QMaterialTag.materialTag;
        QTag tag = QTag.tag;

        query.from(tag, materialTag).join(materialTag.tag, tag).where(materialTag.tag.id.eq(tagId));

        return query.count();
    }

    @Override
    public Long getTagsByNumeroCursos(Long tagId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QCursoTag cursoTag = QCursoTag.cursoTag;
        QTag tag = QTag.tag;

        query.from(tag, cursoTag).join(cursoTag.tag, tag).where(cursoTag.tag.id.eq(tagId));

        return query.count();
    }
}