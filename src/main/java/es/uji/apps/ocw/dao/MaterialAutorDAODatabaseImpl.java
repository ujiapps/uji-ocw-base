package es.uji.apps.ocw.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.ocw.model.MaterialAutor;
import es.uji.apps.ocw.model.QMaterialAutor;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class MaterialAutorDAODatabaseImpl extends BaseDAODatabaseImpl implements MaterialAutorDAO
{
    @Override
    public List<MaterialAutor> getMaterialAutor()
    {
        JPAQuery query = new JPAQuery(entityManager);
        QMaterialAutor materialAutor = QMaterialAutor.materialAutor;

        query.from(materialAutor).orderBy(materialAutor.autor.apellido1.asc());

        return query.list(materialAutor);
    }

    @Override
    public List<MaterialAutor> getMaterialesAutoresByAutor(Long autorId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QMaterialAutor materialAutor = QMaterialAutor.materialAutor;

        query.from(materialAutor).where(materialAutor.autor.id.eq(autorId));

        return query.list(materialAutor);
    }
}
