package es.uji.apps.ocw.dao;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;

import es.uji.apps.ocw.model.CursoMaterial;
import es.uji.apps.ocw.model.Grupo;
import es.uji.apps.ocw.model.QCursoMaterial;
import es.uji.apps.ocw.model.QGrupo;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class GrupoDAODatabaseImpl extends BaseDAODatabaseImpl implements GrupoDAO
{

    @Override
    public List<Grupo> getGruposByCursoId(Long cursoId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QGrupo grupo = QGrupo.grupo;

        query.from(grupo).where(grupo.curso.id.eq(cursoId)).orderBy(grupo.orden.asc());

        return query.list(grupo);

    }

    @Override
    public List<CursoMaterial> getMaterialesByGrupoId(Long grupoId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QCursoMaterial cursoMaterial = QCursoMaterial.cursoMaterial;

        query.from(cursoMaterial).where(cursoMaterial.grupo.id.eq(grupoId))
                .orderBy(cursoMaterial.orden.asc());

        return query.list(cursoMaterial);
    }

    @Override
    @Transactional
    public void incrementaOrdenMaterialesCuyoOrdenMayorQue(Long cursoId, Long grupoId, Long orden,
            int incremento)
    {
        QCursoMaterial cursoMaterial = QCursoMaterial.cursoMaterial;

        JPAUpdateClause updateClause = new JPAUpdateClause(entityManager,
                cursoMaterial);
        updateClause
                .where(cursoMaterial.grupo.id.eq(grupoId).and(cursoMaterial.curso.id.eq(cursoId))
                        .and(cursoMaterial.orden.gt(orden)))
                .set(cursoMaterial.orden,
                        cursoMaterial.orden.add(incremento)).execute();
    }

    @Override
    @Transactional
    public void incrementaOrdenGruposCuyoOrdenMayorQue(Long cursoId, Long orden,
            int incremento)
    {
        QGrupo grupo = QGrupo.grupo;

        JPAUpdateClause updateClause = new JPAUpdateClause(entityManager,
                grupo);
        updateClause
                .where(grupo.curso.id.eq(cursoId)
                        .and(grupo.orden.gt(orden)))
                .set(grupo.orden,
                        grupo.orden.add(incremento)).execute();
        
    }

    @Override
    @Transactional
    public void incrementaOrdenMaterialesGrupoRaizCuyoOrdenMayorQue(Long cursoId, Long orden,
            int incremento)
    {
        QCursoMaterial cursoMaterial = QCursoMaterial.cursoMaterial;

        JPAUpdateClause updateClause = new JPAUpdateClause(entityManager,
                cursoMaterial);
        updateClause
                .where(cursoMaterial.grupo.id.isNull().and(cursoMaterial.curso.id.eq(cursoId))
                        .and(cursoMaterial.orden.gt(orden)))
                .set(cursoMaterial.orden,
                        cursoMaterial.orden.add(incremento)).execute();        
    }
}