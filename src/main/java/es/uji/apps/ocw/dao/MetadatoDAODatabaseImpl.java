package es.uji.apps.ocw.dao;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.ocw.model.MaterialMetadato;
import es.uji.apps.ocw.model.Metadato;
import es.uji.apps.ocw.model.MetadatoIdioma;
import es.uji.apps.ocw.model.MetadatoValor;
import es.uji.apps.ocw.model.QIdioma;
import es.uji.apps.ocw.model.QMaterialMetadato;
import es.uji.apps.ocw.model.QMetadato;
import es.uji.apps.ocw.model.QMetadatoIdioma;
import es.uji.apps.ocw.model.QMetadatoValor;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class MetadatoDAODatabaseImpl extends BaseDAODatabaseImpl implements MetadatoDAO
{
    @Override
    public List<Metadato> getMetadatos()
    {
        JPAQuery query = new JPAQuery(entityManager);

        QMetadato metadato = QMetadato.metadato;
        QMetadatoIdioma metadatoIdioma = QMetadatoIdioma.metadatoIdioma;
        QIdioma idioma = QIdioma.idioma;

        query.from(metadato).leftJoin(metadato.metadatosIdiomas, metadatoIdioma).fetch()
                .leftJoin(metadatoIdioma.idioma, idioma).orderBy(metadato.id.asc());

        return query.distinct().list(metadato);
    }

    @Override
    public List<MaterialMetadato> getMetadatosByMaterial(Long materialId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QMaterialMetadato materialMetadato = QMaterialMetadato.materialMetadato;
        query.from(materialMetadato).where(materialMetadato.material.id.eq(materialId));
        return query.list(materialMetadato);
    }

    @Override
    public Metadato getMetadato(Long metadatoId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QMetadato metadato = QMetadato.metadato;
        QMetadatoIdioma metadatoIdioma = QMetadatoIdioma.metadatoIdioma;
        QIdioma idioma = QIdioma.idioma;

        query.from(metadato).leftJoin(metadato.metadatosIdiomas, metadatoIdioma).fetch()
                .leftJoin(metadatoIdioma.idioma, idioma).where(metadato.id.eq(metadatoId));

        return query.uniqueResult(metadato);
    }

    @Override
    public Metadato updateMetadato(Metadato metadato)
    {
        Set<MetadatoIdioma> auxSet = new HashSet<MetadatoIdioma>();

        for (MetadatoIdioma metadatoIdioma : metadato.getMetadatosIdiomas())
        {
            if (metadatoIdioma.getNombre() != null && !metadatoIdioma.getNombre().equals(""))
            {
                auxSet.add(metadatoIdioma);
            }
            else
            {
                delete(MetadatoIdioma.class, metadatoIdioma.getId());
            }
        }

        metadato.setMetadatosIdiomas(auxSet);

        return update(metadato);
    }

    @Override
    public List<MetadatoValor> getMetadatoValores()
    {
        JPAQuery query = new JPAQuery(entityManager);
        QMetadatoValor metadatoValor = QMetadatoValor.metadatoValor;
        query.from(metadatoValor).orderBy(metadatoValor.orden.asc());
        return query.list(metadatoValor);
    }

    @Override
    public List<MetadatoValor> getMetadatoValoresByMetadatoId(Long metadatoId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QMetadatoValor metadatoValor = QMetadatoValor.metadatoValor;
        query.from(metadatoValor).where(metadatoValor.metadato.id.eq(metadatoId))
                .orderBy(metadatoValor.orden.asc());
        return query.list(metadatoValor);
    }

}
