package es.uji.apps.ocw.dao;

import java.util.List;

import es.uji.apps.ocw.model.CursoMaterialGrupo;
import es.uji.commons.db.BaseDAO;

public interface CursoMaterialGrupoDAO extends BaseDAO
{

    List<CursoMaterialGrupo> getElementosByCurso(Long cursoId);

}
