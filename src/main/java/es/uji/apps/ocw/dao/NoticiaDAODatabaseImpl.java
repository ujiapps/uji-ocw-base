package es.uji.apps.ocw.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import es.uji.apps.ocw.model.Noticia;

@Repository
public class NoticiaDAODatabaseImpl implements NoticiaDAO
{
    @Override
    public List<Noticia> getUltimasNoticias(String acronimoIdioma)
    {
        List<Noticia> listaNoticias = new ArrayList<Noticia>();

        Noticia noticia = new Noticia();
        noticia.setTitulo("Setmana Internacional de l’Accés Obert");
        noticia.setFecha("23/10/2012");
        noticia.setContenido("L'UJI se suma a la celebració de la sisena Setmana Internacional de l'Accés Obert (<a href=\"http://www.openaccessweek.org\">http://www.openaccessweek.org</a>)<br />Més informació: <a href=\"http://www.uji.es/CA/noticies/detall&amp;id_a=30342907\">http://www.uji.es/CA/noticies/detall&amp;id_a=30342907</a>");


        Noticia noticia2 = new Noticia();
        noticia2.setTitulo("Nova edició del Premi MECD-Universia a la iniciativa Open Course Ware");
        noticia2.setFecha("25/01/2013");
        noticia2.setContenido("Entre el 24 de Gener i el 14 de Febrer es poden presentar candidatures als premis «MECD-Universia a la iniciativa OCW» <a href=\"http://promociones.universia.es/microsites/premios/ocw/index.jsp\">http://promociones.universia.es/microsites/premios/ocw/index.jsp</a>");

        Noticia noticia3 = new Noticia();
        noticia3.setTitulo("Dia internacional de la cultura lliure");
        noticia3.setFecha("18/5/2013");
        noticia3.setContenido("El 18 de Maig es celebra a tot el planeta el Dia internacional de la cultura lliure <a href=\"http://www.culturefreedomday.org/index.php\">http://www.culturefreedomday.org/index.php</a>");

        Noticia noticia4 = new Noticia();
        noticia4.setTitulo("Convocatòria per a l’elaboració, publicació i posada en marxa de MOOCs");
        noticia4.setFecha("21/05/2013");
        noticia4.setContenido("En la adreça <a href=\"http://ujiapps.uji.es/perfils/pdipas/pdi/moocs/conv1314/\">http://ujiapps.uji.es/perfils/pdipas/pdi/moocs/conv1314/</a> s'han publicat les bases de la Convocatòria per a l’elaboració, publicació i posada en marxa de MOOCs (Cursos Massius Oberts en Línia) per al curs 2013/2014.");

        Noticia noticia5 = new Noticia();
        noticia5.setTitulo("Convocatòria per a l’elaboració i publicació de materials docents lliures per al curs 2013/14");
        noticia5.setFecha("06/06/2013");
        noticia5.setContenido("En la adreça <a href=\"http://ujiapps.uji.es/perfils/pdipas/pdi/materials-docents/1314/\">http://ujiapps.uji.es/perfils/pdipas/pdi/materials-docents/1314/</a> s'han publicat les bases de la Convocatòria per a l’elaboració i publicació de materials docents lliures per al curs 2013/14. La convocatòria finalitza el 24 de juny de 2013.");

        listaNoticias.add(noticia5);
        listaNoticias.add(noticia4);
        listaNoticias.add(noticia3);
        listaNoticias.add(noticia2);
        listaNoticias.add(noticia);

//        Noticia noticia2 = new Noticia();
//        noticia2.setTitulo("Versió Beta del portal Libertas");
//        noticia2.setFecha("11/09/2012");
//        noticia2.setContenido("Disponible la versió Beta del portal Libertas: <a href=\"http://libertas.uji.es\">http://libertas.uji.es</a>");
//
//        listaNoticias.add(noticia2);

        return listaNoticias;
    }

}
