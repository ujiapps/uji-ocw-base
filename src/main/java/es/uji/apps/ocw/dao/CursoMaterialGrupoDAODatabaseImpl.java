package es.uji.apps.ocw.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.ocw.model.CursoMaterialGrupo;
import es.uji.apps.ocw.model.QCursoMaterialGrupo;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class CursoMaterialGrupoDAODatabaseImpl extends BaseDAODatabaseImpl implements
        CursoMaterialGrupoDAO
{

    @Override
    public List<CursoMaterialGrupo> getElementosByCurso(Long cursoId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QCursoMaterialGrupo qCursoMaterialGrupo = QCursoMaterialGrupo.cursoMaterialGrupo;

        query.from(qCursoMaterialGrupo).where(qCursoMaterialGrupo.cursoId.eq(cursoId))
                .orderBy(qCursoMaterialGrupo.orden.asc());

        return query.list(qCursoMaterialGrupo);
    }

}