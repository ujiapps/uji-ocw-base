package es.uji.apps.ocw.dao;

import java.util.List;

import es.uji.apps.ocw.model.MaterialMetadato;
import es.uji.apps.ocw.model.Metadato;
import es.uji.apps.ocw.model.MetadatoValor;
import es.uji.commons.db.BaseDAO;

public interface MetadatoDAO extends BaseDAO
{
    List<Metadato> getMetadatos();

    List<MaterialMetadato> getMetadatosByMaterial(Long materialId);

    Metadato getMetadato(Long metadatoId);

    Metadato updateMetadato(Metadato metadato);
    
    List<MetadatoValor> getMetadatoValores();

    List<MetadatoValor> getMetadatoValoresByMetadatoId (Long metadatoId);
}
