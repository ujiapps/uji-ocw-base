package es.uji.apps.ocw.dao;

import java.util.List;

import es.uji.apps.ocw.model.TagCuenta;
import es.uji.commons.db.BaseDAO;

public interface TagCuentaDAO extends BaseDAO
{
    public List<TagCuenta> getTagsCuenta();

}
