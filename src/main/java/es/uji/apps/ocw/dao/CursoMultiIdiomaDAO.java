package es.uji.apps.ocw.dao;

import java.util.List;

import es.uji.apps.ocw.model.CursoMultiIdioma;
import es.uji.commons.db.BaseDAO;

public interface CursoMultiIdiomaDAO extends BaseDAO
{

    List<CursoMultiIdioma> getCursosByIdsAndIdioma(List<Long> listaCursosId, String acronimoIdioma);

    List<CursoMultiIdioma> getCursosByIdsAndIdiomaVistaPrevia(List<Long> listaCursosId, String acronimoIdioma);
    
}
