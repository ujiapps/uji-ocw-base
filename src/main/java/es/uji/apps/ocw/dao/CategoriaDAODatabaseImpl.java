package es.uji.apps.ocw.dao;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.ocw.model.Categoria;
import es.uji.apps.ocw.model.CategoriaIdioma;
import es.uji.apps.ocw.model.CategoriaIdiomaPrincipal;
import es.uji.apps.ocw.model.QCategoria;
import es.uji.apps.ocw.model.QCategoriaIdioma;
import es.uji.apps.ocw.model.QCategoriaIdiomaPrincipal;
import es.uji.apps.ocw.model.QCursoCategoria;
import es.uji.apps.ocw.model.QIdioma;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.model.lookup.LookupItem;

@Repository
public class CategoriaDAODatabaseImpl extends BaseDAODatabaseImpl implements CategoriaDAO
{
    @Override
    public List<Categoria> getCategorias()
    {
        JPAQuery query = new JPAQuery(entityManager);
        QCategoria categoria = QCategoria.categoria;
        QCategoriaIdioma categoriaIdioma = QCategoriaIdioma.categoriaIdioma;
        QIdioma idioma = QIdioma.idioma;

        query.from(categoria).join(categoria.categoriasIdiomas, categoriaIdioma).fetch()
                .join(categoriaIdioma.idioma, idioma).orderBy(categoria.orden.asc());

        return query.distinct().list(categoria);
    }

    @Override
    public List<CategoriaIdioma> getCategoriaIdiomaByCategoriaId(Long categoriaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QCategoriaIdioma categoriaIdioma = QCategoriaIdioma.categoriaIdioma;

        query.from(categoriaIdioma).where(categoriaIdioma.categoria.id.eq(categoriaId));

        return query.list(categoriaIdioma);
    }

    @Override
    public Categoria insertCategoria(Categoria categoria)
    {
        Set<CategoriaIdioma> categoriaIdiomasNew = new HashSet<CategoriaIdioma>();
        Set<CategoriaIdioma> categoriasIdiomas = categoria.getCategoriasIdiomas();
        categoria.setCategoriasIdiomas(null);

        insert(categoria);

        for (CategoriaIdioma categoriaIdioma : categoriasIdiomas)
        {
            categoriaIdioma.setCategoria(categoria);
            categoriaIdiomasNew.add(insert(categoriaIdioma));
        }

        categoria.setCategoriasIdiomas(categoriaIdiomasNew);
        return (categoria);

    }

    @Override
    public Categoria updateCategoria(Categoria categoria)
    {
        Set<CategoriaIdioma> categoriasIdiomas = categoria.getCategoriasIdiomas();

        update(categoria);

        for (CategoriaIdioma categoriaIdioma : categoriasIdiomas)
        {
            CategoriaIdioma categoriaIdiomaDb = get(CategoriaIdioma.class, categoriaIdioma.getId())
                    .get(0);

            categoriaIdiomaDb.setNombre(categoriaIdioma.getNombre());

            categoriaIdiomaDb.update();
        }
        return (categoria);

    }

    @Override
    public List<CategoriaIdiomaPrincipal> getCategoriasIdiomaPrincipal()
    {
        JPAQuery query = new JPAQuery(entityManager);
        QCategoriaIdiomaPrincipal categoria = QCategoriaIdiomaPrincipal.categoriaIdiomaPrincipal;

        query.from(categoria).orderBy(categoria.orden.asc());

        return query.distinct().list(categoria);

    }

    @Override
    public CategoriaIdiomaPrincipal getCategoriaIdiomaPrincipalById(Long categoriaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QCategoriaIdiomaPrincipal categoria = QCategoriaIdiomaPrincipal.categoriaIdiomaPrincipal;

        query.from(categoria).where(categoria.id.eq(categoriaId));

        return query.list(categoria).get(0);
    }

    @Override
    public List<LookupItem> search(String query)
    {
        JPAQuery queryCategorias = new JPAQuery(entityManager);
        QCategoriaIdiomaPrincipal qCategoriaIdiomaPrincipal = QCategoriaIdiomaPrincipal.categoriaIdiomaPrincipal;

        List<LookupItem> result = new ArrayList<LookupItem>();

        if (query != null && !query.isEmpty())
        {
            queryCategorias.from(qCategoriaIdiomaPrincipal).where(
                    qCategoriaIdiomaPrincipal.nombre.toLowerCase().like(
                            "%" + query.toLowerCase() + "%"));

            for (CategoriaIdiomaPrincipal categoriaIdiomaPrincipal : queryCategorias
                    .list(qCategoriaIdiomaPrincipal))
            {
                LookupItem lookupItem = new LookupItem();
                lookupItem.setId(String.valueOf(categoriaIdiomaPrincipal.getId()));
                lookupItem.setNombre(categoriaIdiomaPrincipal.getNombre());

                result.add(lookupItem);
            }
        }

        return result;
    }

    @Override
    public List<CategoriaIdiomaPrincipal> getCategoriasByCursoId(Long cursoId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QCategoriaIdiomaPrincipal categoria = QCategoriaIdiomaPrincipal.categoriaIdiomaPrincipal;
        QCursoCategoria cursoCategoria = QCursoCategoria.cursoCategoria;

        query.from(categoria, cursoCategoria).where(
                cursoCategoria.curso.id.eq(cursoId).and(
                        cursoCategoria.categoria.id.eq(categoria.id)));

        return query.list(categoria);
    }

    @Override
    public List<Categoria> getCategoriasByCursoIdAndIdioma(Long cursoId, String idioma)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QCategoria categoria = QCategoria.categoria;
        QCategoriaIdioma categoriaIdioma = QCategoriaIdioma.categoriaIdioma;
        QCursoCategoria cursoCategoria = QCursoCategoria.cursoCategoria;

        query.from(categoria)
                .join(categoria.categoriasIdiomas, categoriaIdioma)
                .fetch()
                .join(categoria.cursosCategorias, cursoCategoria)
                .where(categoriaIdioma.idioma.acronimo.eq(idioma).and(
                        cursoCategoria.curso.id.eq(cursoId)));

        return query.list(categoria);
    }

    @Override
    public List<Categoria> getCategoriasByIdioma(String acronimoIdioma)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QCategoria qCategoria = QCategoria.categoria;
        QCategoriaIdioma categoriaIdioma = QCategoriaIdioma.categoriaIdioma;

        query.from(qCategoria).join(qCategoria.categoriasIdiomas, categoriaIdioma).fetch()
                .where(categoriaIdioma.idioma.acronimo.eq(acronimoIdioma))
                .orderBy(qCategoria.orden.asc());

        return query.list(qCategoria);
    }

    @Override
    public Categoria getCategoriaByIdAndIdioma(Long categoriaId, String acronimoIdioma)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QCategoria categoria = QCategoria.categoria;
        QCategoriaIdioma categoriaIdioma = QCategoriaIdioma.categoriaIdioma;

        query.from(categoria)
                .join(categoria.categoriasIdiomas, categoriaIdioma)
                .fetch()
                .where(categoriaIdioma.idioma.acronimo.eq(acronimoIdioma).and(
                        categoria.id.eq(categoriaId))).orderBy(categoria.orden.asc());

        if (query.list(categoria).size() > 0) {
            return query.list(categoria).get(0);
        } else {
            return new Categoria();
        }
    }

}
