package es.uji.apps.ocw.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.ocw.model.Asignatura;
import es.uji.apps.ocw.model.Autor;
import es.uji.apps.ocw.model.AutorCompleto;
import es.uji.apps.ocw.model.QAsignatura;
import es.uji.apps.ocw.model.QAutorCompleto;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.model.lookup.LookupItem;

@Repository
public class AsignaturaDAODatabaseImpl extends BaseDAODatabaseImpl implements AsignaturaDAO
{
    @Override
    public List<Asignatura> getAsignaturas()
    {
        JPAQuery query = new JPAQuery(entityManager);

        QAsignatura asignatura = QAsignatura.asignatura;

        query.from(asignatura).orderBy(asignatura.nombreCa.asc());

        return query.list(asignatura);
    }

    @Override
    public List<LookupItem> search(String cadena)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QAsignatura asignatura = QAsignatura.asignatura;

        List<LookupItem> result = new ArrayList<LookupItem>();

        if (cadena != null && !cadena.isEmpty())
        {
            query.from(asignatura)
                    .where(asignatura.id.concat(" ").concat(asignatura.nombreCa).concat(" ")
                            .concat(asignatura.nombreEs).concat(" ").concat(asignatura.nombreUk)
                            .concat(" ").lower().like("%" + cadena.toLowerCase() + "%"))
                    .orderBy(asignatura.id.asc());

            List<Asignatura> listaAsignaturas = query.list(asignatura);

            for (Asignatura asignaturaEncontrada : listaAsignaturas)
            {
                LookupItem lookupItem = new LookupItem();
                lookupItem.setId(String.valueOf(asignaturaEncontrada.getId()));
                lookupItem.setNombre(asignaturaEncontrada.getNombreCa());

                result.add(lookupItem);
            }
        }

        return result;
    }

}
