package es.uji.apps.ocw.dao;

import java.util.List;

import es.uji.apps.ocw.model.Autor;
import es.uji.apps.ocw.model.AutorCompleto;
import es.uji.commons.db.BaseDAO;
import es.uji.commons.db.LookupDAO;

public interface AutorDAO extends BaseDAO, LookupDAO
{
    Long getNumMaterialesAutor(Long autorId);
   
    List<Autor> getAutores();

    List<AutorCompleto> getAutorCompleto(Long autorId);    
}