package es.uji.apps.ocw.dao;

import es.uji.commons.db.BaseDAO;

public interface CursoMaterialDAO extends BaseDAO
{
    Long getMaxOrdenCursoMaterialByGrupo(Long grupoId);
}
