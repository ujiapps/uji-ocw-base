package es.uji.apps.ocw.dao;

import java.util.List;
import java.util.Map;

import es.uji.apps.ocw.model.Asignatura;
import es.uji.apps.ocw.model.Categoria;
import es.uji.apps.ocw.model.Curso;
import es.uji.apps.ocw.model.CursoAsignatura;
import es.uji.apps.ocw.model.CursoAutorizado;
import es.uji.apps.ocw.model.CursoCategoria;
import es.uji.apps.ocw.model.CursoIdioma;
import es.uji.apps.ocw.model.CursoIdiomaPrincipal;
import es.uji.apps.ocw.model.CursoImagen;
import es.uji.apps.ocw.model.CursoMaterial;
import es.uji.apps.ocw.model.CursoTag;
import es.uji.commons.db.BaseDAO;

public interface CursoDAO extends BaseDAO
{
    String getNombrePersona(Long personaId);

    List<Curso> getCursosByCategoriaId(Long categoriaId);

    List<CursoIdiomaPrincipal> getCursosConFiltros(Map<String, String> filtro);

    List<CursoMaterial> getMateriales(Long cursoId);

    void deleteAsignaturaById(Long cursoAsignaturaId);

    List<CursoAsignatura> getAsignaturas(Long cursoId);

    Asignatura getAsignaturaById(String asignaturaId);

    List<CursoAsignatura> getAsignaturasByCodigo(Long cursoId, String asignaturaId);

    List<CursoIdioma> getCursosIdiomas(Long cursoId);

    List<CursoAutorizado> getAutorizados(Long cursoId);

    boolean isAutorizadoByCurso(Long cursoId, Long personaId);

    void deleteAutorizadoById(Long cursoAutorizadoId);

    List<CursoTag> getTags(Long cursoId);

    boolean isPropietario(Long cursoId, Long personaId);

    boolean estaAutorizado(Long cursoId, Long personaId);

    CursoIdiomaPrincipal getCursoIdiomaPrincipal(Long cursoId);

    List<CursoImagen> getImagenesByCursoId(Long cursoId);

    void deleteCursoCategoriaById(Long id);

    List<CursoCategoria> getCursoCategoriasByCursoId(Long cursoId);

    boolean existeCursoCategoria(Long cursoId, Long categoriaId);

    List<CursoMaterial> getMaterialesSinGrupo(Long cursoId);

    List<CursoImagen> getImagenPrincipal(Long cursoId);

    List<Categoria> getCategorias(Long cursoId);

    List<CursoIdioma> getTodosCursosIdiomas(Long cursoId);

    List<CursoCategoria> getCursosCategoriasByCursosIds(List<Long> listaCursosIds);

    List<CursoTag> getEtiquetasFromCursosIds(List<Long> listaCursosIds);

    List<CursoImagen> getImagenFromCursosIds(List<Long> listaCursosIds);

    List<Curso> getCursosPaginadosByCategoriaId(Long categoriaId, Integer numPagina,
            int numCursosPorPagina);

    List<Curso> getTodosLosCursosPaginados(Integer numPagina, int numCursosPorPagina);

    List<Curso> getUltimosCursos(int numMaxCursosAVisualizar);

    List<Curso> getCursosMasVisitados(int numMaxCursosAVisualizar);

    Integer getNumeroTotalPaginas(int numCursosPorPagina);

    Integer getNumeroTotalPaginasByCategoriaId(Long categoriaId, int numCursosPorPagina);
    
    List<Curso> getCursosByMaterialId(Long materialId);

    List<Curso> getCursosPaginadosBusquedaSimple(String cadena, int pagina, int numCursosPorPagina);

    Integer getNumeroTotalPaginasBusquedaSimple(String cadena, int numCursosPorPagina);
    
    List<Curso> getCursosPaginadosByTagId(Long tagId, Integer numPagina, int numCursosPorPagina);
    
    Integer getNumeroTotalPaginasByTagId(Long tagId, int numCursosPorPagina);

}
