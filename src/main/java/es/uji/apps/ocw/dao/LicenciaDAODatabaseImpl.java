package es.uji.apps.ocw.dao;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.ocw.model.Licencia;
import es.uji.apps.ocw.model.LicenciaIdioma;
import es.uji.apps.ocw.model.QIdioma;
import es.uji.apps.ocw.model.QLicencia;
import es.uji.apps.ocw.model.QLicenciaIdioma;
import es.uji.apps.ocw.model.QMaterial;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class LicenciaDAODatabaseImpl extends BaseDAODatabaseImpl implements LicenciaDAO
{
    @Override
    public List<Licencia> getLicencias()
    {
        JPAQuery query = new JPAQuery(entityManager);

        QLicencia licencia = QLicencia.licencia;
        QLicenciaIdioma licenciaIdioma = QLicenciaIdioma.licenciaIdioma;
        QIdioma idioma = QIdioma.idioma;

        query.from(licencia).leftJoin(licencia.licenciasIdiomas, licenciaIdioma).fetch()
                .leftJoin(licenciaIdioma.idioma, idioma);

        return query.distinct().list(licencia);
    }

    @Override
    public Licencia getLicencia(Long licenciaId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QLicencia licencia = QLicencia.licencia;
        QLicenciaIdioma licenciaIdioma = QLicenciaIdioma.licenciaIdioma;
        QIdioma idioma = QIdioma.idioma;

        query.from(licencia).leftJoin(licencia.licenciasIdiomas, licenciaIdioma).fetch()
                .leftJoin(licenciaIdioma.idioma, idioma).where(licencia.id.eq(licenciaId));

        return query.uniqueResult(licencia);
    }

    @Override
    public Licencia updateLicencia(Licencia licencia)
    {
        Set<LicenciaIdioma> auxSet = new HashSet<LicenciaIdioma>();

        for (LicenciaIdioma licenciaIdioma : licencia.getLicenciasIdiomas())
        {
            if (licenciaIdioma.getTitulo() != null && !licenciaIdioma.getTitulo().equals(""))
            {
                auxSet.add(licenciaIdioma);
            }
            else if (licenciaIdioma.getId() != null)
            {
                delete(LicenciaIdioma.class, licenciaIdioma.getId());
            }
        }

        licencia.setLicenciasIdiomas(auxSet);

        return update(licencia);
    }

    @Override
    public Long getNumMaterialesConLicencia(Long licenciaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QMaterial material = QMaterial.material;

        query.from(material).where(material.licencia.id.eq(licenciaId));

        return query.count();
    }
}
