package es.uji.apps.ocw.dao;

import java.util.List;

import es.uji.apps.ocw.model.Categoria;
import es.uji.apps.ocw.model.CategoriaIdioma;
import es.uji.apps.ocw.model.CategoriaIdiomaPrincipal;
import es.uji.commons.db.BaseDAO;
import es.uji.commons.db.LookupDAO;
import es.uji.commons.model.lookup.LookupItem;

public interface CategoriaDAO extends BaseDAO, LookupDAO
{
    List<Categoria> getCategorias();

    List<CategoriaIdioma> getCategoriaIdiomaByCategoriaId(Long categoriaId);

    Categoria insertCategoria(Categoria categoria);

    Categoria updateCategoria(Categoria categoria);

    List<CategoriaIdiomaPrincipal> getCategoriasIdiomaPrincipal();

    CategoriaIdiomaPrincipal getCategoriaIdiomaPrincipalById(Long categoriaId);

    List<LookupItem> search(String cadena);

    List<CategoriaIdiomaPrincipal> getCategoriasByCursoId(Long cursoId);
    
    List<Categoria> getCategoriasByCursoIdAndIdioma(Long cursoId, String idioma);

    List<Categoria> getCategoriasByIdioma(String acronimoIdioma);

    Categoria getCategoriaByIdAndIdioma(Long categoriaId, String acronimoIdioma);

}
