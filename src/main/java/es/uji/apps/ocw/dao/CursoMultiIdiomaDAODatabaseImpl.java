package es.uji.apps.ocw.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.ocw.model.CursoMultiIdioma;
import es.uji.apps.ocw.model.QCursoMultiIdioma;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class CursoMultiIdiomaDAODatabaseImpl extends BaseDAODatabaseImpl implements
        CursoMultiIdiomaDAO
{

    @Override
    public List<CursoMultiIdioma> getCursosByIdsAndIdioma(List<Long> listaCursosId,
            String acronimoIdioma)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QCursoMultiIdioma qCursoMultiIdioma = QCursoMultiIdioma.cursoMultiIdioma;

        if (listaCursosId.size() == 0)
        {
            return new ArrayList<CursoMultiIdioma>();
        }
        else
        {
            query.from(qCursoMultiIdioma).where(
                    qCursoMultiIdioma.id.in(listaCursosId).and(
                            qCursoMultiIdioma.iso.eq(acronimoIdioma).and(
                                    qCursoMultiIdioma.estado.eq("F").and(
                                            qCursoMultiIdioma.visible.eq("S").and(
                                                    qCursoMultiIdioma.fechaBaja.isNull())))));

            return query.list(qCursoMultiIdioma);
        }
    }

    @Override
    public List<CursoMultiIdioma> getCursosByIdsAndIdiomaVistaPrevia(List<Long> listaCursosId,
            String acronimoIdioma)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QCursoMultiIdioma qCursoMultiIdioma = QCursoMultiIdioma.cursoMultiIdioma;

        if (listaCursosId.size() == 0)
        {
            return new ArrayList<CursoMultiIdioma>();
        }
        else
        {
            query.from(qCursoMultiIdioma).where(
                    qCursoMultiIdioma.id.in(listaCursosId).and(
                            qCursoMultiIdioma.iso.eq(acronimoIdioma)));

            return query.list(qCursoMultiIdioma);
        }
    }

}
