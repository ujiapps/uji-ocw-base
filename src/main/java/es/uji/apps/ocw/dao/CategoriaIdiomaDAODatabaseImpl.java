package es.uji.apps.ocw.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.ocw.model.CategoriaIdioma;
import es.uji.apps.ocw.model.QCategoriaIdioma;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class CategoriaIdiomaDAODatabaseImpl extends BaseDAODatabaseImpl implements
        CategoriaIdiomaDAO
{
    @Override
    public List<CategoriaIdioma> getCategoriaIdiomaByCategoria(Long categoriaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QCategoriaIdioma categoriaIdioma = QCategoriaIdioma.categoriaIdioma;

        query.from(categoriaIdioma).where(categoriaIdioma.categoria.id.eq(categoriaId));

        return query.list(categoriaIdioma);
    }

    @Override
    public List<CategoriaIdioma> getCategoriaIdiomaByIdioma(Long idiomaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QCategoriaIdioma categoriaIdioma = QCategoriaIdioma.categoriaIdioma;

        query.from(categoriaIdioma).where(categoriaIdioma.idioma.id.eq(idiomaId));

        return query.list(categoriaIdioma);
    }

}
