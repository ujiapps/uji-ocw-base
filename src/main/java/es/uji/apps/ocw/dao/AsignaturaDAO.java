package es.uji.apps.ocw.dao;

import java.util.List;

import es.uji.apps.ocw.model.Asignatura;
import es.uji.commons.db.BaseDAO;
import es.uji.commons.db.LookupDAO;

public interface AsignaturaDAO extends BaseDAO, LookupDAO
{
    List<Asignatura> getAsignaturas();
}