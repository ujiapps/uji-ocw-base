package es.uji.apps.ocw.dao;

import java.util.List;

import es.uji.apps.ocw.model.Enlace;
import es.uji.commons.db.BaseDAO;

public interface EnlaceDAO extends BaseDAO
{
    public List<Enlace> getEnlaces();

    public Enlace getEnlace(Long enlaceId);

    public Enlace updateEnlace(Enlace enlace);

    public List<Enlace> getEnlacesInteres(String idioma, int numMaxEnlacesAVisualizar);

    public List<Enlace> getEnlacesOCW(String idioma);

    public List<Enlace> getEnlacesInteresPaginados(String idioma, int numEnlacesPorPagina,
            int pagina);

    public Integer getNumeroTotalPaginasEnlaces(String idioma, int numEnlacesPorPagina);

}
