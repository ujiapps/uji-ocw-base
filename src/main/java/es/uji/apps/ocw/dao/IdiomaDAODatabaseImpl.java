package es.uji.apps.ocw.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.ocw.model.Idioma;
import es.uji.apps.ocw.model.QIdioma;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class IdiomaDAODatabaseImpl extends BaseDAODatabaseImpl implements IdiomaDAO
{
    @Override
    public List<Idioma> getIdiomasEdicion()
    {
        JPAQuery query = new JPAQuery(entityManager);
        QIdioma idioma = QIdioma.idioma;

        query.from(idioma).where(idioma.edicion.eq("S")).orderBy(idioma.orden.asc());

        return query.list(idioma);
    }
    
    @Override
    public List<Idioma> getIdiomas()
    {
        JPAQuery query = new JPAQuery(entityManager);
        QIdioma idioma = QIdioma.idioma;

        query.from(idioma).orderBy(idioma.nombre.asc());

        return query.list(idioma);
    }    
   
    
}
