package es.uji.apps.ocw.dao;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.ocw.model.Enlace;
import es.uji.apps.ocw.model.EnlaceIdioma;
import es.uji.apps.ocw.model.QEnlace;
import es.uji.apps.ocw.model.QEnlaceIdioma;
import es.uji.apps.ocw.model.QIdioma;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class EnlaceDAODatabaseImpl extends BaseDAODatabaseImpl implements EnlaceDAO
{
    @Override
    public List<Enlace> getEnlaces()
    {
        JPAQuery query = new JPAQuery(entityManager);

        QEnlace enlace = QEnlace.enlace;
        QEnlaceIdioma enlaceIdioma = QEnlaceIdioma.enlaceIdioma;
        QIdioma idioma = QIdioma.idioma;

        query.from(enlace).leftJoin(enlace.enlacesIdiomas, enlaceIdioma).fetch()
                .leftJoin(enlaceIdioma.idioma, idioma);

        return query.distinct().list(enlace);
    }

    @Override
    public Enlace getEnlace(Long enlaceId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QEnlace enlace = QEnlace.enlace;
        QEnlaceIdioma enlaceIdioma = QEnlaceIdioma.enlaceIdioma;
        QIdioma idioma = QIdioma.idioma;

        query.from(enlace).leftJoin(enlace.enlacesIdiomas, enlaceIdioma).fetch()
                .leftJoin(enlaceIdioma.idioma, idioma).where(enlace.id.eq(enlaceId));

        return query.uniqueResult(enlace);
    }

    @Override
    public Enlace updateEnlace(Enlace enlace)
    {
        Set<EnlaceIdioma> auxSet = new HashSet<EnlaceIdioma>();

        for (EnlaceIdioma enlaceIdioma : enlace.getEnlacesIdiomas())
        {
            if (enlaceIdioma.getUrl() != null && !enlaceIdioma.getUrl().equals(""))
            {
                auxSet.add(enlaceIdioma);
            }
            else if (enlaceIdioma.getId() != null)
            {
                delete(EnlaceIdioma.class, enlaceIdioma.getId());
            }
        }

        enlace.setEnlacesIdiomas(auxSet);

        return update(enlace);
    }

    @Override
    public List<Enlace> getEnlacesInteres(String idioma, int numMaxEnlacesAVisualizar)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QEnlace qEnlace = QEnlace.enlace;
        QEnlaceIdioma qEnlaceIdioma = QEnlaceIdioma.enlaceIdioma;
        QIdioma qIdioma = QIdioma.idioma;

        query.from(qEnlace).leftJoin(qEnlace.enlacesIdiomas, qEnlaceIdioma).fetch()
                .leftJoin(qEnlaceIdioma.idioma, qIdioma)
                .where(qEnlace.ocw.eq(false).and(qIdioma.acronimo.eq(idioma)))
                .orderBy(qEnlace.orden.asc()).limit(numMaxEnlacesAVisualizar);

        return query.list(qEnlace);
    }

    @Override
    public List<Enlace> getEnlacesOCW(String idioma)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QEnlace qEnlace = QEnlace.enlace;
        QEnlaceIdioma qEnlaceIdioma = QEnlaceIdioma.enlaceIdioma;
        QIdioma qIdioma = QIdioma.idioma;

        query.from(qEnlace).leftJoin(qEnlace.enlacesIdiomas, qEnlaceIdioma).fetch()
                .leftJoin(qEnlaceIdioma.idioma, qIdioma)
                .where(qEnlace.ocw.eq(true).and(qIdioma.acronimo.eq(idioma)))
                .orderBy(qEnlace.orden.asc());

        return query.list(qEnlace);
    }

    @Override
    public List<Enlace> getEnlacesInteresPaginados(String idioma, int numEnlacesPorPagina,
            int pagina)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QEnlace qEnlace = QEnlace.enlace;
        QEnlaceIdioma qEnlaceIdioma = QEnlaceIdioma.enlaceIdioma;
        QIdioma qIdioma = QIdioma.idioma;

        query.from(qEnlace).leftJoin(qEnlace.enlacesIdiomas, qEnlaceIdioma).fetch()
                .leftJoin(qEnlaceIdioma.idioma, qIdioma)
                .where(qEnlace.ocw.eq(false).and(qIdioma.acronimo.eq(idioma)))
                .orderBy(qEnlace.orden.asc()).limit(numEnlacesPorPagina)
                .offset((pagina - 1) * numEnlacesPorPagina);

        return query.list(qEnlace);
    }

    @Override
    public Integer getNumeroTotalPaginasEnlaces(String idioma, int numEnlacesPorPagina)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QEnlace qEnlace = QEnlace.enlace;
        QEnlaceIdioma qEnlaceIdioma = QEnlaceIdioma.enlaceIdioma;
        QIdioma qIdioma = QIdioma.idioma;

        query.from(qEnlace).leftJoin(qEnlace.enlacesIdiomas, qEnlaceIdioma).fetch()
                .leftJoin(qEnlaceIdioma.idioma, qIdioma)
                .where(qEnlace.ocw.eq(false).and(qIdioma.acronimo.eq(idioma)))
                .orderBy(qEnlace.orden.asc());
        
        Long total = query.count();
        
        Float numPaginas = total.floatValue() / numEnlacesPorPagina;

        return (int) Math.ceil(numPaginas);
    }
}
