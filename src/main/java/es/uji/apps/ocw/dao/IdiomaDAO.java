package es.uji.apps.ocw.dao;

import java.util.List;

import es.uji.apps.ocw.model.Idioma;
import es.uji.commons.db.BaseDAO;

public interface IdiomaDAO extends BaseDAO
{
    List<Idioma> getIdiomasEdicion();

    List<Idioma> getIdiomas();
}
