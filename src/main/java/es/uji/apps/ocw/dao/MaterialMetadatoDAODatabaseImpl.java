package es.uji.apps.ocw.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.ocw.model.MaterialMetadato;
import es.uji.apps.ocw.model.QMaterialMetadato;
import es.uji.apps.ocw.model.QMetadato;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class MaterialMetadatoDAODatabaseImpl extends BaseDAODatabaseImpl implements
        MaterialMetadatoDAO
{

    @Override
    public List<MaterialMetadato> getMetadatosByMaterial(Long materialId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QMaterialMetadato materialMetadato = QMaterialMetadato.materialMetadato;
        QMetadato metadato = QMetadato.metadato;

        query.from(materialMetadato).join(materialMetadato.metadato, metadato)
                .where(materialMetadato.material.id.eq(materialId)).fetch()
                .orderBy(materialMetadato.metadato.tag.asc(), materialMetadato.valor.asc());

        return query.list(materialMetadato);

    }
}