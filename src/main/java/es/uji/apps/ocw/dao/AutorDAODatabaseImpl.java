package es.uji.apps.ocw.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.ocw.model.Autor;
import es.uji.apps.ocw.model.AutorCompleto;
import es.uji.apps.ocw.model.QAutorCompleto;
import es.uji.apps.ocw.model.QMaterialAutor;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.model.lookup.LookupItem;

@Repository
public class AutorDAODatabaseImpl extends BaseDAODatabaseImpl implements AutorDAO
{
    @Override
    public List<Autor> getAutores()
    {
        JPAQuery query = new JPAQuery(entityManager);

        QAutorCompleto autor = QAutorCompleto.autorCompleto;

        query.from(autor).orderBy(autor.apellido1.asc(), autor.apellido2.asc());

        List<Autor> autores = new ArrayList<Autor>();

        for (AutorCompleto autorCompleto : query.list(autor))
        {
            autores.add(Autor.getAutorFromAutorCompleto(autorCompleto));
        }

        return autores;
    }

    @Override
    public Long getNumMaterialesAutor(Long autorId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QMaterialAutor materialAutor = QMaterialAutor.materialAutor;

        query.from(materialAutor).where(materialAutor.autor.id.eq(autorId));

        return query.count();
    }

    @Override
    public List<AutorCompleto> getAutorCompleto(Long autorId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QAutorCompleto autor = QAutorCompleto.autorCompleto;

        query.from(autor).where(autor.id.eq(autorId));

        return query.list(autor);

    }

    @Override
    public List<LookupItem> search(String cadena)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QAutorCompleto autor = QAutorCompleto.autorCompleto;

        List<LookupItem> result = new ArrayList<LookupItem>();

        if (cadena != null && !cadena.isEmpty())
        {
            query.from(autor)
                    .where(autor.nombre.concat(" ").concat(autor.apellido1).concat(" ")
                            .concat(autor.apellido2).lower().like("%" + cadena.toLowerCase() + "%"))
                    .orderBy(autor.apellido1.asc(), autor.apellido2.asc(), autor.nombre.asc());

            List<AutorCompleto> listaAutores = query.list(autor);

            for (AutorCompleto autorEncontrado : listaAutores)
            {
                LookupItem lookupItem = new LookupItem();
                lookupItem.setId(String.valueOf(autorEncontrado.getId()));
                lookupItem.setNombre(autorEncontrado.getApellido1() + " "
                        + autorEncontrado.getApellido2() + ", " + autorEncontrado.getNombre());

                result.add(lookupItem);
            }
        }

        return result;
    }
}
