package es.uji.apps.ocw.dao;

import java.util.List;

import es.uji.apps.ocw.model.MaterialMetadato;
import es.uji.commons.db.BaseDAO;

public interface MaterialMetadatoDAO extends BaseDAO
{

    List<MaterialMetadato> getMetadatosByMaterial(Long materialId);

}
