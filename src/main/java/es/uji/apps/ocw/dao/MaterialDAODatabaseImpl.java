package es.uji.apps.ocw.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.mysema.query.Tuple;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.QTuple;

import es.uji.apps.ocw.model.*;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.model.lookup.LookupItem;

@Repository
public class MaterialDAODatabaseImpl extends BaseDAODatabaseImpl implements MaterialDAO
{
    @Override
    public List<Material> getMaterialesByPersona(Long personaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QMaterial material = QMaterial.material;
        QPersona persona = QPersona.persona;
        QIdioma idioma = QIdioma.idioma;

        query.from(material, persona, idioma)
                .where(material.personaId.eq(persona.id), material.idioma.id.eq(idioma.id),
                        persona.id.eq(personaId)).fetch();

        return query.list(material);
    }

    @Override
    public List<MaterialCompleto> getMateriales()
    {
        JPAQuery query = new JPAQuery(entityManager);
        QMaterialCompleto materialCompleto = QMaterialCompleto.materialCompleto;

        query.from(materialCompleto).orderBy(materialCompleto.titulo.asc());

        return query.list(materialCompleto);
    }

    @Override
    public List<MaterialCompleto> getMaterialesConFiltros(Map<String, String> filtro)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QMaterialCompleto materialCompleto = QMaterialCompleto.materialCompleto;

        query.from(materialCompleto);

        if (filtro.get("cadena") != null && !filtro.get("cadena").isEmpty())
        {
            String cadena = filtro.get("cadena");
            query.where(materialCompleto.titulo
                    .lower()
                    .like("%" + cadena.toLowerCase() + "%")
                    .or(materialCompleto.descripcion.lower().like("%" + cadena.toLowerCase() + "%"))
                    .or(materialCompleto.nombrePersona.lower().like(
                            "%" + cadena.toLowerCase() + "%")));
        }

        if (filtro.get("idiomaId") != null && !filtro.get("idiomaId").isEmpty())
        {
            Long idiomaId = Long.parseLong(filtro.get("idiomaId"));
            query.where(materialCompleto.idioma.eq(idiomaId));
        }

        if (filtro.get("personaId") != null && !filtro.get("personaId").isEmpty())
        {
            Long personaId = Long.parseLong(filtro.get("personaId"));
            query.where(materialCompleto.persona.eq(personaId));
        }
        return query.list(materialCompleto);
    }

    @Override
    public List<MaterialTag> getTags(Long materialId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QMaterialTag materialTag = QMaterialTag.materialTag;
        QTag tag = QTag.tag;

        query.from(materialTag).join(materialTag.tag, tag)
                .where(materialTag.material.id.eq(materialId)).fetch();

        return query.list(materialTag);

    }

    @Override
    public List<MaterialAutor> getAutores(Long materialId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QMaterialAutor materialAutor = QMaterialAutor.materialAutor;
        QAutorCompleto autor = QAutorCompleto.autorCompleto;

        query.from(materialAutor, autor)
                .where(materialAutor.autor.id.eq(autor.id).and(
                        materialAutor.material.id.eq(materialId)))
                .orderBy(autor.apellido1.asc(), autor.apellido2.asc(), autor.nombre.asc());

        return query.list(materialAutor);
    }

    @Override
    public List<CursoMaterial> getCursos(Long materialId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QCursoMaterial materialCurso = QCursoMaterial.cursoMaterial;
        QCurso curso = QCurso.curso;

        query.from(materialCurso).join(materialCurso.curso, curso)
                .where(materialCurso.material.id.eq(materialId)).fetch();

        return query.list(materialCurso);

    }

    @Override
    public List<LookupItem> search(String cadena)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QMaterial material = QMaterial.material;
        List<LookupItem> result = new ArrayList<LookupItem>();

        if (cadena != null && !cadena.isEmpty())
        {
            query.from(material).where(
                    material.titulo.lower().like("%" + cadena.toLowerCase() + "%"));
            List<Tuple> listaMateriales = query.list(new QTuple(material.id, material.titulo));

            for (Tuple materialEncontrado : listaMateriales)
            {
                LookupItem lookupItem = new LookupItem();
                lookupItem.setId(materialEncontrado.get(material.id).toString());
                lookupItem.setNombre(materialEncontrado.get(material.titulo));
                result.add(lookupItem);
            }
        }
        return result;
    }

    @Override
    public List<Material> getUltimosMateriales(int numMaxMaterialesAVisualizar)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QMaterial qMaterial = QMaterial.material;
        QIdioma qIdioma = QIdioma.idioma;

        query.from(qMaterial).join(qMaterial.idioma, qIdioma).orderBy(qMaterial.fecha.desc())
                .limit(numMaxMaterialesAVisualizar);

        return query.list(qMaterial);
    }

    @Override
    public List<Material> getMaterialesMasVisitados(int numMaxMaterialesAVisualizar)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QMaterial qMaterial = QMaterial.material;
        QIdioma qIdioma = QIdioma.idioma;
        QAccesoMaterial qAccesoMaterial = QAccesoMaterial.accesoMaterial;

        query.from(qMaterial, qAccesoMaterial).join(qMaterial.idioma, qIdioma)
                .where(qMaterial.id.eq(qAccesoMaterial.materialId))
                .orderBy(qAccesoMaterial.numeroAccesos.desc()).limit(numMaxMaterialesAVisualizar);

        return query.list(qMaterial);
    }

    @Override
    public List<Material> getTodosMaterialesPaginados(int numMaterialesPorPagina, int pagina)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QMaterial qMaterial = QMaterial.material;
        QIdioma qIdioma = QIdioma.idioma;
        QMaterialTag qMaterialTag = QMaterialTag.materialTag;
        QTag qTag = QTag.tag;

        query.from(qMaterial).join(qMaterial.materialesTags, qMaterialTag)
                .join(qMaterialTag.tag, qTag).join(qMaterial.idioma, qIdioma).fetch()
                .orderBy(qMaterial.fecha.desc()).limit(numMaterialesPorPagina)
                .offset((pagina - 1) * numMaterialesPorPagina);

        return query.list(qMaterial);
    }

    @Override
    public Integer getNumeroTotalPaginasMateriales(int numMaterialesPorPagina)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QMaterial qMaterial = QMaterial.material;

        query.from(qMaterial);

        Long total = query.count();
        Float numPaginas = total.floatValue() / numMaterialesPorPagina;

        return (int) Math.ceil(numPaginas);
    }

    @Override
    public List<Material> getMaterialesByCursoId(Long cursoId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QMaterial material = QMaterial.material;
        QPersona persona = QPersona.persona;
        QIdioma idioma = QIdioma.idioma;

        Long personaId = new Long(0);

        query.from(material, persona, idioma)
                .where(material.personaId.eq(persona.id), material.idioma.id.eq(idioma.id),
                        persona.id.eq(personaId)).fetch();

        return query.list(material);
    }

    @Override
    public Material getMaterialById(Long materialId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QMaterial qMaterial = QMaterial.material;
        query.from(qMaterial).where(qMaterial.id.eq(materialId));

        List<Tuple> materiales = query.list(new QTuple(qMaterial.id, qMaterial.contentType,
                qMaterial.descripcion, qMaterial.fecha, qMaterial.handleId,
                qMaterial.nombreFichero, qMaterial.personaId, qMaterial.titulo, qMaterial.url,
                qMaterial.xml, qMaterial.idioma, qMaterial.licencia));

        if (materiales.isEmpty())
        {
            return null;
        }

        Tuple materialTuple = materiales.get(0);
        Material material = new Material();
        material.setId(materialTuple.get(qMaterial.id));
        material.setContentType(materialTuple.get(qMaterial.contentType));
        material.setDescripcion(materialTuple.get(qMaterial.descripcion));
        material.setFecha(materialTuple.get(qMaterial.fecha));
        material.setHandleId(materialTuple.get(qMaterial.handleId));
        material.setNombreFichero(materialTuple.get(qMaterial.nombreFichero));
        material.setPersonaId(materialTuple.get(qMaterial.personaId));
        material.setTitulo(materialTuple.get(qMaterial.titulo));
        material.setUrl(materialTuple.get(qMaterial.url));
        material.setXml(materialTuple.get(qMaterial.xml));
        material.setIdioma(materialTuple.get(qMaterial.idioma));
        material.setLicencia(materialTuple.get(qMaterial.licencia));
        return material;
    }

    // @Override
    // public List<GrupoMaterialesUI> getMaterialesAgrupadosByCursoId(Long cursoId)
    // {
    // JPAQuery query = new JPAQuery(jpaTemplate.getEntityManager());
    // QCursoMaterialGrupo qCursoMaterialGrupo = QCursoMaterialGrupo.cursoMaterialGrupo;
    //
    // query.from(qCursoMaterialGrupo).where(qCursoMaterialGrupo.cursoId.eq(cursoId))
    // .orderBy(qCursoMaterialGrupo.orden.asc());
    //
    // List<CursoMaterialGrupo> listaCursoMaterialGrupos = query.list(qCursoMaterialGrupo);
    //
    // List<GrupoMaterialesUI> listaGruposMaterialesUI = new ArrayList<GrupoMaterialesUI>();
    // Map<Long, GrupoMaterialesUI> grupoMaterialesUIMap = new HashMap<Long, GrupoMaterialesUI>();
    //
    // for (CursoMaterialGrupo cursoMaterialGrupo : listaCursoMaterialGrupos)
    // {
    // if (cursoMaterialGrupo.getTipo().equals("GRUPO"))
    // {
    //
    // GrupoMaterialesUI grupoMaterialesUI = new GrupoMaterialesUI();
    // grupoMaterialesUI.setId(cursoMaterialGrupo.getGrupoId().toString());
    // grupoMaterialesUI.setNombre(cursoMaterialGrupo.getTitulo());
    // grupoMaterialesUI.setDescripcion(cursoMaterialGrupo.getDescripcion());
    //
    // listaGruposMaterialesUI.add(grupoMaterialesUI);
    // grupoMaterialesUIMap.put(cursoMaterialGrupo.getGrupoId(), grupoMaterialesUI);
    //
    // }
    // else if (cursoMaterialGrupo.getTipo().equals("MAT"))
    // {
    // // Creamos un nuevo material
    // MaterialUI materialUI = new MaterialUI();
    // materialUI.setTitulo(cursoMaterialGrupo.getTitulo());
    // // materialUI.setDescripcion(cursoMaterialGrupo.getDescripcion());
    // // Faltan campos, modificar esta vista o crear una nueva
    //
    // if (!grupoMaterialesUIMap.containsKey(cursoMaterialGrupo.getGrupoId()))
    // {
    // // Material no asociado a un grupo
    // GrupoMaterialesUI grupoMaterialesUI = new GrupoMaterialesUI();
    // grupoMaterialesUI.getListaMaterialesUI().add(materialUI);
    // listaGruposMaterialesUI.add(grupoMaterialesUI);
    // }
    // else
    // {
    // grupoMaterialesUIMap.get(cursoMaterialGrupo.getGrupoId())
    // .getListaMaterialesUI().add(materialUI);
    // }
    // }
    // }
    //
    // return listaGruposMaterialesUI;
    // }
}