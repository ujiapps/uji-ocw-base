package es.uji.apps.ocw.dao;

import java.util.List;

import es.uji.apps.ocw.model.CategoriaIdioma;
import es.uji.commons.db.BaseDAO;

public interface CategoriaIdiomaDAO extends BaseDAO
{

    List<CategoriaIdioma> getCategoriaIdiomaByIdioma(Long idiomaId);

    List<CategoriaIdioma> getCategoriaIdiomaByCategoria(Long categoriaId);
}
