package es.uji.apps.ocw.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.ocw.model.CursoMaterial;
import es.uji.apps.ocw.model.QCursoMaterial;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class CursoMaterialDAODatabaseImpl extends BaseDAODatabaseImpl implements CursoMaterialDAO
{

    @Override
    public Long getMaxOrdenCursoMaterialByGrupo(Long grupoId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QCursoMaterial qCursoMaterial = QCursoMaterial.cursoMaterial;

        query.from(qCursoMaterial)
                .where(qCursoMaterial.grupo.id.eq(grupoId).and(qCursoMaterial.orden.isNotNull()))
                .orderBy(qCursoMaterial.orden.desc());

        List<CursoMaterial> materialesGrupo = query.list(qCursoMaterial);
        return materialesGrupo.isEmpty() ? null : materialesGrupo.get(0).getOrden();
    }

}