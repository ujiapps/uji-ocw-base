package es.uji.apps.ocw.dao;

import java.util.List;

import es.uji.apps.ocw.model.Tag;
import es.uji.commons.db.BaseDAO;

public interface TagDAO extends BaseDAO
{
    List<Tag> getTags();

    List<Tag> getTagsByMaterial(Long materialId);
  
    List<Tag> getTagByName(String tagString);

    Long getTagsByNumeroMateriales(Long tagId);
    
    Long getTagsByNumeroCursos(Long tagId);
}
