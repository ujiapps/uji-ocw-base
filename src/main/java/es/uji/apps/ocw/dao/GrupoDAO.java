package es.uji.apps.ocw.dao;

import java.util.List;

import es.uji.apps.ocw.model.CursoMaterial;
import es.uji.apps.ocw.model.Grupo;
import es.uji.commons.db.BaseDAO;

public interface GrupoDAO extends BaseDAO
{
    List<Grupo> getGruposByCursoId(Long cursoId);

    List<CursoMaterial> getMaterialesByGrupoId(Long grupoId);

    void incrementaOrdenMaterialesCuyoOrdenMayorQue(Long cursoId, Long grupoId, Long orden, int incremento);

    void incrementaOrdenGruposCuyoOrdenMayorQue(Long cursoId, Long orden, int incremento);

    void incrementaOrdenMaterialesGrupoRaizCuyoOrdenMayorQue(Long cursoId, Long orden, int incremento);   
    
}
