package es.uji.apps.ocw.dao;

import java.util.List;

import es.uji.apps.ocw.model.Noticia;

public interface NoticiaDAO
{
    List<Noticia> getUltimasNoticias(String acronimoIdioma);
}