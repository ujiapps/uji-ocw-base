package es.uji.apps.ocw.dao;

import java.text.MessageFormat;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.expr.BooleanExpression;

import es.uji.apps.ocw.model.Asignatura;
import es.uji.apps.ocw.model.Categoria;
import es.uji.apps.ocw.model.Curso;
import es.uji.apps.ocw.model.CursoAsignatura;
import es.uji.apps.ocw.model.CursoAutorizado;
import es.uji.apps.ocw.model.CursoCategoria;
import es.uji.apps.ocw.model.CursoIdioma;
import es.uji.apps.ocw.model.CursoIdiomaPrincipal;
import es.uji.apps.ocw.model.CursoImagen;
import es.uji.apps.ocw.model.CursoMaterial;
import es.uji.apps.ocw.model.CursoTag;
import es.uji.apps.ocw.model.Persona;
import es.uji.apps.ocw.model.QAccesoCurso;
import es.uji.apps.ocw.model.QAsignatura;
import es.uji.apps.ocw.model.QCategoria;
import es.uji.apps.ocw.model.QCategoriaIdioma;
import es.uji.apps.ocw.model.QCurso;
import es.uji.apps.ocw.model.QCursoAsignatura;
import es.uji.apps.ocw.model.QCursoAutorizado;
import es.uji.apps.ocw.model.QCursoCategoria;
import es.uji.apps.ocw.model.QCursoIdioma;
import es.uji.apps.ocw.model.QCursoIdiomaPrincipal;
import es.uji.apps.ocw.model.QCursoImagen;
import es.uji.apps.ocw.model.QCursoMaterial;
import es.uji.apps.ocw.model.QCursoMultiIdioma;
import es.uji.apps.ocw.model.QCursoTag;
import es.uji.apps.ocw.model.QIdioma;
import es.uji.apps.ocw.model.QMaterial;
import es.uji.apps.ocw.model.QPersona;
import es.uji.apps.ocw.model.QTag;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class CursoDAODatabaseImpl extends BaseDAODatabaseImpl implements CursoDAO
{
    @Override
    public String getNombrePersona(Long personaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QPersona qPersona = QPersona.persona;

        query.from(qPersona).where(qPersona.id.eq(personaId));

        Persona persona = query.list(qPersona).get(0);

        return MessageFormat.format("{0} {1} {2}", persona.getNombre(), persona.getApellido1(),
                persona.getApellido2());
    }

    @Override
    public List<CursoIdiomaPrincipal> getCursosConFiltros(Map<String, String> filtro)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QCursoIdiomaPrincipal qCurso = QCursoIdiomaPrincipal.cursoIdiomaPrincipal;
        query.from(qCurso);

        filtraNombre(filtro, query, qCurso);
        filtraCategoria(filtro, query, qCurso);
        filtraAutorizadoOPropietario(filtro, query, qCurso);

        return query.list(qCurso);
    }

    @Override
    public List<Curso> getCursosByCategoriaId(Long categoriaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QCurso curso = QCurso.curso;
        QCursoCategoria cursoCategoria = QCursoCategoria.cursoCategoria;

        query.from(curso).join(curso.cursosCategorias, cursoCategoria)
                .where(cursoCategoria.categoria.id.eq(categoriaId));

        return query.list(curso);
    }

    @Override
    public List<CursoImagen> getImagenesByCursoId(Long cursoId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QCursoImagen cursoImagen = QCursoImagen.cursoImagen;

        query.from(cursoImagen).where(cursoImagen.curso.id.eq(cursoId))
                .orderBy(cursoImagen.id.asc());

        return query.list(cursoImagen);
    }

    private void filtraAutorizadoOPropietario(Map<String, String> filtro, JPAQuery query,
            QCursoIdiomaPrincipal qCurso)
    {
        if (filtro.get("personaId") != null && !filtro.get("personaId").isEmpty())
        {
            JPAQuery subQuery = new JPAQuery(entityManager);

            Long personaId = Long.parseLong(filtro.get("personaId"));

            QCursoAutorizado qCursoAutorizado = QCursoAutorizado.cursoAutorizado;

            subQuery.from(qCursoAutorizado).where(qCursoAutorizado.personaId.eq(personaId));

            List<Long> subQueryResult = subQuery.list(qCursoAutorizado.curso.id);

            BooleanExpression whereClause = qCurso.personaId.eq(personaId);

            if (subQueryResult != null && !subQueryResult.isEmpty())
            {
                whereClause = whereClause.or(qCurso.id.in(subQueryResult));
            }

            query.where(whereClause);
        }
    }

    private void filtraCategoria(Map<String, String> filtro, JPAQuery query,
            QCursoIdiomaPrincipal qCurso)
    {
        if (filtro.get("categoriaId") != null && !filtro.get("categoriaId").isEmpty())
        {
            Long categoriaId = Long.parseLong(filtro.get("categoriaId"));
            QCursoCategoria qCursoCategoria = QCursoCategoria.cursoCategoria;
            query.from(qCursoCategoria).where(
                    qCursoCategoria.curso.id.eq(qCurso.id).and(
                            qCursoCategoria.categoria.id.eq(categoriaId)));
        }
    }

    private void filtraNombre(Map<String, String> filtro, JPAQuery query,
            QCursoIdiomaPrincipal qCurso)
    {
        if (filtro.get("cadena") != null && !filtro.get("cadena").isEmpty())
        {
            String cadena = filtro.get("cadena");
            query.where(qCurso.nombre.lower().like("%" + cadena.toLowerCase() + "%"));
        }
    }

    @Override
    public List<CursoMaterial> getMateriales(Long cursoId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QCursoMaterial cursoMaterial = QCursoMaterial.cursoMaterial;
        QMaterial material = QMaterial.material;

        query.from(cursoMaterial).join(cursoMaterial.material, material).fetch()
                .where(cursoMaterial.curso.id.eq(cursoId));

        return query.list(cursoMaterial);
    }

    @Override
    public List<CursoAsignatura> getAsignaturas(Long cursoId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QCursoAsignatura cursoAsignatura = QCursoAsignatura.cursoAsignatura;

        query.from(cursoAsignatura).where(cursoAsignatura.curso.id.eq(cursoId));
        return query.list(cursoAsignatura);
    }

    @Override
    public List<CursoAsignatura> getAsignaturasByCodigo(Long cursoId, String asignaturaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QCursoAsignatura cursoAsignatura = QCursoAsignatura.cursoAsignatura;

        query.from(cursoAsignatura).where(
                cursoAsignatura.curso.id.eq(cursoId).and(
                        cursoAsignatura.asignaturaId.eq(asignaturaId)));
        return query.list(cursoAsignatura);
    }

    @Override
    public Asignatura getAsignaturaById(String asignaturaId)

    {
        JPAQuery query = new JPAQuery(entityManager);
        QAsignatura asignatura = QAsignatura.asignatura;

        query.from(asignatura).where(asignatura.id.eq(asignaturaId));

        return query.list(asignatura).get(0);
    }

    @Override
    @Transactional
    public void deleteAsignaturaById(Long cursoAsignaturaId)
    {
        QCursoAsignatura cursoAsignatura = QCursoAsignatura.cursoAsignatura;

        JPADeleteClause deleteClause = new JPADeleteClause(entityManager, cursoAsignatura);

        deleteClause.where(cursoAsignatura.id.eq(cursoAsignaturaId)).execute();
    }

    @Override
    public List<CursoIdioma> getCursosIdiomas(Long cursoId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QCursoIdioma cursoIdioma = QCursoIdioma.cursoIdioma;

        query.from(cursoIdioma).where(cursoIdioma.curso.id.eq(cursoId));

        return query.list(cursoIdioma);
    }

    @Override
    public List<CursoAutorizado> getAutorizados(Long cursoId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QCursoAutorizado cursoAutorizado = QCursoAutorizado.cursoAutorizado;

        query.from(cursoAutorizado).where(cursoAutorizado.curso.id.eq(cursoId));
        return query.list(cursoAutorizado);
    }

    @Override
    public boolean isAutorizadoByCurso(Long cursoId, Long personaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QCursoAutorizado cursoAutorizado = QCursoAutorizado.cursoAutorizado;

        query.from(cursoAutorizado).where(
                cursoAutorizado.curso.id.eq(cursoId).and(cursoAutorizado.personaId.eq(personaId)));
        return query.list(cursoAutorizado).size() > 0;
    }

    @Override
    @Transactional
    public void deleteAutorizadoById(Long cursoAutorizadoId)
    {
        QCursoAutorizado cursoAutorizado = QCursoAutorizado.cursoAutorizado;

        JPADeleteClause deleteClause = new JPADeleteClause(entityManager, cursoAutorizado);

        deleteClause.where(cursoAutorizado.id.eq(cursoAutorizadoId)).execute();
    }

    @Override
    public List<CursoTag> getTags(Long cursoId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QCursoTag cursoTag = QCursoTag.cursoTag;
        QTag tag = QTag.tag;

        query.from(cursoTag).join(cursoTag.tag, tag).where(cursoTag.curso.id.eq(cursoId)).fetch();

        return query.list(cursoTag);
    }

    @Override
    public boolean isPropietario(Long cursoId, Long personaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QCurso curso = QCurso.curso;

        query.from(curso).where(curso.id.eq(cursoId).and(curso.personaId.eq(personaId)));

        return (query.list(curso).size() > 0);
    }

    @Override
    public boolean estaAutorizado(Long cursoId, Long personaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QCurso curso = QCurso.curso;
        QCursoAutorizado cursoAutorizado = QCursoAutorizado.cursoAutorizado;

        query.from(cursoAutorizado).join(cursoAutorizado.curso, curso)
                .where(curso.id.eq(cursoId).and(cursoAutorizado.personaId.eq(personaId)));

        return (query.list(curso).size() > 0);
    }

    @Override
    public CursoIdiomaPrincipal getCursoIdiomaPrincipal(Long cursoId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QCursoIdiomaPrincipal cursoIdiomaPrincipal = QCursoIdiomaPrincipal.cursoIdiomaPrincipal;

        query.from(cursoIdiomaPrincipal).where(cursoIdiomaPrincipal.id.eq(cursoId));

        return query.list(cursoIdiomaPrincipal).get(0);
    }

    @Override
    @Transactional
    public void deleteCursoCategoriaById(Long id)
    {
        QCursoCategoria cursoCategoria = QCursoCategoria.cursoCategoria;

        JPADeleteClause deleteClause = new JPADeleteClause(entityManager, cursoCategoria);

        deleteClause.where(cursoCategoria.id.eq(id)).execute();
    }

    @Override
    public List<CursoCategoria> getCursoCategoriasByCursoId(Long cursoId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QCursoCategoria cursoCategoria = QCursoCategoria.cursoCategoria;

        query.from(cursoCategoria).where(cursoCategoria.curso.id.eq(cursoId));

        return query.list(cursoCategoria);
    }

    @Override
    public boolean existeCursoCategoria(Long cursoId, Long categoriaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QCursoCategoria cursoCategoria = QCursoCategoria.cursoCategoria;

        query.from(cursoCategoria).where(cursoCategoria.curso.id.eq(cursoId),
                cursoCategoria.categoria.id.eq(categoriaId));

        return (query.list(cursoCategoria).size() > 0);
    }

    @Override
    public List<CursoMaterial> getMaterialesSinGrupo(Long cursoId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QCursoMaterial cursoMaterial = QCursoMaterial.cursoMaterial;
        QMaterial material = QMaterial.material;

        query.from(cursoMaterial).join(cursoMaterial.material, material).fetch()
                .where(cursoMaterial.curso.id.eq(cursoId).and(cursoMaterial.grupo.isNull()));

        return query.list(cursoMaterial);
    }

    @Override
    public List<CursoImagen> getImagenPrincipal(Long cursoId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QCursoImagen cursoImagen = QCursoImagen.cursoImagen;
        QCurso curso = QCurso.curso;

        query.from(cursoImagen, curso).join(cursoImagen.curso, curso).where(curso.id.eq(cursoId))
                .orderBy(cursoImagen.id.desc()).limit(1);

        return query.list(cursoImagen);

    }

    @Override
    public List<Categoria> getCategorias(Long cursoId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QCategoria qCategoria = QCategoria.categoria;
        QCursoCategoria qCursoCategoria = QCursoCategoria.cursoCategoria;

        query.from(qCategoria).join(qCategoria.cursosCategorias, qCursoCategoria)
                .where(qCursoCategoria.curso.id.eq(cursoId));

        return query.list(qCategoria);

    }

    @Override
    public List<CursoIdioma> getTodosCursosIdiomas(Long cursoId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QCursoIdioma qCursoIdioma = QCursoIdioma.cursoIdioma;

        query.from(qCursoIdioma).where(qCursoIdioma.curso.id.eq(cursoId))
                .orderBy(qCursoIdioma.idioma.orden.asc());

        return query.list(qCursoIdioma);
    }

    public void queryBusquedaSimple(JPAQuery query, String idioma, String cadenaBusqueda)
    {
        QCursoMultiIdioma cursoMultiIdioma = QCursoMultiIdioma.cursoMultiIdioma;
        QCursoTag cursoTag = QCursoTag.cursoTag;
        QTag tag = QTag.tag;

        /*
         * query.from(cursoMultiIdioma, cursoTag, tag)
         * .where(cursoTag.curso.id.eq(cursoMultiIdioma.))
         */
    }

    @Override
    public List<CursoCategoria> getCursosCategoriasByCursosIds(List<Long> listaCursosIds)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QCursoCategoria cursoCategoria = QCursoCategoria.cursoCategoria;
        QCategoriaIdioma categoriaIdioma = QCategoriaIdioma.categoriaIdioma;
        QCategoria categoria = QCategoria.categoria;
        QIdioma idioma = QIdioma.idioma;

        query.from(cursoCategoria).join(cursoCategoria.categoria, categoria)
                .join(categoriaIdioma.idioma, idioma).fetch()
                .where(cursoCategoria.curso.id.in(listaCursosIds));

        return query.list(cursoCategoria);
    }

    /*
     * @Override public List<Curso> getCursosPaginadosBusquedaSimple(String cadenaBusqueda, int
     * numCursosPorPagina, int pagina) { JPAQuery query = new JPAQuery(entityManager); QCurso curso
     * = QCurso.curso; QCursoIdioma cursoIdioma = QCursoIdioma.cursoIdioma; QCursoCategoria
     * cursoCategoria = QCursoCategoria.cursoCategoria; QCategoria categoria = QCategoria.categoria;
     * QCategoriaIdioma categoriaIdioma = QCategoriaIdioma.categoriaIdioma; QCursoTag cursoTag =
     * QCursoTag.cursoTag; QTag tag = QTag.tag;
     * 
     * query.from(curso) .join(curso.cursosIdiomas, cursoIdioma) .fetch()
     * .join(curso.cursosCategorias, cursoCategoria) .join(cursoCategoria.categoria, categoria)
     * .join(categoria.categoriasIdiomas, categoriaIdioma) .join(curso.cursosTags, cursoTag)
     * .join(cursoTag.tag, tag) .where(curso.estado .eq("F") .and(curso.visible.eq("S"))
     * .and(curso.fechaBaja.isNull()) .and((cursoIdioma.nombre .lower() .like("%" +
     * cadenaBusqueda.toLowerCase() + "%") .or(cursoIdioma.descripcion.lower().like( "%" +
     * cadenaBusqueda.toLowerCase() + "%")) .or(categoriaIdioma.nombre.lower().like( "%" +
     * cadenaBusqueda.toLowerCase() + "%")).or(tag.nombre .lower().like("%" +
     * cadenaBusqueda.toLowerCase() + "%")))))
     * .orderBy(curso.fechaCreacion.desc()).distinct().limit(numCursosPorPagina) .offset((pagina -
     * 1) * numCursosPorPagina);
     * 
     * return query.list(curso); }
     */

    @Override
    public List<CursoTag> getEtiquetasFromCursosIds(List<Long> listaCursosIds)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QCursoTag cursoTag = QCursoTag.cursoTag;
        QTag tag = QTag.tag;

        query.from(cursoTag).join(cursoTag.tag, tag).fetch()
                .where(cursoTag.curso.id.in(listaCursosIds));

        return query.list(cursoTag);

    }

    @Override
    public List<CursoImagen> getImagenFromCursosIds(List<Long> listaCursosIds)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QCursoImagen cursoImagen = QCursoImagen.cursoImagen;

        query.from(cursoImagen).where(cursoImagen.curso.id.in(listaCursosIds));

        return query.list(cursoImagen);
    }

    @Override
    public List<Curso> getCursosPaginadosByCategoriaId(Long categoriaId, Integer numPagina,
            int numCursosPorPagina)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QCurso qCurso = QCurso.curso;
        QCursoCategoria qCursoCategoria = QCursoCategoria.cursoCategoria;

        query.from(qCurso)
                .join(qCurso.cursosCategorias, qCursoCategoria)
                .where(qCurso.estado.eq("F").and(qCurso.id.eq(qCursoCategoria.curso.id))
                        .and(qCursoCategoria.categoria.id.eq(categoriaId))
                        .and(qCurso.visible.eq("S").and(qCurso.fechaBaja.isNull())))
                .orderBy(qCurso.fechaCreacion.desc()).limit(numCursosPorPagina)
                .offset((numPagina - 1) * numCursosPorPagina);

        return query.list(qCurso);
    }

    @Override
    public List<Curso> getTodosLosCursosPaginados(Integer numPagina, int numCursosPorPagina)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QCurso qCurso = QCurso.curso;

        query.from(qCurso)
                .where(qCurso.estado.eq("F").and(
                        qCurso.visible.eq("S").and(qCurso.fechaBaja.isNull())))
                .orderBy(qCurso.fechaCreacion.desc()).limit(numCursosPorPagina)
                .offset((numPagina - 1) * numCursosPorPagina);

        return query.list(qCurso);
    }

    @Override
    public List<Curso> getUltimosCursos(int numMaxCursosAVisualizar)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QCurso qCurso = QCurso.curso;

        query.from(qCurso)
                .where(qCurso.estado.eq("F").and(
                        qCurso.visible.eq("S").and(qCurso.fechaBaja.isNull())))
                .orderBy(qCurso.fechaCreacion.desc()).limit(numMaxCursosAVisualizar);

        return query.list(qCurso);
    }

    @Override
    public List<Curso> getCursosMasVisitados(int numMaxCursosAVisualizar)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QCurso qCurso = QCurso.curso;
        QCursoIdioma qCursoIdioma = QCursoIdioma.cursoIdioma;
        QIdioma qIdioma = QIdioma.idioma;
        QAccesoCurso qAccesoCurso = QAccesoCurso.accesoCurso;

        query.from(qCurso, qAccesoCurso)
                .where(qCurso.id.eq(qAccesoCurso.cursoId).and(
                        qCurso.estado.eq("F").and(
                                qCurso.visible.eq("S").and(qCurso.fechaBaja.isNull()))))
                .orderBy(qAccesoCurso.numeroAccesos.desc()).limit(numMaxCursosAVisualizar);

        return query.list(qCurso);
    }

    @Override
    public Integer getNumeroTotalPaginas(int numCursosPorPagina)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QCurso qCurso = QCurso.curso;

        query.from(qCurso).where(
                qCurso.estado.eq("F").and(qCurso.visible.eq("S").and(qCurso.fechaBaja.isNull())));

        Long total = query.count();

        Float numPaginas = total.floatValue() / numCursosPorPagina;

        int numPagRed = (int) Math.ceil(numPaginas);

        if (numPagRed == 0)
        {
            numPagRed = 1;
        }

        return numPagRed;
    }

    @Override
    public Integer getNumeroTotalPaginasByCategoriaId(Long categoriaId, int numCursosPorPagina)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QCurso qCurso = QCurso.curso;
        QCursoCategoria qCursoCategoria = QCursoCategoria.cursoCategoria;

        query.from(qCurso)
                .join(qCurso.cursosCategorias, qCursoCategoria)
                .where(qCursoCategoria.categoria.id.eq(categoriaId).and(
                        qCurso.estado.eq("F").and(
                                qCurso.visible.eq("S").and(qCurso.fechaBaja.isNull()))));

        Long total = query.count();

        Float numPaginas = total.floatValue() / numCursosPorPagina;

        int numPagRed = (int) Math.ceil(numPaginas);

        if (numPagRed == 0)
        {
            numPagRed = 1;
        }

        return numPagRed;
    }

    @Override
    public List<Curso> getCursosByMaterialId(Long materialId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QCurso qCurso = QCurso.curso;
        QCursoMaterial qCursoMaterial = QCursoMaterial.cursoMaterial;

        query.from(qCurso)
                .join(qCurso.cursosMateriales, qCursoMaterial)
                .where(qCursoMaterial.material.id.eq(materialId).and(
                        qCurso.estado.eq("F").and(
                                qCurso.visible.eq("S").and(qCurso.fechaBaja.isNull()))));

        return query.list(qCurso);
    }

    @Override
    public Integer getNumeroTotalPaginasBusquedaSimple(String cadenaBusqueda, int numCursosPorPagina)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QCurso curso = QCurso.curso;
        QCursoIdioma cursoIdioma = QCursoIdioma.cursoIdioma;

        query.from(curso)
                .join(curso.cursosIdiomas, cursoIdioma)
                .where(curso.estado.eq("F").and(
                        curso.visible.eq("S").and(
                                curso.fechaBaja.isNull().and(
                                        cursoIdioma.nombre.toLowerCase().like(
                                                "%" + cadenaBusqueda.toLowerCase() + "%")))));

        Integer total = query.distinct().list(curso).size();

        Float numPaginas = total.floatValue() / numCursosPorPagina;

        int numPagRed = (int) Math.ceil(numPaginas);

        return numPagRed;
    }

    @Override
    public List<Curso> getCursosPaginadosBusquedaSimple(String cadenaBusqueda, int pagina,
            int numMaxCursosPorPagina)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QCurso curso = QCurso.curso;
        QCursoIdioma cursoIdioma = QCursoIdioma.cursoIdioma;

        query.from(curso)
                .join(curso.cursosIdiomas, cursoIdioma)
                .where(curso.estado.eq("F").and(
                        curso.visible.eq("S").and(
                                curso.fechaBaja.isNull().and(
                                        cursoIdioma.nombre.toLowerCase().like(
                                                "%" + cadenaBusqueda.toLowerCase() + "%")))));

        query.orderBy(curso.fechaCreacion.desc()).limit(numMaxCursosPorPagina)
                .offset((pagina - 1) * numMaxCursosPorPagina);

        return query.distinct().list(curso);

    }

    @Override
    public List<Curso> getCursosPaginadosByTagId(Long tagId, Integer numPagina,
            int numCursosPorPagina)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QCurso qCurso = QCurso.curso;
        QCursoTag qCursoTag = QCursoTag.cursoTag;

        query.from(qCurso)
                .join(qCurso.cursosTags, qCursoTag)
                .where(qCurso.estado.eq("F").and(
                        qCurso.visible.eq("S").and(
                                qCurso.fechaBaja.isNull().and(qCursoTag.tag.id.eq(tagId)))))
                .orderBy(qCurso.fechaCreacion.desc()).limit(numCursosPorPagina)
                .offset((numPagina - 1) * numCursosPorPagina);

        return query.distinct().list(qCurso);
    }

    @Override
    public Integer getNumeroTotalPaginasByTagId(Long tagId, int numCursosPorPagina)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QCurso qCurso = QCurso.curso;
        QCursoTag qCursoTag = QCursoTag.cursoTag;

        query.from(qCurso)
                .join(qCurso.cursosTags, qCursoTag)
                .where(qCurso.estado.eq("F").and(
                        qCurso.visible.eq("S").and(
                                qCurso.fechaBaja.isNull().and(qCursoTag.tag.id.eq(tagId)))));

        Integer total = query.distinct().list(qCurso).size();

        Float numPaginas = total.floatValue() / numCursosPorPagina;

        int numPagRed = (int) Math.ceil(numPaginas);

        return numPagRed;
    }

    // private void queryBusquedaSimple(JPAQuery query, QCurso curso)
    // {
    // QCursoIdioma cursoIdioma = QCursoIdioma.cursoIdioma;
    // QCursoCategoria cursoCategoria = QCursoCategoria.cursoCategoria;
    // QCategoria categoria = QCategoria.categoria;
    // QCategoriaIdioma categoriaIdioma = QCategoriaIdioma.categoriaIdioma;
    // QCursoTag cursoTag = QCursoTag.cursoTag;
    // QTag tag = QTag.tag;
    //
    // query.from(curso)
    // .join(curso.cursosIdiomas, cursoIdioma)
    // .fetch()
    // .join(curso.cursosCategorias, cursoCategoria)
    // .join(cursoCategoria.categoria, categoria)
    // .join(categoria.categoriasIdiomas, categoriaIdioma)
    // .join(curso.cursosTags, cursoTag)
    // .join(cursoTag.tag, tag)
    // .where(curso.estado
    // .eq("F")
    // .and(curso.visible.eq("S"))
    // .and(curso.fechaBaja.isNull())
    // .and((cursoIdioma.nombre
    // .lower()
    // .like("%" + cadenaBusqueda.toLowerCase() + "%")
    // .or(cursoIdioma.descripcion.lower().like(
    // "%" + cadenaBusqueda.toLowerCase() + "%"))
    // .or(categoriaIdioma.nombre.lower().like(
    // "%" + cadenaBusqueda.toLowerCase() + "%")).or(tag.nombre
    // .lower().like("%" + cadenaBusqueda.toLowerCase() + "%")))))
    // .distinct();
    // }

}