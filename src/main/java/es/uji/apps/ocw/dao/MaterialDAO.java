package es.uji.apps.ocw.dao;

import java.util.List;
import java.util.Map;

import es.uji.apps.ocw.model.CursoMaterial;
import es.uji.apps.ocw.model.Material;
import es.uji.apps.ocw.model.MaterialAutor;
import es.uji.apps.ocw.model.MaterialCompleto;
import es.uji.apps.ocw.model.MaterialTag;
import es.uji.commons.db.BaseDAO;
import es.uji.commons.db.LookupDAO;

public interface MaterialDAO extends BaseDAO, LookupDAO
{
    List<MaterialCompleto> getMateriales();

    List<Material> getMaterialesByPersona(Long personaId);

    List<MaterialTag> getTags(Long materialId);

    List<MaterialCompleto> getMaterialesConFiltros(Map<String, String> filtro);

    List<MaterialAutor> getAutores(Long materialId);

    List<CursoMaterial> getCursos(Long materialId);

    List<Material> getUltimosMateriales(int numMaxMaterialesAVisualizar);

    List<Material> getMaterialesMasVisitados(int numMaxMaterialesAVisualizar);

    List<Material> getTodosMaterialesPaginados(int numMaterialesPorPagina, int pagina);

    Integer getNumeroTotalPaginasMateriales(int numMaterialesPorPagina);

    List<Material> getMaterialesByCursoId(Long cursoId);

    Material getMaterialById(Long materialId);
}
