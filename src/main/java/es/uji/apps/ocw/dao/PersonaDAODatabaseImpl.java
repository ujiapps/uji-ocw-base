package es.uji.apps.ocw.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.ocw.model.Persona;
import es.uji.apps.ocw.model.QPersona;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.model.lookup.LookupItem;

@Repository
public class PersonaDAODatabaseImpl extends BaseDAODatabaseImpl implements PersonaDAO
{

    @Override
    public List<Persona> getPersonaById(Long personaId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QPersona personas = QPersona.persona;

        query.from(personas).where(personas.id.eq(personaId));

        List<Persona> listaPersonas = query.list(personas);

        return listaPersonas;

    }

    @Override
    public List<Persona> getPersonas()
    {
        JPAQuery query = new JPAQuery(entityManager);

        QPersona personas = QPersona.persona;

        query.from(personas).orderBy(personas.apellido1.asc(), personas.apellido2.asc(),
                personas.nombre.asc());

        return query.list(personas);
    }

    @Override
    public List<LookupItem> search(String query)
    {
        JPAQuery queryPersonas = new JPAQuery(entityManager);

        QPersona personas = QPersona.persona;

        List<LookupItem> result = new ArrayList<LookupItem>();

        if (query != null && !query.isEmpty())
        {
            queryPersonas.from(personas).where(
                    personas.id.stringValue().concat(" ").concat(personas.nombre).concat(" ")
                            .concat(personas.apellido1).concat(" ").concat(personas.apellido2)
                            .lower().like("%" + query.toLowerCase() + "%")).orderBy(personas.apellido1.asc(), personas.apellido2.asc(), personas.nombre.asc());

            List<Persona> listaPersonas = queryPersonas.list(personas);

            for (Persona persona : listaPersonas)
            {
                LookupItem lookupItem = new LookupItem();
                lookupItem.setId(String.valueOf(persona.getId()));
                lookupItem.setNombre(persona.getApellido1() + " " + persona.getApellido2() + ", "
                        + persona.getNombre());

                result.add(lookupItem);
            }
        }

        return result;
    }
}