package es.uji.apps.ocw;

@SuppressWarnings("serial")
public class CursoAutorizadoExistenteException extends GeneralOCWException
{
    public CursoAutorizadoExistenteException(String msg)
    {
        super(msg);
    }

    public CursoAutorizadoExistenteException()
    {
        super();
    }
}
