package es.uji.apps.ocw;

@SuppressWarnings("serial")
public class CursoAsignaturaExistenteException extends GeneralOCWException
{
    public CursoAsignaturaExistenteException(String msg)
    {
        super(msg);
    }

    public CursoAsignaturaExistenteException()
    {
        super();
    }
}
