package es.uji.apps.ocw;

@SuppressWarnings("serial")
public class EmailNoUjiException extends GeneralOCWException
{
    public EmailNoUjiException(String msg)
    {
        super(msg);
    }

    public EmailNoUjiException()
    {
        super();
    }
}
