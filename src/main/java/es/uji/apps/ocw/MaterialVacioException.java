package es.uji.apps.ocw;

@SuppressWarnings("serial")
public class MaterialVacioException extends GeneralOCWException
{
    public MaterialVacioException(String msg)
    {
        super(msg);
    }

    public MaterialVacioException()
    {
        super("El material pujat està buit");
    }
}
