package es.uji.apps.ocw.services.rss;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import es.uji.apps.ocw.model.Curso;
import es.uji.apps.ocw.model.CursoMultiIdioma;

public class CursoRSS
{
    private Date fechaCreacion;
    private String persona;
    private String titulo;
    private String descripcion;
    private String url;
    private List<String> categorias;
    private List<String> tags;
    private OCWEnclosure imagen;

    public Date getFechaCreacion()
    {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion)
    {
        this.fechaCreacion = fechaCreacion;
    }

    public String getPersona()
    {
        return persona;
    }

    public void setPersona(String persona)
    {
        this.persona = persona;
    }

    public String getTitulo()
    {
        return titulo;
    }

    public void setTitulo(String titulo)
    {
        this.titulo = titulo;
    }

    public String getDescripcion()
    {
        return descripcion;
    }

    public void setDescripcion(String descripcion)
    {
        this.descripcion = descripcion;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public static List<CursoRSS> getUltimosCincoCursosRSS()
    {
        String idioma = getIdioma("CA");

        List<Curso> listaUltimosCursos = Curso.getUltimosCincoCursos();
        List<Long> listaCursosIds = extraeIdsDeCursos(listaUltimosCursos);
        List<CursoMultiIdioma> listaCursosMultiIdioma = CursoMultiIdioma.getCursosByIdsAndIdioma(
                listaCursosIds, idioma);
        List<CursoRSS> listaCursosRSS = cursosMultiIdiomaToRSS(listaUltimosCursos,
                listaCursosMultiIdioma);

        return listaCursosRSS;
    }

    public static List<CursoRSS> getCincoCursosMasVisitadosRSS()
    {
        String idioma = getIdioma("CA");

        List<Curso> listaCursos = Curso.getCincoCursosMasVisitados();
        List<Long> listaCursosIds = extraeIdsDeCursos(listaCursos);
        List<CursoMultiIdioma> listaCursosMultiIdioma = CursoMultiIdioma.getCursosByIdsAndIdioma(
                listaCursosIds, idioma);
        List<CursoRSS> listaCursosRSS = cursosMultiIdiomaToRSS(listaCursos, listaCursosMultiIdioma);

        return listaCursosRSS;
    }

    private static String getIdioma(String cookieIdioma)
    {
        if (cookieIdioma != null)
        {
            return cookieIdioma.toUpperCase();
        }
        else
        {
            return "CA";
        }
    }

    private static List<Long> extraeIdsDeCursos(List<Curso> listaUltimosCursos)
    {
        List<Long> listaCursosIds = new ArrayList<Long>();
        for (Curso curso : listaUltimosCursos)
        {
            listaCursosIds.add(curso.getId());
        }

        return listaCursosIds;
    }

    private static List<CursoRSS> cursosMultiIdiomaToRSS(List<Curso> listaCursos,
            List<CursoMultiIdioma> listaCursosMultiIdioma)
    {

        Map<Long, CursoRSS> hashCursosRSS = new HashMap<Long, CursoRSS>();

        for (CursoMultiIdioma cursoMultiIdioma : listaCursosMultiIdioma)
        {
            CursoRSS cursoRSS = hashCursosRSS.get(cursoMultiIdioma.getId());
            if (cursoRSS == null)
            {
                cursoRSS = new CursoRSS();

                cursoRSS.setTitulo(cursoMultiIdioma.getNombre());
                cursoRSS.setDescripcion(cursoMultiIdioma.getDescripcion());
                cursoRSS.setFechaCreacion(cursoMultiIdioma.getFechaCreacion());
                cursoRSS.setPersona(cursoMultiIdioma.getCreador());
                cursoRSS.setCategorias(new ArrayList<String>());
                cursoRSS.setTags(new ArrayList<String>());
                cursoRSS.setUrl(MessageFormat.format("http://ocw.uji.es/curso/{0,number,#}", cursoMultiIdioma.getId()));

                hashCursosRSS.put(cursoMultiIdioma.getId(), cursoRSS);
                OCWEnclosure imagen = new OCWEnclosure();
                if (cursoMultiIdioma.getImageId() == null)
                {
                    imagen.setUrl("http://ujiapps.uji.es/ocw/img/uji.png");
                    imagen.setType("image/png");
                }
                else
                {
                    imagen.setType(cursoMultiIdioma.getContenTypeImagen());
                    imagen.setUrl(MessageFormat.format(
                            "http://ujiapps.uji.es/ocw/curso/{0,number,#}/imagen/{1,number,#}",
                            cursoMultiIdioma.getId(), cursoMultiIdioma.getImageId()));
                }
                cursoRSS.setImagen(imagen);
            }

            String categoriaNombre = cursoMultiIdioma.getCategoriaNombre();
            String tagNombre = cursoMultiIdioma.getTagNombre();

            if (categoriaNombre != null && !cursoRSS.getCategorias().contains(categoriaNombre))
            {
                cursoRSS.getCategorias().add(categoriaNombre);
            }

            if (tagNombre != null && !cursoRSS.getTags().contains(tagNombre))
            {
                cursoRSS.getTags().add(tagNombre);
            }
        }

        List<CursoRSS> listaCursosRSS = new ArrayList<CursoRSS>();
        for (Curso curso : listaCursos)
        {
            listaCursosRSS.add(hashCursosRSS.get(curso.getId()));
        }

        return listaCursosRSS;

    }

    public List<String> getCategorias()
    {
        return categorias;
    }

    public void setCategorias(List<String> categorias)
    {
        this.categorias = categorias;
    }

    public List<String> getTags()
    {
        return tags;
    }

    public void setTags(List<String> tags)
    {
        this.tags = tags;
    }

    public OCWEnclosure getImagen()
    {
        return imagen;
    }

    public void setImagen(OCWEnclosure imagen)
    {
        this.imagen = imagen;
    }
}