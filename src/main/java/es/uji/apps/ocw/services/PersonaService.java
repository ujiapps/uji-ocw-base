package es.uji.apps.ocw.services;

import java.text.MessageFormat;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import es.uji.apps.ocw.model.Persona;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

@Path("persona")
public class PersonaService extends CoreBaseService
{
    public static Logger log = Logger.getLogger(PersonaService.class);

    @GET
    @Produces(MediaType.APPLICATION_XML)
    public List<UIEntity> getPersonas()
    {
        List<Persona> personas = Persona.getPersonas();
        List<UIEntity> listaPersonas = UIEntity.toUI(personas);

        for (UIEntity persona : listaPersonas)
        {
            persona.put(
                    "nombreCompleto",
                    MessageFormat.format("{0} {1}, {2}", persona.get("apellido1"),
                            persona.get("apellido2"), persona.get("nombre")));
        }

        return listaPersonas;
    }
}
