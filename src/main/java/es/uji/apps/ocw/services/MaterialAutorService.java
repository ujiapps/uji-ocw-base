package es.uji.apps.ocw.services;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import es.uji.apps.ocw.model.Autor;
import es.uji.apps.ocw.model.AutorCompleto;
import es.uji.apps.ocw.model.Material;
import es.uji.apps.ocw.model.MaterialAutor;
import es.uji.apps.ocw.model.Persona;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import es.uji.commons.sso.AccessManager;
import es.uji.commons.sso.exceptions.UnauthorizedUserException;

@Path("materialAutor")
public class MaterialAutorService extends CoreBaseService

{
    @GET
    @Produces(MediaType.TEXT_XML)
    public List<UIEntity> getAutoresByMaterial(@QueryParam("materialId") String materialId)
            throws UnauthorizedUserException, RegistroNoEncontradoException
    {
        List<UIEntity> lista = new ArrayList<UIEntity>();

        Material material = Material.getMaterialById(Long.parseLong(materialId));

        if (material != null)
        {
            List<MaterialAutor> listaMaterialAutores = material.getAutores();

            for (MaterialAutor materialAutor : listaMaterialAutores)
            {
                UIEntity entity = UIEntity.toUI(materialAutor);
                AutorCompleto autor = Autor.getAutorCompletoById(materialAutor.getAutor().getId());
                entity.put("nombre", autor.getNombre());
                entity.put("apellido1", autor.getApellido1());
                entity.put("apellido2", autor.getApellido2());
                entity.put("email", autor.getEmail());
                entity.put("autorId", autor.getId());
                entity.put("materialId", materialAutor.getMaterial().getId());
                lista.add(entity);
            }
        }

        return lista;
    }

    @POST
    @Produces(MediaType.TEXT_XML)
    public List<UIEntity> insertAutorToMaterial(UIEntity entity) throws UnauthorizedUserException,
            RegistroNoEncontradoException
    {
        Persona persona = new Persona(AccessManager.getConnectedUserId(request));

        Material material = Material.getMaterialById(Long.parseLong(entity.get("materialId")));
        Autor autor = Autor.getAutorById(Long.parseLong(entity.get("autorId")));
        MaterialAutor materialAutor = material.insertAutor(autor, persona);

        List<UIEntity> lista = new ArrayList<UIEntity>();

        UIEntity returnEntity = UIEntity.toUI(materialAutor);
        AutorCompleto autorCompleto = Autor.getAutorCompletoById(materialAutor.getAutor().getId());
        returnEntity.put("nombre", autorCompleto.getNombre());
        returnEntity.put("apellido1", autorCompleto.getApellido1());
        returnEntity.put("apellido2", autorCompleto.getApellido2());
        returnEntity.put("email", autorCompleto.getEmail());
        returnEntity.put("autorId", autorCompleto.getId());
        returnEntity.put("materialId", materialAutor.getMaterial().getId());
        lista.add(returnEntity);

        return lista;

    }

    @DELETE
    @Path("{materialAutorId}")
    @Produces(MediaType.TEXT_XML)
    public ResponseMessage deleteAutorFromMaterial(
            @PathParam("materialAutorId") String materialAutorId, UIEntity entity)
            throws UnauthorizedUserException, RegistroNoEncontradoException
    {
        Persona persona = new Persona(AccessManager.getConnectedUserId(request));

        Material material = Material.getMaterialById(Long.parseLong(entity.get("materialId")));

        material.deleteAutor(Long.parseLong(materialAutorId), persona);

        return new ResponseMessage(true, "El registre s'ha esborrat satisfactoriament");
    }
}
