package es.uji.apps.ocw.services;

import com.sun.jersey.multipart.BodyPart;
import com.sun.jersey.multipart.BodyPartEntity;
import com.sun.jersey.multipart.FormDataMultiPart;
import es.uji.apps.ocw.MaterialVacioException;
import es.uji.apps.ocw.model.*;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.ParametrosObligatoriosException;
import es.uji.commons.rest.exceptions.RegistroConHijosException;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import es.uji.commons.sso.AccessManager;
import es.uji.commons.sso.exceptions.UnauthorizedUserException;
import org.springframework.transaction.annotation.Transactional;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Path("material")
public class MaterialService extends CoreBaseService
{

    private String getParamString(FormDataMultiPart multiPart, String param)
    {
        try
        {
            return multiPart.getField(param).getValue();
        }
        catch (NullPointerException e)
        {
            return "";
        }
    }

    private byte[] inputStreamToByteArray(InputStream inputStream)
    {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        int size = 1024;
        byte[] buffer = new byte[size];
        int len;

        try
        {
            while ((len = inputStream.read(buffer, 0, size)) != -1)
            {
                outputStream.write(buffer, 0, len);
            }

            return outputStream.toByteArray();
        }
        catch (Exception e)
        {
        }

        return null;
    }

    @GET
    @Produces(MediaType.TEXT_XML)
    public List<UIEntity> getMaterialesConFiltros(@QueryParam("cadena") String cadena,
            @QueryParam("idiomaId") String idiomaId)
    {
        Map<String, String> filtro = new HashMap<String, String>();

        if (cadena != null && !cadena.isEmpty())
        {
            filtro.put("cadena", cadena);
        }

        if (idiomaId != null && !idiomaId.isEmpty())
        {
            filtro.put("idiomaId", idiomaId);
        }

        Persona persona = new Persona(AccessManager.getConnectedUserId(request));

        return UIEntity.toUI(Material.getMaterialesConFiltros(filtro, persona));
    }

    @GET
    @Path("{materialId}")
    @Produces(MediaType.TEXT_XML)
    public List<UIEntity> getMaterialById(@PathParam("materialId") String materialId)
            throws UnauthorizedUserException, RegistroNoEncontradoException
    {
        List<UIEntity> result = new ArrayList<UIEntity>();
        Material material = Material.getMaterialById(Long.parseLong(materialId));

        if (material != null)
        {
            UIEntity entity = UIEntity.toUI(material);
            entity.put("idiomaId", material.getIdioma().getId());
            entity.put("licenciaId", material.getLicencia().getId());
            entity.put("tags", material.getTagsEnCadena());

            DateFormat formatter = new SimpleDateFormat("dd/M/yyyy");
            entity.put("fecha", formatter.format(material.getFecha()));

            result.add(entity);
        }

        return result;
    }

    @GET
    @Path("{materialId}/cursos")
    @Produces(MediaType.TEXT_XML)
    public List<UIEntity> getCursoByMaterialId(@PathParam("materialId") String materialId)
            throws UnauthorizedUserException, RegistroNoEncontradoException
    {
        List<UIEntity> result = new ArrayList<UIEntity>();
        Material material = Material.getMaterialById(Long.parseLong(materialId));

        Persona persona = new Persona(AccessManager.getConnectedUserId(request));

        if (material != null)
        {
            List<CursoMaterial> listaCursoMaterial = material.getCursos();

            for (CursoMaterial cursoMaterial : listaCursoMaterial)
            {

                CursoIdiomaPrincipal cursoIdiomaPrincipal = cursoMaterial.getCurso()
                        .getCursoIdiomaPrincipal();

                UIEntity entity = UIEntity.toUI(cursoIdiomaPrincipal);

                entity.put("cursoId", cursoIdiomaPrincipal.getId());
                entity.put("nombre", cursoIdiomaPrincipal.getNombre());
                entity.put("estado", cursoIdiomaPrincipal.getEstado());
                entity.put("visible", cursoIdiomaPrincipal.getVisible());
                entity.put("fechaCreacion", cursoIdiomaPrincipal.getFechaCreacion());

                Persona personaCrea = Persona.getPersonaById(cursoIdiomaPrincipal.getPersonaId());
                entity.put("persona", personaCrea.getNombre() + " " + personaCrea.getApellido1()
                        + " " + personaCrea.getApellido2());

                result.add(entity);

            }
        }

        return result;
    }

    @DELETE
    @Path("{materialId}")
    @Consumes(MediaType.TEXT_XML)
    @Transactional(rollbackFor = Exception.class)
    public void borraMaterial(@PathParam("materialId") String materialId)
            throws RegistroConHijosException, RegistroNoEncontradoException,
            UnauthorizedUserException
    {
        Persona persona = new Persona(AccessManager.getConnectedUserId(request));

        Material material = Material.getMaterialById(Long.parseLong(materialId));
        try
        {
            material.deleteTags(persona);
            material.delete(persona);
        }
        catch (Exception e)
        {
            throw new RegistroConHijosException();
        }
    }

    @GET
    @Path("{materialId}/raw")
    public Response getMaterialDocumento(@PathParam("materialId") String materialId)
            throws RegistroNoEncontradoException, Exception
    {
        Material material = Material.getMaterialConFicheroById(Long.parseLong(materialId));

        byte[] documento = material.getDocumento();
        if (documento == null)
        {
            throw new MaterialVacioException();
        }

        return Response
                .ok(documento)
                .header("Content-Disposition",
                        "attachment; filename = " + material.getNombreFichero())
                .header("Content-Length", documento.length).type(material.getContentType()).build();
    }

    @DELETE
    @Path("{materialId}/adjunto")
    public ResponseMessage deleteMaterialDocumento(@PathParam("materialId") String materialId)
            throws RegistroNoEncontradoException, Exception
    {
        Persona persona = new Persona(AccessManager.getConnectedUserId(request));

        Material material = Material.getMaterialById(Long.parseLong(materialId));
        material.borraAdjunto(persona);
        return new ResponseMessage(true);
    }

    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.TEXT_XML)
    @Transactional
    public List<UIEntity> postMaterial(FormDataMultiPart multiPart)
            throws ParametrosObligatoriosException, UnauthorizedUserException,
            RegistroNoEncontradoException
    {
        String cadenaId = multiPart.getField("id").getValue();
        List<UIEntity> listaUIEntity = new ArrayList<UIEntity>();

        String titulo = getParamString(multiPart, "titulo");
        String idiomaId = multiPart.getField("idiomaId").getValue();
        String licenciaId = multiPart.getField("licenciaId").getValue();

        if (titulo.isEmpty())
        {
            throw new ParametrosObligatoriosException("titulo");
        }
        else if (idiomaId.isEmpty())
        {
            throw new ParametrosObligatoriosException("idiomaId");

        }
        else if (licenciaId.isEmpty())
        {
            throw new ParametrosObligatoriosException("licenciaId");

        }

        if (cadenaId.isEmpty())
        {
            listaUIEntity = insertMaterial(multiPart);
        }
        else
        {
            listaUIEntity = updateMaterial(multiPart);
        }

        return listaUIEntity;
    }

    private List<UIEntity> insertMaterial(FormDataMultiPart multiPart)
            throws UnauthorizedUserException, RegistroNoEncontradoException, NumberFormatException
    {

        Persona persona = new Persona(AccessManager.getConnectedUserId(request));

        Material material = new Material();

        String titulo = getParamString(multiPart, "titulo");
        String descripcion = getParamString(multiPart, "descripcion");
        String url = getParamString(multiPart, "url");
        String fecha = getParamString(multiPart, "fecha");

        Long personaId = AccessManager.getConnectedUserId(request);
        String idiomaId = multiPart.getField("idiomaId").getValue();
        String licenciaId = multiPart.getField("licenciaId").getValue();
        String etiquetas = multiPart.getField("tags").getValue();

        Idioma idioma = Idioma.getIdiomaById(Long.parseLong(idiomaId));
        Licencia licencia = Licencia.getLicenciaById(Long.parseLong(licenciaId));

        try
        {
            DateFormat formatter = new SimpleDateFormat("dd/M/yyyy");
            Date date = (Date) formatter.parse(fecha);
            material.setFecha(date);
        }
        catch (ParseException e)
        {
            material.setFecha(new Date());
        }

        material.setTitulo(titulo);
        material.setPersonaId(personaId);
        material.setIdioma(idioma);
        material.setLicencia(licencia);

        material.setUrl(url);
        material.setDescripcion(descripcion);

        for (BodyPart bodyPart : multiPart.getBodyParts())
        {
            String mimeType = bodyPart.getHeaders().getFirst("Content-Type");
            if (mimeType != null && !mimeType.isEmpty())
            {
                String fileName = "";
                String header = bodyPart.getHeaders().getFirst("Content-Disposition");
                Pattern fileNamePattern = Pattern.compile(".*filename=\"(.*)\"");
                Matcher m = fileNamePattern.matcher(header);
                if (m.matches())
                {
                    fileName = m.group(1);
                }
                BodyPartEntity bpe = (BodyPartEntity) bodyPart.getEntity();
                InputStream documento = bpe.getInputStream();
                material.setContentType(mimeType);
                material.setNombreFichero(fileName);
                material.setDocumento(inputStreamToByteArray(documento));
                break;
            }
        }

        material = material.insert();
        material.parseaTagsByString(etiquetas, persona);

        UIEntity uiEntity = UIEntity.toUI(material);
        return Collections.singletonList(uiEntity);
    }

    private List<UIEntity> updateMaterial(FormDataMultiPart multiPart)
            throws UnauthorizedUserException, RegistroNoEncontradoException
    {

        Persona persona = new Persona(AccessManager.getConnectedUserId(request));

        Long id = Long.parseLong(multiPart.getField("id").getValue());
        Material material = Material.getMaterialById(id);

        String titulo = getParamString(multiPart, "titulo");
        String descripcion = getParamString(multiPart, "descripcion");
        String url = getParamString(multiPart, "url");
        String fecha = getParamString(multiPart, "fecha");

        String idiomaId = multiPart.getField("idiomaId").getValue();
        String licenciaId = multiPart.getField("licenciaId").getValue();
        String etiquetas = multiPart.getField("tags").getValue();

        Idioma idioma = Idioma.getIdiomaById(Long.parseLong(idiomaId));
        Licencia licencia = Licencia.getLicenciaById(Long.parseLong(licenciaId));

        try
        {
            DateFormat formatter = new SimpleDateFormat("dd/M/yyyy");
            Date date = (Date) formatter.parse(fecha);
            material.setFecha(date);
        }
        catch (ParseException e)
        {
            material.setFecha(new Date());
        }

        material.setTitulo(titulo);
        material.setIdioma(idioma);
        material.setLicencia(licencia);

        material.setDescripcion(descripcion);

        if (!url.isEmpty())
        {
            material.setUrl(url);
            material.setContentType(null);
            material.setNombreFichero(null);
            material.setDocumento(null);
        }
        else
        {
            material.setUrl(null);
            for (BodyPart bodyPart : multiPart.getBodyParts())
            {
                String mimeType = bodyPart.getHeaders().getFirst("Content-Type");
                if (mimeType != null && !mimeType.isEmpty())
                {
                    String fileName = "";
                    String header = bodyPart.getHeaders().getFirst("Content-Disposition");
                    Pattern fileNamePattern = Pattern.compile(".*filename=\"(.*)\"");
                    Matcher m = fileNamePattern.matcher(header);
                    if (m.matches())
                    {
                        fileName = m.group(1);
                    }

                    if (!fileName.isEmpty())
                    {
                        BodyPartEntity bpe = (BodyPartEntity) bodyPart.getEntity();
                        InputStream documento = bpe.getInputStream();
                        material.setContentType(mimeType);
                        material.setNombreFichero(fileName);
                        material.setDocumento(inputStreamToByteArray(documento));
                    }
                    break;
                }
            }
        }

        material.update(persona);
        material.parseaTagsByString(etiquetas, persona);

        UIEntity uiEntity = UIEntity.toUI(material);
        return Collections.singletonList(uiEntity);
    }
}
