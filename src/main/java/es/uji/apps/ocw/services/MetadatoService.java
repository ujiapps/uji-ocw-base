package es.uji.apps.ocw.services;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import es.uji.apps.ocw.model.Idioma;
import es.uji.apps.ocw.model.MaterialMetadato;
import es.uji.apps.ocw.model.Metadato;
import es.uji.apps.ocw.model.MetadatoIdioma;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.ParametrosObligatoriosException;
import es.uji.commons.rest.exceptions.RegistroConHijosException;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

@Path("metadato")
public class MetadatoService extends CoreBaseService
{
    @GET
    @Produces(MediaType.TEXT_XML)
    public List<UIEntity> getMetadatos()
    {
        List<Metadato> lista = Metadato.getMetadatos();

        List<UIEntity> listaUI = new ArrayList<UIEntity>();

        for (Metadato metadato : lista)
        {
            UIEntity metadatoUI = UIEntity.toUI(metadato);
            setAtributosMultidioma(metadato, metadatoUI);
            listaUI.add(metadatoUI);
        }

        return listaUI;
    }

    @POST
    @Produces(MediaType.TEXT_XML)
    public UIEntity insertMetadato(UIEntity entity) throws ParametrosObligatoriosException
    {
        if (entity.get("tag") == null)
        {
            return new UIEntity();
        }
        
        List<Idioma> idiomas = Idioma.getIdiomasEdicion();

        String nombreIdiomaPrincipal = null;
        try
        {
            nombreIdiomaPrincipal = entity.get("nombre__"
                    + idiomas.get(0).getAcronimo().toLowerCase());
        }
        catch (Exception e)
        {
        }

        Metadato.compruebaCamposObligatorios(entity.get("tag"), nombreIdiomaPrincipal);

        Metadato metadato = new Metadato();
        metadato.setLimitado((entity.get("limitado").equals("true") ? "S" : "N"));
        metadato.setObligatorio((entity.get("obligatorio").equals("true") ? "S" : "N"));
        metadato.setTag(entity.get("tag"));

        Set<MetadatoIdioma> metadatoIdiomas = new HashSet<MetadatoIdioma>();

        for (Idioma idioma : idiomas)
        {
            String nombre = entity.get("nombre__" + idioma.getAcronimo().toLowerCase());

            if (nombre != null)
            {
                MetadatoIdioma metadatoIdioma = new MetadatoIdioma();
                metadatoIdioma.setNombre(nombre);
                metadatoIdioma.setIdioma(idioma);
                metadatoIdioma.setMetadato(metadato);
                metadatoIdiomas.add(metadatoIdioma);
            }
        }

        metadato.setMetadatosIdiomas(metadatoIdiomas);

        metadato = metadato.insert();
        UIEntity metadatoUI = UIEntity.toUI(metadato);
        setAtributosMultidioma(metadato, metadatoUI);

        return metadatoUI;
    }

    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_XML)
    @Consumes(MediaType.TEXT_XML)
    public ResponseMessage delete(@PathParam("id") String id) throws RegistroNoEncontradoException,
            RegistroConHijosException, NumberFormatException
    {
        Metadato.delete(Long.parseLong(id));
        return new ResponseMessage(true);
    }

    @PUT
    @Path("{id}")
    @Consumes(MediaType.TEXT_XML)
    @Produces(MediaType.TEXT_XML)
    public ResponseMessage update(@PathParam("id") String id, UIEntity entity)
            throws RegistroNoEncontradoException, ParametrosObligatoriosException
    {
        Metadato metadato = Metadato.getMetadatoById(Long.parseLong(id));

        List<Idioma> idiomas = Idioma.getIdiomasEdicion();

        String nombreIdiomaPrincipal = null;
        try
        {
            nombreIdiomaPrincipal = entity.get("nombre__"
                    + idiomas.get(0).getAcronimo().toLowerCase());
        }
        catch (Exception e)
        {
        }

        Metadato.compruebaCamposObligatorios(entity.get("tag"), nombreIdiomaPrincipal);

        metadato.setLimitado((entity.get("limitado").equals("true") ? "S" : "N"));
        metadato.setObligatorio((entity.get("obligatorio").equals("true") ? "S" : "N"));
        metadato.setTag(entity.get("tag"));

        Set<MetadatoIdioma> metadatoIdiomas = metadato.getMetadatosIdiomas();

        boolean exists = false;
        for (Idioma idioma : idiomas)
        {
            String nombre = entity.get("nombre__" + idioma.getAcronimo().toLowerCase());

            for (MetadatoIdioma metadatoIdioma : metadatoIdiomas)
            {
                if (metadatoIdioma.getIdioma().getAcronimo().equals(idioma.getAcronimo()))
                {
                    metadatoIdioma.setNombre(nombre);
                    exists = true;
                }
            }
            if (!exists && nombre != null && !nombre.equals("") )
            {
                MetadatoIdioma metadatoIdioma = new MetadatoIdioma();
                metadatoIdioma.setNombre(nombre);
                metadatoIdioma.setIdioma(idioma);
                metadatoIdioma.setMetadato(metadato);
                metadatoIdiomas.add(metadatoIdioma);
            }
            exists = false;
        }

        metadato.setMetadatosIdiomas(metadatoIdiomas);

        metadato.update();

        return new ResponseMessage(true);
    }

    @GET
    @Path("{materialId}")
    @Produces(MediaType.TEXT_XML)
    public List<UIEntity> getMetadatoByMaterial(@PathParam("materialId") Long materialId)
    {
        List<MaterialMetadato> lista = Metadato.getMetadatosByMaterial(materialId);
        List<UIEntity> listaUI = UIEntity.toUI(lista);

        return listaUI;

    }

    private void setAtributosMultidioma(Metadato metadato, UIEntity entity)
    {
        for (MetadatoIdioma metadatoIdioma : metadato.getMetadatosIdiomas())
        {
            if (metadatoIdioma.getNombre() != null && !metadatoIdioma.getNombre().equals(""))
            {
                entity.put(metadatoIdioma.getIdioma().getAcronimo(), "nombre",
                        metadatoIdioma.getNombre());
            }
        }
    }
}
