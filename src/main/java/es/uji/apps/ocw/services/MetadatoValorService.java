package es.uji.apps.ocw.services;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import es.uji.apps.ocw.model.Metadato;
import es.uji.apps.ocw.model.MetadatoValor;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.ParametrosObligatoriosException;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

@Path("metadatoValor")
public class MetadatoValorService extends CoreBaseService

{
    @GET
    @Produces(MediaType.TEXT_XML)
    public List<UIEntity> getMetadatoValoresByMetadato(@QueryParam("metadatoId") String metadatoId)
    {
        List<UIEntity> lista = new ArrayList<UIEntity>();

        try
        {
            List<MetadatoValor> listaMetadatoValores = MetadatoValor
                    .getMetadatoValoresByMetadatoId(Long.parseLong(metadatoId));

            for (MetadatoValor metadatoValor : listaMetadatoValores)
            {
                UIEntity entity = UIEntity.toUI(metadatoValor);
                entity.put("metadatoId", metadatoId);
                lista.add(entity);
            }
        }
        catch (Exception e)
        {
            lista = new ArrayList<UIEntity>();
        }

        return lista;
    }

    @POST
    @Produces(MediaType.TEXT_XML)
    public UIEntity insertMetadatoValor(UIEntity entity) throws RegistroNoEncontradoException,
            NumberFormatException, ParametrosObligatoriosException
    {
        Metadato metadato = Metadato.getMetadatoById(getMetadatoId(entity));
        MetadatoValor metadatoValor = entity.toModel(MetadatoValor.class);

        if (metadatoValor.getValor() == null && metadatoValor.getOrden() == null)
        {
            return new UIEntity();
        }

        metadatoValor.setMetadato(metadato);
        metadatoValor = metadatoValor.insert();

        return UIEntity.toUI(metadatoValor);
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.TEXT_XML)
    public ResponseMessage updateMetadatoValor(@PathParam("id") String id, UIEntity entity)
            throws RegistroNoEncontradoException, NumberFormatException, ParametrosObligatoriosException
    {
        Metadato metadato = Metadato.getMetadatoById(getMetadatoId(entity));
        MetadatoValor metadatoValor = MetadatoValor.getMetadatoValor(Long.parseLong(id));

        metadatoValor.setMetadato(metadato);
        metadatoValor.setValor(entity.get("valor"));
        metadatoValor.setOrden(Long.parseLong(entity.get("orden")));

        metadatoValor.update();

        return new ResponseMessage(true);
    }

    @DELETE
    @Path("{id}")
    @Produces(MediaType.TEXT_XML)
    public ResponseMessage deleteMetadatoValor(@PathParam("id") String id, UIEntity entity)
            throws RegistroNoEncontradoException
    {
        MetadatoValor.delete(Long.parseLong(id));

        return new ResponseMessage(true);
    }

    private static Long getMetadatoId(UIEntity entity)
    {
        Long metadatoId = new Long(0);

        try
        {
            metadatoId = Long.parseLong(entity.get("metadatoId"));
        }
        catch (Exception e)
        {
        }

        return metadatoId;
    }
}