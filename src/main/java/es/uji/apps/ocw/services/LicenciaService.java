package es.uji.apps.ocw.services;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.multipart.BodyPart;
import com.sun.jersey.multipart.FormDataMultiPart;

import es.uji.apps.ocw.model.Idioma;
import es.uji.apps.ocw.model.Licencia;
import es.uji.apps.ocw.model.LicenciaIdioma;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.ParametrosObligatoriosException;
import es.uji.commons.rest.exceptions.RegistroConHijosException;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

@Path("licencia")
public class LicenciaService extends CoreBaseService
{
    private String getParamString(FormDataMultiPart multiPart, String param)
    {
        try
        {
            return multiPart.getField(param).getValue();
        }
        catch (NullPointerException e)
        {
            return "";
        }
    }

    @GET
    @Produces(MediaType.TEXT_XML)
    public List<UIEntity> getLicencias()
    {
        List<UIEntity> result = new ArrayList<UIEntity>();

        List<Licencia> listaLicencias = Licencia.getLicencias();

        for (Licencia licencia : listaLicencias)
        {
            UIEntity licenciaUI = UIEntity.toUI(licencia);
            if (licencia.getImagen() == null)
            {
                licenciaUI.put("imagen", "No");
            }
            else
            {
                licenciaUI.put("imagen", "Si");
            }
            setAtributosMultidioma(licencia, licenciaUI);

            result.add(licenciaUI);
        }

        return result;
    }

    @POST
    @Produces(MediaType.TEXT_XML)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public UIEntity insertLicencia(FormDataMultiPart multiPart)
            throws ParametrosObligatoriosException, RegistroNoEncontradoException
    {
        Licencia licencia = new Licencia();

        licencia.setCodigo(multiPart.getField("codigo").getValue());

        Set<LicenciaIdioma> licenciasIdiomas = new HashSet<LicenciaIdioma>();

        List<Idioma> idiomas = Idioma.getIdiomasEdicion();

        for (Idioma idioma : idiomas)
        {
            String titulo = getParamString(multiPart, "titulo__"
                    + idioma.getAcronimo().toLowerCase());
            String url = getParamString(multiPart, "url__" + idioma.getAcronimo().toLowerCase());

            if (!titulo.equals("") || !url.equals(""))
            {
                LicenciaIdioma licenciaIdioma = new LicenciaIdioma();
                licenciaIdioma.setTitulo(titulo);
                licenciaIdioma.setUrl(url);
                licenciaIdioma.setIdioma(idioma);
                licenciaIdioma.setLicencia(licencia);
                licenciasIdiomas.add(licenciaIdioma);
            }
        }
        licencia.setLicenciasIdiomas(licenciasIdiomas);

        licencia.setImagen(inputStreamToByteArray(multiPart.getField("imagen").getEntityAs(
                InputStream.class)));

        if (licencia.getImagen() != null)
        {
            for (BodyPart bodyPart : multiPart.getBodyParts())
            {
                String mimeType = bodyPart.getHeaders().getFirst("Content-Type");
                if (mimeType != null && !mimeType.isEmpty())
                {
                    licencia.setContentType(mimeType);
                    break;
                }
            }
        }

        licencia = licencia.insert();

        UIEntity entity = UIEntity.toUI(licencia);

        return entity;
    }

    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_XML)
    public ResponseMessage delete(@PathParam("id") String id) throws RegistroNoEncontradoException,
            RegistroConHijosException
    {
        Licencia.delete(Long.parseLong(id));
        return new ResponseMessage(true);
    }

    @POST
    @Path("{id}")
    @Produces(MediaType.TEXT_XML)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public ResponseMessage update(@PathParam("id") String id, FormDataMultiPart multiPart)
            throws RegistroNoEncontradoException, IOException, ParametrosObligatoriosException
    {
        Licencia licencia = Licencia.getLicencia(Long.parseLong(id));

        licencia.setCodigo(getParamString(multiPart, "codigo"));

        Set<LicenciaIdioma> licenciaIdiomas = licencia.getLicenciasIdiomas();
        List<Idioma> idiomas = Idioma.getIdiomasEdicion();

        boolean exists = false;
        for (Idioma idioma : idiomas)
        {
            String titulo = getParamString(multiPart, "titulo__"
                    + idioma.getAcronimo().toLowerCase());
            String url = getParamString(multiPart, "url__" + idioma.getAcronimo().toLowerCase());

            for (LicenciaIdioma licenciaIdioma : licenciaIdiomas)
            {
                if (licenciaIdioma.getIdioma().getAcronimo().equals(idioma.getAcronimo()))
                {
                    licenciaIdioma.setTitulo(titulo);
                    licenciaIdioma.setUrl(url);
                    exists = true;
                    break;
                }
            }
            if (!exists)
            {
                if (!titulo.equals("") || !url.equals(""))
                {
                    LicenciaIdioma licenciaIdioma = new LicenciaIdioma();
                    licenciaIdioma.setTitulo(titulo);
                    licenciaIdioma.setUrl(url);
                    licenciaIdioma.setIdioma(idioma);
                    licenciaIdioma.setLicencia(licencia);
                    licenciaIdiomas.add(licenciaIdioma);
                }
            }
            exists = false;
        }

        licencia.setLicenciasIdiomas(licenciaIdiomas);

        InputStream imagenIS = multiPart.getField("imagen").getEntityAs(InputStream.class);

        byte[] imagen = inputStreamToByteArray(imagenIS);

        if (imagen != null && imagen.length > 0)
        {
            licencia.setImagen(imagen);
            for (BodyPart bodyPart : multiPart.getBodyParts())
            {
                String mimeType = bodyPart.getHeaders().getFirst("Content-Type");
                if (mimeType != null && !mimeType.isEmpty())
                {
                    licencia.setContentType(mimeType);
                    break;
                }
            }
        }

        licencia.update();

        return new ResponseMessage(true);
    }

    @GET
    @Path("{id}")
    @Consumes(MediaType.TEXT_XML)
    @Produces(MediaType.APPLICATION_XML)
    public List<UIEntity> getLicencia(@PathParam("id") String id)
            throws RegistroNoEncontradoException
    {
        Licencia licencia = Licencia.getLicencia(Long.parseLong(id));

        UIEntity entity = UIEntity.toUI(licencia);
        setAtributosMultidioma(licencia, entity);

        return Collections.singletonList(entity);
    }

    private void setAtributosMultidioma(Licencia licencia, UIEntity entity)
    {
        for (LicenciaIdioma licenciaIdioma : licencia.getLicenciasIdiomas())
        {
            if (licenciaIdioma.getTitulo() != null && !licenciaIdioma.getTitulo().equals(""))
            {
                entity.put(licenciaIdioma.getIdioma().getAcronimo(), "titulo",
                        licenciaIdioma.getTitulo());
            }

            if (licenciaIdioma.getUrl() != null && !licenciaIdioma.getUrl().equals(""))
            {
                entity.put(licenciaIdioma.getIdioma().getAcronimo(), "url", licenciaIdioma.getUrl());
            }
        }
    }

    @GET
    @Path("{id}/img")
    public byte[] getLicenciaImagen(@PathParam("id") String id)
    {
        byte[] imagen = null;

        try
        {
            Licencia licencia = Licencia.getLicencia(Long.parseLong(id));
            if (licencia.getImagen() != null)
            {
                imagen = licencia.getImagen();
            }
        }
        catch (RegistroNoEncontradoException e)
        {
        }

        if (imagen == null)
        {
            try
            {
                imagen = inputStreamToByteArray(new FileInputStream(servletConfig
                        .getServletContext().getRealPath("img/blank.png")));
            }
            catch (FileNotFoundException e)
            {
            }
        }

        return imagen;
    }

    private byte[] inputStreamToByteArray(InputStream inputStream)
    {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        int size = 1024;
        byte[] buffer = new byte[size];
        int len;

        try
        {
            while ((len = inputStream.read(buffer, 0, size)) != -1)
            {
                outputStream.write(buffer, 0, len);
            }

            return outputStream.toByteArray();
        }
        catch (Exception e)
        {
        }

        return null;
    }

    @DELETE
    @Path("{id}/img")
    public void deleteLicenciaImagen(@PathParam("id") String id)
            throws RegistroNoEncontradoException
    {
        Licencia.deleteLicenciaImagen(Long.parseLong(id));
    }

    @GET
    @Path("{id}/hasImg")
    @Produces(MediaType.TEXT_PLAIN)
    public String hasLicenciaImagen(@PathParam("id") String id)
            throws RegistroNoEncontradoException
    {
        Licencia licencia = Licencia.getLicencia(Long.parseLong(id));
        return (licencia.getImagen() != null) ? "Si" : "No";
    }

}
