package es.uji.apps.ocw.services;

import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.springframework.transaction.annotation.Transactional;

import com.sun.jersey.multipart.BodyPart;
import com.sun.jersey.multipart.FormDataMultiPart;

import es.uji.apps.ocw.CursoAsignaturaExistenteException;
import es.uji.apps.ocw.CursoAutorizadoExistenteException;
import es.uji.apps.ocw.CursoCategoriaExistenteException;
import es.uji.apps.ocw.GeneralOCWException;
import es.uji.apps.ocw.model.*;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.StreamUtils;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.ParametrosObligatoriosException;
import es.uji.commons.rest.exceptions.RegistroConHijosException;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import es.uji.commons.sso.AccessManager;
import es.uji.commons.sso.exceptions.UnauthorizedUserException;

@Path("curso")
public class CursoService extends CoreBaseService
{
    private UIEntity getCursoUI(Curso curso)
    {
        List<CursoIdioma> listaCursoIdiomas = curso.getTodosCursosIdiomas();

        UIEntity entity = UIEntity.toUI(curso);

        entity.put("id", curso.getId());

        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

        if (curso.getFechaCreacion() != null)
        {
            entity.put("fechaCreacion", formatter.format(curso.getFechaCreacion()));
        }

        if (curso.getFechaBaja() != null)
        {
            entity.put("fechaBaja", formatter.format(curso.getFechaBaja()));
        }

        entity.put("persona", curso.getNombrePersona());
        entity.put("personaId", curso.getPersonaId());
        entity.put("visible", curso.getVisible());
        entity.put("estado", curso.getEstado());
        entity.put("tags", curso.getTagsEnCadena());

        for (CursoIdioma cursoIdioma : listaCursoIdiomas)
        {
            entity.put(cursoIdioma.getIdioma().getAcronimo(), "nombre", cursoIdioma.getNombre());

            if (cursoIdioma.getPeriodo() != null && !cursoIdioma.getPeriodo().isEmpty())
            {
                entity.put(cursoIdioma.getIdioma().getAcronimo().toLowerCase(), "periodo",
                        cursoIdioma.getPeriodo());
            }
            if (cursoIdioma.getDescripcion() != null && !cursoIdioma.getDescripcion().isEmpty())
            {
                entity.put(cursoIdioma.getIdioma().getAcronimo(), "descripcion",
                        cursoIdioma.getDescripcion());
            }
            if (cursoIdioma.getPrograma() != null && !cursoIdioma.getPrograma().isEmpty())
            {
                entity.put(cursoIdioma.getIdioma().getAcronimo(), "programa",
                        cursoIdioma.getPrograma());
            }
            if (cursoIdioma.getGuiaAprendizaje() != null
                    && !cursoIdioma.getGuiaAprendizaje().isEmpty())
            {
                entity.put(cursoIdioma.getIdioma().getAcronimo(), "guiaAprendizaje",
                        cursoIdioma.getGuiaAprendizaje());
            }
        }

        return entity;
    }

    @GET
    @Produces(MediaType.TEXT_XML)
    public List<UIEntity> getCursosConFiltros(@QueryParam("cadena") String cadena,
            @QueryParam("categoriaId") String categoriaId)
    {
        Map<String, String> filtro = new HashMap<String, String>();

        Persona persona = new Persona(AccessManager.getConnectedUserId(request));

        if (cadena != null && !cadena.isEmpty())
        {
            filtro.put("cadena", cadena);
        }

        if (categoriaId != null && !categoriaId.isEmpty())
        {
            filtro.put("categoriaId", categoriaId);
        }

        List<CursoIdiomaPrincipal> cursos = Curso.getCursosConFiltros(filtro, persona);

        List<UIEntity> lista = new ArrayList<UIEntity>();

        for (CursoIdiomaPrincipal curso : cursos)
        {
            UIEntity entity = UIEntity.toUI(curso);
            List<CategoriaIdiomaPrincipal> listaCategorias = Categoria.getCategoriasByCursoId(curso
                    .getId());
            StringBuilder categorias = new StringBuilder();
            String delim = "";
            for (CategoriaIdiomaPrincipal categoria : listaCategorias)
            {
                categorias.append(delim).append(categoria.getNombre());
                delim = ", ";
            }
            entity.put("categorias", categorias);
            lista.add(entity);
        }

        return lista;
    }

    @GET
    @Path("{cursoId}")
    @Produces(MediaType.TEXT_XML)
    public List<UIEntity> getCursoById(@PathParam("cursoId") String cursoId)
            throws RegistroNoEncontradoException
    {
        UIEntity result = getCursoUI(Curso.getCursoById(Long.parseLong(cursoId)));
        return Collections.singletonList(result);
    }

    @GET
    @Path("{cursoId}/material")
    @Produces(MediaType.TEXT_XML)
    public List<UIEntity> getMaterialesByCurso(@PathParam("cursoId") String cursoId)
            throws RegistroNoEncontradoException
    {
        List<UIEntity> lista = new ArrayList<UIEntity>();

        Curso curso = Curso.getCursoById(Long.parseLong(cursoId));

        for (CursoMaterial cursoMaterial : curso.getMateriales())
        {
            UIEntity entity = UIEntity.toUI(cursoMaterial);
            Material material = cursoMaterial.getMaterial();
            entity.put("materialId", material.getId());
            entity.put("cursoId", curso.getId());
            entity.put("titulo", material.getTitulo());
            entity.put("fecha", material.getFecha());
            entity.put("tipo", material.getContentType());
            entity.put("creador", material.getPersonaId());
            lista.add(entity);
        }

        return lista;
    }

    @DELETE
    @Path("{cursoId}/material/{materialId}")
    @Produces(MediaType.APPLICATION_XML)
    @Consumes(MediaType.TEXT_XML)
    public ResponseMessage deleteMaterial(@PathParam("cursoId") String cursoId,
            @PathParam("materialId") String materialId) throws RegistroNoEncontradoException,
            UnauthorizedUserException
    {
        Persona persona = new Persona(AccessManager.getConnectedUserId(request));

        Curso curso = Curso.getCursoById(Long.parseLong(cursoId));
        curso.deleteCursoMaterialById(Long.parseLong(materialId), persona);
        return new ResponseMessage(true);
    }

    @POST
    @Produces(MediaType.APPLICATION_XML)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Transactional
    public List<UIEntity> postCurso(MultivaluedMap<String, String> params)
            throws ParametrosObligatoriosException, UnauthorizedUserException,
            RegistroNoEncontradoException
    {
        List<UIEntity> listaUIEntity = new ArrayList<UIEntity>();

        String id = params.getFirst("id");
        String fechaCreacion = params.getFirst("fechaCreacion");

        if (fechaCreacion.isEmpty())
        {
            throw new ParametrosObligatoriosException("fechaCreacion");
        }

        if (id.isEmpty())
        {
            listaUIEntity = insertCurso(params);
        }
        else
        {
            listaUIEntity = updateCurso(params);
        }

        return listaUIEntity;
    }

    @DELETE
    @Path("{cursoId}")
    @Produces(MediaType.APPLICATION_XML)
    @Consumes(MediaType.TEXT_XML)
    public void deleteCurso(@PathParam("cursoId") String cursoId)
            throws RegistroNoEncontradoException, UnauthorizedUserException,
            RegistroConHijosException
    {
        Persona persona = new Persona(AccessManager.getConnectedUserId(request));

        Curso curso = Curso.getCursoById(Long.parseLong(cursoId));
        curso.delete(persona);
    }

    private List<UIEntity> insertCurso(MultivaluedMap<String, String> params)
            throws RegistroNoEncontradoException, NumberFormatException, UnauthorizedUserException
    {
        Curso curso = new Curso();

        Long personaId = AccessManager.getConnectedUserId(request);
        String fechaCreacion = params.getFirst("fechaCreacion");
        String fechaBaja = params.getFirst("fechaBaja");
        String categoriaId = params.getFirst("categoriaId");

        DateFormat formatter = new SimpleDateFormat("dd/M/yyyy");
        try
        {
            Date date = (Date) formatter.parse(fechaCreacion);
            curso.setFechaCreacion(date);
        }
        catch (ParseException e)
        {
            curso.setFechaCreacion(new Date());
        }

        try
        {
            Date date = (Date) formatter.parse(fechaBaja);
            curso.setFechaBaja(date);
        }
        catch (ParseException e)
        {
            curso.setFechaBaja(null);
        }

        curso.setEstado("I");
        curso.setVisible("N");
        curso.setPersonaId(personaId);

        curso = curso.insert();

        List<Idioma> idiomas = Idioma.getIdiomasEdicion();
        for (Idioma idioma : idiomas)
        {
            String nombre = params.getFirst("nombre__" + idioma.getAcronimo().toLowerCase());
            String periodo = params.getFirst("periodo__" + idioma.getAcronimo().toLowerCase());

            if (!((nombre == null || nombre.isEmpty()) && (periodo == null || periodo.isEmpty())))
            {
                CursoIdioma cursoIdioma = new CursoIdioma();
                cursoIdioma.setCurso(curso);
                cursoIdioma.setOrden(idioma.getOrden());
                cursoIdioma.setIdioma(idioma);

                if (!(periodo == null || periodo.isEmpty()))
                {
                    cursoIdioma.setPeriodo(periodo);
                }

                if (!(nombre == null || nombre.isEmpty()))
                {
                    cursoIdioma.setNombre(nombre);
                }

                cursoIdioma.insert();
            }
        }

        Categoria categoria = Categoria.getCategoriaById(Long.parseLong(categoriaId));
        Persona persona = Persona.getPersonaById(personaId);
        curso.addCursoCategoria(categoria, persona);

        return Collections.singletonList(UIEntity.toUI(curso));
    }

    private List<UIEntity> updateCurso(MultivaluedMap<String, String> params)
            throws UnauthorizedUserException, RegistroNoEncontradoException
    {
        String id = params.getFirst("id");
        Curso curso = Curso.getCursoById(Long.parseLong(id));

        String fechaCreacion = params.getFirst("fechaCreacion");
        String fechaBaja = params.getFirst("fechaBaja");
        String estado = params.getFirst("estado");
        String visible = params.getFirst("visible");
        String tags = params.getFirst("tags");

        DateFormat formatter = new SimpleDateFormat("dd/M/yyyy");
        try
        {
            Date dateFechaCreacion = (Date) formatter.parse(fechaCreacion);
            curso.setFechaCreacion(dateFechaCreacion);
        }
        catch (ParseException e)
        {
            curso.setFechaCreacion(new Date());
        }

        try
        {
            Date dateFechaBaja = (Date) formatter.parse(fechaBaja);
            curso.setFechaBaja(dateFechaBaja);
        }
        catch (ParseException e)
        {
            curso.setFechaBaja(null);
        }

        curso.setEstado(estado);
        if (visible != null && !visible.isEmpty())
        {
            curso.setVisible(visible);
        }
        else
        {
            curso.setVisible("N");
        }

        Set<CursoIdioma> cursoIdiomas = curso.getCursosIdiomas();
        List<Idioma> idiomas = Idioma.getIdiomasEdicion();
        boolean exists = false;
        for (Idioma idioma : idiomas)
        {
            String nombre = params.getFirst("nombre__" + idioma.getAcronimo().toLowerCase());
            String descripcion = params.getFirst("descripcion__"
                    + idioma.getAcronimo().toLowerCase());
            String programa = params.getFirst("programa__" + idioma.getAcronimo().toLowerCase());
            String guiaAprendizaje = params.getFirst("guiaAprendizaje__"
                    + idioma.getAcronimo().toLowerCase());
            String periodo = params.getFirst("periodo__" + idioma.getAcronimo().toLowerCase());

            for (CursoIdioma cursoIdioma : cursoIdiomas)
            {
                if (cursoIdioma.getIdioma().getAcronimo().equals(idioma.getAcronimo()))
                {
                    cursoIdioma.setNombre(nombre);
                    cursoIdioma.setPeriodo(periodo);
                    cursoIdioma.setDescripcion(descripcion);
                    cursoIdioma.setPrograma(programa);
                    cursoIdioma.setGuiaAprendizaje(guiaAprendizaje);
                    cursoIdioma.update();
                    exists = true;
                    break;
                }
            }
            if (!exists)
            {
                if (!((nombre == null) || nombre.isEmpty())
                        && (periodo == null || periodo.isEmpty())
                        && (descripcion == null || descripcion.isEmpty())
                        && (programa == null || programa.isEmpty())
                        && (guiaAprendizaje == null || guiaAprendizaje.isEmpty()))
                {
                    CursoIdioma cursoIdioma = new CursoIdioma();
                    cursoIdioma.setCurso(curso);
                    cursoIdioma.setIdioma(idioma);
                    cursoIdioma.setOrden(idioma.getOrden());
                    cursoIdioma.setNombre(nombre);
                    cursoIdioma.setPeriodo(periodo);
                    cursoIdioma.setDescripcion(descripcion);
                    cursoIdioma.setPrograma(programa);
                    cursoIdioma.setGuiaAprendizaje(guiaAprendizaje);
                    cursoIdioma = cursoIdioma.insert();
                    cursoIdiomas.add(cursoIdioma);
                }
            }
            exists = false;
        }

        Persona persona = new Persona(AccessManager.getConnectedUserId(request));
        curso.update(persona);
        curso.parseaTagsByString(tags);

        return Collections.singletonList(UIEntity.toUI(curso));
    }

    @POST
    @Path("{cursoId}/material")
    @Produces(MediaType.TEXT_XML)
    public List<UIEntity> insertMetadatoToMaterial(UIEntity entity)
            throws UnauthorizedUserException, RegistroNoEncontradoException
    {

        if (entity.get("materialId") == null || entity.get("materialId").isEmpty()
                || entity.get("cursoId") == null || entity.get("cursoId").isEmpty())
        {
            return new ArrayList<UIEntity>();
        }

        Persona persona = new Persona(AccessManager.getConnectedUserId(request));

        Material material = Material.getMaterialById(Long.parseLong(entity.get("materialId")));
        Curso curso = Curso.getCursoById(Long.parseLong(entity.get("cursoId")));

        CursoMaterial cursoMaterial = curso.addMaterial(material, persona);

        if (entity.get("grupoId") != null && Long.parseLong(entity.get("grupoId")) != -1)
        {
            Grupo grupo = Grupo.getGrupoById(Long.parseLong(entity.get("grupoId")));
            cursoMaterial.setGrupo(grupo);
            cursoMaterial.update();
        }

        if (entity.get("orden") != null && !entity.get("orden").isEmpty())
        {
            cursoMaterial.setOrden(Long.parseLong(entity.get("orden")));
            cursoMaterial.update();
        }
        else
        {
            Long nuevoOrden = CursoMaterial.getSiquienteOrdenByGrupo(entity.getLong("grupoId"));
            cursoMaterial.setOrden(nuevoOrden);
            cursoMaterial.update();
        }

        UIEntity entity2 = UIEntity.toUI(cursoMaterial);
        entity2.put("titulo", material.getTitulo());
        entity2.put("materialId", material.getId());
        entity2.put("fecha", material.getFecha());
        entity2.put("cursoId", curso.getId());
        if (material.getContentType() != null)
        {
            entity2.put("tipo", material.getContentType());
        }
        entity2.put("creador", material.getPersonaId());

        return Collections.singletonList(entity2);
    }

    @PUT
    @Path("{cursoId}/material/{cursoMaterialId}")
    @Consumes(MediaType.TEXT_XML)
    public ResponseMessage updateGrupo(@PathParam("cursoId") String cursoId,
            @PathParam("cursoMaterialId") String cursoMaterialId, UIEntity entity)
            throws RegistroNoEncontradoException, NumberFormatException
    {
        CursoMaterial cursoMaterial = CursoMaterial.getCursoMaterialById(Long
                .parseLong(cursoMaterialId));
        cursoMaterial.setDescripcion(entity.get("descripcion"));
        cursoMaterial.update();
        return new ResponseMessage(true);
    }

    @GET
    @Path("{cursoId}/imagen")
    @Produces(MediaType.TEXT_XML)
    public List<UIEntity> getImagenesByCurso(@PathParam("cursoId") String cursoId)
            throws RegistroNoEncontradoException
    {
        List<UIEntity> lista = new ArrayList<UIEntity>();

        Curso curso = Curso.getCursoById(Long.parseLong(cursoId));

        if (curso != null)
        {
            List<CursoImagen> listaCursoImagenes = curso.getImagenes();

            for (CursoImagen cursoImagen : listaCursoImagenes)
            {
                UIEntity entity = UIEntity.toUI(cursoImagen);
                entity.put("id", cursoImagen.getId());
                entity.put("cursoId", cursoImagen.getCurso().getId());
                entity.put("url", "/ocw/rest/curso/" + cursoImagen.getCurso().getId() + "/imagen/"
                        + cursoImagen.getId());
                lista.add(entity);
            }
        }

        return lista;
    }

    @GET
    @Path("{cursoId}/imagen/{cursoImagenId}")
    public Response getCursoImagen(@PathParam("cursoId") String cursoId,
            @PathParam("cursoImagenId") String cursoImagenId) throws RegistroNoEncontradoException
    {
        CursoImagen cursoImagen = CursoImagen.getCursoImagenById(Long.parseLong(cursoImagenId));

        byte[] documento = cursoImagen.getImagen();
        String contentType = cursoImagen.getContentType();

        return Response.ok(documento).header("Content-Length", documento.length).type(contentType)
                .build();
    }

    @DELETE
    @Path("{cursoId}/imagen/{cursoImagenId}")
    @Transactional
    public ResponseMessage deleteCursoImagen(@PathParam("cursoId") String cursoId,
            @PathParam("cursoImagenId") String cursoImagenId) throws RegistroNoEncontradoException,
            UnauthorizedUserException
    {
        Persona persona = new Persona(AccessManager.getConnectedUserId(request));

        CursoImagen cursoImagen = CursoImagen.getCursoImagenById(Long.parseLong(cursoImagenId));

        cursoImagen.deleteCursoImagen(Long.parseLong(cursoImagenId), persona);
        return new ResponseMessage(true, "Les imatges s'han esborrat satisfactoriament");

    }

    @POST
    @Path("{cursoId}/imagen")
    @Produces(MediaType.APPLICATION_XML)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Transactional
    public ResponseMessage uploadImagen(@PathParam("cursoId") String cursoId,
            FormDataMultiPart multiPart) throws UnauthorizedUserException, IOException,
            RegistroNoEncontradoException
    {
        Persona persona = new Persona(AccessManager.getConnectedUserId(request));

        Curso curso = Curso.getCursoById(Long.parseLong(cursoId));

        if (curso != null)
        {
            CursoImagen cursoImagen = new CursoImagen();
            cursoImagen.setCurso(curso);

            cursoImagen.setImagen(StreamUtils.inputStreamToByteArray(multiPart.getField("imagen")
                    .getEntityAs(InputStream.class)));

            for (BodyPart bodyPart : multiPart.getBodyParts())
            {
                String mimeType = bodyPart.getHeaders().getFirst("Content-Type");
                if (mimeType != null && !mimeType.isEmpty())
                {
                    cursoImagen.setContentType(mimeType);
                    break;
                }
            }
            cursoImagen = cursoImagen.insert(persona);

        }

        return new ResponseMessage(true);
    }

    @GET
    @Path("{cursoId}/asignatura")
    @Produces(MediaType.TEXT_XML)
    public List<UIEntity> getAsignaturasByCurso(@PathParam("cursoId") String cursoId)
            throws RegistroNoEncontradoException
    {
        List<UIEntity> lista = new ArrayList<UIEntity>();

        Curso curso = Curso.getCursoById(Long.parseLong(cursoId));

        if (curso != null)
        {
            List<CursoAsignatura> listaCursoAsignaturas = curso.getAsignaturas();

            for (CursoAsignatura cursoAsignatura : listaCursoAsignaturas)
            {
                UIEntity entity = UIEntity.toUI(cursoAsignatura);
                Asignatura asignatura = curso.getAsignaturaById(cursoAsignatura.getAsignaturaId());
                entity.put("asignatura", asignatura.getNombreCa());
                lista.add(entity);
            }
        }

        return lista;
    }

    @POST
    @Path("{cursoId}/asignatura")
    @Produces(MediaType.TEXT_XML)
    public List<UIEntity> insertCursoAsignatura(UIEntity entity)
            throws CursoAsignaturaExistenteException, UnauthorizedUserException,
            RegistroNoEncontradoException
    {
        Persona persona = new Persona(AccessManager.getConnectedUserId(request));

        String asignaturaId = entity.get("asignaturaId");
        Long cursoId = Long.parseLong(entity.get("cursoId"));

        if (asignaturaId == null || cursoId == null)
        {
            return new ArrayList<UIEntity>();
        }

        Curso curso = Curso.getCursoById(cursoId);
        if (curso.getAsignaturasByCodigo(asignaturaId).size() > 0)
        {
            throw new CursoAsignaturaExistenteException("Error. La assignatura ja estaba inserida");
        }
        else
        {
            CursoAsignatura cursoAsignatura = curso.addAsignatura(entity.get("asignaturaId"),
                    persona);
            Asignatura asignatura = curso.getAsignaturaById(cursoAsignatura.getAsignaturaId());

            UIEntity entity2 = UIEntity.toUI(cursoAsignatura);
            entity2.put("asignatura", asignatura.getNombreCa());

            return Collections.singletonList(entity2);
        }
    }

    @DELETE
    @Path("{cursoId}/asignatura/{asignaturaId}")
    @Produces(MediaType.APPLICATION_XML)
    @Consumes(MediaType.TEXT_XML)
    public ResponseMessage deleteCursoAsignatura(@PathParam("cursoId") String cursoId,
            @PathParam("asignaturaId") String asignaturaId) throws RegistroNoEncontradoException,
            UnauthorizedUserException
    {
        Persona persona = new Persona(AccessManager.getConnectedUserId(request));

        Curso curso = Curso.getCursoById(Long.parseLong(cursoId));
        curso.deleteAsignaturaById(Long.parseLong(asignaturaId), persona);
        return new ResponseMessage(true);
    }

    @GET
    @Path("{cursoId}/autorizado")
    @Produces(MediaType.TEXT_XML)
    public List<UIEntity> getAutorizadosByCurso(@PathParam("cursoId") String cursoId)
            throws RegistroNoEncontradoException
    {
        List<UIEntity> lista = new ArrayList<UIEntity>();

        Curso curso = Curso.getCursoById(Long.parseLong(cursoId));

        if (curso != null)
        {
            List<CursoAutorizado> listaCursoAutorizados = curso.getAutorizados();

            for (CursoAutorizado cursoAutorizado : listaCursoAutorizados)
            {
                UIEntity entity = UIEntity.toUI(cursoAutorizado);
                Persona persona = Persona.getPersonaById(cursoAutorizado.getPersonaId());
                String nombreCompleto = (((persona.getNombre() != null) ? persona.getNombre() : "")
                        + ((persona.getApellido1() != null) ? " " + persona.getApellido1() : "") + ((persona
                        .getApellido2() != null) ? " " + persona.getApellido2() : "")).trim();
                entity.put("nombre", nombreCompleto);
                lista.add(entity);
            }
        }

        return lista;
    }

    @POST
    @Path("{cursoId}/autorizado")
    @Produces(MediaType.TEXT_XML)
    public List<UIEntity> insertCursoAutorizado(UIEntity entity)
            throws CursoAutorizadoExistenteException, UnauthorizedUserException,
            RegistroNoEncontradoException
    {
        Persona persona = new Persona(AccessManager.getConnectedUserId(request));

        Long personaAutorizadaId = Long.parseLong(entity.get("personaId"));
        Long cursoId = Long.parseLong(entity.get("cursoId"));

        if (personaAutorizadaId == null || cursoId == null)
        {
            return new ArrayList<UIEntity>();
        }

        Curso curso = Curso.getCursoById(cursoId);
        if (curso.isAutorizadoByCurso(personaAutorizadaId))
        {
            throw new CursoAutorizadoExistenteException(
                    "Error. La persona autoritzada ja estaba inserida");
        }
        else
        {
            CursoAutorizado cursoAutorizado = curso.addAutorizado(personaAutorizadaId, persona);
            Persona personaAutorizada = Persona.getPersonaById(cursoAutorizado.getPersonaId());

            UIEntity entity2 = UIEntity.toUI(cursoAutorizado);
            entity2.put("nombre",
                    personaAutorizada.getNombre() + " " + personaAutorizada.getApellido1() + " "
                            + personaAutorizada.getApellido2());

            return Collections.singletonList(entity2);
        }
    }

    @DELETE
    @Path("{cursoId}/autorizado/{id}")
    @Consumes(MediaType.TEXT_XML)
    public void deleteCursoAutorizado(@PathParam("cursoId") String cursoId,
            @PathParam("id") String id) throws RegistroNoEncontradoException,
            UnauthorizedUserException
    {
        Persona persona = new Persona(AccessManager.getConnectedUserId(request));

        Curso curso = Curso.getCursoById(Long.parseLong(cursoId));
        curso.deleteAutorizadoById(Long.parseLong(id), persona);
    }

    @SuppressWarnings("unchecked")
    @GET
    @Path("{cursoId}/categoria")
    @Produces(MediaType.TEXT_XML)
    public List<UIEntity> getCategoriasByCurso(@PathParam("cursoId") String cursoId)
            throws RegistroNoEncontradoException
    {
        List<UIEntity> lista = new ArrayList<UIEntity>();

        Curso curso = Curso.getCursoById(Long.parseLong(cursoId));
        for (CursoCategoria cursoCategoria : curso.getCursoCategoriasByCursoId())
        {

            CategoriaIdiomaPrincipal categoriaIdiomaPrincipal = cursoCategoria.getCategoria()
                    .getCategoriaIdiomaPrincipalById();
            {
                UIEntity entity = UIEntity.toUI(cursoCategoria);
                entity.put("id", cursoCategoria.getId());
                entity.put("categoriaId", categoriaIdiomaPrincipal.getId());
                entity.put("orden", categoriaIdiomaPrincipal.getOrden());
                entity.put("nombre", categoriaIdiomaPrincipal.getNombre());
                entity.put("cursoId", cursoId);
                lista.add(entity);

            }
        }
        return lista;
    }

    @POST
    @Path("{cursoId}/categoria")
    @Produces(MediaType.TEXT_XML)
    public List<UIEntity> insertCategoria(UIEntity entity) throws UnauthorizedUserException,
            RegistroNoEncontradoException, CursoCategoriaExistenteException
    {

        if (entity.get("categoriaId") == null || entity.get("categoriaId").isEmpty()
                || entity.get("cursoId") == null || entity.get("cursoId").isEmpty())
        {
            return new ArrayList<UIEntity>();
        }

        Persona persona = new Persona(AccessManager.getConnectedUserId(request));

        Categoria categoria = Categoria.getCategoriaById(Long.parseLong(entity.get("categoriaId")));
        Curso curso = Curso.getCursoById(Long.parseLong(entity.get("cursoId")));

        if (curso.existeCursoCategoria(categoria.getId()))
        {
            throw new CursoCategoriaExistenteException("Error. La categoria ja estaba inserida");
        }
        else
        {
            CursoCategoria cursoCategoria = curso.addCursoCategoria(categoria, persona);
            CategoriaIdiomaPrincipal categoriaIdiomaPrincipal = cursoCategoria.getCategoria()
                    .getCategoriaIdiomaPrincipalById();

            UIEntity entity2 = UIEntity.toUI(cursoCategoria);
            entity2.put("id", cursoCategoria.getId());
            entity2.put("categoriaId", categoriaIdiomaPrincipal.getId());
            entity2.put("orden", categoriaIdiomaPrincipal.getOrden());
            entity2.put("nombre", categoriaIdiomaPrincipal.getNombre());
            entity2.put("cursoId", curso.getId());

            return Collections.singletonList(entity2);
        }
    }

    @DELETE
    @Path("{cursoId}/categoria/{id}")
    @Produces(MediaType.APPLICATION_XML)
    @Consumes(MediaType.TEXT_XML)
    public ResponseMessage deleteCategoria(@PathParam("cursoId") String cursoId,
            @PathParam("id") String id) throws RegistroNoEncontradoException,
            UnauthorizedUserException, GeneralOCWException
    {
        Persona persona = new Persona(AccessManager.getConnectedUserId(request));

        Curso curso = Curso.getCursoById(Long.parseLong(cursoId));
        if (curso.getCursoCategoriasByCursoId().size() <= 1)
        {
            throw new GeneralOCWException("Error, No es poden esborrar totes les categories.");
        }
        else
        {
            curso.deleteCursoCategoriaById(Long.parseLong(id), persona);
            return new ResponseMessage(true);
        }
    }

}
