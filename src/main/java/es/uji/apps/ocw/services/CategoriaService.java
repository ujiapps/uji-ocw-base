package es.uji.apps.ocw.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.commons.lang3.StringUtils;

import es.uji.apps.ocw.model.Categoria;
import es.uji.apps.ocw.model.CategoriaIdioma;
import es.uji.apps.ocw.model.CategoriaIdiomaPrincipal;
import es.uji.apps.ocw.model.Idioma;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroConHijosException;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

@Path("categoria")
public class CategoriaService extends CoreBaseService
{
    private UIEntity getCategoriaUI(Categoria categoria)
    {
        Set<CategoriaIdioma> listaCategoriaIdiomas = categoria.getCategoriasIdiomas();

        UIEntity categoriaUI = UIEntity.toUI(categoria);

        for (CategoriaIdioma categoriaIdioma : listaCategoriaIdiomas)
        {
            if (categoriaIdioma.getNombre() != null && !categoriaIdioma.getNombre().isEmpty())
            {
                categoriaUI.put(categoriaIdioma.getIdioma().getAcronimo(), "nombre",
                        categoriaIdioma.getNombre());
            }
        }

        return categoriaUI;
    }

    @GET
    @Produces(MediaType.TEXT_XML)
    public List<UIEntity> getCategorias()
    {
        ArrayList<UIEntity> result = new ArrayList<UIEntity>();

        List<Categoria> listaCategorias = Categoria.getCategorias();

        for (Categoria categoria : listaCategorias)
        {

            result.add(getCategoriaUI(categoria));
        }

        return result;
    }

    @GET
    @Path("idiomaPrincipal")
    @Produces(MediaType.TEXT_XML)
    public List<UIEntity> getCategoriasIdiomaPrincipal()
    {
        ArrayList<UIEntity> result = new ArrayList<UIEntity>();

        List<CategoriaIdiomaPrincipal> listaCategorias = Categoria.getCategoriasIdiomaPrincipal();

        for (CategoriaIdiomaPrincipal categoria : listaCategorias)
        {
            UIEntity entity = UIEntity.toUI(categoria);
            entity.put("orden", categoria.getOrden());
            entity.put("nombre", categoria.getNombre());
            entity.put("id", categoria.getId());
            result.add(entity);
        }

        return result;
    }

    @POST
    @Consumes(MediaType.TEXT_XML)
    @Produces(MediaType.TEXT_XML)
    public List<UIEntity> insertCategoria(UIEntity entity)
    {
        Categoria categoria = new Categoria();
        String orden = entity.get("orden");

        if (entity.get("orden") == null || entity.get("orden").isEmpty()
                || entity.get("nombre__ca") == null || entity.get("nombre__ca").isEmpty()
                || !StringUtils.isNumeric(orden))
        {
            return new ArrayList<UIEntity>();
        }

        categoria.setOrden(Long.parseLong(entity.get("orden")));

        Set<CategoriaIdioma> categoriasIdiomas = new HashSet<CategoriaIdioma>();
        List<Idioma> idiomas = Idioma.getIdiomasEdicion();

        for (Idioma idioma : idiomas)
        {
            String nombre = entity.get("nombre__" + idioma.getAcronimo().toLowerCase());
            if (nombre != null && !nombre.isEmpty() && !nombre.equals(""))
            {
                CategoriaIdioma categoriaIdioma = new CategoriaIdioma();
                categoriaIdioma.setIdioma(idioma);
                categoriaIdioma.setNombre(nombre);
                categoriasIdiomas.add(categoriaIdioma);
            }
        }
        categoria.setCategoriasIdiomas(categoriasIdiomas);
        categoria = categoria.insert();

        return Collections.singletonList(getCategoriaUI(categoria));
    }

    @DELETE
    @Path("{categoriaId}")
    @Produces(MediaType.APPLICATION_XML)
    @Consumes(MediaType.TEXT_XML)
    public ResponseMessage delete(@PathParam("categoriaId") String categoriaId)
            throws RegistroNoEncontradoException, RegistroConHijosException
    {
        Categoria.delete(Long.parseLong(categoriaId));
        return new ResponseMessage(true, "El registre s'ha esborrat satisfactoriament");
    }

    @PUT
    @Path("{id}")
    @Consumes(MediaType.TEXT_XML)
    @Produces(MediaType.TEXT_XML)
    public ResponseMessage update(@PathParam("id") String id, UIEntity entity)
            throws RegistroNoEncontradoException
    {
        Categoria categoria = Categoria.getCategoriaById(Long.parseLong(id));
        categoria.setOrden(Long.parseLong(entity.get("orden")));

        List<CategoriaIdioma> categoriaIdiomas = categoria
                .getCategoriaIdiomasByCategoriaId(categoria.getId());

        for (CategoriaIdioma categoriaIdioma : categoriaIdiomas)
        {
            categoriaIdioma.setNombre(entity.get("nombre__"
                    + categoriaIdioma.getIdioma().getAcronimo().toLowerCase()));
            categoriaIdioma.update();
        }

        categoria.update();
        return new ResponseMessage(true);
    }
}
