package es.uji.apps.ocw.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import es.uji.apps.ocw.EmailNoUjiException;
import es.uji.apps.ocw.model.Autor;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroConHijosException;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

@Path("autor")
public class AutorService
{
    @GET
    @Produces(MediaType.TEXT_XML)
    public List<UIEntity> getAutores()
    {
        List<Autor> lista = Autor.getAutores();
        List<UIEntity> listaUI = UIEntity.toUI(lista);
        return listaUI;
    }

    @DELETE
    @Path("{autorId}")
    @Consumes(MediaType.TEXT_XML)
    public void borraAutor(@PathParam("autorId") String autorId)
            throws RegistroConHijosException, RegistroNoEncontradoException
    {
        Autor.delete(Long.parseLong(autorId));
    }

    @POST
    @Produces(MediaType.TEXT_XML)
    public List<UIEntity> insertAutor(UIEntity entity) throws EmailNoUjiException
    {
        if ((entity.get("nombre") == null || entity.get("apellido1") == null || entity.get("email") == null)
                && entity.get("personaId") == null)
        {
            return new ArrayList<UIEntity>();
        }

        if (entity.get("personaId") == null
                && (entity.get("email").contains("@uji.es") || entity.get("email").contains(
                        ".uji.es")))
        {
            throw new EmailNoUjiException(
                    "L'e-mail de una persona externa no pot ser del domini uji.");
        }

        Autor autor = entity.toModel(Autor.class);
        autor = autor.insert();

        return Collections.singletonList(UIEntity.toUI(autor));
    }

    @PUT
    @Path("{autorId}")
    @Consumes(MediaType.TEXT_XML)
    @Produces(MediaType.TEXT_XML)
    public List<UIEntity> updateAutor(@PathParam("autorId") String id, UIEntity entity) throws EmailNoUjiException
    {
        if (entity.get("personaId") == null
                && (entity.get("email").contains("@uji.es") || entity.get("email").contains(
                        ".uji.es")))
        {
            throw new EmailNoUjiException(
                    "L'e-mail de una persona externa no pot ser del domini uji.");
        }

        Autor autor = entity.toModel(Autor.class).update();
        return Collections.singletonList(UIEntity.toUI(autor));
    }
}
