package es.uji.apps.ocw.services;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.CookieParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;

import org.springframework.stereotype.Service;

@Service
@Path("webresource")
public class WebResourceService
{
    @GET
    @Path("language")
    public Response setlang(@Context HttpServletRequest request, @CookieParam("uji-lang") String cookieIdioma, @QueryParam("lang") String lang) throws URISyntaxException 
    {
        List<String> langs = new ArrayList<String>();
        
        langs.add("en");
        langs.add("es");
        langs.add("ca");
        
        if (lang == null || langs.indexOf(lang.toLowerCase()) == -1) {
            lang = cookieIdioma.toUpperCase();
        }
        
        NewCookie langCookie = new NewCookie("uji-lang", lang.toUpperCase(), "/", "uji.es", "", -1, false);
        
        String url = request.getHeader("referer");
        
        if (url == null) {
            url = "http://www.uji.es";
        }
        
        URI uri = new URI(url);
        return Response.temporaryRedirect(uri).cookie(langCookie).build();
        
    }
}