package es.uji.apps.ocw.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import es.uji.apps.ocw.model.Idioma;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroConHijosException;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

@Path("idioma")
public class IdiomaService extends CoreBaseService
{
    @GET
    @Produces(MediaType.TEXT_XML)
    public List<UIEntity> getIdiomas()
    {
        List<Idioma> listaIdiomas = Idioma.getIdiomas();
        List<UIEntity> lista = UIEntity.toUI(listaIdiomas);
        return lista;

    }

    @POST
    @Produces(MediaType.TEXT_XML)
    public List<UIEntity> insertIdioma(UIEntity entity)
    {
        if ((entity.get("nombre") == null || entity.get("acronimo") == null || entity.get("orden") == null)
                && entity.get("edicion") == null)
        {
            return new ArrayList<UIEntity>();
        }

        Idioma idioma = entity.toModel(Idioma.class);

        if (entity.get("edicion").equals("true"))
        {
            idioma.setEdicion("S");
        }
        else
        {
            idioma.setEdicion("N");
        }
        idioma = idioma.insert();
        entity.put("id", idioma.getId());
        entity.put("edicion", idioma.getEdicion());
        return Collections.singletonList(entity);
    }

    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_XML)
    @Consumes(MediaType.TEXT_XML)
    public ResponseMessage delete(@PathParam("id") String id) throws RegistroConHijosException,
            NumberFormatException
    {
        Idioma.delete(Long.parseLong(id));
        return new ResponseMessage(true);
    }

    @PUT
    @Path("{id}")
    @Consumes(MediaType.TEXT_XML)
    @Produces(MediaType.TEXT_XML)
    public List<UIEntity> update(@PathParam("id") String id, UIEntity entity)
            throws RegistroNoEncontradoException
    {
        Idioma idioma = Idioma.getIdiomaById(Long.parseLong(id));
        idioma.setNombre(entity.get("nombre"));
        idioma.setAcronimo(entity.get("acronimo"));
        idioma.setOrden(Long.parseLong(entity.get("orden")));
        if (entity.get("edicion").equals("true"))
        {
            idioma.setEdicion("S");
        }
        else
        {
            idioma.setEdicion("N");
        }
        idioma.update();

        UIEntity newEntity = UIEntity.toUI(idioma);
        return Collections.singletonList(newEntity);
    }

    @GET
    @Path("edicion")
    @Produces(MediaType.TEXT_XML)
    public List<UIEntity> getIdiomasEdicion()
    {
        List<Idioma> listaIdiomas = Idioma.getIdiomasEdicion();
        List<UIEntity> lista = UIEntity.toUI(listaIdiomas);
        return lista;
    }

}
