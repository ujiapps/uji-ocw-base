package es.uji.apps.ocw.services.rss;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.MediaType;

import org.w3c.dom.Document;

import com.sun.syndication.feed.synd.SyndCategory;
import com.sun.syndication.feed.synd.SyndCategoryImpl;
import com.sun.syndication.feed.synd.SyndContent;
import com.sun.syndication.feed.synd.SyndContentImpl;
import com.sun.syndication.feed.synd.SyndEnclosure;
import com.sun.syndication.feed.synd.SyndEnclosureImpl;
import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.synd.SyndEntryImpl;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.feed.synd.SyndFeedImpl;
import com.sun.syndication.feed.synd.SyndPerson;
import com.sun.syndication.feed.synd.SyndPersonImpl;
import com.sun.syndication.io.FeedException;
import com.sun.syndication.io.SyndFeedOutput;

public class OCWFeed implements RSSFeed
{
    private SyndFeed feed;

    public OCWFeed() throws MalformedURLException
    {
        feed = new SyndFeedImpl();

        feed.setFeedType("rss_2.0");
        feed.setTitle("Universitat Jaume I OpenCourseware");
        feed.setLink("http://ocw.uji.es/");
        feed.setLanguage("es-es");
        feed.setCopyright("(c) 2011 Universitat Jaume I");
        feed.setDescription("Canal RSS Universitat Jaume I OpenCourseware");
    }

    @SuppressWarnings("unchecked")
    public void addEntry(EntryMetadata metadata)
    {
        SyndEntry entry = new SyndEntryImpl();
        
        entry.setTitle(metadata.getTitle());
        entry.setPublishedDate(metadata.getPubDate());
        entry.setLink(metadata.getLink());
        
        setDescription(metadata, entry);
        setAuthors(metadata, entry);
        setCategories(metadata, entry);
        setEnclosures(metadata, entry);

        feed.getEntries().add(entry);
    }

    private void setDescription(EntryMetadata metadata, SyndEntry entry)
    {
        SyndContent descriptionContent = new SyndContentImpl();
        descriptionContent.setType(MediaType.TEXT_PLAIN);
        descriptionContent.setValue(metadata.getDescription());
        entry.setDescription(descriptionContent);
    }

    private void setEnclosures(EntryMetadata metadata, SyndEntry entry)
    {
        List<SyndEnclosure> enclosureList = new ArrayList<SyndEnclosure>();
        
        for (Enclosure enclosure : metadata.getEnclosures())
        {
            SyndEnclosure syndEnclosure = new SyndEnclosureImpl();
            syndEnclosure.setType(enclosure.getType());
            syndEnclosure.setUrl(enclosure.getUrl());
            
            if (enclosure.getLength() != null)
            {
                syndEnclosure.setLength(enclosure.getLength());
            }
            
            enclosureList.add(syndEnclosure);
        }
        
        entry.setEnclosures(enclosureList);
    }

    private void setCategories(EntryMetadata metadata, SyndEntry entry)
    {
        List<SyndCategory> categoryList = new ArrayList<SyndCategory>();
        
        for (String category : metadata.getCategories())
        {
            SyndCategory syndCategory = new SyndCategoryImpl();
            syndCategory.setName(category);
            
            categoryList.add(syndCategory);
        }
        
        entry.setCategories(categoryList);
    }

    private void setAuthors(EntryMetadata metadata, SyndEntry entry)
    {
        List<SyndPerson> authorList = new ArrayList<SyndPerson>();
        
        for (String author : metadata.getAuthors())
        {
            SyndPerson syndPerson = new SyndPersonImpl();
            syndPerson.setName(author);
            syndPerson.setEmail("x");
            
            authorList.add(syndPerson);
        }
        
        entry.setAuthors(authorList);
    }

    public Document getXML() throws FeedException
    {
        SyndFeedOutput output = new SyndFeedOutput();
        return output.outputW3CDom(feed);
    }
}