package es.uji.apps.ocw.services;

import com.sun.syndication.io.FeedException;
import es.uji.apps.ocw.model.*;
import es.uji.apps.ocw.services.rss.*;
import es.uji.apps.ocw.ui.*;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.content.ContentCleaner;
import es.uji.commons.rest.content.PlainTextHTMLCleaner;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import es.uji.commons.sso.AccessManager;
import es.uji.commons.web.template.HTMLTemplate;
import es.uji.commons.web.template.Template;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.MessageFormat;
import java.util.*;

@Service
@Path("publicacion")
public class PublicacionService extends CoreBaseService
{
    private final String urlBase;
    private final String urlBasePreview;
    private String idiomaPrincipal;
    private String idiomaAplicacion;

    public PublicacionService(@Value("${uji.webapp.urlBase}") String urlBase,
            @Value("${uji.webapp.urlBasePreview}") String urlBasePreview)
    {
        this.urlBase = urlBase;
        this.urlBasePreview = urlBasePreview;

        try
        {
            this.setIdiomaPrincipal(Idioma.getIdiomasEdicion().get(0).getAcronimo());
        }
        catch (Exception e)
        {
            this.setIdiomaPrincipal("CA");
        }
    }

    @GET
    @Produces(MediaType.TEXT_HTML)
    public Template portada(@CookieParam("uji-lang") String cookieIdioma)
            throws RegistroNoEncontradoException, UnsupportedEncodingException
    {
        String idioma = getIdioma(cookieIdioma);

        List<Noticia> listaNoticias = Noticia.getUltimasNoticias(idioma);

        List<Curso> listaUltimosCursos = Curso.getUltimosCincoCursos();
        List<Long> listaCursosIds = extraeIdsDeCursos(listaUltimosCursos);
        List<CursoMultiIdioma> listaCursosMultiIdioma = CursoMultiIdioma
                .getCursosByIdsAndIdioma(listaCursosIds, idioma);
        Map<Long, CursoUI> hashUltimosCursosUI = cursosMultiIdiomaToUI(listaCursosMultiIdioma);
        List<CursoUI> listaUltimosCursosUI = mapToList(listaUltimosCursos, hashUltimosCursosUI);

        List<Material> listaMaterialesMasVisitados = Material.getMaterialesMasVisitados();
        List<MaterialUI> listaMaterialesMasVisitadosUI = materialesToUIPortada(
                listaMaterialesMasVisitados);

        List<Curso> listaCursosMasVisitados = Curso.getCincoCursosMasVisitados();
        listaCursosIds = extraeIdsDeCursos(listaCursosMasVisitados);
        listaCursosMultiIdioma = CursoMultiIdioma.getCursosByIdsAndIdioma(listaCursosIds, idioma);
        Map<Long, CursoUI> hashCursosMasVisitadosUI = cursosMultiIdiomaToUI(listaCursosMultiIdioma);
        List<CursoUI> listaCursosMasVisitadosUI = mapToList(listaCursosMasVisitados,
                hashCursosMasVisitadosUI);

        List<Enlace> listaEnlacesInteres = Enlace.getEnlacesInteres(idioma);
        List<EnlaceUI> listaEnlacesInteresUI = enlacesToUI(listaEnlacesInteres);

        BreadCrumb breadcrumb = new BreadCrumb(this.urlBase, idioma.toLowerCase());
        breadcrumb.generaMigasPortada();

        Template template = new HTMLTemplate("ocw/portada", new Locale(idioma), "ocw");
        rellenarComunesTemplate(template, idioma);
        template.put("noticias", listaNoticias);
        template.put("ultimosCursos", listaUltimosCursosUI);
        template.put("cursosMasVisitados", listaCursosMasVisitadosUI);
        template.put("materialesMasVisitados", listaMaterialesMasVisitadosUI);
        template.put("enlacesInteres", listaEnlacesInteresUI);
        template.put("breadcrumb", breadcrumb);

        return template;
    }

    private List<CursoUI> mapToList(List<Curso> listaUltimosCursos,
            Map<Long, CursoUI> hashUltimosCursosUI)
    {
        List<CursoUI> listaCursosUI = new ArrayList<CursoUI>();

        for (Curso curso : listaUltimosCursos)
        {
            listaCursosUI.add(hashUltimosCursosUI.get(curso.getId()));
        }
        return listaCursosUI;
    }

    private List<Long> extraeIdsDeCursos(List<Curso> listaUltimosCursos)
    {
        List<Long> listaCursosIds = new ArrayList<Long>();
        for (Curso curso : listaUltimosCursos)
        {
            listaCursosIds.add(curso.getId());
        }

        return listaCursosIds;
    }

    private String getIdioma(String cookieIdioma)
    {
        if (cookieIdioma != null)
        {
            idiomaAplicacion = cookieIdioma.toUpperCase();
            return cookieIdioma.toUpperCase();
        }
        else
        {
            idiomaAplicacion = "CA";
            return "CA";
        }
    }

    @GET
    @Path("curso/{cursoId}")
    @Produces(MediaType.TEXT_HTML)
    public Template detalleDeCurso(@PathParam("cursoId") String cursoId,
            @CookieParam("uji-lang") String cookieIdioma) throws RegistroNoEncontradoException
    {
        String idioma = getIdioma(cookieIdioma);

        // Obtenemos el curso según el idioma
        List<Long> listaCursosIds = new ArrayList<Long>(1);
        listaCursosIds.add(Long.parseLong(cursoId));
        List<CursoMultiIdioma> listaCursosMultiIdioma = CursoMultiIdioma
                .getCursosByIdsAndIdioma(listaCursosIds, idioma);

        // Contabilizamos acceso al curso
        Acceso acceso = new Acceso();
        acceso.setCurso(Curso.getCursoById(Long.parseLong(cursoId)));
        acceso.setFecha(new Date());
        acceso.setIp(request.getRemoteAddr());
        Acceso.contabilizaAcceso(acceso);

        return getTemplateDetalleDeCurso(cursoId, listaCursosMultiIdioma, idioma, this.urlBase);
    }

    @GET
    @Path("curso/{cursoId}/preview")
    @Produces(MediaType.TEXT_HTML)
    public Template detalleDeCursoVistaPrevia(@PathParam("cursoId") String cursoId,
            @CookieParam("uji-lang") String cookieIdioma) throws RegistroNoEncontradoException
    {
        String idioma = getIdioma(cookieIdioma);

        // Obtenemos el curso según el idioma
        List<Long> listaCursosIds = new ArrayList<Long>(1);
        listaCursosIds.add(Long.parseLong(cursoId));
        Curso curso = Curso.getCursoById(Long.parseLong(cursoId));

        Long userId = AccessManager.getConnectedUserId(request);
        if (userId == null)
        {
            throw new RuntimeException("Usuario no conectado");
        }

        Persona persona = Persona.getPersonaById(userId);
        if (!persona.isAdministrador() && !curso.esPropietario(persona)
                && !curso.isAutorizadoByCurso(persona.getId()))
        {
            throw new RegistroNoEncontradoException();
        }

        List<CursoMultiIdioma> listaCursosMultiIdioma = CursoMultiIdioma
                .getCursosByIdsAndIdiomaVistaPrevia(listaCursosIds, idioma);

        return getTemplateDetalleDeCurso(cursoId, listaCursosMultiIdioma, idioma,
                this.urlBasePreview);
    }

    private Template getTemplateDetalleDeCurso(String cursoId,
            List<CursoMultiIdioma> listaCursosMultiIdioma, String idioma, String url)
    {
        Map<Long, CursoUI> hashCursosUI = cursosMultiIdiomaToUI(listaCursosMultiIdioma);

        CursoUI cursoUI = hashCursosUI.get(Long.parseLong(cursoId));

        // Materiales agrupados

        List<CursoMaterialGrupo> listaCursoMateriales = CursoMaterialGrupo
                .getElementosByCurso(Long.parseLong(cursoId));
        List<MaterialGrupoUI> listaMaterialGrupoUI = new ArrayList<MaterialGrupoUI>();

        for (CursoMaterialGrupo cursoMaterialGrupo : listaCursoMateriales)
        {
            MaterialGrupoUI materialGrupoUI = new MaterialGrupoUI();

            if (cursoMaterialGrupo.getTipo().equals("GRUPO"))
            {
                materialGrupoUI.setTitulo(cursoMaterialGrupo.getTitulo());
                materialGrupoUI.setDescripcion(cursoMaterialGrupo.getDescripcion());
                materialGrupoUI.setTipo("GRUPO");
            }
            else if (cursoMaterialGrupo.getTipo().equals("MAT"))
            {
                materialGrupoUI.setId(cursoMaterialGrupo.getMaterialId().toString());
                materialGrupoUI.setTitulo(cursoMaterialGrupo.getTitulo());
                String desc = cursoMaterialGrupo.getDescripcion();
                if (desc == null || desc.trim().isEmpty())
                {
                    desc = cursoMaterialGrupo.getDescripcionMaterial();
                }
                materialGrupoUI.setDescripcion(removeHtmlTagsFromString(desc));
                materialGrupoUI.setFecha(cursoMaterialGrupo.getFecha(), idiomaAplicacion);
                materialGrupoUI
                        .setIcono(getIconoFromContentType(cursoMaterialGrupo.getContentType()));
                materialGrupoUI.setIdioma(cursoMaterialGrupo.getIdioma());
                materialGrupoUI.setUrlMaterial(url + "material/" + materialGrupoUI.getId());
                if (cursoMaterialGrupo.getContentType() == null
                        || cursoMaterialGrupo.getContentType().equals(""))
                {
                    materialGrupoUI.setNombreFichero("URL");
                    materialGrupoUI.setUrl(cursoMaterialGrupo.getUrl());
                    materialGrupoUI.setTipo("ENLACE");
                }
                else
                {
                    materialGrupoUI.setNombreFichero(cursoMaterialGrupo.getNombreFichero());
                    materialGrupoUI.setUrl(
                            url + "material/" + cursoMaterialGrupo.getMaterialId() + "/raw");
                    materialGrupoUI.setTipo("DOCUMENTO");
                }
            }

            materialGrupoUI.setNivel(cursoMaterialGrupo.getNivel().toString());

            listaMaterialGrupoUI.add(materialGrupoUI);
        }

        BreadCrumb breadcrumb = new BreadCrumb(url, idioma);
        breadcrumb.generaMigasDetalleCurso(cursoUI);

        Template template = new HTMLTemplate("ocw/curso", new Locale(idioma), "ocw");
        rellenarComunesTemplate(template, idioma);
        template.put("curso", cursoUI);
        template.put("materiales", listaMaterialGrupoUI);
        template.put("breadcrumb", breadcrumb);

        return template;
    }

    @GET
    @Path("cursos/categoria/{categoriaId}")
    @Produces(MediaType.TEXT_HTML)
    public Template cursosPorCategoria(@PathParam("categoriaId") String categoriaId,
            @QueryParam("pagina") String numPagina, @CookieParam("uji-lang") String cookieIdioma)
            throws RegistroNoEncontradoException
    {
        String idioma = getIdioma(cookieIdioma);

        Categoria categoriaActiva = Categoria.getCategoriaByIdAndIdioma(Long.parseLong(categoriaId),
                idioma);

        CategoriaUI categoriaUI = categoriaToUI(categoriaActiva);

        Integer pagina = 1;
        Integer totalPaginas = Curso
                .getNumeroTotalPaginasByCategoriaId(Long.parseLong(categoriaId));

        if (numPagina != null && !numPagina.isEmpty() && Integer.parseInt(numPagina) > 1)
        {
            pagina = Integer.parseInt(numPagina);

            if (pagina > totalPaginas)
            {
                pagina = totalPaginas;
            }
        }

        String parametros = "";
        Pagination pagination = new Pagination(parametros, pagina, totalPaginas);

        List<Curso> listaCursosPorCategoria = Curso
                .getCursosPaginadosByCategoriaId(Long.parseLong(categoriaId), pagina);
        List<Long> listaCursosIds = extraeIdsDeCursos(listaCursosPorCategoria);
        List<CursoMultiIdioma> listaCursosMultiIdioma = CursoMultiIdioma
                .getCursosByIdsAndIdioma(listaCursosIds, idioma);
        Map<Long, CursoUI> hashCursosPorCategoriaUI = cursosMultiIdiomaToUI(listaCursosMultiIdioma);
        List<CursoUI> listaCursosPorCategoriaUI = mapToList(listaCursosPorCategoria,
                hashCursosPorCategoriaUI);

        BreadCrumb breadcrumb = new BreadCrumb(this.urlBase, idioma);
        breadcrumb.generaMigasListaCursos(categoriaUI);

        Template template = new HTMLTemplate("ocw/cursos-por-categoria", new Locale(idioma), "ocw");
        rellenarComunesTemplate(template, idioma);
        template.put("cursosPorCategoria", listaCursosPorCategoriaUI);
        template.put("categoria", categoriaUI);
        template.put("pagination", pagination);
        template.put("breadcrumb", breadcrumb);

        return template;
    }

    @GET
    @Path("beta")
    @Produces(MediaType.TEXT_HTML)
    public Template beta(@CookieParam("uji-lang") String cookieIdioma)
            throws RegistroNoEncontradoException
    {
        String idioma = getIdioma(cookieIdioma);

        BreadCrumb breadcrumb = new BreadCrumb(this.urlBase, idioma);
        breadcrumb.generaMigasQueEs();

        Template template = new HTMLTemplate("ocw/beta", new Locale(idioma), "ocw");
        rellenarComunesTemplate(template, idioma);
        template.put("breadcrumb", breadcrumb);

        return template;
    }

    @GET
    @Path("quees")
    @Produces(MediaType.TEXT_HTML)
    public Template quees(@CookieParam("uji-lang") String cookieIdioma)
            throws RegistroNoEncontradoException
    {
        String idioma = getIdioma(cookieIdioma);

        BreadCrumb breadcrumb = new BreadCrumb(this.urlBase, idioma);
        breadcrumb.generaMigasQueEs();

        Template template = new HTMLTemplate("ocw/quees", new Locale(idioma), "ocw");
        rellenarComunesTemplate(template, idioma);
        template.put("breadcrumb", breadcrumb);

        return template;
    }

    @GET
    @Path("faq")
    @Produces(MediaType.TEXT_HTML)
    public Template faq(@CookieParam("uji-lang") String cookieIdioma)
            throws RegistroNoEncontradoException
    {
        String idioma = getIdioma(cookieIdioma);

        BreadCrumb breadcrumb = new BreadCrumb(this.urlBase, idioma);
        breadcrumb.generaMigasFaq();

        Template template = new HTMLTemplate("ocw/faq", new Locale(idioma), "ocw");
        rellenarComunesTemplate(template, idioma);
        template.put("breadcrumb", breadcrumb);

        return template;
    }

    @GET
    @Path("cursos/")
    @Produces(MediaType.TEXT_HTML)
    public Template cursos(@QueryParam("pagina") String numPagina,
            @CookieParam("uji-lang") String cookieIdioma) throws RegistroNoEncontradoException
    {
        String idioma = getIdioma(cookieIdioma);

        Integer pagina = 1;
        Integer totalPaginas = Curso.getNumeroTotalPaginas();

        if (numPagina != null && !numPagina.isEmpty() && Integer.parseInt(numPagina) > 1)
        {
            pagina = Integer.parseInt(numPagina);

            if (pagina > totalPaginas)
            {
                pagina = totalPaginas;
            }
        }

        List<Curso> listaCursos = Curso.getTodosLosCursosPaginados(pagina);
        List<Long> listaCursosIds = extraeIdsDeCursos(listaCursos);
        List<CursoMultiIdioma> listaCursosMultiIdioma = CursoMultiIdioma
                .getCursosByIdsAndIdioma(listaCursosIds, idioma);
        Map<Long, CursoUI> hashCursosUI = cursosMultiIdiomaToUI(listaCursosMultiIdioma);
        List<CursoUI> listaCursosUI = mapToList(listaCursos, hashCursosUI);

        String parametros = "";
        Pagination pagination = new Pagination(parametros, pagina, totalPaginas);

        BreadCrumb breadcrumb = new BreadCrumb(this.urlBase, idioma);
        breadcrumb.generaMigasListaCursos();

        Template template = new HTMLTemplate("ocw/cursos", new Locale(idioma), "ocw");
        rellenarComunesTemplate(template, idioma);
        template.put("cursos", listaCursosUI);
        template.put("pagination", pagination);
        template.put("breadcrumb", breadcrumb);

        return template;
    }

    @GET
    @Path("busqueda")
    @Produces(MediaType.TEXT_HTML)
    public Template busquedaSimple(@QueryParam("cadena") String cadena,
            @QueryParam("pagina") String numPagina, @CookieParam("uji-lang") String cookieIdioma)
            throws RegistroNoEncontradoException, UnsupportedEncodingException
    {
        String idioma = getIdioma(cookieIdioma);
        Integer totalPaginas = Curso.getNumeroTotalPaginasBusquedaSimple(cadena);
        Integer pagina = 1;

        if (numPagina != null && !numPagina.isEmpty() && Integer.parseInt(numPagina) > 1)
        {
            pagina = Integer.parseInt(numPagina);

            if (pagina > totalPaginas)
            {
                pagina = totalPaginas;
            }
        }

        List<Curso> listaCursos = Curso.getCursosPaginadosBusquedaSimple(cadena, pagina);
        List<Long> listaCursosIds = extraeIdsDeCursos(listaCursos);
        List<CursoMultiIdioma> listaCursosMultiIdioma = CursoMultiIdioma
                .getCursosByIdsAndIdioma(listaCursosIds, idioma);
        Map<Long, CursoUI> hashCursosUI = cursosMultiIdiomaToUI(listaCursosMultiIdioma);
        List<CursoUI> listaCursosBusquedaUI = mapToList(listaCursos, hashCursosUI);

        String parametros = "cadena=" + cadena;
        Pagination pagination = new Pagination(parametros, pagina, totalPaginas);

        BreadCrumb breadcrumb = new BreadCrumb(this.urlBase, idioma);
        breadcrumb.generaMigasListaCursosBusqueda();

        Template template = new HTMLTemplate("ocw/cursos-busqueda", new Locale(idioma), "ocw");
        rellenarComunesTemplate(template, idioma);
        template.put("textoBusquedaPag", URLEncoder.encode(cadena, "UTF-8"));
        template.put("cursosBusqueda", listaCursosBusquedaUI);
        template.put("pagination", pagination);
        template.put("breadcrumb", breadcrumb);

        return template;
    }

    @GET
    @Path("enlaces")
    @Produces(MediaType.TEXT_HTML)
    public Template enlacesInteres(@QueryParam("pagina") String numPagina,
            @CookieParam("uji-lang") String cookieIdioma)
    {
        String idioma = getIdioma(cookieIdioma);

        Integer pagina = 1;
        Integer totalPaginas = Enlace.getNumeroTotalPaginasEnlaces(idioma);

        if (numPagina != null && !numPagina.isEmpty() && Integer.parseInt(numPagina) > 1)
        {
            pagina = Integer.parseInt(numPagina);

            if (pagina > totalPaginas)
            {
                pagina = totalPaginas;
            }
        }

        String parametros = "";
        Pagination pagination = new Pagination(parametros, pagina, totalPaginas);

        List<Enlace> listaEnlacesInteres = Enlace.getEnlacesInteresPaginados(idioma, pagina);
        List<EnlaceUI> listaEnlacesInteresUI = enlacesToUI(listaEnlacesInteres);

        BreadCrumb breadcrumb = new BreadCrumb(this.urlBase, idioma);
        breadcrumb.generaMigasEnlacesInteres();

        Template template = new HTMLTemplate("ocw/enlaces-interes", new Locale(idioma), "ocw");
        rellenarComunesTemplate(template, idioma);
        template.put("enlacesInteres", listaEnlacesInteresUI);
        template.put("pagination", pagination);
        template.put("breadcrumb", breadcrumb);

        return template;
    }

    @GET
    @Path("materiales")
    @Produces(MediaType.TEXT_HTML)
    public Template materiales(@QueryParam("pagina") String numPagina,
            @CookieParam("uji-lang") String cookieIdioma)
    {
        String idioma = getIdioma(cookieIdioma);

        Integer pagina = 1;
        Integer totalPaginas = Material.getNumeroTotalPaginasMateriales();

        if (numPagina != null && !numPagina.isEmpty() && Integer.parseInt(numPagina) > 1)
        {
            pagina = Integer.parseInt(numPagina);

            if (pagina > totalPaginas)
            {
                pagina = totalPaginas;
            }
        }

        String parametros = "";
        Pagination pagination = new Pagination(parametros, pagina, totalPaginas);

        List<Material> listaMateriales = Material.getTodosMaterialesPaginados(pagina);
        List<MaterialUI> listaMaterialesUI = materialesToUI(listaMateriales);

        BreadCrumb breadcrumb = new BreadCrumb(this.urlBase, idioma);
        breadcrumb.generaMigasMateriales();

        Template template = new HTMLTemplate("ocw/materiales", new Locale(idioma), "ocw");
        rellenarComunesTemplate(template, idioma);
        template.put("materiales", listaMaterialesUI);
        template.put("pagination", pagination);
        template.put("breadcrumb", breadcrumb);

        return template;
    }

    @GET
    @Path("material/{materialId}")
    public Template detalleMaterial(@PathParam("materialId") String materialId,
            @CookieParam("uji-lang") String cookieIdioma)
            throws RegistroNoEncontradoException, NumberFormatException
    {
        String idioma = getIdioma(cookieIdioma);

        Material material = Material.getMaterialById(Long.parseLong(materialId));

        // Contabilizamos acceso al material
        Acceso acceso = new Acceso();
        acceso.setMaterial(material);
        acceso.setFecha(new Date());
        acceso.setIp(request.getRemoteAddr());
        acceso.setDescripcion("ACCESO");
        Acceso.contabilizaAcceso(acceso);

        MaterialUI materialUI = materialToUI(material);

        // Obtenemos los autores
        List<MaterialAutor> listaMaterialAutores = material.getAutores();
        List<String> autores = new ArrayList<String>();

        for (MaterialAutor materialAutor : listaMaterialAutores)
        {
            AutorCompleto autor = Autor.getAutorCompletoById(materialAutor.getAutor().getId());
            String nombre = autor.getNombre() + " " + autor.getApellido1() + " "
                    + autor.getApellido2();
            autores.add(nombre.trim());
        }
        materialUI.setAutores(autores);

        // Obtenemos los cursos
        List<Curso> listaCursos = Curso.getCursosByMaterialId(Long.parseLong(materialId));
        List<Long> listaCursosIds = extraeIdsDeCursos(listaCursos);
        List<CursoMultiIdioma> listaCursosMultiIdioma = CursoMultiIdioma
                .getCursosByIdsAndIdioma(listaCursosIds, idioma);
        Map<Long, CursoUI> hashCursosUI = cursosMultiIdiomaToUI(listaCursosMultiIdioma);
        List<CursoUI> listaCursosUI = mapToList(listaCursos, hashCursosUI);

        // List<CursoUI> listaCursosUI = cursosToUI(listaCursos);

        BreadCrumb breadcrumb = new BreadCrumb(this.urlBase, idioma);
        breadcrumb.generaMigasDetalleMaterial(material);

        Template template = new HTMLTemplate("ocw/material", new Locale(idioma), "ocw");
        rellenarComunesTemplate(template, idioma);
        template.put("material", materialUI);
        template.put("cursos", listaCursosUI);
        template.put("breadcrumb", breadcrumb);

        return template;
    }

    @GET
    @Path("rss")
    @Produces("application/rss+xml")
    public Document rss() throws IOException, FeedException, ParserConfigurationException
    {
        RSSFeed feed = new OCWFeed();

        List<CursoRSS> cursosRSS = CursoRSS.getUltimosCincoCursosRSS();

        for (CursoRSS cursoRSS : cursosRSS)
        {
            EntryMetadata metadata = new EntryMetadata();
            metadata.setTitle(cursoRSS.getTitulo());
            metadata.setLink(cursoRSS.getUrl());
            metadata.setPubDate(cursoRSS.getFechaCreacion());
            metadata.setDescription(cursoRSS.getDescripcion());
            for (String categoria : cursoRSS.getCategorias())
            {
                metadata.addCategory(categoria);
            }

            Enclosure enclosure = new Enclosure();
            enclosure.setType(cursoRSS.getImagen().getType());
            enclosure.setUrl(cursoRSS.getImagen().getUrl());
            metadata.addEnclosure(enclosure);

            feed.addEntry(metadata);

        }
        return feed.getXML();
    }

    @GET
    @Path("rss/cursos/mas-visitados")
    @Produces("application/rss+xml")
    public Document cursosMasVisitadosRss()
            throws IOException, FeedException, ParserConfigurationException
    {
        RSSFeed feed = new OCWFeed();

        List<CursoRSS> cursosRSS = CursoRSS.getCincoCursosMasVisitadosRSS();

        for (CursoRSS cursoRSS : cursosRSS)
        {
            EntryMetadata metadata = new EntryMetadata();
            metadata.setTitle(cursoRSS.getTitulo());
            metadata.setLink(cursoRSS.getUrl());
            metadata.setPubDate(cursoRSS.getFechaCreacion());
            metadata.setDescription(cursoRSS.getDescripcion());
            for (String categoria : cursoRSS.getCategorias())
            {
                metadata.addCategory(categoria);
            }

            Enclosure enclosure = new Enclosure();
            enclosure.setType(cursoRSS.getImagen().getType());
            enclosure.setUrl(cursoRSS.getImagen().getUrl());
            metadata.addEnclosure(enclosure);

            feed.addEntry(metadata);

        }
        return feed.getXML();
    }

    @GET
    @Path("material/{materialId}/raw")
    public Response getMaterialDocumento(@PathParam("materialId") String materialId)
            throws RegistroNoEncontradoException, Exception
    {
        Material material = Material.getMaterialConFicheroById(Long.parseLong(materialId));

        byte[] documento = null;
        String nombreFichero = null;
        String contentType = null;

        documento = material.getDocumento();

        if (documento != null)
        {
            nombreFichero = material.getNombreFichero();
            contentType = material.getContentType();

            // Contabilizamos descarga del material
            Acceso acceso = new Acceso();
            acceso.setMaterial(material);
            acceso.setFecha(new Date());
            acceso.setIp(request.getRemoteAddr());
            acceso.setDescripcion("DESCARGA");
            Acceso.contabilizaAcceso(acceso);

        }

        return Response.ok(documento)
                .header("Content-Disposition", "attachment; filename = \"" + nombreFichero + "\"")
                .header("Content-Length", documento.length).type(contentType).build();
    }

    @GET
    @Path("cursos/tag/{tagId}")
    @Produces(MediaType.TEXT_HTML)
    public Template cursosPorTag(@PathParam("tagId") String tagId,
            @QueryParam("pagina") String numPagina, @CookieParam("uji-lang") String cookieIdioma)
            throws RegistroNoEncontradoException, UnsupportedEncodingException
    {
        String idioma = getIdioma(cookieIdioma);

        /*
         * Categoria categoriaActiva = Categoria.getCategoriaByIdAndIdioma(
         * Long.parseLong(categoriaId), idioma);
         * 
         * CategoriaUI categoriaUI = categoriaToUI(categoriaActiva);
         */

        Tag tag = Tag.getTagById(Long.parseLong(tagId));
        TagUI tagUI = new TagUI();
        tagUI.setId(tag.getId().toString());
        tagUI.setNombre(tag.getNombre());

        Integer pagina = 1;
        Integer totalPaginas = Curso.getNumeroTotalPaginasByTagId(Long.parseLong(tagId));

        if (numPagina != null && !numPagina.isEmpty() && Integer.parseInt(numPagina) > 1)
        {
            pagina = Integer.parseInt(numPagina);

            if (pagina > totalPaginas)
            {
                pagina = totalPaginas;
            }
        }

        String parametros = "";
        Pagination pagination = new Pagination(parametros, pagina, totalPaginas);

        List<Curso> listaCursosPorTag = Curso.getCursosPaginadosByTagId(Long.parseLong(tagId),
                pagina);
        List<Long> listaCursosIds = extraeIdsDeCursos(listaCursosPorTag);
        List<CursoMultiIdioma> listaCursosMultiIdioma = CursoMultiIdioma
                .getCursosByIdsAndIdioma(listaCursosIds, idioma);
        Map<Long, CursoUI> hashCursosPorTagUI = cursosMultiIdiomaToUI(listaCursosMultiIdioma);
        List<CursoUI> listaCursosPorTagUI = mapToList(listaCursosPorTag, hashCursosPorTagUI);

        BreadCrumb breadcrumb = new BreadCrumb(this.urlBase, idioma);
        breadcrumb.generaMigasListaCursosPorTag(tag);

        Template template = new HTMLTemplate("ocw/cursos-por-tag", new Locale(idioma), "ocw");
        rellenarComunesTemplate(template, idioma);
        template.put("cursosPorTag", listaCursosPorTagUI);
        template.put("tag", tagUI);
        template.put("pagination", pagination);
        template.put("breadcrumb", breadcrumb);

        return template;
    }

    private List<EnlaceUI> enlacesToUI(List<Enlace> listaEnlaces)
    {
        List<EnlaceUI> enlacesUI = new ArrayList<EnlaceUI>();

        for (Enlace enlace : listaEnlaces)
        {
            EnlaceUI enlaceUI = new EnlaceUI();

            for (EnlaceIdioma enlaceIdioma : enlace.getEnlacesIdiomas()) // 1 resultado
            {
                enlaceUI.setUrl(enlaceIdioma.getUrl());
                enlaceUI.setNombre(enlaceIdioma.getNombre());
                enlaceUI.setDescripcion(enlaceIdioma.getDescripcion());
            }

            enlacesUI.add(enlaceUI);
        }

        return enlacesUI;
    }

    private void rellenarComunesTemplate(Template template, String idiomaSeleccionado)
    {
        List<Categoria> listaCategorias = Categoria.getCategoriasByIdioma(idiomaSeleccionado);
        List<CategoriaUI> listaCategoriasUI = categoriasToUI(idiomaSeleccionado, listaCategorias);

        List<Enlace> listaEnlacesOCW = Enlace.getEnlacesOCW(idiomaSeleccionado);
        List<EnlaceUI> listaEnlacesOCWUI = enlacesToUI(listaEnlacesOCW);

        List<TagCuenta> listaTagsCuenta = TagCuenta.getTagsCuenta();
        List<TagUI> nubeTags = tagCuentaToUI(listaTagsCuenta);

        template.put("urlBase", urlBase);
        template.put("page_title", "OpenCourseWare");
        template.put("categorias", listaCategoriasUI);
        template.put("enlacesOCW", listaEnlacesOCWUI);
        template.put("nubeTags", nubeTags);
        template.put("idioma", idiomaSeleccionado);
    }

    private List<TagUI> tagCuentaToUI(List<TagCuenta> listaTagsCuenta)
    {
        List<TagUI> listaTagsUI = new ArrayList<TagUI>();

        for (TagCuenta tagCuenta : listaTagsCuenta)
        {
            TagUI tagUI = new TagUI();

            tagUI.setId(tagCuenta.getId().toString());
            tagUI.setNombre(tagCuenta.getNombre());

            tagUI.setImportancia(getImportancia(tagCuenta.getTotal()));

            listaTagsUI.add(tagUI);

            if (listaTagsUI.size() > 8)
            {
                break;
            }
        }

        return listaTagsUI;
    }

    private String getImportancia(Long total)
    {

        String importancia = "w1";

        if (total > 20)
        {
            importancia = "w5";
        }
        else if (total > 9)
        {
            importancia = "w4";
        }
        else if (total > 3)
        {
            importancia = "w3";
        }
        else if (total > 1)
        {
            importancia = "w2";
        }

        return importancia;
    }

    public String getIdiomaPrincipal()
    {
        return idiomaPrincipal;
    }

    public void setIdiomaPrincipal(String idiomaPrincipal)
    {
        this.idiomaPrincipal = idiomaPrincipal;
    }

    private Map<Long, CursoUI> cursosMultiIdiomaToUI(List<CursoMultiIdioma> listaCursosMultiIdioma)
    {

        Map<Long, CursoUI> hashCursosUI = new HashMap<Long, CursoUI>();
        Map<Long, List<Long>> hashCursosCategorias = new HashMap<Long, List<Long>>();
        Map<Long, List<Long>> hashCursosTags = new HashMap<Long, List<Long>>();

        for (CursoMultiIdioma cursoMultiIdioma : listaCursosMultiIdioma)
        {
            CursoUI cursoUI = hashCursosUI.get(cursoMultiIdioma.getId());
            if (cursoUI == null)
            {
                cursoUI = new CursoUI();

                cursoUI.setId(cursoMultiIdioma.getId().toString());
                cursoUI.setTitulo(cursoMultiIdioma.getNombre());
                cursoUI.setDescripcion(
                        sustituyeSaltosDeLineaHTML(cursoMultiIdioma.getDescripcion()));

                if (cursoMultiIdioma.getDescripcion() != null)
                {
                    if (cursoMultiIdioma.getDescripcion().length() > 297)
                    {
                        cursoUI.setDescripcionLimpia(
                                cursoMultiIdioma.getDescripcion().substring(0, 297));
                    }
                    else
                    {
                        cursoUI.setDescripcionLimpia(cursoMultiIdioma.getDescripcion());
                    }
                }

                cursoUI.setFecha(cursoMultiIdioma.getFechaCreacion(), idiomaAplicacion);
                cursoUI.setPeriodo(cursoMultiIdioma.getPeriodo());
                cursoUI.setResponsable(cursoMultiIdioma.getCreador());
                cursoUI.setPrograma(sustituyeSaltosDeLineaHTML(cursoMultiIdioma.getPrograma()));
                cursoUI.setGuia(sustituyeSaltosDeLineaHTML(cursoMultiIdioma.getGuiaAprendizaje()));
                cursoUI.setAccesos(cursoMultiIdioma.getNumeroAccesos().toString());

                hashCursosUI.put(cursoMultiIdioma.getId(), cursoUI);
                hashCursosCategorias.put(cursoMultiIdioma.getId(), new ArrayList<Long>());
                hashCursosTags.put(cursoMultiIdioma.getId(), new ArrayList<Long>());

                Long imagenId = cursoMultiIdioma.getImageId();

                if (imagenId != null)
                {
                    cursoUI.setImagen(MessageFormat.format(
                            "http://ujiapps.uji.es/ocw/rest/curso/{0}/imagen/{1,number,#}",
                            cursoUI.getId(), imagenId));
                }
            }

            Long categoriaId = cursoMultiIdioma.getCategoriaId();
            String categoriaNombre = cursoMultiIdioma.getCategoriaNombre();
            Long tagId = cursoMultiIdioma.getTagId();
            String tagNombre = cursoMultiIdioma.getTagNombre();

            if (categoriaId != null)
            {
                List<Long> categoriasIds = hashCursosCategorias.get(cursoMultiIdioma.getId());

                if (!categoriasIds.contains(categoriaId))
                {
                    List<CategoriaUI> categoriasUI = cursoUI.getCategorias();
                    CategoriaUI categoriaUI = new CategoriaUI();
                    categoriaUI.setId(categoriaId.toString());
                    categoriaUI.setNombre(categoriaNombre);
                    categoriasUI.add(categoriaUI);
                    cursoUI.setCategorias(categoriasUI);
                    categoriasIds.add(categoriaId);
                }
            }

            if (tagId != null)
            {
                List<Long> tagsIds = hashCursosTags.get(cursoMultiIdioma.getId());

                if (!tagsIds.contains(tagId))
                {
                    List<TagUI> tagsUI = cursoUI.getTags();
                    TagUI tagUI = new TagUI();
                    tagUI.setId(tagId.toString());
                    tagUI.setNombre(tagNombre);
                    tagsUI.add(tagUI);
                    cursoUI.setTags(tagsUI);
                    tagsIds.add(tagId);
                }
            }

        }

        return hashCursosUI;
    }

    private String sustituyeSaltosDeLineaHTML(String cadena)
    {
        if (cadena == null)
        {
            return null;
        }
        return cadena.replace("\n", "<br />");
    }

    private List<MaterialUI> materialesToUI(List<Material> listaMateriales)
    {
        List<MaterialUI> materialesUI = new ArrayList<MaterialUI>();

        for (Material material : listaMateriales)
        {
            MaterialUI materialUI = materialToUI(material);
            materialesUI.add(materialUI);
        }

        return materialesUI;
    }

    private List<MaterialUI> materialesToUIPortada(List<Material> listaMateriales)
            throws UnsupportedEncodingException
    {
        List<MaterialUI> materialesUI = new ArrayList<MaterialUI>();
        ContentCleaner cleaner = new PlainTextHTMLCleaner();

        for (Material material : listaMateriales)
        {
            MaterialUI materialUI = materialToUI(material);

            String result = "";

            if (materialUI.getDescripcion() != null)
            {
                result = cleaner.clean(materialUI.getDescripcion());
            }

            materialUI.setDescripcion(result);

            materialesUI.add(materialUI);
        }

        return materialesUI;
    }

    private MaterialUI materialToUI(Material material)
    {
        MaterialUI materialUI = new MaterialUI();

        materialUI.setId(material.getId());
        materialUI.setTitulo(material.getTitulo());
        materialUI.setDescripcion(material.getDescripcion());
        materialUI.setFecha(material.getFecha(), idiomaAplicacion);
        materialUI.setIcono(getIconoFromContentType(material.getContentType()));
        materialUI.setIdioma(material.getIdioma().getNombre());

        if (material.getContentType() == null || material.getContentType().equals(""))
        {
            materialUI.setNombreFichero("URL");
            materialUI.setUrl(material.getUrl());
        }
        else
        {
            materialUI.setNombreFichero(material.getNombreFichero());
            materialUI.setUrl(MessageFormat.format("{0}material/{1,number,#}/raw", this.urlBase,
                    material.getId()));
        }

        List<TagUI> listaTagsUI = new ArrayList<TagUI>();
        for (MaterialTag tag : material.getTags())
        {
            TagUI tagUI = new TagUI();
            tagUI.setId(tag.getTag().getId().toString());
            tagUI.setNombre(tag.getTag().getNombre());
            listaTagsUI.add(tagUI);
        }
        materialUI.setTags(listaTagsUI);

        return materialUI;
    }

    private CategoriaUI categoriaToUI(Categoria categoria)
    {

        CategoriaUI categoriaUI = new CategoriaUI();
        CategoriaIdioma categoriaIdioma = categoria.getCategoriasIdiomas().iterator().next();
        categoriaUI.setId(categoria.getId().toString());
        categoriaUI.setNombre(categoriaIdioma.getNombre());

        return categoriaUI;
    }

    private List<CategoriaUI> categoriasToUI(String acronimoIdioma, List<Categoria> listaCategorias)
    {
        List<CategoriaUI> listaCategoriasUI = new ArrayList<CategoriaUI>();

        for (Categoria categoria : Categoria.getCategoriasByIdioma(acronimoIdioma))
        {
            listaCategoriasUI.add(categoriaToUI(categoria));
        }
        return listaCategoriasUI;
    }

    private static String getIconoFromContentType(String contentType)
    {
        Map<String, String> tiposDeContenido = new HashMap<String, String>();
        String url = "http://static.uji.es/img/icons/famfam/silk/page_white.png";

        if (contentType != null)
        {
            tiposDeContenido.put("application/pdf",
                    "http://static.uji.es/img/icons/famfam/silk/page_white_acrobat.png");
            tiposDeContenido.put("application/",
                    "http://static.uji.es/img/icons/famfam/silk/binary_icon.png");
            tiposDeContenido.put("application/octet-stream",
                    "http://static.uji.es/img/icons/famfam/silk/binary_icon.png");
            tiposDeContenido.put("application/msword",
                    "http://static.uji.es/img/icons/famfam/silk/page_word.png");
            tiposDeContenido.put("application/rtf",
                    "http://static.uji.es/img/icons/famfam/silk/page_word.png");
            tiposDeContenido.put("text/html",
                    "http://static.uji.es/img/icons/famfam/silk/html.png");
            tiposDeContenido.put("text/",
                    "http://static.uji.es/img/icons/famfam/silk/page_white_text.png");
            tiposDeContenido.put("image/", "http://static.uji.es/img/icons/famfam/silk/image.png");
            tiposDeContenido.put("audio/", "http://static.uji.es/img/icons/famfam/silk/sound.png");
            tiposDeContenido.put("video/", "http://static.uji.es/img/icons/famfam/silk/film.png");

            if (tiposDeContenido.get(contentType) != null
                    && !tiposDeContenido.get(contentType).isEmpty())
            {
                url = tiposDeContenido.get(contentType);
            }
            else
            {
                for (String key : tiposDeContenido.keySet())
                {
                    if (key.indexOf(contentType) == 0)
                    {
                        url = tiposDeContenido.get(contentType);
                        break;
                    }
                }
            }
        }

        return url;
    }

    private static String removeHtmlTagsFromString(String htmlText)
    {
        try
        {
            return htmlText.replaceAll("\\<[^>]+>", "");
        }
        catch (NullPointerException e)
        {
            return "";
        }
    }
}
