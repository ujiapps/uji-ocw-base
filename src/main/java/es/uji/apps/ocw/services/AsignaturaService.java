package es.uji.apps.ocw.services;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import es.uji.apps.ocw.model.Asignatura;
import es.uji.commons.rest.UIEntity;

@Path("asignaturas")
public class AsignaturaService
{
    @GET
    @Produces(MediaType.TEXT_XML)
    public List<UIEntity> getAsignaturas()
    {
        List<Asignatura> lista = Asignatura.getAsignaturas();
        List<UIEntity> listaUI = UIEntity.toUI(lista);
        return listaUI;
    }
}
