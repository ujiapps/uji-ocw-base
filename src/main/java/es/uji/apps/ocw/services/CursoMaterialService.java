package es.uji.apps.ocw.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import es.uji.apps.ocw.model.CursoMaterial;
import es.uji.apps.ocw.model.CursoMaterialGrupo;
import es.uji.apps.ocw.model.TreeRowNode;
import es.uji.apps.ocw.model.TreeRowsetCursoMaterial;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroConHijosException;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

@Path("cursoMaterial")
public class CursoMaterialService extends CoreBaseService
{

    @POST
    @Produces(MediaType.APPLICATION_XML)
    public TreeRowsetCursoMaterial getMaterialesByCurso(@FormParam("cursoId") String cursoId)
            throws RegistroNoEncontradoException
    {

        TreeRowsetCursoMaterial treeRowset = new TreeRowsetCursoMaterial();

        if (cursoId == null || cursoId.isEmpty())
        {
            return treeRowset;
        }

        HashMap<Long, ArrayList<CursoMaterialGrupo>> nodos = new HashMap<Long, ArrayList<CursoMaterialGrupo>>();
        nodos.put(new Long(0), new ArrayList<CursoMaterialGrupo>());
        
        for (CursoMaterialGrupo elemento : CursoMaterialGrupo.getElementosByCurso(Long
                .parseLong(cursoId)))
        {
            if (nodos.get(elemento.getGrupoId()) == null)
            {
                nodos.put(elemento.getGrupoId(), new ArrayList<CursoMaterialGrupo>());
            }
            if (elemento.getNivel() == 1)
            {
                nodos.get(new Long(0)).add(elemento);
            }
            else
            {
                nodos.get(elemento.getGrupoId()).add(elemento);
            }
        }

        treeRowset = ordenaEstructuraToRowSet(nodos);

        return treeRowset;

    }

    private TreeRowsetCursoMaterial ordenaEstructuraToRowSet(
            HashMap<Long, ArrayList<CursoMaterialGrupo>> nodos)
    {

        TreeRowsetCursoMaterial treeRowset = new TreeRowsetCursoMaterial();

        ArrayList<CursoMaterialGrupo> raiz = nodos.get(new Long(0));

        for (CursoMaterialGrupo elemento : raiz)
        {
            if (elemento.getTipo().equals("MAT"))
            {
                treeRowset.getRow().add(materialToTreeRow(elemento));
            }
            else
            {
                ArrayList<CursoMaterialGrupo> hijos = nodos.get(elemento.getGrupoId());
                treeRowset.getRow().add(grupoToTreeRow(elemento, hijos));
            }
        }

        return treeRowset;

    }

    private TreeRowNode materialToTreeRow(CursoMaterialGrupo elementoRaiz)
    {
        TreeRowNode nodo = new TreeRowNode();

        nodo.setId(elementoRaiz.getCursoMaterialId().toString());
        nodo.setTitle(elementoRaiz.getTitulo());
        nodo.setText(elementoRaiz.getTitulo());
        nodo.setTipo(elementoRaiz.getContentType());
        nodo.setDescripcion(elementoRaiz.getDescripcion());
        Long orden = elementoRaiz.getOrden();
        if (orden != null)
        {
            nodo.setOrden(orden.toString());
        }

        return nodo;
    }

    private TreeRowNode grupoToTreeRow(CursoMaterialGrupo elementoRaiz,
            ArrayList<CursoMaterialGrupo> hijos)
    {
        TreeRowNode nodo = new TreeRowNode();
        for (CursoMaterialGrupo hijo : hijos)
        {
            nodo.getHijos().add(materialToTreeRow(hijo));
        }
        nodo.setId(elementoRaiz.getGrupoId().toString());
        nodo.setTitle(elementoRaiz.getTitulo());
        nodo.setText(elementoRaiz.getTitulo());
        nodo.setDescripcion(elementoRaiz.getDescripcion());
        nodo.setLeaf("false");
        return nodo;
    }

    @PUT
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.TEXT_XML)
    @Path("mover/{cursoMaterialId}")
    public List<UIEntity> mueveCursoMaterial(@PathParam("cursoMaterialId") String cursoMaterialId,
            MultivaluedMap<String, String> params) throws RegistroNoEncontradoException,
            NumberFormatException
    {
        CursoMaterial cursoMaterial = CursoMaterial.getCursoMaterialById(Long
                .parseLong(cursoMaterialId));
        Long anteriorId = Long.parseLong(params.getFirst("anteriorId"));
        String tipoAnterior = params.getFirst("tipoAnterior");

        Long grupoPadreId = Long.parseLong(params.getFirst("grupoPadre"));
        cursoMaterial.moverMaterial(anteriorId, grupoPadreId, tipoAnterior);

        UIEntity entity = UIEntity.toUI(cursoMaterial);
        return Collections.singletonList(entity);
    }

    @DELETE
    @Path("{cursoMaterialId}")
    @Consumes(MediaType.TEXT_XML)
    public void deleteCursoMaterial(@PathParam("cursoMaterialId") String cursoMaterialId)
            throws RegistroConHijosException, RegistroNoEncontradoException

    {
        CursoMaterial.delete(Long.parseLong(cursoMaterialId));
    }
}
