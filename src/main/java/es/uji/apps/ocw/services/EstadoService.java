package es.uji.apps.ocw.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import es.uji.apps.ocw.model.Tag;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

@Path("estado")
public class EstadoService
{
//    @GET
//    @Produces(MediaType.TEXT_XML)
//    public List<UIEntity> getEstados()
//    {
//        List<Estado> listaEstados = Tag.getEstados();
//        List<UIEntity> lista = UIEntity.toUI(listaEstados);
//        return lista;
//
//    }
//
//    @POST
//    @Produces(MediaType.TEXT_XML)
//    public List<UIEntity> insertTag(UIEntity entity)
//    {
//        if (entity.get("nombre") == null)
//        {
//            return new ArrayList<UIEntity>();
//        }
//
//        Tag tag = entity.toModel(Tag.class);
//        tag = tag.insert();
//        return Collections.singletonList(UIEntity.toUI(tag));
//    }
//
//    @DELETE
//    @Path("{id}")
//    @Produces(MediaType.APPLICATION_XML)
//    @Consumes(MediaType.TEXT_XML)
//    public ResponseMessage deleteTag(@PathParam("id") String id)
//            throws RegistroNoEncontradoException
//    {
//        Tag.delete(Long.parseLong(id));
//        return new ResponseMessage(true);
//    }
//
//    @PUT
//    @Path("{id}")
//    @Consumes(MediaType.TEXT_XML)
//    public ResponseMessage updateTag(@PathParam("tagId") String id, UIEntity tag)
//    {
//        tag.toModel(Tag.class).update();
//        return new ResponseMessage(true);
//    }
}
