package es.uji.apps.ocw.services.rss;

import org.w3c.dom.Document;

import com.sun.syndication.io.FeedException;

public interface RSSFeed
{
    void addEntry(EntryMetadata metadata);

    Document getXML() throws FeedException;
}