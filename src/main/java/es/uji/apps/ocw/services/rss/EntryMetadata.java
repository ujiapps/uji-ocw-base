package es.uji.apps.ocw.services.rss;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class EntryMetadata
{
    private String title;
    private String description;
    private String guid;
    private Date pubDate;
    private String link;
    private List<String> authors;
    private List<String> categories;
    private List<Enclosure> enclosures;
    
    public EntryMetadata()
    {
        authors = new ArrayList<String>();
        categories = new ArrayList<String>();
        enclosures = new ArrayList<Enclosure>();
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getGuid()
    {
        return guid;
    }

    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    public Date getPubDate()
    {
        return pubDate;
    }

    public void setPubDate(Date pubDate)
    {
        this.pubDate = pubDate;
    }

    public String getLink()
    {
        return link;
    }

    public void setLink(String link)
    {
        this.link = link;
    }

    public List<String> getAuthors()
    {
        return authors;
    }

    public List<String> getCategories()
    {
        return categories;
    }

    public List<Enclosure> getEnclosures()
    {
        return enclosures;
    }
    
    public void addAuthor(String author)
    {
        authors.add(author);
    }
    
    public void addEnclosure(Enclosure enclosure)
    {
        enclosures.add(enclosure);
    }
    
    public void addCategory(String category)
    {
        categories.add(category);
    }
}