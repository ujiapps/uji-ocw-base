package es.uji.apps.ocw.services;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import es.uji.apps.ocw.model.Material;
import es.uji.apps.ocw.model.MaterialTag;
import es.uji.apps.ocw.model.Persona;
import es.uji.apps.ocw.model.Tag;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import es.uji.commons.sso.AccessManager;
import es.uji.commons.sso.exceptions.UnauthorizedUserException;

@Path("materialTag")
public class MaterialTagService extends CoreBaseService

{
    @GET
    @Produces(MediaType.TEXT_XML)
    public List<UIEntity> getTagsByMaterial(@QueryParam("materialId") String materialId)
            throws UnauthorizedUserException, RegistroNoEncontradoException
    {
        List<UIEntity> lista = new ArrayList<UIEntity>();

        Material material = Material.getMaterialById(Long.parseLong(materialId));

        if (material != null)
        {
            List<MaterialTag> listaMaterialTags = material.getTags();

            for (MaterialTag materialTag : listaMaterialTags)
            {
                UIEntity entity = UIEntity.toUI(materialTag);
                entity.put("nombre", materialTag.getTag().getNombre());
                entity.put("tagId", materialTag.getTag().getId());
                entity.put("materialId", materialTag.getMaterial().getId());
                lista.add(entity);
            }
        }

        return lista;
    }

    @POST
    @Produces(MediaType.TEXT_XML)
    public List<UIEntity> insertTagToMaterial(UIEntity entity) throws UnauthorizedUserException,
            RegistroNoEncontradoException
    {
        if (entity.get("tagId") == null || entity.get("tagId").isEmpty())
        {
            return new ArrayList<UIEntity>();
        }

        Persona persona = new Persona(AccessManager.getConnectedUserId(request));
        Material material = Material.getMaterialById(Long.parseLong(entity.get("materialId")));
        Tag tag = Tag.getTagById(Long.parseLong(entity.get("tagId")));

        MaterialTag materialTag = material.insertTag(tag, persona);

        List<UIEntity> lista = new ArrayList<UIEntity>();
        UIEntity returnEntity = UIEntity.toUI(materialTag);
        returnEntity.put("nombre", materialTag.getTag().getNombre());
        returnEntity.put("tagId", materialTag.getTag().getId());
        returnEntity.put("materialId", materialTag.getMaterial().getId());

        lista.add(returnEntity);

        return lista;
    }

    @DELETE
    @Path("{materialTagId}")
    @Produces(MediaType.TEXT_XML)
    public ResponseMessage deleteTagFromMaterial(@PathParam("materialTagId") String materialTagId,
            UIEntity entity) throws UnauthorizedUserException, RegistroNoEncontradoException
    {
        Persona persona = new Persona(AccessManager.getConnectedUserId(request));
        Material material = Material.getMaterialById(Long.parseLong(entity.get("materialId")));
        material.deleteTag(Long.parseLong(materialTagId), persona);

        return new ResponseMessage(true, "El registre s'ha esborrat satisfactoriament");
    }
}
