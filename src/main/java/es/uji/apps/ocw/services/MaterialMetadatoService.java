package es.uji.apps.ocw.services;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import es.uji.apps.ocw.model.Material;
import es.uji.apps.ocw.model.MaterialMetadato;
import es.uji.apps.ocw.model.Metadato;
import es.uji.apps.ocw.model.Persona;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import es.uji.commons.sso.AccessManager;
import es.uji.commons.sso.exceptions.UnauthorizedUserException;

@Path("materialMetadato")
public class MaterialMetadatoService extends CoreBaseService

{
    @GET
    @Produces(MediaType.TEXT_XML)
    public List<UIEntity> getMetadatosByMaterial(@QueryParam("materialId") String materialId)
    {
        List<UIEntity> lista = new ArrayList<UIEntity>();

        List<MaterialMetadato> listaMaterialMetadatos = MaterialMetadato
                .getMetadatosByMaterial(Long.parseLong(materialId));

        if (listaMaterialMetadatos.size() > 0)
        {

            for (MaterialMetadato materialMetadato : listaMaterialMetadatos)
            {
                UIEntity entity = UIEntity.toUI(materialMetadato);
                entity.put("tag", materialMetadato.getMetadato().getTag());
                entity.put("limitado", materialMetadato.getMetadato().getLimitado());
                entity.put("obligatorio", materialMetadato.getMetadato().getObligatorio());
                entity.put("materialId", materialMetadato.getMaterial().getId());
                entity.put("metadatoId", materialMetadato.getMetadato().getId());
                entity.put("valor", materialMetadato.getValor());

                lista.add(entity);
            }
        }

        return lista;
    }

    @POST
    @Produces(MediaType.TEXT_XML)
    public List<UIEntity> insertMetadatoToMaterial(UIEntity entity)
            throws RegistroNoEncontradoException, NumberFormatException, UnauthorizedUserException
    {

        if (entity.get("metadatoId") == null || entity.get("metadatoId").isEmpty())
        {
            return new ArrayList<UIEntity>();
        }

        Persona persona = new Persona(AccessManager.getConnectedUserId(request));

        Material material = Material.getMaterialById(Long.parseLong(entity.get("materialId")));
        Metadato metadato = Metadato.getMetadatoById(Long.parseLong(entity.get("metadatoId")));
        MaterialMetadato materialMetadato = new MaterialMetadato();
        materialMetadato = materialMetadato
                .insert(material, metadato, entity.get("valor"), persona);

        List<UIEntity> lista = new ArrayList<UIEntity>();

        UIEntity entity2 = UIEntity.toUI(materialMetadato);
        entity2.put("tag", materialMetadato.getMetadato().getTag());
        entity2.put("limitado", materialMetadato.getMetadato().getLimitado());
        entity2.put("obligatorio", materialMetadato.getMetadato().getObligatorio());
        entity2.put("materialId", materialMetadato.getMaterial().getId());
        entity2.put("metadatoId", materialMetadato.getMetadato().getId());
        entity2.put("valor", materialMetadato.getValor());
        lista.add(entity2);

        return lista;
    }

    @DELETE
    @Path("{materialMetadatoId}")
    @Produces(MediaType.TEXT_XML)
    public ResponseMessage deleteMetadatoFromMaterial(
            @PathParam("materialMetadatoId") String materialMetadatoId, UIEntity entity)
            throws UnauthorizedUserException, RegistroNoEncontradoException
    {
        Persona persona = new Persona(AccessManager.getConnectedUserId(request));
        Material material = Material.getMaterialById(Long.parseLong(entity.get("materialId")));

        MaterialMetadato.delete(Long.parseLong(materialMetadatoId), material, persona);

        return new ResponseMessage(true, "El registre s'ha esborrat satisfactoriament");
    }

    @PUT
    @Path("{materialMetadatoId}")
    @Consumes(MediaType.TEXT_XML)
    public ResponseMessage updateAutor(@PathParam("materialMetadatoId") String materialMetadatoId,
            UIEntity entity) throws RegistroNoEncontradoException, NumberFormatException,
            UnauthorizedUserException
    {
        MaterialMetadato materialMetadato = MaterialMetadato.getMaterialMetadatoById(Long
                .parseLong(materialMetadatoId));
        Metadato metadato = Metadato.getMetadatoById(Long.parseLong(entity.get("metadatoId")));
        Persona persona = new Persona(AccessManager.getConnectedUserId(request));
        Material material = Material.getMaterialById(Long.parseLong(entity.get("materialId")));
        String valor = entity.get("valor");

        materialMetadato.setMaterial(material);
        materialMetadato.setMetadato(metadato);
        materialMetadato.setValor(valor);

        materialMetadato.update(persona);
        return new ResponseMessage(true);
    }
}
