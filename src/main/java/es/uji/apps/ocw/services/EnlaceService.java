package es.uji.apps.ocw.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;

import es.uji.apps.ocw.model.Enlace;
import es.uji.apps.ocw.model.EnlaceIdioma;
import es.uji.apps.ocw.model.Idioma;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.ParametrosObligatoriosException;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

@Path("enlace")
public class EnlaceService extends CoreBaseService
{
    private String getParamString(MultivaluedMap<String, String> params, String param)
    {
        if (params.containsKey(param))
        {
            return params.getFirst(param);
        }
        else
        {
            return "";
        }
    }

    @GET
    @Produces(MediaType.TEXT_XML)
    public List<UIEntity> getEnlaces()
    {
        List<UIEntity> result = new ArrayList<UIEntity>();

        List<Enlace> listaEnlaces = Enlace.getEnlaces();

        for (Enlace enlace : listaEnlaces)
        {
            UIEntity enlaceUI = UIEntity.toUI(enlace);
            setOCWEnlace(enlace, enlaceUI);
            setAtributosMultidioma(enlace, enlaceUI);

            result.add(enlaceUI);
        }

        return result;
    }

    @GET
    @Path("{enlaceId}")
    @Produces(MediaType.TEXT_XML)
    public List<UIEntity> getEnlace(@PathParam("enlaceId") String enlaceId)
            throws RegistroNoEncontradoException, NumberFormatException
    {
        Enlace enlace = Enlace.getEnlace(Long.parseLong(enlaceId));

        UIEntity entity = UIEntity.toUI(enlace);
        setOCWEnlace(enlace, entity);
        setAtributosMultidioma(enlace, entity);

        return Collections.singletonList(entity);
    }

    @POST
    @Produces(MediaType.TEXT_XML)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public UIEntity insertEnlace(MultivaluedMap<String, String> params)
            throws ParametrosObligatoriosException
    {
        Enlace enlace = new Enlace();
        enlace.setOrden(Long.parseLong(getParamString(params, "orden")));
        if (params.containsKey("ocw"))
        {
            enlace.setOcw(true);
        }

        Set<EnlaceIdioma> enlacesIdiomas = new HashSet<EnlaceIdioma>();

        List<Idioma> idiomas = Idioma.getIdiomasEdicion();

        for (Idioma idioma : idiomas)
        {
            String url = getParamString(params, "url__" + idioma.getAcronimo().toLowerCase());
            String nombre = getParamString(params, "nombre__" + idioma.getAcronimo().toLowerCase());
            String descripcion = getParamString(params, "descripcion__"
                    + idioma.getAcronimo().toLowerCase());

            if (!url.equals("") || !nombre.equals("") || !descripcion.equals(""))
            {
                EnlaceIdioma enlaceIdioma = new EnlaceIdioma();
                enlaceIdioma.setUrl(url);
                enlaceIdioma.setNombre(nombre);
                enlaceIdioma.setDescripcion(descripcion);
                enlaceIdioma.setIdioma(idioma);
                enlaceIdioma.setEnlace(enlace);
                enlacesIdiomas.add(enlaceIdioma);
            }
        }
        enlace.setEnlacesIdiomas(enlacesIdiomas);

        enlace.compruebaCamposObligatorios(idiomas.get(0).getAcronimo());

        enlace = enlace.insert();

        UIEntity entity = UIEntity.toUI(enlace);

        return entity;
    }

    @PUT
    @Path("{enlaceId}")
    @Produces(MediaType.TEXT_XML)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public ResponseMessage update(@PathParam("enlaceId") String enlaceId,
            MultivaluedMap<String, String> params) throws RegistroNoEncontradoException,
            ParametrosObligatoriosException
    {
        Enlace enlace = Enlace.getEnlace(Long.parseLong(enlaceId));

        enlace.setOrden(Long.parseLong(getParamString(params, "orden")));
        if (params.containsKey("ocw"))
        {
            enlace.setOcw(true);
        }
        else
        {
            enlace.setOcw(false);
        }

        Set<EnlaceIdioma> enlacesIdiomas = enlace.getEnlacesIdiomas();
        List<Idioma> idiomas = Idioma.getIdiomasEdicion();

        boolean exists = false;
        for (Idioma idioma : idiomas)
        {
            String url = getParamString(params, "url__" + idioma.getAcronimo().toLowerCase());
            String nombre = getParamString(params, "nombre__" + idioma.getAcronimo().toLowerCase());
            String descripcion = getParamString(params, "descripcion__"
                    + idioma.getAcronimo().toLowerCase());

            for (EnlaceIdioma enlaceIdioma : enlacesIdiomas)
            {
                if (enlaceIdioma.getIdioma().getAcronimo().equals(idioma.getAcronimo()))
                {
                    enlaceIdioma.setUrl(url);
                    enlaceIdioma.setNombre(nombre);
                    enlaceIdioma.setDescripcion(descripcion);
                    exists = true;
                    break;
                }
            }
            if (!exists)
            {
                if (!url.equals("") || !nombre.equals("") || !descripcion.equals(""))
                {
                    EnlaceIdioma enlaceIdioma = new EnlaceIdioma();
                    enlaceIdioma.setUrl(url);
                    enlaceIdioma.setNombre(nombre);
                    enlaceIdioma.setDescripcion(descripcion);
                    enlaceIdioma.setIdioma(idioma);
                    enlaceIdioma.setEnlace(enlace);
                    enlacesIdiomas.add(enlaceIdioma);
                }
            }
            exists = false;
        }

        enlace.setEnlacesIdiomas(enlacesIdiomas);

        enlace.compruebaCamposObligatorios(idiomas.get(0).getAcronimo());

        enlace.update();

        return new ResponseMessage(true);
    }

    @DELETE
    @Path("{enlaceId}")
    @Produces(MediaType.APPLICATION_XML)
    public ResponseMessage delete(@PathParam("enlaceId") String enlaceId)
            throws RegistroNoEncontradoException, NumberFormatException
    {
        Enlace.delete(Long.parseLong(enlaceId));
        return new ResponseMessage(true);
    }

    private void setOCWEnlace(Enlace enlace, UIEntity entity)
    {
        if (enlace.getOcw())
        {
            entity.put("ocw", "Si");
        }
        else
        {
            entity.put("ocw", "No");
        }
    }

    private void setAtributosMultidioma(Enlace enlace, UIEntity entity)
    {
        for (EnlaceIdioma enlaceIdioma : enlace.getEnlacesIdiomas())
        {
            if (enlaceIdioma.getDescripcion() != null && !enlaceIdioma.getDescripcion().equals(""))
            {
                entity.put(enlaceIdioma.getIdioma().getAcronimo(), "descripcion",
                        enlaceIdioma.getDescripcion());
            }

            if (enlaceIdioma.getNombre() != null && !enlaceIdioma.getNombre().equals(""))
            {
                entity.put(enlaceIdioma.getIdioma().getAcronimo(), "nombre",
                        enlaceIdioma.getNombre());
            }

            if (enlaceIdioma.getUrl() != null && !enlaceIdioma.getUrl().equals(""))
            {
                entity.put(enlaceIdioma.getIdioma().getAcronimo(), "url", enlaceIdioma.getUrl());
            }
        }
    }

}
