package es.uji.apps.ocw.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import es.uji.apps.ocw.model.Curso;
import es.uji.apps.ocw.model.Grupo;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroConHijosException;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

@Path("grupo")
public class GrupoService extends CoreBaseService
{
    @POST
    @Produces(MediaType.APPLICATION_XML)
    public List<UIEntity> insertGrupo(UIEntity entity) throws RegistroNoEncontradoException
    {

        if (entity.get("cursoId") == null || entity.get("nombre") == null)
        {
            return new ArrayList<UIEntity>();
        }

        Curso curso = Curso.getCursoById(Long.parseLong(entity.get("cursoId")));
        Grupo grupo = new Grupo();
        grupo.setCurso(curso);
        grupo.setNombre(entity.get("nombre"));
        grupo.setOrden(Long.parseLong(entity.get("orden")));
        grupo = grupo.insert();

        return Collections.singletonList(UIEntity.toUI(grupo));

    }

    @GET
    @Produces(MediaType.TEXT_XML)
    public List<UIEntity> getGruposByCursoId(@QueryParam("cursoId") String cursoId)
            throws RegistroNoEncontradoException, NumberFormatException
    {
        List<UIEntity> result = new ArrayList<UIEntity>();

        if (cursoId != null)
        {
            for (Grupo grupo : Grupo.getGruposByCursoId(Long.parseLong(cursoId)))
            {
                UIEntity entity = UIEntity.toUI(grupo);
                result.add(entity);
            }
        }

        return result;
    }

    @PUT
    @Path("{grupoId}")
    @Consumes(MediaType.TEXT_XML)
    public ResponseMessage updateGrupo(@PathParam("grupoId") String grupoId, UIEntity entity)
            throws RegistroNoEncontradoException, NumberFormatException
    {
        Grupo grupo = Grupo.getGrupoById(Long.parseLong(grupoId));
        grupo.setNombre(entity.get("nombre"));
        grupo.setDescripcion(entity.get("descripcion"));
        grupo.update();
        return new ResponseMessage(true);
    }
    
    @DELETE
    @Path("{grupoId}")
    @Consumes(MediaType.TEXT_XML)
    public void deleteGrupo(@PathParam("grupoId") String grupoId) throws RegistroConHijosException,
            RegistroNoEncontradoException

    {
        Grupo.delete(Long.parseLong(grupoId));
    }
    
    @PUT
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.TEXT_XML)
    @Path("mover/{grupoId}")
    public Response mueveMaterial(@PathParam("grupoId") String grupoId,
            MultivaluedMap<String, String> params) throws RegistroNoEncontradoException
    {
        Grupo grupo = Grupo.getGrupoById(Long.parseLong(grupoId));
        Long anteriorId = Long.parseLong(params.getFirst("anteriorId"));
        String tipoAnterior = params.getFirst("tipoAnterior");

        grupo.moverGrupo(anteriorId, tipoAnterior);
        return Response.ok().build();
    }

}
