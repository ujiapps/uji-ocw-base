package es.uji.apps.ocw.ui;

import java.util.ArrayList;
import java.util.List;

public class Pagination
{
    private int paginaAnterior = 1;
    private int paginaActual;
    private int paginaSiguiente;
    private int totalPaginas;
    private String parametros;
    private List<BotonPaginador> botones;
    private boolean hayMasPaginasAntes = false;
    private boolean hayMasPaginasDespues = false;

    public static final int NUM_CURSOS_POR_PAGINA = 5;
    public static final int MAX_NUM_PAGINAS_VISIBLES = 6;

    public Pagination(String parametros, int numPaginaActual, int totalPaginas)
    {

        setBotones(new ArrayList<BotonPaginador>());
        int numPaginasVisibles = MAX_NUM_PAGINAS_VISIBLES;

        if (totalPaginas < numPaginasVisibles)
        {
            numPaginasVisibles = totalPaginas;
        }

        if ((numPaginaActual - 3) >= 1 && (numPaginaActual + 3) <= totalPaginas)
        {

            for (int i = (numPaginaActual - 3); i <= numPaginaActual + 4; i++)
            {
                getBotones().add(new BotonPaginador(i, numPaginaActual));
            }

            this.setHayMasPaginasAntes(true);
            this.setHayMasPaginasDespues(true);
        }
        else if ((numPaginaActual - 3) < 1)
        {
            for (int i = 1; i <= numPaginasVisibles; i++)
            {
                getBotones().add(new BotonPaginador(i, numPaginaActual));
            }

            if ((numPaginaActual + 3) < totalPaginas)
            {
                this.setHayMasPaginasDespues(true);
            }

        }
        else
        {
            for (int i = (numPaginaActual - 3); i <= totalPaginas; i++)
            {
                getBotones().add(new BotonPaginador(i, numPaginaActual));
            }

            if ((numPaginaActual - 3) > 1)
            {
                this.setHayMasPaginasAntes(true);
            }
        }

        if (numPaginaActual > 1)
        {
            this.setPaginaAnterior(numPaginaActual - 1);
        }

        if (numPaginaActual < totalPaginas)
        {
            this.setPaginaSiguiente(numPaginaActual + 1);
        }
        else
        {
            this.setPaginaSiguiente(totalPaginas);
        }

        this.setPaginaActual(numPaginaActual);
        this.setParametros("&" + parametros);
        this.setTotalPaginas(totalPaginas);
        this.setBotones(botones);
    }

    public int getTotalPaginas()
    {
        return totalPaginas;
    }

    public void setTotalPaginas(int totalPaginas)
    {
        this.totalPaginas = totalPaginas;
    }

    public boolean isHayMasPaginasAntes()
    {
        return hayMasPaginasAntes;
    }

    public void setHayMasPaginasAntes(boolean hayMasPaginasAntes)
    {
        this.hayMasPaginasAntes = hayMasPaginasAntes;
    }

    public boolean isHayMasPaginasDespues()
    {
        return hayMasPaginasDespues;
    }

    public void setHayMasPaginasDespues(boolean hayMasPaginasDespues)
    {
        this.hayMasPaginasDespues = hayMasPaginasDespues;
    }

    public String getParametros()
    {
        return parametros;
    }

    public void setParametros(String parametros)
    {
        this.parametros = parametros;
    }

    public int getPaginaAnterior()
    {
        return paginaAnterior;
    }

    public void setPaginaAnterior(int paginaAnterior)
    {
        this.paginaAnterior = paginaAnterior;
    }

    public int getPaginaSiguiente()
    {
        return paginaSiguiente;
    }

    public void setPaginaSiguiente(int paginaSiguiente)
    {
        this.paginaSiguiente = paginaSiguiente;
    }

    public List<BotonPaginador> getBotones()
    {
        return botones;
    }

    public void setBotones(List<BotonPaginador> botones)
    {
        this.botones = botones;
    }

    public int getPaginaActual()
    {
        return paginaActual;
    }

    public void setPaginaActual(int paginaActual)
    {
        this.paginaActual = paginaActual;
    }
}
