package es.uji.apps.ocw.ui;

import es.uji.apps.ocw.model.Material;
import es.uji.apps.ocw.model.Tag;

import java.io.FileInputStream;
import java.io.UnsupportedEncodingException;
import java.text.MessageFormat;
import java.util.*;

public class BreadCrumb
{
    private String urlBase;

    private List<BreadCrumbItem> elementos;

    private Map<String, Properties> configurationRegistry;

    private String language;

    public BreadCrumb(String urlBase, String language)
    {
        this.setUrlBase(urlBase);
        this.setLanguage(language.toLowerCase());
        configurationRegistry = new HashMap<String, Properties>();

        try
        {
            Properties diskConfiguration = new Properties();
            diskConfiguration.load(new FileInputStream(MessageFormat.format(
                    "/etc/uji/ocw/i18n/i18n_{0}.properties", this.getLanguage())));
            configurationRegistry.put(this.getLanguage(), diskConfiguration);
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }

    }

    public String resolveMessage(String key)
    {
        Properties configuration = configurationRegistry.get(getLanguage());
        String str = configuration.getProperty(key);
        if (str == null)
        {
            return null;
        }
        try
        {
            return new String(str.getBytes("latin1"), "UTF-8");
        }
        catch (UnsupportedEncodingException e)
        {
            return str;
        }
    }

    public List<BreadCrumbItem> getElementos()
    {
        return elementos;
    }

    public void setElementos(List<BreadCrumbItem> elementos)
    {
        this.elementos = elementos;
    }

    public void generaMigasDetalleCurso(CursoUI curso)
    {

        List<BreadCrumbItem> listaElementos = new ArrayList<BreadCrumbItem>();

        CategoriaUI categoriaPrincipal = curso.getCategorias().get(0);
        BreadCrumbItem item = new BreadCrumbItem();
        item.setDescripcion(this.resolveMessage("breadcrumb.inicioocw"));
        item.setUrl(this.getUrlBase());
        listaElementos.add(item);

        BreadCrumbItem item2 = new BreadCrumbItem();
        item2.setDescripcion(this.resolveMessage("breadcrumb.cursos"));

        item2.setUrl(MessageFormat.format("{0}cursos", this.getUrlBase()));
        listaElementos.add(item2);

        BreadCrumbItem item3 = new BreadCrumbItem();
        item3.setDescripcion(categoriaPrincipal.getNombre());
        item3.setUrl(MessageFormat.format("{0}cursos/categoria/{1}", this.getUrlBase(),
                categoriaPrincipal.getId()));
        listaElementos.add(item3);

        BreadCrumbItem item4 = new BreadCrumbItem();
        item4.setDescripcion(curso.getTitulo());
        item4.setUrl(MessageFormat.format("{0}curso/{1}", this.getUrlBase(), curso.getId()));
        item4.setTieneEnlace(false);
        listaElementos.add(item4);

        this.setElementos(listaElementos);

    }

    public void generaMigasPortada()
    {
        List<BreadCrumbItem> listaElementos = new ArrayList<BreadCrumbItem>();
        BreadCrumbItem item = new BreadCrumbItem();
        item.setDescripcion(this.resolveMessage("breadcrumb.inicioocw"));
        item.setUrl(this.getUrlBase());
        item.setTieneEnlace(false);
        listaElementos.add(item);

        this.setElementos(listaElementos);

    }

    public void generaMigasListaCursos()
    {
        List<BreadCrumbItem> listaElementos = new ArrayList<BreadCrumbItem>();

        BreadCrumbItem item = new BreadCrumbItem();
        item.setDescripcion(this.resolveMessage("breadcrumb.inicioocw"));
        item.setUrl(this.getUrlBase());
        listaElementos.add(item);

        BreadCrumbItem item2 = new BreadCrumbItem();
        item2.setDescripcion(this.resolveMessage("breadcrumb.cursos"));
        item2.setUrl(MessageFormat.format("{0}cursos", this.getUrlBase()));
        item2.setTieneEnlace(false);
        listaElementos.add(item2);

        this.setElementos(listaElementos);

    }

    public void generaMigasListaCursos(CategoriaUI categoriaActiva)
    {
        List<BreadCrumbItem> listaElementos = new ArrayList<BreadCrumbItem>();

        BreadCrumbItem item = new BreadCrumbItem();
        item.setDescripcion(this.resolveMessage("breadcrumb.inicioocw"));

        item.setUrl(this.getUrlBase());
        listaElementos.add(item);

        BreadCrumbItem item2 = new BreadCrumbItem();
        item2.setDescripcion(this.resolveMessage("breadcrumb.cursos"));
        item2.setUrl(MessageFormat.format("{0}cursos", this.getUrlBase()));
        listaElementos.add(item2);

        BreadCrumbItem item3 = new BreadCrumbItem();
        item3.setDescripcion(categoriaActiva.getNombre());
        item3.setUrl(MessageFormat.format("{0}cursos/categoria/{1}", this.getUrlBase(),
                categoriaActiva.getId()));
        item3.setTieneEnlace(false);
        listaElementos.add(item3);

        this.setElementos(listaElementos);

    }

    public void generaMigasListaCursosBusqueda()
    {
        List<BreadCrumbItem> listaElementos = new ArrayList<BreadCrumbItem>();
        BreadCrumbItem item = new BreadCrumbItem();
        item.setDescripcion(this.resolveMessage("breadcrumb.inicioocw"));
        item.setUrl(this.getUrlBase());
        listaElementos.add(item);

        BreadCrumbItem item2 = new BreadCrumbItem();
        item2.setDescripcion(this.resolveMessage("breadcrumb.busqueda"));
        item2.setUrl(MessageFormat.format("{0}busqueda", this.getUrlBase()));
        item2.setTieneEnlace(false);
        listaElementos.add(item2);

        this.setElementos(listaElementos);

    }

    public void generaMigasEnlacesInteres()
    {
        List<BreadCrumbItem> listaElementos = new ArrayList<BreadCrumbItem>();

        BreadCrumbItem item = new BreadCrumbItem();
        item.setDescripcion(this.resolveMessage("breadcrumb.inicioocw"));
        item.setUrl(this.getUrlBase());
        listaElementos.add(item);

        BreadCrumbItem item2 = new BreadCrumbItem();
        item2.setDescripcion(this.resolveMessage("breadcrumb.enlacesdeinteres"));
        item2.setUrl(MessageFormat.format("{0}enlaces", this.getUrlBase()));
        item2.setTieneEnlace(false);
        listaElementos.add(item2);

        this.setElementos(listaElementos);
    }

    public void generaMigasMateriales()
    {
        List<BreadCrumbItem> listaElementos = new ArrayList<BreadCrumbItem>();

        BreadCrumbItem item = new BreadCrumbItem();
        item.setDescripcion(this.resolveMessage("breadcrumb.inicioocw"));
        item.setUrl(this.getUrlBase());
        listaElementos.add(item);

        BreadCrumbItem item2 = new BreadCrumbItem();
        item2.setDescripcion(this.resolveMessage("breadcrumb.materiales"));
        item2.setUrl(MessageFormat.format("{0}materiales", this.getUrlBase()));
        item2.setTieneEnlace(false);
        listaElementos.add(item2);

        this.setElementos(listaElementos);
    }

    public void generaMigasDetalleMaterial(Material material)
    {
        List<BreadCrumbItem> listaElementos = new ArrayList<BreadCrumbItem>();

        BreadCrumbItem item = new BreadCrumbItem();
        item.setDescripcion(this.resolveMessage("breadcrumb.inicioocw"));
        item.setUrl(this.getUrlBase());
        listaElementos.add(item);

        BreadCrumbItem item2 = new BreadCrumbItem();
        item2.setDescripcion(this.resolveMessage("breadcrumb.materiales"));
        item2.setUrl(MessageFormat.format("{0}materiales", this.getUrlBase()));
        item2.setTieneEnlace(true);
        listaElementos.add(item2);

        BreadCrumbItem item3 = new BreadCrumbItem();
        item3.setDescripcion(material.getTitulo());
        item3.setUrl(MessageFormat.format("{0}material/{1}", this.getUrlBase(), material.getId()));
        item3.setTieneEnlace(false);
        listaElementos.add(item3);

        this.setElementos(listaElementos);
    }

    public void generaMigasListaCursosPorTag(Tag tag)
    {
        List<BreadCrumbItem> listaElementos = new ArrayList<BreadCrumbItem>();

        BreadCrumbItem item = new BreadCrumbItem();
        item.setDescripcion(this.resolveMessage("breadcrumb.inicioocw"));

        item.setUrl(this.getUrlBase());
        listaElementos.add(item);

        BreadCrumbItem item2 = new BreadCrumbItem();
        item2.setDescripcion(this.resolveMessage("breadcrumb.cursos"));
        item2.setUrl(MessageFormat.format("{0}cursos", this.getUrlBase()));
        listaElementos.add(item2);

        BreadCrumbItem item3 = new BreadCrumbItem();
        item3.setDescripcion(tag.getNombre());
        item3.setUrl(MessageFormat.format("{0}cursos/tag/{1}", this.getUrlBase(), tag.getId()));
        item3.setTieneEnlace(false);
        listaElementos.add(item3);

        this.setElementos(listaElementos);
    }

    public String getUrlBase()
    {
        return urlBase;
    }

    public void setUrlBase(String urlBase)
    {
        this.urlBase = urlBase;
    }

    public String getLanguage()
    {
        return language;
    }

    public void setLanguage(String language)
    {
        this.language = language;
    }

    public void generaMigasQueEs()
    {
        List<BreadCrumbItem> listaElementos = new ArrayList<BreadCrumbItem>();

        BreadCrumbItem item = new BreadCrumbItem();
        item.setDescripcion(this.resolveMessage("breadcrumb.inicioocw"));
        item.setUrl(this.getUrlBase());
        listaElementos.add(item);

        BreadCrumbItem item2 = new BreadCrumbItem();
        item2.setDescripcion(this.resolveMessage("menu.quees"));
        item2.setUrl(MessageFormat.format("{0}quees", this.getUrlBase()));
        item2.setTieneEnlace(false);
        listaElementos.add(item2);

        this.setElementos(listaElementos);

    }

    public void generaMigasFaq()
    {
        List<BreadCrumbItem> listaElementos = new ArrayList<BreadCrumbItem>();

        BreadCrumbItem item = new BreadCrumbItem();
        item.setDescripcion(this.resolveMessage("breadcrumb.inicioocw"));
        item.setUrl(this.getUrlBase());
        listaElementos.add(item);

        BreadCrumbItem item2 = new BreadCrumbItem();
        item2.setDescripcion(this.resolveMessage("menu.faq"));
        item2.setUrl(MessageFormat.format("{0}faq", this.getUrlBase()));
        item2.setTieneEnlace(false);
        listaElementos.add(item2);

        this.setElementos(listaElementos);

    }

}
