package es.uji.apps.ocw.ui;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class MaterialGrupoUI
{
    private String id;

    private String titulo;

    private String descripcion;

    private String fecha;

    private String icono;

    private String nombreFichero;

    private String url;

    private String nivel;

    private String idioma;

    private String urlMaterial;

    private String tipo;

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getTitulo()
    {
        return titulo;
    }

    public void setTitulo(String titulo)
    {
        this.titulo = titulo;
    }

    public String getDescripcion()
    {
        return descripcion;
    }

    public void setDescripcion(String descripcion)
    {
        this.descripcion = descripcion;
    }

    public String getFecha()
    {
        return fecha;
    }

    public void setFecha(String fecha)
    {
        this.fecha = fecha;
    }

    public String getIcono()
    {
        return icono;
    }

    public void setIcono(String icono)
    {
        this.icono = icono;
    }

    public String getNombreFichero()
    {
        return nombreFichero;
    }

    public void setNombreFichero(String nombreFichero)
    {
        this.nombreFichero = nombreFichero;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public String getNivel()
    {
        return nivel;
    }

    public void setNivel(String nivel)
    {
        this.nivel = nivel;
    }

    public String getIdioma()
    {
        return idioma;
    }

    public void setIdioma(String idioma)
    {
        this.idioma = idioma;
    }

    public String getUrlMaterial()
    {
        return urlMaterial;
    }

    public void setUrlMaterial(String urlMaterial)
    {
        this.urlMaterial = urlMaterial;
    }

    public void setFecha(Date fechaCreacion, String iso)
    {
        Locale locale = new Locale(iso);
        DateFormat formatter = new SimpleDateFormat("dd MMM yyyy", locale);

        this.fecha = formatter.format(fechaCreacion);
    }

    public String getTipo()
    {
        return tipo;
    }

    public void setTipo(String tipo)
    {
        this.tipo = tipo;
    }
}
