package es.uji.apps.ocw.ui;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class MaterialUI
{
    private Long id;
    private String titulo;
    private String descripcion;
    private String icono;
    private String fecha;
    private String idioma;
    private String url;
    private String nombreFichero;
    private List<TagUI> tags;
    private List<String> autores;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getTitulo()
    {
        return titulo;
    }

    public void setTitulo(String titulo)
    {
        this.titulo = titulo;
    }

    public String getDescripcion()
    {
        return descripcion;
    }

    public void setDescripcion(String descripcion)
    {
        this.descripcion = descripcion;
    }

    public String getFecha()
    {
        return fecha;
    }

    public void setFecha(String fecha)
    {
        this.fecha = fecha;
    }

    public String getIcono()
    {
        return icono;
    }

    public void setIcono(String icono)
    {
        this.icono = icono;
    }

    public String getIdioma()
    {
        return idioma;
    }

    public void setIdioma(String idioma)
    {
        this.idioma = idioma;
    }

    public String getNombreFichero()
    {
        return nombreFichero;
    }

    public void setNombreFichero(String nombreFichero)
    {
        this.nombreFichero = nombreFichero;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public List<String> getAutores()
    {
        return autores;
    }

    public void setAutores(List<String> autores)
    {
        this.autores = autores;
    }

    public List<TagUI> getTags()
    {
        return tags;
    }

    public void setTags(List<TagUI> tags)
    {
        this.tags = tags;
    }
    
    public void setFecha(Date fechaCreacion, String iso)
    {
        Locale locale = new Locale(iso);
        DateFormat formatter = new SimpleDateFormat("dd MMM yyyy", locale);

        this.fecha = formatter.format(fechaCreacion);
    }

}
