package es.uji.apps.ocw.ui;

public class BotonPaginador
{
    private int numero;
    private boolean actual = false;

    public BotonPaginador(int numero, int numPaginaActual)
    
    {
        this.setNumero(numero);
        if (numero == numPaginaActual) {
            this.setActual(true);
        }
    }

    public int getNumero()
    {
        return numero;
    }

    public void setNumero(int numero)
    {
        this.numero = numero;
    }

    public boolean isActual()
    {
        return actual;
    }

    public void setActual(boolean actual)
    {
        this.actual = actual;
    }

}
