package es.uji.apps.ocw.ui;

import java.util.ArrayList;
import java.util.List;

public class GrupoMaterialesUI
{
    String id;

    String nombre;

    String descripcion;

    List<MaterialUI> listaMaterialesUI;

    public GrupoMaterialesUI()
    {
        listaMaterialesUI = new ArrayList<MaterialUI>();
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getDescripcion()
    {
        return descripcion;
    }

    public void setDescripcion(String descripcion)
    {
        this.descripcion = descripcion;
    }

    public List<MaterialUI> getListaMaterialesUI()
    {
        return listaMaterialesUI;
    }

    public void setListaMaterialesUI(List<MaterialUI> listaMaterialesUI)
    {
        this.listaMaterialesUI = listaMaterialesUI;
    }
}
