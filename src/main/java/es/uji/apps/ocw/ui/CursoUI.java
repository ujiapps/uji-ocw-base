package es.uji.apps.ocw.ui;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class CursoUI
{
    private String id;
    private String titulo;
    private String descripcion;
    private String descripcionLimpia;
    private String programa;
    private String guia;
    private String periodo;
    private String fecha;
    private String imagen;
    private List<TagUI> tags;
    private List<CategoriaUI> categorias;
    private String responsable;
    private String accesos;

    public CursoUI()
    {
        List<TagUI> listaTags = new ArrayList<TagUI>();
        setTags(listaTags);

        List<CategoriaUI> listaCategorias = new ArrayList<CategoriaUI>();
        setCategorias(listaCategorias);
        setImagen("http://ujiapps.uji.es/ocw/img/uji.png");
    }

    public String getTitulo()
    {
        return titulo;
    }

    public void setTitulo(String titulo)
    {
        this.titulo = titulo;
    }

    public String getDescripcion()
    {
        return descripcion;
    }

    public void setDescripcion(String descripcion)
    {
        this.descripcion = descripcion;
    }

    public String getDescripcionLimpia()
    {
        return descripcionLimpia;
    }

    public void setDescripcionLimpia(String descripcionLimpia)
    {
        this.descripcionLimpia = descripcionLimpia;
    }

    public String getFecha()
    {
        return fecha;
    }

    public void setFecha(String fecha)
    {
        this.fecha = fecha;
    }

    public String getImagen()
    {
        return imagen;
    }

    public void setImagen(String imagen)
    {
        this.imagen = imagen;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getPrograma()
    {
        return programa;
    }

    public void setPrograma(String programa)
    {
        this.programa = programa;
    }

    public String getGuia()
    {
        return guia;
    }

    public void setGuia(String guia)
    {
        this.guia = guia;
    }

    public String getPeriodo()
    {
        return periodo;
    }

    public void setPeriodo(String periodo)
    {
        this.periodo = periodo;
    }

    public String getResponsable()
    {
        return responsable;
    }

    public void setResponsable(String responsable)
    {
        this.responsable = responsable;
    }

    public List<TagUI> getTags()
    {
        return tags;
    }

    public void setTags(List<TagUI> tags)
    {
        this.tags = tags;
    }

    public List<CategoriaUI> getCategorias()
    {
        return categorias;
    }

    public void setCategorias(List<CategoriaUI> categorias)
    {
        this.categorias = categorias;
    }

    public String getAccesos()
    {
        return accesos;
    }

    public void setAccesos(String accesos)
    {
        this.accesos = accesos;
    }

    public void setFecha(Date fechaCreacion, String iso)
    {
        Locale locale = new Locale(iso);
        DateFormat formatter = new SimpleDateFormat("dd MMM yyyy", locale);

        this.fecha = formatter.format(fechaCreacion);
    }

}
