package es.uji.apps.ocw.ui;

public class BreadCrumbItem
{
    private boolean tieneEnlace = true;
    private String url;
    private String descripcion;

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public String getDescripcion()
    {
        return descripcion;
    }

    public void setDescripcion(String descripcion)
    {
        this.descripcion = descripcion;
    }

    public boolean isTieneEnlace()
    {
        return tieneEnlace;
    }

    public void setTieneEnlace(boolean tieneEnlace)
    {
        this.tieneEnlace = tieneEnlace;
    }
}
