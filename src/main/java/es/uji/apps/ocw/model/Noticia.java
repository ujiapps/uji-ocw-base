package es.uji.apps.ocw.model;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.apps.ocw.dao.NoticiaDAO;

@Component
public class Noticia
{
    private String fecha;
    private String titulo;
    private String contenido;
    private List<String> tags;
    
    private static NoticiaDAO noticiaDAO;
    
    @Autowired
    public void setNoticiaDAO(NoticiaDAO noticiaDAO)
    {
        Noticia.noticiaDAO = noticiaDAO;
    }
    
    public Noticia()
    {
        tags = new ArrayList<String>();        
    }

    public String getFecha()
    {
        return fecha;
    }

    public void setFecha(String fecha)
    {
        this.fecha = fecha;
    }

    public String getTitulo()
    {
        return titulo;
    }

    public void setTitulo(String titulo)
    {
        this.titulo = titulo;
    }

    public String getContenido()
    {
        return contenido;
    }

    public void setContenido(String contenido)
    {
        this.contenido = contenido;
    }

    public List<String> getTags()
    {
        return tags;
    }

    public void setTags(List<String> tags)
    {
        this.tags = tags;
    }
    
    public static List<Noticia> getUltimasNoticias(String acronimoIdioma)
    {
        return noticiaDAO.getUltimasNoticias(acronimoIdioma);
    }
}