package es.uji.apps.ocw.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the OCW_USUARIOS_BONITA database table.
 * 
 */
@Entity
@Table(name = "OCW_USUARIOS_BONITA")
public class UsuarioBonita implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String email;

    private String nombre;

    @Column(name = "NOMBRE_COMPLETO")
    private String nombreCompleto;

    // bi-directional many-to-one association to UsuarioRolBonita
    @OneToMany(mappedBy = "usuarioBonita")
    private Set<UsuarioRolBonita> usuariosRolesBonitas;

    public UsuarioBonita()
    {
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getEmail()
    {
        return this.email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getNombre()
    {
        return this.nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getNombreCompleto()
    {
        return this.nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto)
    {
        this.nombreCompleto = nombreCompleto;
    }

    public Set<UsuarioRolBonita> getUsuariosRolesBonitas()
    {
        return this.usuariosRolesBonitas;
    }

    public void setUsuariosRolesBonitas(Set<UsuarioRolBonita> usuariosRolesBonitas)
    {
        this.usuariosRolesBonitas = usuariosRolesBonitas;
    }

}