package es.uji.apps.ocw.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.apps.ocw.dao.TagCuentaDAO;


/**
 * The persistent class for the OCW_VW_NUBE_TAGS database table.
 * 
 */
@SuppressWarnings("serial")
@Entity
@Table(name="OCW_VW_NUBE_TAGS")
@Component
public class TagCuenta implements Serializable {

	private Long cursos;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	private Long materiales;

	private String nombre;

	private Long total;

    protected static TagCuentaDAO tagCuentaDAO;

    @Autowired
    protected void setTagCuentaDAO(TagCuentaDAO tagCuentaDAO)
    {
        TagCuenta.tagCuentaDAO = tagCuentaDAO;
    }
    
    public TagCuenta() {
    }

	public Long getCursos() {
		return this.cursos;
	}

	public void setCursos(Long cursos) {
		this.cursos = cursos;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getMateriales() {
		return this.materiales;
	}

	public void setMateriales(Long materiales) {
		this.materiales = materiales;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Long getTotal() {
		return this.total;
	}

	public void setTotal(Long total) {
		this.total = total;
	}

    public static List<TagCuenta> getTagsCuenta()
    {
        return tagCuentaDAO.getTagsCuenta();
    }

}