package es.uji.apps.ocw.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.apps.ocw.dao.CategoriaIdiomaDAO;

/**
 * The persistent class for the OCW_CATEGORIAS_IDIOMAS database table.
 * 
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "OCW_CATEGORIAS_IDIOMAS")
@Component
public class CategoriaIdioma implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String nombre;

    // bi-directional many-to-one association to Categoria
    @ManyToOne
    @JoinColumn(name = "CATEGORIA_ID")
    private Categoria categoria;

    // bi-directional many-to-one association to Idioma
    @ManyToOne
    @JoinColumn(name = "IDIOMA_ID")
    private Idioma idioma;

    protected static CategoriaIdiomaDAO categoriaIdiomaDAO;

    @Autowired
    protected void setCategoriaIdiomaDAO(CategoriaIdiomaDAO categoriaIdiomaDAO)
    {
        CategoriaIdioma.categoriaIdiomaDAO = categoriaIdiomaDAO;
    }

    public CategoriaIdioma()
    {
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return this.nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public Categoria getCategoria()
    {
        return this.categoria;
    }

    public void setCategoria(Categoria categoria)
    {
        this.categoria = categoria;
    }

    public Idioma getIdioma()
    {
        return this.idioma;
    }

    public void setIdioma(Idioma idioma)
    {
        this.idioma = idioma;
    }

    public CategoriaIdioma update()
    {
        return categoriaIdiomaDAO.update(this);
    }

    public CategoriaIdioma insert()
    {
        return categoriaIdiomaDAO.insert(this);
    }

    public void delete()
    {
        categoriaIdiomaDAO.delete(this);
    }

    public List<CategoriaIdioma> getCategoriaIdiomaByCategoria(Long categoriaId)
    {
        return categoriaIdiomaDAO.getCategoriaIdiomaByCategoria(categoriaId);
    }

}