package es.uji.apps.ocw.model;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.apps.ocw.dao.TagDAO;
import es.uji.commons.rest.exceptions.RegistroConHijosException;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

/**
 * The persistent class for the OCW_TAGS database table.
 * 
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "OCW_TAGS")
@Component
public class Tag implements Serializable
{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String nombre;

    // bi-directional many-to-one association to CursoTag
    @OneToMany(mappedBy = "tag")
    private Set<CursoTag> cursosTags;

    // bi-directional many-to-one association to MaterialTag
    @OneToMany(mappedBy = "tag")
    private Set<MaterialTag> materialesTags;

    protected static TagDAO tagDAO;

    @Autowired
    protected void setTagDAO(TagDAO tagDAO)
    {
        Tag.tagDAO = tagDAO;
    }

    public Tag()
    {
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return this.nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public Set<CursoTag> getCursosTags()
    {
        return this.cursosTags;
    }

    public void setCursosTags(Set<CursoTag> cursosTags)
    {
        this.cursosTags = cursosTags;
    }

    public Set<MaterialTag> getMaterialesTags()
    {
        return this.materialesTags;
    }

    public void setMaterialesTags(Set<MaterialTag> materialesTags)
    {
        this.materialesTags = materialesTags;
    }

    public static List<Tag> getTags()
    {
        return tagDAO.getTags();
    }

    public Tag insert()
    {
        return tagDAO.insert(this);
    }

    public Tag update()
    {
        return tagDAO.update(this);
    }

    public static void delete(Long tagId) throws RegistroNoEncontradoException,
            RegistroConHijosException
    {
        List<Tag> listaTags = tagDAO.get(Tag.class, tagId);

        if (listaTags != null && listaTags.size() > 0)
        {
            if (tagDAO.getTagsByNumeroMateriales(tagId) > 0) 

            {
                throw new RegistroConHijosException(
                        "El tag no es pot esborrar perque està assignat a algun material.");               
            }            
            else if  (tagDAO.getTagsByNumeroCursos(tagId) > 0)
            {
                throw new RegistroConHijosException(
                        "El tag no es pot esborrar perque està assignat a algun curs.");                
            }            
            else
            {
                tagDAO.delete(Tag.class, tagId);
            }     
        }
        else
        {
            throw new RegistroNoEncontradoException();
        }

    }
    
    public void borraTagSiNoSeUtiliza(Long tagId)
    {
        if ((tagDAO.getTagsByNumeroMateriales(tagId) == 0) &&  (tagDAO.getTagsByNumeroCursos(tagId) == 0))
        {
            tagDAO.delete(Tag.class, tagId);
        }        
    }

    public static Tag getTagById(Long tagId) throws RegistroNoEncontradoException
    {
        List<Tag> tag = tagDAO.get(Tag.class, tagId);
        
        if (tag.size() > 0) {
            return tag.get(0);
        } else {
            throw new RegistroNoEncontradoException("El registre no s'ha encontrat.");
        }
    }

    public static List<Tag> getTagByName(String tagString)
    {
        return tagDAO.getTagByName(tagString);
    }

  }