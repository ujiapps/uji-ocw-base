package es.uji.apps.ocw.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.apps.ocw.dao.AccesoDAO;
import es.uji.commons.db.BaseDAO;

/**
 * The persistent class for the OCW_ACCESOS database table.
 * 
 */
@Entity
@Table(name = "OCW_ACCESOS")
@Component
public class Acceso implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String descripcion;

    @Temporal(TemporalType.DATE)
    private Date fecha;

    private String ip;

    // bi-directional many-to-one association to Curso
    @ManyToOne
    @JoinColumn(name = "CURSO_ID")
    private Curso curso;

    // bi-directional many-to-one association to Material
    @ManyToOne
    @JoinColumn(name = "MATERIAL_ID")
    private Material material;

    private static AccesoDAO accesoDAO;

    @Autowired
    public void setAccesoDAO(AccesoDAO accesoDAO)
    {
        Acceso.accesoDAO = accesoDAO;
    }

    public Acceso()
    {
    }

    private static BaseDAO baseDAO;

    @Autowired
    protected void setBaseDAO(BaseDAO baseDAO)
    {
        Acceso.baseDAO = baseDAO;
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getDescripcion()
    {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion)
    {
        this.descripcion = descripcion;
    }

    public Date getFecha()
    {
        return this.fecha;
    }

    public void setFecha(Date fecha)
    {
        this.fecha = fecha;
    }

    public String getIp()
    {
        return this.ip;
    }

    public void setIp(String ip)
    {
        this.ip = ip;
    }

    public Curso getCurso()
    {
        return this.curso;
    }

    public void setCurso(Curso curso)
    {
        this.curso = curso;
    }

    public Material getMaterial()
    {
        return this.material;
    }

    public void setMaterial(Material material)
    {
        this.material = material;
    }

    public static void contabilizaAcceso(Acceso acceso)
    {
        accesoDAO.insert(acceso);
    }
}