package es.uji.apps.ocw.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;


/**
 * The persistent class for the OCW_VW_LICENCIAS database table.
 * 
 */
@Entity
@SuppressWarnings("serial")
@Table(name="OCW_VW_LICENCIAS")
public class LicenciaIdiomaPrincipal implements Serializable {

	private String codigo;

    @Lob()
	private String contenido;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

    @Lob()
	private byte[] imagen;

	private String titulo;

	private String url;

    public LicenciaIdiomaPrincipal() {
    }

	public String getCodigo() {
		return this.codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getContenido() {
		return this.contenido;
	}

	public void setContenido(String contenido) {
		this.contenido = contenido;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public byte[] getImagen() {
		return this.imagen;
	}

	public void setImagen(byte[] imagen) {
		this.imagen = imagen;
	}

	public String getTitulo() {
		return this.titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}