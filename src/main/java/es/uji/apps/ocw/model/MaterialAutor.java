package es.uji.apps.ocw.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.apps.ocw.dao.MaterialAutorDAO;

/**
 * The persistent class for the OCW_MATERIALES_AUTORES database table.
 * 
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "OCW_MATERIALES_AUTORES")
@Component
public class MaterialAutor implements Serializable
{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String estado;

    private String hash;

    @Column(name = "HASH_NO")
    private String hashNo;

    @Column(name = "HASH_SI")
    private String hashSi;

    // bi-directional many-to-one association to Autor
    @ManyToOne
    @JoinColumn(name = "AUTOR_ID")
    private Autor autor;

    // bi-directional many-to-one association to Material
    @ManyToOne
    @JoinColumn(name = "MATERIAL_ID")
    private Material material;

    public static MaterialAutorDAO materialAutorDAO;

    @Autowired
    public void setMaterialAutorDAO(MaterialAutorDAO materialAutorDAO)
    {
        MaterialAutor.materialAutorDAO = materialAutorDAO;
    }

    public MaterialAutor()
    {
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getEstado()
    {
        return this.estado;
    }

    public void setEstado(String estado)
    {
        this.estado = estado;
    }

    public String getHash()
    {
        return this.hash;
    }

    public void setHash(String hash)
    {
        this.hash = hash;
    }

    public String getHashNo()
    {
        return this.hashNo;
    }

    public void setHashNo(String hashNo)
    {
        this.hashNo = hashNo;
    }

    public String getHashSi()
    {
        return this.hashSi;
    }

    public void setHashSi(String hashSi)
    {
        this.hashSi = hashSi;
    }

    public Autor getAutor()
    {
        return this.autor;
    }

    public void setAutor(Autor autor)
    {
        this.autor = autor;
    }

    public Material getMaterial()
    {
        return this.material;
    }

    public void setMaterial(Material material)
    {
        this.material = material;
    }

    public static List<MaterialAutor> getMaterialAutor()
    {
        return materialAutorDAO.getMaterialAutor();
    }

    public static List<MaterialAutor> getMaterialesAutoresByAutor(Long autorID)
    {
        return materialAutorDAO.getMaterialesAutoresByAutor(autorID);
    }
}