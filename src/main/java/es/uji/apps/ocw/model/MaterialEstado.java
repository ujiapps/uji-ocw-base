package es.uji.apps.ocw.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * The persistent class for the OCW_MATERIALES_ESTADOS database table.
 * 
 */
@Entity
@Table(name = "OCW_MATERIALES_ESTADOS")
public class MaterialEstado implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String estado;

    @Column(name = "ID_PROCESO")
    private String idProceso;

    @Column(name = "INICIO_PUBLICACION")
    private Long inicioPublicacion;

    // bi-directional many-to-one association to Material
    @ManyToOne
    @JoinColumn(name = "MATERIAL_ID")
    private Material material;

    public MaterialEstado()
    {
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getEstado()
    {
        return this.estado;
    }

    public void setEstado(String estado)
    {
        this.estado = estado;
    }

    public String getIdProceso()
    {
        return this.idProceso;
    }

    public void setIdProceso(String idProceso)
    {
        this.idProceso = idProceso;
    }

    public Long getInicioPublicacion()
    {
        return this.inicioPublicacion;
    }

    public void setInicioPublicacion(Long inicioPublicacion)
    {
        this.inicioPublicacion = inicioPublicacion;
    }

    public Material getMaterial()
    {
        return this.material;
    }

    public void setMaterial(Material material)
    {
        this.material = material;
    }

}