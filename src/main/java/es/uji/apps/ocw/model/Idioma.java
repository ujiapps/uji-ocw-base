package es.uji.apps.ocw.model;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.apps.ocw.dao.IdiomaDAO;
import es.uji.commons.rest.exceptions.RegistroConHijosException;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

/**
 * The persistent class for the OCW_IDIOMAS database table.
 * 
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "OCW_IDIOMAS")
@Component
public class Idioma implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String acronimo;

    private String edicion;

    private String nombre;

    private Long orden;

    // bi-directional many-to-one association to CategoriaIdioma
    @OneToMany(mappedBy = "idioma")
    private Set<CategoriaIdioma> categoriasIdiomas;

    // bi-directional many-to-one association to CursoIdioma
    @OneToMany(mappedBy = "idioma")
    private Set<CursoIdioma> cursosIdiomas;

    // bi-directional many-to-one association to LicenciaIdioma
    @OneToMany(mappedBy = "idioma")
    private Set<LicenciaIdioma> licenciasIdiomas;

    // bi-directional many-to-one association to Material
    @OneToMany(mappedBy = "idioma")
    private Set<Material> materiales;

    // bi-directional many-to-one association to MetadatoIdioma
    @OneToMany(mappedBy = "idioma")
    private Set<MetadatoIdioma> metadatosIdiomas;

    protected static IdiomaDAO idiomaDAO;

    @Autowired
    protected void setIdiomaDAO(IdiomaDAO idiomaDAO)
    {
        Idioma.idiomaDAO = idiomaDAO;
    }

    public Idioma()
    {
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getAcronimo()
    {
        return this.acronimo;
    }

    public void setAcronimo(String acronimo)
    {
        this.acronimo = acronimo;
    }

    public String getEdicion()
    {
        return this.edicion;
    }

    public void setEdicion(String edicion)
    {
        this.edicion = edicion;
    }

    public String getNombre()
    {
        return this.nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public Long getOrden()
    {
        return this.orden;
    }

    public void setOrden(Long orden)
    {
        this.orden = orden;
    }

    public Set<CategoriaIdioma> getCategoriasIdiomas()
    {
        return this.categoriasIdiomas;
    }

    public void setCategoriasIdiomas(Set<CategoriaIdioma> categoriasIdiomas)
    {
        this.categoriasIdiomas = categoriasIdiomas;
    }

    public Set<CursoIdioma> getCursosIdiomas()
    {
        return this.cursosIdiomas;
    }

    public void setCursosIdiomas(Set<CursoIdioma> cursosIdiomas)
    {
        this.cursosIdiomas = cursosIdiomas;
    }

    public Set<LicenciaIdioma> getLicenciasIdiomas()
    {
        return this.licenciasIdiomas;
    }

    public void setLicenciasIdiomas(Set<LicenciaIdioma> licenciasIdiomas)
    {
        this.licenciasIdiomas = licenciasIdiomas;
    }

    public Set<Material> getMateriales()
    {
        return this.materiales;
    }

    public void setMateriales(Set<Material> materiales)
    {
        this.materiales = materiales;
    }

    public Set<MetadatoIdioma> getMetadatosIdiomas()
    {
        return this.metadatosIdiomas;
    }

    public void setMetadatosIdiomas(Set<MetadatoIdioma> metadatosIdiomas)
    {
        this.metadatosIdiomas = metadatosIdiomas;
    }

    public static List<Idioma> getIdiomasEdicion()
    {
        return idiomaDAO.getIdiomasEdicion();
    }

    public static List<Idioma> getIdiomas()
    {
        return idiomaDAO.getIdiomas();
    }

    public static Idioma getIdiomaById(Long idiomaId) throws RegistroNoEncontradoException
    {
        List<Idioma> idioma = idiomaDAO.get(Idioma.class, idiomaId);
        
        if (idioma.size() > 0) {
            return idioma.get(0);
        } else {
            throw new RegistroNoEncontradoException("El registre no s'ha encontrat.");
        }
    }

    public Idioma update()
    {
        return idiomaDAO.update(this);
    }

    public Idioma insert()
    {
        return idiomaDAO.insert(this);
    }

    public static void delete(long IdiomaId) throws RegistroConHijosException
    {
        try
        {
            idiomaDAO.delete(Idioma.class, IdiomaId);
        }
        catch (Exception e)
        {
            throw new RegistroConHijosException("El idioma no es pot esborrar");
        }

    }

}