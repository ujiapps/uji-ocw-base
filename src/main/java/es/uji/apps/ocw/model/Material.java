package es.uji.apps.ocw.model;

import java.io.Serializable;
import java.util.*;

import javax.persistence.*;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import es.uji.apps.ocw.dao.MaterialDAO;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import es.uji.commons.sso.dao.ApaDAO;
import es.uji.commons.sso.exceptions.UnauthorizedUserException;

/**
 * The persistent class for the OCW_MATERIALES database table.
 * 
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "OCW_MATERIALES")
@Component
public class Material implements Serializable
{
    private static final int NUM_MAX_MATERIALES_A_VISUALIZAR = 7;

    private static final int NUM_MATERIALES_POR_PAGINA = 5;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "CONTENT_TYPE")
    private String contentType;

    private String descripcion;

    @Lob()
    private byte[] documento;

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA")
    private Date fecha;

    @Column(name = "HANDLE_ID")
    private String handleId;

    @Column(name = "NOMBRE_FICHERO")
    private String nombreFichero;

    @Column(name = "PERSONA_ID")
    private Long personaId;

    private String titulo;

    private String url;

    @Lob()
    private String xml;

    // bi-directional many-to-one association to Acceso
    @OneToMany(mappedBy = "material")
    private Set<Acceso> accesos;

    // bi-directional many-to-one association to CursoMaterial
    @OneToMany(mappedBy = "material")
    private Set<CursoMaterial> cursosMateriales;

    // bi-directional many-to-one association to Idioma
    @ManyToOne
    @JoinColumn(name = "IDIOMA_ID")
    private Idioma idioma;

    // bi-directional many-to-one association to Licencia
    @ManyToOne
    @JoinColumn(name = "LICENCIA_ID")
    private Licencia licencia;

    // bi-directional many-to-one association to MaterialAutor
    @OneToMany(mappedBy = "material")
    private Set<MaterialAutor> materialesAutores;

    // bi-directional many-to-one association to MaterialEstado
    @OneToMany(mappedBy = "material")
    private Set<MaterialEstado> materialEstados;

    // bi-directional many-to-one association to MaterialMetadato
    @OneToMany(mappedBy = "material")
    private Set<MaterialMetadato> materialMetadatos;

    // bi-directional many-to-one association to MaterialTag
    @OneToMany(mappedBy = "material")
    private Set<MaterialTag> materialesTags;

    protected static MaterialDAO materialDAO;

    @Autowired
    protected void setMaterialDAO(MaterialDAO materialDAO)
    {
        Material.materialDAO = materialDAO;
    }

    protected static ApaDAO apaDAO;

    @Autowired
    protected void setApaDAO(ApaDAO apaDAO)
    {
        Material.apaDAO = apaDAO;
    }

    public Material()
    {
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getContentType()
    {
        return this.contentType;
    }

    public void setContentType(String contentType)
    {
        this.contentType = contentType;
    }

    public String getDescripcion()
    {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion)
    {
        this.descripcion = descripcion;
    }

    public byte[] getDocumento()
    {
        return this.documento;
    }

    public void setDocumento(byte[] documento)
    {
        this.documento = documento;
    }

    public Date getFecha()
    {
        return this.fecha;
    }

    public void setFecha(Date fecha)
    {
        this.fecha = fecha;
    }

    public String getHandleId()
    {
        return this.handleId;
    }

    public void setHandleId(String handleId)
    {
        this.handleId = handleId;
    }

    public String getNombreFichero()
    {
        return this.nombreFichero;
    }

    public void setNombreFichero(String nombreFichero)
    {
        this.nombreFichero = nombreFichero;
    }

    public Long getPersonaId()
    {
        return this.personaId;
    }

    public void setPersonaId(Long personaId)
    {
        this.personaId = personaId;
    }

    public String getTitulo()
    {
        return this.titulo;
    }

    public void setTitulo(String titulo)
    {
        this.titulo = titulo;
    }

    public String getUrl()
    {
        return this.url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public String getXml()
    {
        return this.xml;
    }

    public void setXml(String xml)
    {
        this.xml = xml;
    }

    public Set<Acceso> getAccesos()
    {
        return this.accesos;
    }

    public void setAccesos(Set<Acceso> accesos)
    {
        this.accesos = accesos;
    }

    public Set<CursoMaterial> getCursosMateriales()
    {
        return this.cursosMateriales;
    }

    public void setCursosMateriales(Set<CursoMaterial> cursosMateriales)
    {
        this.cursosMateriales = cursosMateriales;
    }

    public Idioma getIdioma()
    {
        return this.idioma;
    }

    public void setIdioma(Idioma idioma)
    {
        this.idioma = idioma;
    }

    public Licencia getLicencia()
    {
        return this.licencia;
    }

    public void setLicencia(Licencia licencia)
    {
        this.licencia = licencia;
    }

    public Set<MaterialAutor> getMaterialesAutores()
    {
        return this.materialesAutores;
    }

    public void setMaterialesAutores(Set<MaterialAutor> materialesAutores)
    {
        this.materialesAutores = materialesAutores;
    }

    public Set<MaterialEstado> getMaterialesEstados()
    {
        return this.materialEstados;
    }

    public void setMaterialesEstados(Set<MaterialEstado> materialesEstados)
    {
        this.materialEstados = materialesEstados;
    }

    public Set<MaterialMetadato> getMaterialMetadatos()
    {
        return this.materialMetadatos;
    }

    public void setMaterialMetadatos(Set<MaterialMetadato> materialMetadatos)
    {
        this.materialMetadatos = materialMetadatos;
    }

    public Set<MaterialTag> getMaterialesTags()
    {
        return this.materialesTags;
    }

    public void setMaterialesTags(Set<MaterialTag> materialesTags)
    {
        this.materialesTags = materialesTags;
    }

    @Transactional
    public void delete(Persona persona) throws UnauthorizedUserException
    {
        if (!esPropietario(persona))
        {
            persona.checkEsAdministrador();
        }
        materialDAO.delete(this);
        materialDAO.flush();

    }

    public static Material getMaterialById(Long materialId) throws RegistroNoEncontradoException
    {
        Material material = materialDAO.getMaterialById(materialId);
        if (material == null)
        {
            throw new RegistroNoEncontradoException("El registre no s'ha encontrat.");
        }

        return material;
    }

    public static Material getMaterialConFicheroById(Long materialId)
            throws RegistroNoEncontradoException
    {
        List<Material> material = materialDAO.get(Material.class, materialId);

        if (material.size() > 0)
        {
            return material.get(0);
        }
        else
        {
            throw new RegistroNoEncontradoException("El registre no s'ha encontrat.");
        }
    }

    public List<MaterialTag> getTags()
    {
        return materialDAO.getTags(this.getId());
    }

    public List<CursoMaterial> getCursos()
    {
        return materialDAO.getCursos(this.getId());
    }

    public static List<String> cadenaEtiquetasToList(String cadena)
    {
        List<String> listaEtiquetas = new ArrayList<String>();

        if (!cadena.isEmpty() && cadena.length() != 0)
        {
            String[] etiquetas = cadena.split(",\\s*");

            for (String tag : etiquetas)
            {
                tag = StringUtils.strip(tag);
                listaEtiquetas.add(tag);
            }
        }
        return listaEtiquetas;
    }

    public List<String> filtraEtiquetasYaRelacionadas(List<String> listaEtiquetas)
    {
        for (MaterialTag materialTag : this.getTags())
        {
            String tag = materialTag.getTag().getNombre();
            if (listaEtiquetas.contains(tag))
            {
                listaEtiquetas.remove(tag);
            }
        }

        return listaEtiquetas;
    }

    public void parseaTagsByString(String etiquetas, Persona persona)
            throws UnauthorizedUserException
    {
        List<String> listaEtiquetas = cadenaEtiquetasToList(etiquetas);
        this.borraTagsNoUtilizados(listaEtiquetas);
        listaEtiquetas = filtraEtiquetasYaRelacionadas(listaEtiquetas);

        for (String tagString : listaEtiquetas)
        {
            List<Tag> tagList = Tag.getTagByName(tagString);
            if (tagList.size() == 0)
            {
                Tag tag = new Tag();
                tag.setNombre(tagString);
                tag = tag.insert();
                this.insertTag(tag, persona);
            }
            else
            {
                this.insertTag(tagList.get(0), persona);
            }
        }
    }

    public void borraTagsNoUtilizados(List<String> listaEtiquetas)
    {
        for (MaterialTag materialTag : this.getTags())
        {

            if (!listaEtiquetas.contains(materialTag.getTag().getNombre())
                    || listaEtiquetas.isEmpty())
            {
                materialDAO.delete(MaterialTag.class, materialTag.getId());

                materialTag.getTag().borraTagSiNoSeUtiliza(materialTag.getTag().getId());

            }
        }
    }

    public MaterialTag insertTag(Tag tag, Persona persona) throws UnauthorizedUserException
    {
        if (!esPropietario(persona))
        {
            persona.checkEsAdministrador();
        }

        MaterialTag materialTag = new MaterialTag();
        materialTag.setMaterial(this);
        materialTag.setTag(tag);

        return materialDAO.insert(materialTag);
    }

    public void update(Persona persona) throws UnauthorizedUserException
    {
        if (!esPropietario(persona))
        {
            persona.checkEsAdministrador();
        }

        materialDAO.update(this);
    }

    public void deleteTag(long materialTagId, Persona persona) throws UnauthorizedUserException
    {

        if (!esPropietario(persona))
        {
            persona.checkEsAdministrador();
        }

        materialDAO.delete(MaterialTag.class, materialTagId);
    }

    public static List<MaterialCompleto> getMaterialesConFiltros(Map<String, String> filtro,
            Persona persona)
    {
        if (persona.isAdministrador() && filtro.isEmpty())
        {
            return new ArrayList<MaterialCompleto>();
        }

        if (!persona.isAdministrador())
        {
            filtro.put("personaId", persona.getId().toString());
        }
        return materialDAO.getMaterialesConFiltros(filtro);
    }

    public List<MaterialAutor> getAutores()
    {
        return materialDAO.getAutores(this.getId());
    }

    public MaterialAutor insertAutor(Autor autor, Persona persona) throws UnauthorizedUserException
    {
        if (!esPropietario(persona))
        {
            persona.checkEsAdministrador();
        }

        MaterialAutor materialAutor = new MaterialAutor();

        materialAutor.setMaterial(this);
        materialAutor.setAutor(autor);

        return materialDAO.insert(materialAutor);

    }

    public void deleteAutor(Long materialAutorId, Persona persona) throws UnauthorizedUserException
    {
        if (!esPropietario(persona))
        {
            persona.checkEsAdministrador();
        }

        materialDAO.delete(MaterialAutor.class, materialAutorId);

    }

    public Material insert()
    {
        return materialDAO.insert(this);
    }

    public void borraAdjunto(Persona persona) throws UnauthorizedUserException
    {

        if (!esPropietario(persona))
        {
            persona.checkEsAdministrador();
        }

        setDocumento(null);
        setContentType(null);
        setNombreFichero(null);
        materialDAO.update(this);
    }

    public String getTagsEnCadena()
    {
        StringBuilder tags = new StringBuilder();
        String delim = "";
        for (MaterialTag materialTag : this.getTags())
        {
            tags.append(delim).append(materialTag.getTag().getNombre());
            delim = ", ";
        }

        return tags.toString();
    }

    public boolean esPropietario(Persona persona)
    {
        Integer resul = personaId.compareTo(persona.getId());
        return resul == 0;
    }

    public static List<Material> getCincoUltimosMateriales()
    {
        return materialDAO.getUltimosMateriales(NUM_MAX_MATERIALES_A_VISUALIZAR);
    }

    public static List<Material> getMaterialesMasVisitados()
    {
        return materialDAO.getMaterialesMasVisitados(NUM_MAX_MATERIALES_A_VISUALIZAR);
    }

    public static List<Material> getMaterialesByCursoId(Long cursoId)
            throws RegistroNoEncontradoException
    {
        return materialDAO.getMaterialesByCursoId(cursoId);
    }

    public static List<Material> getTodosMaterialesPaginados(int pagina)
    {
        return materialDAO.getTodosMaterialesPaginados(NUM_MATERIALES_POR_PAGINA, pagina);
    }

    public static Integer getNumeroTotalPaginasMateriales()
    {
        return materialDAO.getNumeroTotalPaginasMateriales(NUM_MATERIALES_POR_PAGINA);
    }

    public void deleteTags(Persona persona) throws UnauthorizedUserException
    {
        if (!esPropietario(persona))
        {
            persona.checkEsAdministrador();
        }

        for (MaterialTag materialTag : this.getTags())
        {
            materialDAO.delete(MaterialTag.class, materialTag.getId());
        }
    }

}