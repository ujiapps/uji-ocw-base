package es.uji.apps.ocw.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.commons.db.BaseDAO;


/**
 * The persistent class for the OCW_VW_CATEGORIAS database table.
 * 
 */
@Entity
@SuppressWarnings("serial")
@Table(name="OCW_VW_CATEGORIAS")
@Component
public class CategoriaIdiomaPrincipal implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	private String nombre;

	private BigDecimal orden;

    private static BaseDAO baseDAO;

    @Autowired
    protected void setBaseDAO(BaseDAO baseDAO)
    {
        CategoriaIdiomaPrincipal.baseDAO = baseDAO;
    }
    
    public CategoriaIdiomaPrincipal() {
    }

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public BigDecimal getOrden() {
		return this.orden;
	}

	public void setOrden(BigDecimal orden) {
		this.orden = orden;
	}

    public static List<CategoriaIdiomaPrincipal> getCategorias()
    {
        return baseDAO.get(CategoriaIdiomaPrincipal.class, "1=1");
    }

    public static CategoriaIdiomaPrincipal getCategoriaById(Long categoriaId)
    {
        List<CategoriaIdiomaPrincipal> listaCategorias = baseDAO.get(CategoriaIdiomaPrincipal.class, categoriaId);
        if (listaCategorias.size() > 0) {
            return listaCategorias.get(0);
        }
        return new CategoriaIdiomaPrincipal();
    }
}