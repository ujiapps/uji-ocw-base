package es.uji.apps.ocw.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * The persistent class for the OCW_USUARIOS_ROLES_BONITA database table.
 * 
 */
@Entity
@Table(name = "OCW_USUARIOS_ROLES_BONITA")
public class UsuarioRolBonita implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String rol;

    // bi-directional many-to-one association to UsuarioBonita
    @ManyToOne
    @JoinColumn(name = "USUARIO_ID")
    private UsuarioBonita usuarioBonita;

    public UsuarioRolBonita()
    {
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getRol()
    {
        return this.rol;
    }

    public void setRol(String rol)
    {
        this.rol = rol;
    }

    public UsuarioBonita getUsuariosBonita()
    {
        return this.usuarioBonita;
    }

    public void setUsuariosBonita(UsuarioBonita usuariosBonita)
    {
        this.usuarioBonita = usuariosBonita;
    }

}