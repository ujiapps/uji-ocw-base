package es.uji.apps.ocw.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * The persistent class for the OCW_LICENCIAS_IDIOMAS database table.
 * 
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "OCW_LICENCIAS_IDIOMAS")
public class LicenciaIdioma implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Lob()
    private String contenido;

    private String titulo;

    private String url;

    // bi-directional many-to-one association to Idioma
    @ManyToOne
    @JoinColumn(name = "IDIOMA_ID")
    private Idioma idioma;

    // bi-directional many-to-one association to Licencia
    @ManyToOne
    @JoinColumn(name = "LICENCIA_ID")
    private Licencia licencia;

    public LicenciaIdioma()
    {
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getContenido()
    {
        return this.contenido;
    }

    public void setContenido(String contenido)
    {
        this.contenido = contenido;
    }

    public String getTitulo()
    {
        return this.titulo;
    }

    public void setTitulo(String titulo)
    {
        this.titulo = titulo;
    }

    public String getUrl()
    {
        return this.url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public Idioma getIdioma()
    {
        return this.idioma;
    }

    public void setIdioma(Idioma idioma)
    {
        this.idioma = idioma;
    }

    public Licencia getLicencia()
    {
        return this.licencia;
    }

    public void setLicencia(Licencia licencia)
    {
        this.licencia = licencia;
    }

    /*public static List<LicenciaIdioma> getLicenciaIdiomasByLicenciaId(long licenciaId)
    {
        return licenciaIdiomaDAO.getLicenciaIdiomasByLicenciaId(licenciaId);
    }

    public static LicenciaIdioma getLicenciaIdioma(Long licenciaIdiomaId)
            throws RegistroNoEncontradoException
    {
        List<LicenciaIdioma> listaLicenciaIdiomas = licenciaIdiomaDAO.get(LicenciaIdioma.class,
                licenciaIdiomaId);

        if (listaLicenciaIdiomas != null && listaLicenciaIdiomas.size() > 0)
        {
            return listaLicenciaIdiomas.get(0);
        }
        else
        {
            throw new RegistroNoEncontradoException();
        }
    }

    public LicenciaIdioma insert()
    {
        if (this.getLicencia() == null || this.getIdioma() == null || this.getTitulo() == null)
        {
            return new LicenciaIdioma();
        }
        else
        {
            return licenciaIdiomaDAO.insert(this);
        }
    }

    public static void delete(Long licenciaIdiomaId) throws RegistroNoEncontradoException
    {
        List<LicenciaIdioma> lista = licenciaIdiomaDAO.get(LicenciaIdioma.class, licenciaIdiomaId);

        if (lista != null && lista.size() > 0)
        {
            licenciaIdiomaDAO.delete(LicenciaIdioma.class, licenciaIdiomaId);
        }
        else
        {
            throw new RegistroNoEncontradoException();
        }
    }

    public LicenciaIdioma update() throws RegistroNoEncontradoException
    {
        List<LicenciaIdioma> lista = licenciaIdiomaDAO.get(LicenciaIdioma.class, this.getId());

        if (lista != null && lista.size() > 0)
        {
            return licenciaIdiomaDAO.update(this);
        }
        else
        {
            throw new RegistroNoEncontradoException();
        }
    }*/

}