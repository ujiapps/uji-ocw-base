package es.uji.apps.ocw.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Autowired;

import es.uji.commons.db.BaseDAO;


/**
 * The persistent class for the OCW_VW_ACCESOS_CURSOS database table.
 * 
 */
@Entity
@SuppressWarnings("serial")
@Table(name="OCW_VW_ACCESOS_CURSOS")
public class AccesoCurso implements Serializable {

    @Id
	@Column(name="CURSO_ID")
	private Long cursoId;

	@Column(name="NUMERO_ACCESOS")
	private Long numeroAccesos;

    private static BaseDAO baseDAO;

    @Autowired
    protected void setBaseDAO(BaseDAO baseDAO)
    {
        AccesoCurso.baseDAO = baseDAO;
    }
    
    public AccesoCurso() {
    }

	public Long getCursoId() {
		return this.cursoId;
	}

	public void setCursoId(Long cursoId) {
		this.cursoId = cursoId;
	}

	public Long getNumeroAccesos() {
		return this.numeroAccesos;
	}

	public void setNumeroAccesos(Long numeroAccesos) {
		this.numeroAccesos = numeroAccesos;
	}

}