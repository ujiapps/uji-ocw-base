package es.uji.apps.ocw.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Autowired;

import es.uji.commons.db.BaseDAO;

/**
 * The persistent class for the OCW_VW_ACCESOS_MATERIALES database table.
 * 
 */
@Entity
@SuppressWarnings("serial")
@Table(name = "OCW_VW_ACCESOS_MATERIALES")
public class AccesoMaterial implements Serializable
{

    private static BaseDAO baseDAO;
    @Id
    @Column(name = "MATERIAL_ID")
    private Long materialId;
    @Column(name = "NUMERO_ACCESOS")
    private Long numeroAccesos;

    public AccesoMaterial()
    {
    }

    @Autowired
    protected void setBaseDAO(BaseDAO baseDAO)
    {
        AccesoMaterial.baseDAO = baseDAO;
    }

    public Long getMaterialId()
    {
        return this.materialId;
    }

    public void setMaterialId(Long materialId)
    {
        this.materialId = materialId;
    }

    public Long getNumeroAccesos()
    {
        return this.numeroAccesos;
    }

    public void setNumeroAccesos(Long numeroAccesos)
    {
        this.numeroAccesos = numeroAccesos;
    }

}
