package es.uji.apps.ocw.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * The persistent class for the OCW_METADATOS_IDIOMAS database table.
 * 
 */
@Entity
@Table(name = "OCW_METADATOS_IDIOMAS")
public class MetadatoIdioma implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String nombre;

    // bi-directional many-to-one association to Idioma
    @ManyToOne
    @JoinColumn(name = "IDIOMA_ID")
    private Idioma idioma;

    // bi-directional many-to-one association to Metadato
    @ManyToOne
    @JoinColumn(name = "METADATO_ID")
    private Metadato metadato;

    public MetadatoIdioma()
    {
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return this.nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public Idioma getIdioma()
    {
        return this.idioma;
    }

    public void setIdioma(Idioma idioma)
    {
        this.idioma = idioma;
    }

    public Metadato getMetadato()
    {
        return this.metadato;
    }

    public void setMetadato(Metadato metadato)
    {
        this.metadato = metadato;
    }

}