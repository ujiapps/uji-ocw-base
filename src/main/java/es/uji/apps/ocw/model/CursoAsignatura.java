package es.uji.apps.ocw.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * The persistent class for the OCW_CURSOS_ASIGNATURAS database table.
 * 
 */
@Entity
@SuppressWarnings("serial")
@Table(name = "OCW_CURSOS_ASIGNATURAS")
public class CursoAsignatura implements Serializable
{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "ASIGNATURA_ID")
    private String asignaturaId;

    // bi-directional many-to-one association to Curso
    @ManyToOne
    @JoinColumn(name = "CURSO_ID")
    private Curso curso;
 
    public CursoAsignatura()
    {
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getAsignaturaId()
    {
        return this.asignaturaId;
    }

    public void setAsignaturaId(String asignaturaId)
    {
        this.asignaturaId = asignaturaId;
    }

    public Curso getCurso()
    {
        return this.curso;
    }

    public void setCurso(Curso curso)
    {
        this.curso = curso;
    }

}