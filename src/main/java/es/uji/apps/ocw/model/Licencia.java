package es.uji.apps.ocw.model;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.apps.ocw.dao.LicenciaDAO;
import es.uji.commons.rest.exceptions.ParametrosObligatoriosException;
import es.uji.commons.rest.exceptions.RegistroConHijosException;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

/**
 * The persistent class for the OCW_LICENCIAS database table.
 * 
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "OCW_LICENCIAS")
@Component
public class Licencia implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String codigo;

    @Lob()
    private byte[] imagen;

    @Column(name = "CONTENT_TYPE")
    private String contentType;

    // bi-directional many-to-one association to LicenciaIdioma
    @OneToMany(mappedBy = "licencia", cascade = CascadeType.ALL)
    private Set<LicenciaIdioma> licenciasIdiomas;

    // bi-directional many-to-one association to Material
    @OneToMany(mappedBy = "licencia")
    private Set<Material> materiales;

    private static LicenciaDAO licenciaDAO;

    @Autowired
    public void setLicenciaDAO(LicenciaDAO licenciaDAO)
    {
        Licencia.licenciaDAO = licenciaDAO;
    }

    public Licencia()
    {
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getCodigo()
    {
        return this.codigo;
    }

    public void setCodigo(String codigo)
    {
        this.codigo = codigo;
    }

    public byte[] getImagen()
    {
        return this.imagen;
    }

    public void setImagen(byte[] imagen)
    {
        this.imagen = imagen;
    }

    public String getContentType()
    {
        return contentType;
    }

    public void setContentType(String contentType)
    {
        this.contentType = contentType;
    }

    public Set<LicenciaIdioma> getLicenciasIdiomas()
    {
        return this.licenciasIdiomas;
    }

    public void setLicenciasIdiomas(Set<LicenciaIdioma> licenciasIdiomas)
    {
        this.licenciasIdiomas = licenciasIdiomas;
    }

    public Set<Material> getMateriales()
    {
        return this.materiales;
    }

    public void setMateriales(Set<Material> materiales)
    {
        this.materiales = materiales;
    }

    public static List<Licencia> getLicencias()
    {
        return licenciaDAO.getLicencias();
    }

    public static Licencia getLicencia(Long id) throws RegistroNoEncontradoException
    {
        Licencia licencia = licenciaDAO.getLicencia(id);

        if (licencia == null)
        {
            throw new RegistroNoEncontradoException();
        }

        return licencia;
    }

    public Licencia insert() throws ParametrosObligatoriosException, RegistroNoEncontradoException
    {
        compruebaCamposObligatorios();

        Set<LicenciaIdioma> licenciaIdiomas = this.getLicenciasIdiomas();
        int countTit = 0;
        for (LicenciaIdioma licenciaIdioma : licenciaIdiomas)
        {
            if (licenciaIdioma.getTitulo() != null)
            {
                countTit++;
            }
        }
        if (this.getCodigo() == null || countTit != licenciaIdiomas.size())
        {
            return new Licencia();
        }
        else
        {
            return licenciaDAO.insert(this);
        }
    }

    public static void delete(Long licenciaId) throws RegistroNoEncontradoException,
            RegistroConHijosException
    {
        List<Licencia> listaLicencias = licenciaDAO.get(Licencia.class, licenciaId);

        if (listaLicencias != null && listaLicencias.size() > 0)
        {
            if (licenciaDAO.getNumMaterialesConLicencia(licenciaId) == 0)
            {
                licenciaDAO.delete(Licencia.class, licenciaId);
            }
            else
            {
                throw new RegistroConHijosException();
            }
        }
        else
        {
            throw new RegistroNoEncontradoException();
        }
    }

    public Licencia update() throws RegistroNoEncontradoException, ParametrosObligatoriosException
    {
        compruebaCamposObligatorios();

        List<Licencia> listaLicencias = licenciaDAO.get(Licencia.class, this.getId());

        if (listaLicencias != null && listaLicencias.size() > 0)
        {
            return licenciaDAO.updateLicencia(this);
        }
        else
        {
            throw new RegistroNoEncontradoException();
        }
    }

    public static void deleteLicenciaImagen(Long licenciaId) throws RegistroNoEncontradoException
    {
        List<Licencia> listaLicencias = licenciaDAO.get(Licencia.class, licenciaId);

        if (listaLicencias != null && listaLicencias.size() > 0)
        {
            Licencia licencia = listaLicencias.get(0);
            if (licencia.getImagen() != null)
            {
                licencia.setImagen(null);
                licencia.setContentType(null);
                licenciaDAO.update(licencia);
            }
        }
        else
        {
            throw new RegistroNoEncontradoException();
        }
    }

    private void compruebaCamposObligatorios() throws ParametrosObligatoriosException
    {
        if (codigo.equals(""))
        {
            throw new ParametrosObligatoriosException("codigo");
        }

        for (LicenciaIdioma licenciaIdioma : getLicenciasIdiomas())
        {
            if (licenciaIdioma.getTitulo().equals("") && !licenciaIdioma.getUrl().equals(""))
            {
                throw new ParametrosObligatoriosException("titulo");
            }
        }
    }

    public static Licencia getLicenciaById(Long licenciaId) throws RegistroNoEncontradoException
    {
        List<Licencia> licencia = licenciaDAO.get(Licencia.class, licenciaId);

        if (licencia.size() > 0)
        {
            return licencia.get(0);
        }
        else
        {
            throw new RegistroNoEncontradoException();
        }
    }
}