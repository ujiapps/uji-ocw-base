package es.uji.apps.ocw.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.commons.db.BaseDAO;

/**
 * The persistent class for the OCW_CURSOS_TAGS database table.
 * 
 */
@Entity
@SuppressWarnings("serial")
@Table(name = "OCW_CURSOS_TAGS")
@Component
public class CursoTag implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    // bi-directional many-to-one association to Curso
    @ManyToOne
    @JoinColumn(name = "CURSO_ID")
    private Curso curso;

    // bi-directional many-to-one association to Tag
    @ManyToOne
    @JoinColumn(name = "TAG_ID")
    private Tag tag;

    private static BaseDAO baseDAO;

    @Autowired
    protected void setBaseDAO(BaseDAO baseDAO)
    {
        CursoTag.baseDAO = baseDAO;
    }

    public CursoTag()
    {
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Curso getCurso()
    {
        return this.curso;
    }

    public void setCurso(Curso curso)
    {
        this.curso = curso;
    }

    public Tag getTag()
    {
        return this.tag;
    }

    public void setTag(Tag tag)
    {
        this.tag = tag;
    }

    public CursoTag insert()
    {
        return baseDAO.insert(this);
    }
}