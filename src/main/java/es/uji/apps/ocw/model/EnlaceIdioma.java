package es.uji.apps.ocw.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

/**
 * The persistent class for the OCW_ENLACES_IDIOMAS database table.
 * 
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "OCW_ENLACES_IDIOMAS")
@Component
public class EnlaceIdioma implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String descripcion;

    // bi-directional many-to-one association to Idioma
    @ManyToOne
    @JoinColumn(name = "IDIOMA_ID")
    private Idioma idioma;

    private String nombre;

    private String url;

    // bi-directional many-to-one association to Enlace
    @ManyToOne
    @JoinColumn(name = "ENLACE_ID")
    private Enlace enlace;

    public EnlaceIdioma()
    {
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getDescripcion()
    {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion)
    {
        this.descripcion = descripcion;
    }

    public Idioma getIdioma()
    {
        return this.idioma;
    }

    public void setIdioma(Idioma idioma)
    {
        this.idioma = idioma;
    }

    public String getNombre()
    {
        return this.nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getUrl()
    {
        return this.url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public Enlace getEnlace()
    {
        return this.enlace;
    }

    public void setEnlace(Enlace enlace)
    {
        this.enlace = enlace;
    }

}