package es.uji.apps.ocw.model;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.apps.ocw.dao.EnlaceDAO;
import es.uji.commons.rest.exceptions.ParametrosObligatoriosException;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

/**
 * The persistent class for the OCW_ENLACES database table.
 * 
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "OCW_ENLACES")
@Component
public class Enlace implements Serializable
{
    private static final int NUM_MAX_ENLACES_A_VISUALIZAR = 5;

    private static final int NUM_ENLACES_POR_PAGINA = 5;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Boolean ocw;

    private Long orden;

    // bi-directional many-to-one association to EnlaceIdioma
    @OneToMany(mappedBy = "enlace", cascade = CascadeType.ALL)
    private Set<EnlaceIdioma> enlacesIdiomas;

    protected static EnlaceDAO enlaceDAO;

    @Autowired
    protected void setEnlaceDAO(EnlaceDAO enlaceDAO)
    {
        Enlace.enlaceDAO = enlaceDAO;
    }

    public Enlace()
    {
        ocw = false;
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Boolean getOcw()
    {
        return this.ocw;
    }

    public void setOcw(Boolean ocw)
    {
        this.ocw = ocw;
    }

    public Long getOrden()
    {
        return this.orden;
    }

    public void setOrden(Long orden)
    {
        this.orden = orden;
    }

    public Set<EnlaceIdioma> getEnlacesIdiomas()
    {
        return this.enlacesIdiomas;
    }

    public void setEnlacesIdiomas(Set<EnlaceIdioma> enlacesIdiomas)
    {
        this.enlacesIdiomas = enlacesIdiomas;
    }

    public static List<Enlace> getEnlaces()
    {
        return enlaceDAO.getEnlaces();
    }

    public static Enlace getEnlace(Long enlaceId) throws RegistroNoEncontradoException
    {
        Enlace enlace = enlaceDAO.getEnlace(enlaceId);

        if (enlace == null)
        {
            throw new RegistroNoEncontradoException();
        }

        return enlace;
    }

    public Enlace insert()
    {
        return enlaceDAO.insert(this);
    }

    public static void delete(Long enlaceId) throws RegistroNoEncontradoException
    {
        List<Enlace> listaEnlaces = enlaceDAO.get(Enlace.class, enlaceId);

        if (listaEnlaces != null && listaEnlaces.size() > 0)
        {
            enlaceDAO.delete(Enlace.class, enlaceId);
        }
        else
        {
            throw new RegistroNoEncontradoException();
        }
    }

    public Enlace update() throws RegistroNoEncontradoException
    {
        List<Enlace> listaEnlaces = enlaceDAO.get(Enlace.class, id);

        if (listaEnlaces != null && listaEnlaces.size() > 0)
        {
            return enlaceDAO.updateEnlace(this);
        }
        else
        {
            throw new RegistroNoEncontradoException();
        }
    }

    public void compruebaCamposObligatorios(String idiomaPrincipalAcronim)
            throws ParametrosObligatoriosException
    {
        String parametrosOblig = "ordre, url, nom";
        if (!ocw)
        {
            parametrosOblig += ", descripció";
        }

        if (orden == null)
        {
            throw new ParametrosObligatoriosException(parametrosOblig);
        }

        boolean estaIdiomaPrincipal = false;

        for (EnlaceIdioma enlaceIdioma : getEnlacesIdiomas())
        {
            if (enlaceIdioma.getIdioma().getAcronimo().equals(idiomaPrincipalAcronim))
            {
                estaIdiomaPrincipal = true;
            }

            if (enlaceIdioma.getIdioma().getAcronimo().equals(idiomaPrincipalAcronim)
                    || (!enlaceIdioma.getUrl().equals("") || !enlaceIdioma.getNombre().equals("") || !enlaceIdioma
                            .getDescripcion().equals("")))
            {
                if (enlaceIdioma.getUrl().equals(""))
                {
                    throw new ParametrosObligatoriosException(parametrosOblig);
                }
                if (enlaceIdioma.getNombre().equals(""))
                {
                    throw new ParametrosObligatoriosException(parametrosOblig);
                }
                if (!ocw && enlaceIdioma.getDescripcion().equals(""))
                {
                    throw new ParametrosObligatoriosException(parametrosOblig);
                }
            }
        }

        if (!estaIdiomaPrincipal)
        {
            throw new ParametrosObligatoriosException(parametrosOblig);
        }
    }

    public static List<Enlace> getEnlacesInteres(String idioma)
    {
        return enlaceDAO.getEnlacesInteres(idioma, NUM_MAX_ENLACES_A_VISUALIZAR);
    }

    public static List<Enlace> getEnlacesOCW(String idioma)
    {
        return enlaceDAO.getEnlacesOCW(idioma);
    }

    public static List<Enlace> getEnlacesInteresPaginados(String idioma, int pagina)
    {
        return enlaceDAO.getEnlacesInteresPaginados(idioma, NUM_ENLACES_POR_PAGINA, pagina);
    }

    public static Integer getNumeroTotalPaginasEnlaces(String idioma)
    {
        return enlaceDAO.getNumeroTotalPaginasEnlaces(idioma, NUM_ENLACES_POR_PAGINA);
    }
}