package es.uji.apps.ocw.model;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.apps.ocw.dao.AutorDAO;
import es.uji.commons.rest.exceptions.RegistroConHijosException;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

/**
 * The persistent class for the OCW_AUTORES database table.
 * 
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "OCW_AUTORES")
@Component
public class Autor implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String apellido1;

    private String apellido2;

    private String email;

    private String nombre;

    @Column(name = "PERSONA_ID")
    private Long personaId;

    // bi-directional many-to-one association to MaterialAutor
    @OneToMany(mappedBy = "autor")
    private Set<MaterialAutor> materialesAutores;

    protected static AutorDAO autorDAO;

    @Autowired
    protected void setAutorDAO(AutorDAO autorDAO)
    {
        Autor.autorDAO = autorDAO;
    }

    public Autor()
    {
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getApellido1()
    {
        return this.apellido1;
    }

    public void setApellido1(String apellido1)
    {
        this.apellido1 = apellido1;
    }

    public String getApellido2()
    {

        if (this.apellido2 != null) {
            return this.apellido2;
        } else {
            return "";
        }
    }

    public void setApellido2(String apellido2)
    {
        this.apellido2 = apellido2;
    }

    public String getEmail()
    {
        return this.email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getNombre()
    {
        return this.nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public Long getPersonaId()
    {
        return this.personaId;
    }

    public void setPersonaId(Long personaId)
    {
        this.personaId = personaId;
    }

    public Set<MaterialAutor> getMaterialesAutores()
    {
        return this.materialesAutores;
    }

    public void setMaterialesAutores(Set<MaterialAutor> materialesAutores)
    {
        this.materialesAutores = materialesAutores;
    }

    public static List<Autor> getAutores()
    {
        List<Autor> autores = autorDAO.getAutores();

        return autores;
    }

    public static void delete(long autorId) throws RegistroNoEncontradoException, RegistroConHijosException
    {

        if (autorDAO.getNumMaterialesAutor(autorId) > 0)
        {
            throw new RegistroConHijosException("El autor no és pot esborrar perque té informació rel·lacionada");
        }
        else
        {
            autorDAO.delete(Autor.class, autorId);
        }

    }

    public Autor update()
    {
        return autorDAO.update(this);
    }

    public Autor insert()
    {
        return autorDAO.insert(this);
    }

    public static Autor getAutorById(Long autorId)
    {
        return autorDAO.get(Autor.class, autorId).get(0);
    }

    public static Autor getAutorFromAutorCompleto(AutorCompleto autorCompleto)
    {
        Autor autor = new Autor();
        autor.setId(autorCompleto.getId());
        autor.setNombre(autorCompleto.getNombre());
        autor.setApellido1(autorCompleto.getApellido1());
        autor.setApellido2(autorCompleto.getApellido2());
        autor.setEmail(autorCompleto.getEmail());
        autor.setPersonaId(autorCompleto.getPersonaId());

        return autor;
    }

    public static AutorCompleto getAutorCompletoById(Long autorId) throws RegistroNoEncontradoException
    {
        List<AutorCompleto> autor = autorDAO.getAutorCompleto(autorId);
        
        if (autor.size() > 0) {
            return autor.get(0);
        } else {
            throw new RegistroNoEncontradoException("El registre no s'ha encontrat.");
        }
    }
}