package es.uji.apps.ocw.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the OCW_VW_MATERIALES database table.
 * 
 */
@Entity
@SuppressWarnings("serial")
@Table(name = "OCW_VW_MATERIALES")
public class MaterialCompleto implements Serializable
{

    private String descripcion;

    @Temporal(TemporalType.DATE)
    private Date fecha;

    @Id
    private Long id;

    @Column(name = "IDIOMA_ID")
    private Long idioma;

    @Column(name = "LICENCIA_ID")
    private Long licencia;

    @Column(name = "NOMBRE_FICHERO")
    private String nombreFichero;

    @Column(name = "NOMBRE_IDIOMA")
    private String nombreIdioma;

    @Column(name = "NOMBRE_LICENCIA")
    private String nombreLicencia;

    @Column(name = "NOMBRE_PERSONA")
    private String nombrePersona;

    @Column(name = "PERSONA_ID")
    private Long persona;

    private String titulo;

    private String url;

    @Column(name = "content_type")
    private String contentType;

    public MaterialCompleto()
    {
    }

    public String getDescripcion()
    {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion)
    {
        this.descripcion = descripcion;
    }

    public Date getFecha()
    {
        return this.fecha;
    }

    public void setFecha(Date fecha)
    {
        this.fecha = fecha;
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getIdioma()
    {
        return this.idioma;
    }

    public void setIdioma(Long idioma)
    {
        this.idioma = idioma;
    }

    public Long getLicencia()
    {
        return this.licencia;
    }

    public void setLicencia(Long licencia)
    {
        this.licencia = licencia;
    }

    public String getNombreFichero()
    {
        return this.nombreFichero;
    }

    public void setNombreFichero(String nombreFichero)
    {
        this.nombreFichero = nombreFichero;
    }

    public String getNombreIdioma()
    {
        return this.nombreIdioma;
    }

    public void setNombreIdioma(String nombreIdioma)
    {
        this.nombreIdioma = nombreIdioma;
    }

    public String getNombreLicencia()
    {
        return this.nombreLicencia;
    }

    public void setNombreLicencia(String nombreLicencia)
    {
        this.nombreLicencia = nombreLicencia;
    }

    public String getNombrePersona()
    {
        return this.nombrePersona;
    }

    public void setNombrePersona(String nombrePersona)
    {
        this.nombrePersona = nombrePersona;
    }

    public Long getPersona()
    {
        return this.persona;
    }

    public void setPersona(Long persona)
    {
        this.persona = persona;
    }

    public String getTitulo()
    {
        return this.titulo;
    }

    public void setTitulo(String titulo)
    {
        this.titulo = titulo;
    }

    public String getUrl()
    {
        return this.url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public String getContentType()
    {
        return contentType;
    }

    public void setContentType(String contentType)
    {
        this.contentType = contentType;
    }

}