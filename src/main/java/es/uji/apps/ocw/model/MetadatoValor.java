package es.uji.apps.ocw.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.apps.ocw.dao.MetadatoDAO;
import es.uji.commons.rest.exceptions.ParametrosObligatoriosException;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

/**
 * The persistent class for the OCW_METADATOS_VALORES database table.
 * 
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "OCW_METADATOS_VALORES")
@Component
public class MetadatoValor implements Serializable
{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Long orden;

    private String valor;

    // bi-directional many-to-one association to Metadato
    @ManyToOne
    @JoinColumn(name = "METADATO_ID")
    private Metadato metadato;
    
    private static MetadatoDAO metadatoDAO;

    @Autowired
    public void setMetadatoDAO(MetadatoDAO metadatoDAO)
    {
        MetadatoValor.metadatoDAO = metadatoDAO;
    }

    public MetadatoValor()
    {
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getOrden()
    {
        return this.orden;
    }

    public void setOrden(Long orden)
    {
        this.orden = orden;
    }

    public String getValor()
    {
        return this.valor;
    }

    public void setValor(String valor)
    {
        this.valor = valor;
    }

    public Metadato getMetadato()
    {
        return this.metadato;
    }

    public void setMetadato(Metadato metadato)
    {
        this.metadato = metadato;
    }

    public MetadatoValor insert() throws ParametrosObligatoriosException
    {
        compruebaCamposObligatorios();
        
        return metadatoDAO.insert(this);
    }

    public MetadatoValor update() throws ParametrosObligatoriosException
    {
        compruebaCamposObligatorios();
        
        return metadatoDAO.update(this);
    }

    public static void delete(Long id) throws RegistroNoEncontradoException
    {
        metadatoDAO.delete(MetadatoValor.class, id);
    }

    public static List<MetadatoValor> getMetadatoValoresByMetadatoId(Long metadatoId)
    {
        return metadatoDAO.getMetadatoValoresByMetadatoId(metadatoId);
    }

    public static MetadatoValor getMetadatoValor(Long id)
    {
        return metadatoDAO.get(MetadatoValor.class, id).get(0);
    }

    private void compruebaCamposObligatorios() throws ParametrosObligatoriosException
    {
        if (metadato == null || valor == null || orden == null)
        {
            throw new ParametrosObligatoriosException("valor, orden");
        }
    }

}