package es.uji.apps.ocw.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * The persistent class for the OCW_VW_METADATOS database table.
 * 
 */
@Entity
@SuppressWarnings("serial")
@Table(name="OCW_VW_METADATOS")
public class MetadatoIdiomaPrincipal implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	private String limitado;

	private String nombre;

	private String obligatorio;

	private String tag;

    public MetadatoIdiomaPrincipal() {
    }

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLimitado() {
		return this.limitado;
	}

	public void setLimitado(String limitado) {
		this.limitado = limitado;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getObligatorio() {
		return this.obligatorio;
	}

	public void setObligatorio(String obligatorio) {
		this.obligatorio = obligatorio;
	}

	public String getTag() {
		return this.tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

}