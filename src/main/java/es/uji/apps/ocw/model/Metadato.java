package es.uji.apps.ocw.model;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.apps.ocw.dao.MetadatoDAO;
import es.uji.commons.rest.exceptions.ParametrosObligatoriosException;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

/**
 * The persistent class for the OCW_METADATOS database table.
 * 
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "OCW_METADATOS")
@Component
public class Metadato implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String limitado;

    private String obligatorio;

    private String tag;

    // bi-directional many-to-one association to MaterialMetadato
    @OneToMany(mappedBy = "metadato")
    private Set<MaterialMetadato> materialesMetadatos;

    // bi-directional many-to-one association to MetadatoIdioma
    @OneToMany(mappedBy = "metadato", cascade = CascadeType.ALL)
    private Set<MetadatoIdioma> metadatosIdiomas;

    // bi-directional many-to-one association to MetadatoValor
    @OneToMany(mappedBy = "metadato", cascade = CascadeType.ALL)
    private Set<MetadatoValor> metadatosValores;

    protected static MetadatoDAO metadatoDAO;

    @Autowired
    protected void setMetadatoDAO(MetadatoDAO metadatoDAO)
    {
        Metadato.metadatoDAO = metadatoDAO;
    }

    public Metadato()
    {
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getLimitado()
    {
        return this.limitado;
    }

    public void setLimitado(String limitado)
    {
        this.limitado = limitado;
    }

    public String getObligatorio()
    {
        return this.obligatorio;
    }

    public void setObligatorio(String obligatorio)
    {
        this.obligatorio = obligatorio;
    }

    public String getTag()
    {
        return this.tag;
    }

    public void setTag(String tag)
    {
        this.tag = tag;
    }

    public Set<MaterialMetadato> getMaterialesMetadatos()
    {
        return this.materialesMetadatos;
    }

    public void setMaterialesMetadatos(Set<MaterialMetadato> materialesMetadatos)
    {
        this.materialesMetadatos = materialesMetadatos;
    }

    public static List<Metadato> getMetadatos()
    {
        List<Metadato> metadatos = metadatoDAO.getMetadatos();
        return metadatos;
    }

    public Set<MetadatoIdioma> getMetadatosIdiomas()
    {
        return this.metadatosIdiomas;
    }

    public void setMetadatosIdiomas(Set<MetadatoIdioma> metadatosIdiomas)
    {
        this.metadatosIdiomas = metadatosIdiomas;
    }

    public Set<MetadatoValor> getMetadatosValores()
    {
        return this.metadatosValores;
    }

    public void setMetadatosValores(Set<MetadatoValor> metadatosValores)
    {
        this.metadatosValores = metadatosValores;
    }

    public Metadato insert()
    {
        return metadatoDAO.insert(this);
    }

    public static void delete(Long metadatoId)
    {
        metadatoDAO.delete(Metadato.class, metadatoId);
    }

    public Metadato update() throws RegistroNoEncontradoException, ParametrosObligatoriosException
    {
        List<Metadato> listaMetadatos = metadatoDAO.get(Metadato.class, this.getId());

        if (listaMetadatos != null && listaMetadatos.size() > 0)
        {
            return metadatoDAO.updateMetadato(this);
        }
        else
        {
            throw new RegistroNoEncontradoException();
        }
    }

    public static List<MaterialMetadato> getMetadatosByMaterial(Long materialId)
    {
        return metadatoDAO.getMetadatosByMaterial(materialId);
    }

    public static Metadato getMetadatoById(Long metadatoId) throws RegistroNoEncontradoException
    {
        Metadato metadato = metadatoDAO.getMetadato(metadatoId);

        if (metadato == null)
        {
            throw new RegistroNoEncontradoException();
        }

        return metadato;
    }

    public static void compruebaCamposObligatorios(String tag, String nombreIdiomaPrincipal)
            throws ParametrosObligatoriosException
    {
        if (tag == null)
        {
            throw new ParametrosObligatoriosException("tag");
        }
        else if (nombreIdiomaPrincipal == null)
        {
            throw new ParametrosObligatoriosException(
                    "nombre");
        }
    }

}