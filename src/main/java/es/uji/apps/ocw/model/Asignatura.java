package es.uji.apps.ocw.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.apps.ocw.dao.AsignaturaDAO;

/**
 * The persistent class for the OCW_EXT_ASIGNATURAS database table.
 * 
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "OCW_EXT_ASIGNATURAS")
@Component
public class Asignatura implements Serializable
{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private String id;

    @Column(name = "NOMBRE_CA")
    private String nombreCa;

    @Column(name = "NOMBRE_ES")
    private String nombreEs;

    @Column(name = "NOMBRE_UK")
    private String nombreUk;

    protected static AsignaturaDAO asignaturaDAO;

    @Autowired
    protected void setAsignaturaDAO(AsignaturaDAO asignaturaDAO)
    {
        Asignatura.asignaturaDAO = asignaturaDAO;
    }

    public Asignatura()
    {
    }

    public String getId()
    {
        return this.id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getNombreCa()
    {
        return this.nombreCa;
    }

    public void setNombreCa(String nombreCa)
    {
        this.nombreCa = nombreCa;
    }

    public String getNombreEs()
    {
        return this.nombreEs;
    }

    public void setNombreEs(String nombreEs)
    {
        this.nombreEs = nombreEs;
    }

    public String getNombreUk()
    {
        return this.nombreUk;
    }

    public void setNombreUk(String nombreUk)
    { 
        this.nombreUk = nombreUk;
    }

    public static  List<Asignatura> getAsignaturas (){
        return asignaturaDAO.getAsignaturas();
    }
}