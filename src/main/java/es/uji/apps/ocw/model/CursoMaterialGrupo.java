package es.uji.apps.ocw.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.apps.ocw.dao.CursoMaterialGrupoDAO;


/**
 * The persistent class for the OCW_VW_MATERIALES_POR_CURSO database table.
 * 
 */
@SuppressWarnings("serial")
@Table(name = "OCW_VW_MATERIALES_POR_CURSO")
@Component
@Entity
public class CursoMaterialGrupo implements Serializable
{
    @Id
    @Column(name = "CURSO_ID")
    private Long cursoId;

    @Id
    @Column(name = "GRUPO_ID")
    private Long grupoId;

    @Id
    @Column(name = "MATERIAL_ID")
    private Long materialId;

    @Id
    @Column(name = "CURSO_MATERIAL_ID")
    private Long cursoMaterialId;

    private Long nivel;

    private Long orden;

    @Column(name = "CONTENT_TYPE")
    private String contentType;

    private String descripcion;

    private String tipo;

    private String titulo;

    @Column(name = "DESCRIPCION_MATERIAL")
    private String descripcionMaterial;

    private Date fecha;

    @Column(name = "NOMBRE_FICHERO")
    private String nombreFichero;

    private String url;

    private String acronimo;

    private String idioma;

    private static CursoMaterialGrupoDAO cursoMaterialGrupoDAO;

    @Autowired
    public void setCursoMaterialGrupoDAO(CursoMaterialGrupoDAO cursoMaterialGrupoDAO)
    {
        CursoMaterialGrupo.cursoMaterialGrupoDAO = cursoMaterialGrupoDAO;
    }

    public CursoMaterialGrupo()
    {
    }

    public Long getCursoId()
    {
        return this.cursoId;
    }

    public void setCursoId(Long cursoId)
    {
        this.cursoId = cursoId;
    }

    public Long getGrupoId()
    {
        return this.grupoId;
    }

    public void setGrupoId(Long grupoId)
    {
        this.grupoId = grupoId;
    }

    public Long getMaterialId()
    {
        return materialId;
    }

    public void setMaterialId(Long materialId)
    {
        this.materialId = materialId;
    }

    public Long getCursoMaterialId()
    {
        return this.cursoMaterialId;
    }

    public void setCursoMaterialId(Long cursoMaterialId)
    {
        this.cursoMaterialId = cursoMaterialId;
    }

    public Long getNivel()
    {
        return this.nivel;
    }

    public void setNivel(Long nivel)
    {
        this.nivel = nivel;
    }

    public Long getOrden()
    {
        return this.orden;
    }

    public void setOrden(Long orden)
    {
        this.orden = orden;
    }

    public String getTipo()
    {
        return this.tipo;
    }

    public void setTipo(String tipo)
    {
        this.tipo = tipo;
    }

    public String getTitulo()
    {
        return this.titulo;
    }

    public void setTitulo(String titulo)
    {
        this.titulo = titulo;
    }

    public static List<CursoMaterialGrupo> getElementosByCurso(Long cursoId)
    {
        return cursoMaterialGrupoDAO.getElementosByCurso(cursoId);
    }

    public String getContentType()
    {
        return contentType;
    }

    public void setContentType(String contentType)
    {
        this.contentType = contentType;
    }

    public String getDescripcion()
    {
        return descripcion;
    }

    public void setDescripcion(String descripcion)
    {
        this.descripcion = descripcion;
    }

    public String getDescripcionMaterial()
    {
        return descripcionMaterial;
    }

    public void setDescripcionMaterial(String descripcionMaterial)
    {
        this.descripcionMaterial = descripcionMaterial;
    }

    public Date getFecha()
    {
        return fecha;
    }

    public void setFecha(Date fecha)
    {
        this.fecha = fecha;
    }

    public String getNombreFichero()
    {
        return nombreFichero;
    }

    public void setNombreFichero(String nombreFichero)
    {
        this.nombreFichero = nombreFichero;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public String getAcronimo()
    {
        return acronimo;
    }

    public void setAcronimo(String acronimo)
    {
        this.acronimo = acronimo;
    }

    public String getIdioma()
    {
        return idioma;
    }

    public void setIdioma(String idioma)
    {
        this.idioma = idioma;
    }

}