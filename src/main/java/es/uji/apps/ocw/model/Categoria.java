package es.uji.apps.ocw.model;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.apps.ocw.dao.CategoriaDAO;
import es.uji.commons.rest.exceptions.RegistroConHijosException;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

/**
 * The persistent class for the OCW_CATEGORIAS database table.
 * 
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "OCW_CATEGORIAS")
@Component
public class Categoria implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Long orden;

    // bi-directional many-to-one association to CategoriaIdioma
    @OneToMany(mappedBy = "categoria", cascade = CascadeType.ALL)
    private Set<CategoriaIdioma> categoriasIdiomas;

    // bi-directional many-to-one association to Curso
    @OneToMany(mappedBy = "categoria")
    private Set<CursoCategoria> cursosCategorias;

    protected static CategoriaDAO categoriaDAO;

    @Autowired
    protected void setCategoriaDAO(CategoriaDAO categoriaDAO)
    {
        Categoria.categoriaDAO = categoriaDAO;
    }

    public Categoria()
    {
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getOrden()
    {
        return this.orden;
    }

    public void setOrden(Long orden)
    {
        this.orden = orden;
    }

    public Set<CategoriaIdioma> getCategoriasIdiomas()
    {
        return this.categoriasIdiomas;
    }

    public List<CategoriaIdioma> getCategoriaIdiomasByCategoriaId(Long CategoriaId)
    {
        return categoriaDAO.getCategoriaIdiomaByCategoriaId(CategoriaId);
    }

    public Set<CursoCategoria> getCursosCategorias()
    {
        return this.cursosCategorias;
    }

    public void setCursosCategorias(Set<CursoCategoria> cursosCategorias)
    {
        this.cursosCategorias = cursosCategorias;
    }

    public void setCategoriasIdiomas(Set<CategoriaIdioma> categoriasIdiomas)
    {
        this.categoriasIdiomas = categoriasIdiomas;
    }

    public static List<Categoria> getCategorias()
    {
        return categoriaDAO.getCategorias();
    }

    public static Categoria getCategoriaById(Long CategoriaId) throws RegistroNoEncontradoException
    {
        List<Categoria> categoria = categoriaDAO.get(Categoria.class, CategoriaId);

        if (categoria.size() > 0)
        {
            return categoria.get(0);
        }
        else
        {
            throw new RegistroNoEncontradoException("El registre no s'ha encontrat.");
        }
    }

    public Categoria update()
    {
        return categoriaDAO.update(this);
    }

    public Categoria insert()
    {
        return categoriaDAO.insertCategoria(this);
    }

    public static void delete(long categoriaId) throws RegistroConHijosException,
            RegistroNoEncontradoException
    {
        List<Curso> listaCursos = Curso.getCursosByCategoriaId(categoriaId);

        if (listaCursos == null || listaCursos.size() == 0)
        {
            categoriaDAO.delete(Categoria.class, categoriaId);
        }
        else
        {
            throw new RegistroConHijosException(
                    "La categoria no es pot esborrar perque té informació rel·lacionada.");
        }
    }

    public static List<CategoriaIdiomaPrincipal> getCategoriasByCursoId(Long cursoId) {
        return categoriaDAO.getCategoriasByCursoId(cursoId);
    }
    
    public static List<CategoriaIdiomaPrincipal> getCategoriasIdiomaPrincipal()
    {
        return categoriaDAO.getCategoriasIdiomaPrincipal();
    }

    public CategoriaIdiomaPrincipal getCategoriaIdiomaPrincipalById()
    {
        return categoriaDAO.getCategoriaIdiomaPrincipalById(this.id);
    }

    public static CategoriaIdiomaPrincipal getCategoriaIdiomaPrincipalByCursoId(Long cursoId) throws RegistroNoEncontradoException
    {
        Curso curso = Curso.getCursoById(cursoId);
        
        List<Categoria> categorias = curso.getCategorias();
        Categoria categoria = categorias.get(0);
        
        return CategoriaIdiomaPrincipal.getCategoriaById(categoria.getId());
    }
    
    public static List<Categoria> getCategoriasByCursoIdAndIdioma(Long cursoId, String idioma)
    {
        return categoriaDAO.getCategoriasByCursoIdAndIdioma(cursoId, idioma);
    }

    public static List<Categoria> getCategoriasByIdioma(String acronimoIdioma)
    {
        return categoriaDAO.getCategoriasByIdioma(acronimoIdioma);
    }

    public static Categoria getCategoriaByIdAndIdioma(Long categoriaId, String acronimoIdioma)
    {
        return categoriaDAO.getCategoriaByIdAndIdioma(categoriaId, acronimoIdioma);
    }
}