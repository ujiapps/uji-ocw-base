package es.uji.apps.ocw.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.commons.db.BaseDAO;

/**
 * The persistent class for the OCW_CURSOS_IDIOMAS database table.
 * 
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "OCW_CURSOS_IDIOMAS")
@Component
public class CursoIdioma implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String descripcion;

    @Lob()
    @Column(name = "GUIA_APRENDIZAJE")
    private String guiaAprendizaje;

    private String nombre;

    private Long orden;

    private String periodo;

    @Lob()
    private String programa;

    // bi-directional many-to-one association to Curso
    @ManyToOne
    @JoinColumn(name = "CURSO_ID")
    private Curso curso;

    // bi-directional many-to-one association to Idioma
    @ManyToOne
    @JoinColumn(name = "IDIOMA_ID")
    private Idioma idioma;

    private static BaseDAO baseDAO;

    @Autowired
    public void setBaseDAO(BaseDAO baseDAO)
    {
        CursoIdioma.baseDAO = baseDAO;
    }

    public CursoIdioma()
    {
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getDescripcion()
    {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion)
    {
        this.descripcion = descripcion;
    }

    public String getGuiaAprendizaje()
    {
        return this.guiaAprendizaje;
    }

    public void setGuiaAprendizaje(String guiaAprendizaje)
    {
        this.guiaAprendizaje = guiaAprendizaje;
    }

    public String getNombre()
    {
        return this.nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public Long getOrden()
    {
        return this.orden;
    }

    public void setOrden(Long orden)
    {
        this.orden = orden;
    }

    public String getPeriodo()
    {
        return this.periodo;
    }

    public void setPeriodo(String periodo)
    {
        this.periodo = periodo;
    }

    public String getPrograma()
    {
        return this.programa;
    }

    public void setPrograma(String programa)
    {
        this.programa = programa;
    }

    public Curso getCurso()
    {
        return this.curso;
    }

    public void setCurso(Curso curso)
    {
        this.curso = curso;
    }

    public Idioma getIdioma()
    {
        return this.idioma;
    }

    public void setIdioma(Idioma idioma)
    {
        this.idioma = idioma;
    }

    public CursoIdioma insert() {
        return baseDAO.insert(this);
    }
    
    public void update()
    {
        baseDAO.update(this);
    }
}