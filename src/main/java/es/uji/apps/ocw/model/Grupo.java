package es.uji.apps.ocw.model;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.apps.ocw.dao.GrupoDAO;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

/**
 * The persistent class for the OCW_GRUPOS database table.
 * 
 */
@SuppressWarnings("serial")
@Entity
@Component
@Table(name = "OCW_GRUPOS")
public class Grupo implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    // bi-directional many-to-one association to Curso
    @ManyToOne
    @JoinColumn(name = "CURSO_ID")
    private Curso curso;

    // bi-directional many-to-one association to CursoMaterial
    @OneToMany(mappedBy = "grupo")
    private Set<CursoMaterial> grupoCursoMateriales;

    private String descripcion;

    private String nombre;

    private Long orden;

    private static GrupoDAO grupoDAO;

    public static final int INCREMENTO_ORDEN = 10;

    @Autowired
    public void setGrupoDAO(GrupoDAO grupoDAO)
    {
        Grupo.grupoDAO = grupoDAO;
    }

    public Grupo()
    {
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Curso getCurso()
    {
        return this.curso;
    }

    public void setCurso(Curso curso)
    {
        this.curso = curso;
    }

    public String getNombre()
    {
        return this.nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getDescripcion()
    {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion)
    {
        this.descripcion = descripcion;
    }

    public Long getOrden()
    {
        return this.orden;
    }

    public void setOrden(Long orden)
    {
        this.orden = orden;
    }

    public Set<CursoMaterial> getCursoMateriales()
    {
        return this.grupoCursoMateriales;
    }

    public void setCursoMateriales(Set<CursoMaterial> grupoCursoMateriales)
    {
        this.grupoCursoMateriales = grupoCursoMateriales;
    }

    public Grupo insert()
    {
        return grupoDAO.insert(this);
    }

    public static List<Grupo> getGruposByCursoId(Long cursoId)
    {
        return grupoDAO.getGruposByCursoId(cursoId);
    }

    public List<CursoMaterial> getMateriales()
    {
        return grupoDAO.getMaterialesByGrupoId(id);
    }

    public Grupo update()
    {
        return grupoDAO.update(this);
    }

    public static Grupo getGrupoById(Long grupoId) throws RegistroNoEncontradoException
    {
        List<Grupo> grupo = grupoDAO.get(Grupo.class, grupoId);

        if (grupo.size() > 0)
        {
            return grupo.get(0);
        }
        else
        {
            throw new RegistroNoEncontradoException("El registre no s'ha encontrat.");
        }
    }

    public static void delete(Long grupoId)
    {
        grupoDAO.delete(Grupo.class, grupoId);
    }

    public void moverGrupo(Long anteriorId, String tipoAnterior)
            throws RegistroNoEncontradoException
    {

        Long ordenAnterior = new Long(0);
        Curso curso = this.getCurso();
        
        if (anteriorId != -1) {
            if (tipoAnterior.equals("MAT"))
            {
                CursoMaterial anterior = CursoMaterial.getCursoMaterialById(anteriorId);
                ordenAnterior = anterior.getOrden();
            }
            else
            {
                Grupo anterior = Grupo.getGrupoById(anteriorId);
                ordenAnterior = anterior.getOrden();
            }
        }
        Grupo.incrementaOrdenGrupoRaiz(curso.getId(), ordenAnterior);
        this.setOrden(ordenAnterior + INCREMENTO_ORDEN);
        this.update();
    }

    public static void incrementaOrdenGrupoRaiz(Long cursoId, Long ordenAnterior) throws RegistroNoEncontradoException
    {
        grupoDAO.incrementaOrdenMaterialesGrupoRaizCuyoOrdenMayorQue(cursoId, ordenAnterior, INCREMENTO_ORDEN);
        grupoDAO.incrementaOrdenGruposCuyoOrdenMayorQue(cursoId, ordenAnterior, INCREMENTO_ORDEN);
    }

    public void incrementaOrdenMateriales(Long ordenAnterior)
    {
        grupoDAO.incrementaOrdenMaterialesCuyoOrdenMayorQue(this.getCurso().getId(), this.getId(), ordenAnterior, INCREMENTO_ORDEN);
    }

}