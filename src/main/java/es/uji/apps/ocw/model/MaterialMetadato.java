package es.uji.apps.ocw.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.apps.ocw.dao.MaterialMetadatoDAO;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import es.uji.commons.sso.exceptions.UnauthorizedUserException;

/**
 * The persistent class for the OCW_MATERIALES_METADATOS database table.
 * 
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "OCW_MATERIALES_METADATOS")
@Component
public class MaterialMetadato implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String valor;

    // bi-directional many-to-one association to Material
    @ManyToOne
    @JoinColumn(name = "MATERIAL_ID")
    private Material material;

    // bi-directional many-to-one association to Metadato
    @ManyToOne
    @JoinColumn(name = "METADATO_ID")
    private Metadato metadato;

    protected static MaterialMetadatoDAO materialMetadatoDAO;

    @Autowired
    protected void setMaterialMetadatoDAO(MaterialMetadatoDAO materialMetadatoDAO)
    {
        MaterialMetadato.materialMetadatoDAO = materialMetadatoDAO;
    }

    public MaterialMetadato()
    {
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getValor()
    {
        return this.valor;
    }

    public void setValor(String valor)
    {
        this.valor = valor;
    }

    public Material getMaterial()
    {
        return this.material;
    }

    public void setMaterial(Material material)
    {
        this.material = material;
    }

    public Metadato getMetadato()
    {
        return this.metadato;
    }

    public void setMetadato(Metadato metadato)
    {
        this.metadato = metadato;
    }

    public static List<MaterialMetadato> getMetadatosByMaterial(Long materialId)
    {
        return materialMetadatoDAO.getMetadatosByMaterial(materialId);
    }

    public MaterialMetadato insert(Material material, Metadato metadato, String valor, Persona persona) throws UnauthorizedUserException
    {
        
        if (!material.esPropietario(persona)) {
            persona.checkEsAdministrador();
        }

        this.setMaterial(material);
        this.setMetadato(metadato);
        this.setValor(valor);
        return materialMetadatoDAO.insert(this);
    }

    public static void delete(Long materialMetadatoId, Material material, Persona persona) throws UnauthorizedUserException
    {
        if (!material.esPropietario(persona)) {
            persona.checkEsAdministrador();
        }

        materialMetadatoDAO.delete(MaterialMetadato.class, materialMetadatoId);

    }

    public MaterialMetadato update(Persona persona) throws UnauthorizedUserException
    {
        if (!material.esPropietario(persona)) {
            persona.checkEsAdministrador();
        }
        return materialMetadatoDAO.update(this);
    }

    public static MaterialMetadato getMaterialMetadatoById(Long materialMetadatoId) throws RegistroNoEncontradoException
    {
        List<MaterialMetadato> materialMetadato = materialMetadatoDAO.get(MaterialMetadato.class, materialMetadatoId);
        
        if (materialMetadato.size() > 0) {
            return materialMetadato.get(0);
        } else {
            throw new RegistroNoEncontradoException("El registre no s'ha encontrat.");
        }
    }
}