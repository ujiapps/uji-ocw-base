package es.uji.apps.ocw.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * The persistent class for the OCW_MATERIALES_TAGS database table.
 * 
 */
@Entity
@SuppressWarnings("serial")
@Table(name = "OCW_MATERIALES_TAGS")
public class MaterialTag implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    // bi-directional many-to-one association to Material
    @ManyToOne
    @JoinColumn(name = "MATERIAL_ID")
    private Material material;

    // bi-directional many-to-one association to Tag
    @ManyToOne
    @JoinColumn(name = "TAG_ID")
    private Tag tag;

    public MaterialTag()
    {
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Material getMaterial()
    {
        return this.material;
    }

    public void setMaterial(Material material)
    {
        this.material = material;
    }

    public Tag getTag()
    {
        return this.tag;
    }

    public void setTag(Tag tag)
    {
        this.tag = tag;
    }
    
}