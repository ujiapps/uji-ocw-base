package es.uji.apps.ocw.model;

import java.io.Serializable;
import java.text.MessageFormat;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.commons.db.BaseDAO;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import es.uji.commons.sso.exceptions.UnauthorizedUserException;

/**
 * The persistent class for the OCW_CURSOS_IMAGENES database table.
 * 
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "OCW_CURSOS_IMAGENES")
@Component
public class CursoImagen implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Lob()
    private byte[] imagen;

    // bi-directional many-to-one association to Curso
    @ManyToOne
    @JoinColumn(name = "CURSO_ID")
    private Curso curso;

    @Column(name = "CONTENT_TYPE")
    private String contentType;

    private static BaseDAO baseDAO;

    @Autowired
    public void setBaseDAO(BaseDAO baseDAO)
    {
        CursoImagen.baseDAO = baseDAO;
    }

    public CursoImagen()
    {
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public byte[] getImagen()
    {
        return this.imagen;
    }

    public void setImagen(byte[] imagen)
    {
        this.imagen = imagen;
    }

    public Curso getCurso()
    {
        return this.curso;
    }

    public void setCurso(Curso curso)
    {
        this.curso = curso;
    }

    public String getContentType()
    {
        return this.contentType;
    }

    public void setContentType(String contentType)
    {
        this.contentType = contentType;
    }

    public static CursoImagen getCursoImagenById(Long cursoImagenId) throws RegistroNoEncontradoException
    {
        List<CursoImagen> cursoImagen = baseDAO.get(CursoImagen.class, cursoImagenId);
        
        if (cursoImagen.size() > 0) {
            return cursoImagen.get(0);
        } else {
            throw new RegistroNoEncontradoException("El registre no s'ha encontrat.");
        }
    }

    public CursoImagen insert(Persona persona) throws UnauthorizedUserException
    {
        if (!curso.esPropietario(persona))
        {
            persona.checkEsAdministrador();
        }
        return baseDAO.insert(this);
    }

    public void deleteCursoImagen(Long cursoImagenId, Persona persona) throws UnauthorizedUserException
    {
        if (!curso.esPropietario(persona))
        {
            persona.checkEsAdministrador();
        }
        baseDAO.delete(CursoImagen.class, cursoImagenId);
    }
    
    public String getUrl() {
        return MessageFormat.format("/ocw/rest/curso/{0,number,#}/imagen/{1,number,#}", getCurso().getId(), getId());
    }

}