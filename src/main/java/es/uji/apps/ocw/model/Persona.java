package es.uji.apps.ocw.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.apps.ocw.dao.PersonaDAO;
import es.uji.apps.ocw.model.domains.PerfilAcceso;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import es.uji.commons.sso.dao.ApaDAO;
import es.uji.commons.sso.exceptions.UnauthorizedUserException;

/**
 * The persistent class for the OCW_EXT_PERSONAS database table.
 * 
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "OCW_EXT_PERSONAS")
@Component
public class Persona implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String apellido1;

    private String apellido2;

    private String email;

    private String nombre;

    private static PersonaDAO personaDAO;
    protected static ApaDAO apaDAO;

    @Autowired
    public void setApaDAO(ApaDAO apaDAO)
    {
        Persona.apaDAO = apaDAO;
    }

    @Autowired
    public void setPersonaDAO(PersonaDAO personaDAO)
    {
        Persona.personaDAO = personaDAO;
    }

    public Persona()
    {
    }

    public Persona(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getApellido1()
    {
        return this.apellido1;
    }

    public void setApellido1(String apellido1)
    {
        this.apellido1 = apellido1;
    }

    public String getApellido2()
    {
        if (this.apellido2 != null) {
            return this.apellido2;
        } else {
            return "";
        }
    }

    public void setApellido2(String apellido2)
    {
        this.apellido2 = apellido2;
    }

    public String getEmail()
    {
        return this.email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getNombre()
    {
        return this.nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public static List<Persona> getPersonas()
    {
        return personaDAO.getPersonas();
    }

    public static Persona getPersonaById(Long personaId) throws RegistroNoEncontradoException
    {
        List<Persona> persona = personaDAO.getPersonaById(personaId);

        if (persona.size() > 0)
        {
            return persona.get(0);
        }
        else
        {
            throw new RegistroNoEncontradoException("El registre no s'ha encontrat.");
        }

    }

    public void checkEsAdministrador() throws UnauthorizedUserException
    {
        apaDAO.checkPerfil("OCW", PerfilAcceso.ADMIN.toString(), id);
    }

    public boolean isAdministrador()
    {

        return apaDAO.hasPerfil("OCW", PerfilAcceso.ADMIN.toString(), id);
    }

}