package es.uji.apps.ocw.model;

import java.io.Serializable;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.apps.ocw.dao.CursoDAO;
import es.uji.commons.rest.exceptions.RegistroConHijosException;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import es.uji.commons.sso.exceptions.UnauthorizedUserException;

/**
 * The persistent class for the OCW_CURSOS database table.
 * 
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "OCW_CURSOS")
@Component
public class Curso implements Serializable
{
    private static final int NUM_MAX_CURSOS_A_VISUALIZAR = 5;
    private static final int NUM_CURSOS_POR_PAGINA = 5;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String estado;

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_BAJA")
    private Date fechaBaja;

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_CREACION")
    private Date fechaCreacion;

    @Column(name = "PERSONA_ID")
    private Long personaId;

    private String visible;

    // bi-directional many-to-one association to Acceso
    @OneToMany(mappedBy = "curso")
    private Set<Acceso> accesos;

    // bi-directional many-to-one association to Categoria
    @OneToMany(mappedBy = "curso", cascade = CascadeType.ALL)
    private Set<CursoCategoria> cursosCategorias;

    // bi-directional many-to-one association to CursoAsignatura
    @OneToMany(mappedBy = "curso")
    private Set<CursoAsignatura> cursosAsignaturas;

    // bi-directional many-to-one association to CursoAutorizado
    @OneToMany(mappedBy = "curso")
    private Set<CursoAutorizado> cursosAutorizados;

    // bi-directional many-to-one association to CursoIdioma
    @OneToMany(mappedBy = "curso", cascade = CascadeType.ALL)
    private Set<CursoIdioma> cursosIdiomas;

    // bi-directional many-to-one association to CursoImagen
    @OneToMany(mappedBy = "curso")
    private Set<CursoImagen> cursosImagenes;

    // bi-directional many-to-one association to CursoMaterial
    @OneToMany(mappedBy = "curso")
    private Set<CursoMaterial> cursosMateriales;

    // bi-directional many-to-one association to CursoTag
    @OneToMany(mappedBy = "curso")
    private Set<CursoTag> cursosTags;

    protected static CursoDAO cursoDAO;

    @Autowired
    protected void setCursoDAO(CursoDAO cursoDAO)
    {
        Curso.cursoDAO = cursoDAO;
    }

    public Curso()
    {
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getEstado()
    {
        return this.estado;
    }

    public void setEstado(String estado)
    {
        this.estado = estado;
    }

    public Date getFechaBaja()
    {
        return this.fechaBaja;
    }

    public void setFechaBaja(Date fechaBaja)
    {
        this.fechaBaja = fechaBaja;
    }

    public Date getFechaCreacion()
    {
        return this.fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion)
    {
        this.fechaCreacion = fechaCreacion;
    }

    public Long getPersonaId()
    {
        return this.personaId;
    }

    public void setPersonaId(Long personaId)
    {
        this.personaId = personaId;
    }

    public String getVisible()
    {
        return this.visible;
    }

    public void setVisible(String visible)
    {
        this.visible = visible;
    }

    public Set<Acceso> getAccesos()
    {
        return this.accesos;
    }

    public void setAccesos(Set<Acceso> accesos)
    {
        this.accesos = accesos;
    }

    public Set<CursoAsignatura> getCursosAsignaturas()
    {
        return this.cursosAsignaturas;
    }

    public void setCursosAsignaturas(Set<CursoAsignatura> cursosAsignaturas)
    {
        this.cursosAsignaturas = cursosAsignaturas;
    }

    public Set<CursoAutorizado> getCursosAutorizados()
    {
        return this.cursosAutorizados;
    }

    public void setCursosAutorizados(Set<CursoAutorizado> cursosAutorizados)
    {
        this.cursosAutorizados = cursosAutorizados;
    }

    public Set<CursoIdioma> getCursosIdiomas()
    {
        return this.cursosIdiomas;
    }

    public void setCursosIdiomas(Set<CursoIdioma> cursosIdiomas)
    {
        this.cursosIdiomas = cursosIdiomas;
    }

    public Set<CursoImagen> getCursosImagenes()
    {
        return this.cursosImagenes;
    }

    public void setCursosImagenes(Set<CursoImagen> cursosImagenes)
    {
        this.cursosImagenes = cursosImagenes;
    }

    public Set<CursoMaterial> getCursosMateriales()
    {
        return this.cursosMateriales;
    }

    public void setCursosMateriales(Set<CursoMaterial> cursosMateriales)
    {
        this.cursosMateriales = cursosMateriales;
    }

    public Set<CursoCategoria> getCursosCategorias()
    {
        return this.cursosCategorias;
    }

    public void setCursosCategorias(Set<CursoCategoria> cursosCategorias)
    {
        this.cursosCategorias = cursosCategorias;
    }

    public Set<CursoTag> getCursosTags()
    {
        return this.cursosTags;
    }

    public void setCursosTags(Set<CursoTag> cursosTags)
    {
        this.cursosTags = cursosTags;
    }

    public String getNombrePersona()
    {
        return cursoDAO.getNombrePersona(this.personaId);
    }

    public static Curso getCursoById(Long cursoId) throws RegistroNoEncontradoException
    {
        List<Curso> curso = cursoDAO.get(Curso.class, cursoId);

        if (curso.size() > 0)
        {
            return curso.get(0);
        }
        else
        {
            throw new RegistroNoEncontradoException("El registre no s'ha encontrat.");
        }
    }

    public static List<CursoIdiomaPrincipal> getCursosConFiltros(Map<String, String> filtro,
            Persona persona)
    {
        if (persona.isAdministrador() && filtro.isEmpty())
        {
            return new ArrayList<CursoIdiomaPrincipal>();
        }

        if (!persona.isAdministrador())
        {
            filtro.put("personaId", persona.getId().toString());
        }

        return cursoDAO.getCursosConFiltros(filtro);
    }

    public List<CursoMaterial> getMateriales()
    {
        return cursoDAO.getMateriales(this.id);
    }

    public void deleteCursoMaterialById(Long cursoMaterialId, Persona persona)
            throws UnauthorizedUserException
    {
        if (!esPropietario(persona) && !estaAutorizado(persona))
        {
            persona.checkEsAdministrador();
        }

        cursoDAO.delete(CursoMaterial.class, cursoMaterialId);
    }

    public void deleteAsignaturaById(Long cursoAsignaturaId, Persona persona)
            throws UnauthorizedUserException
    {
        if (!esPropietario(persona) && !estaAutorizado(persona))
        {
            persona.checkEsAdministrador();
        }

        cursoDAO.deleteAsignaturaById(cursoAsignaturaId);
    }

    public CursoMaterial addMaterial(Material material, Persona persona)
            throws UnauthorizedUserException
    {

        if (!esPropietario(persona) && !estaAutorizado(persona))
        {
            persona.checkEsAdministrador();
        }

        CursoMaterial cursoMaterial = new CursoMaterial();

        cursoMaterial.setMaterial(material);
        cursoMaterial.setCurso(this);
        return cursoDAO.insert(cursoMaterial);

    }

    public CursoAsignatura addAsignatura(String asignaturaId, Persona persona)
            throws UnauthorizedUserException
    {
        if (!esPropietario(persona) && !estaAutorizado(persona))
        {
            persona.checkEsAdministrador();
        }
        CursoAsignatura cursoAsignatura = new CursoAsignatura();
        cursoAsignatura.setAsignaturaId(asignaturaId);
        cursoAsignatura.setCurso(this);

        return cursoDAO.insert(cursoAsignatura);
    }

    public CursoCategoria addCursoCategoria(Categoria categoria, Persona persona)
            throws UnauthorizedUserException, RegistroNoEncontradoException
    {
        if (!esPropietario(persona) && !estaAutorizado(persona))
        {
            persona.checkEsAdministrador();
        }
        CursoCategoria cursoCategoria = new CursoCategoria();
        cursoCategoria.setCategoria(categoria);
        cursoCategoria.setCurso(this);

        return cursoDAO.insert(cursoCategoria);
    }

    public List<CursoAsignatura> getAsignaturas()
    {
        return cursoDAO.getAsignaturas(this.id);
    }

    public List<CursoAsignatura> getAsignaturasByCodigo(String asignaturaId)
    {
        return cursoDAO.getAsignaturasByCodigo(this.id, asignaturaId);
    }

    public Asignatura getAsignaturaById(String asignaturaId)
    {
        return cursoDAO.getAsignaturaById(asignaturaId);
    }

    public CursoAutorizado addAutorizado(Long personaId, Persona persona)
            throws UnauthorizedUserException
    {
        if (!esPropietario(persona) && !estaAutorizado(persona))
        {
            persona.checkEsAdministrador();
        }

        CursoAutorizado cursoAutorizado = new CursoAutorizado();
        cursoAutorizado.setPersonaId(personaId);
        cursoAutorizado.setCurso(this);

        return cursoDAO.insert(cursoAutorizado);
    }

    public List<CursoAutorizado> getAutorizados()
    {
        return cursoDAO.getAutorizados(this.id);
    }

    public boolean isAutorizadoByCurso(Long personaId)
    {
        return cursoDAO.isAutorizadoByCurso(this.id, personaId);
    }

    public void deleteAutorizadoById(Long cursoAutorizadoId, Persona persona)
            throws UnauthorizedUserException
    {
        if (!esPropietario(persona) && !estaAutorizado(persona))
        {
            persona.checkEsAdministrador();
        }

        cursoDAO.deleteAutorizadoById(cursoAutorizadoId);
    }

    public void parseaTagsByString(String etiquetas)
    {
        List<String> listaEtiquetas = cadenaEtiquetasToList(etiquetas);
        this.borraTagsNoUtilizados(listaEtiquetas);
        listaEtiquetas = filtraEtiquetasYaRelacionadas(listaEtiquetas);

        for (String tagString : listaEtiquetas)
        {
            List<Tag> tagList = Tag.getTagByName(tagString);
            if (tagList.size() == 0)
            {
                Tag tag = new Tag();
                tag.setNombre(tagString);
                tag = tag.insert();
                this.insertTag(tag);
            }
            else
            {
                this.insertTag(tagList.get(0));
            }
        }
    }

    public void borraTagsNoUtilizados(List<String> listaEtiquetas)
    {
        for (CursoTag cursoTag : this.getTags())
        {
            if (!listaEtiquetas.contains(cursoTag.getTag().getNombre()))
            {
                Tag tag = cursoTag.getTag();
                cursoDAO.delete(CursoTag.class, cursoTag.getId());
                tag.borraTagSiNoSeUtiliza(tag.getId());
            }
        }
    }

    public CursoTag insertTag(Tag tag)
    {
        CursoTag cursoTag = new CursoTag();
        cursoTag.setCurso(this);
        cursoTag.setTag(tag);

        return cursoTag.insert();
    }

    public static List<String> cadenaEtiquetasToList(String cadena)
    {
        List<String> listaEtiquetas = new ArrayList<String>();
        if (!cadena.isEmpty() && cadena.length() != 0)
        {
            String[] etiquetas = cadena.split(",\\s*");

            for (String tag : etiquetas)
            {
                tag = StringUtils.strip(tag);
                listaEtiquetas.add(tag);
            }
        }
        return listaEtiquetas;
    }

    public List<String> filtraEtiquetasYaRelacionadas(List<String> listaEtiquetas)
    {
        for (CursoTag cursoTag : this.getTags())
        {
            String tag = cursoTag.getTag().getNombre();
            if (listaEtiquetas.contains(tag))
            {
                listaEtiquetas.remove(tag);
            }
        }

        return listaEtiquetas;
    }

    public String getTagsEnCadena()
    {

        StringBuilder tags = new StringBuilder();
        String delim = "";
        for (CursoTag cursoTag : this.getTags())
        {
            tags.append(delim).append(cursoTag.getTag().getNombre());
            delim = ", ";
        }

        return tags.toString();
    }

    public List<CursoTag> getTags()
    {
        return cursoDAO.getTags(this.getId());
    }

    public Curso insert()
    {
        return cursoDAO.insert(this);
    }

    public void update(Persona persona) throws UnauthorizedUserException
    {
        if (!esPropietario(persona) && !estaAutorizado(persona))
        {
            persona.checkEsAdministrador();
        }

        cursoDAO.update(this);
    }

    public List<CursoImagen> getImagenes()
    {
        return cursoDAO.getImagenesByCursoId(this.id);
    }

    public void delete(Persona persona) throws UnauthorizedUserException, RegistroConHijosException
    {
        if (!esPropietario(persona) && !estaAutorizado(persona))
        {
            persona.checkEsAdministrador();
        }

        try
        {
            cursoDAO.delete(Curso.class, id);
        }
        catch (Exception e)
        {
            throw new RegistroConHijosException(
                    "No es pot borrar un curso que té registres associats.");
        }
    }

    public boolean esPropietario(Persona persona)
    {
        Integer resul = personaId.compareTo(persona.getId());
        return resul == 0;
    }

    private boolean estaAutorizado(Persona persona)
    {
        return cursoDAO.estaAutorizado(id, persona.getId());
    }

    public static List<Curso> getCursosByCategoriaId(Long categoriaId)
    {
        return cursoDAO.getCursosByCategoriaId(categoriaId);
    }

    public CursoIdiomaPrincipal getCursoIdiomaPrincipal()
    {
        return cursoDAO.getCursoIdiomaPrincipal(this.id);
    }

    public void deleteCursoCategoriaById(Long id, Persona persona) throws UnauthorizedUserException
    {
        if (!esPropietario(persona) && !estaAutorizado(persona))
        {
            persona.checkEsAdministrador();
        }

        cursoDAO.deleteCursoCategoriaById(id);
    }

    public List<CursoCategoria> getCursoCategoriasByCursoId()
    {
        return cursoDAO.getCursoCategoriasByCursoId(this.id);

    }

    public boolean existeCursoCategoria(Long categoriaId)

    {
        return cursoDAO.existeCursoCategoria(this.id, categoriaId);
    }

    public List<CursoMaterial> getMaterialesSinGrupo()
    {
        return cursoDAO.getMaterialesSinGrupo(this.id);
    }

    public List<String> getNombreTagByCursoId(Curso curso)
    {
        List<String> listaTags = new ArrayList<String>();
        for (CursoTag cursoTag : this.getTags())
        {
            listaTags.add(cursoTag.getTag().getNombre());
        }

        return listaTags;
    }

    public String getImagenPrincipal()
    {
        List<CursoImagen> listaImagenes = cursoDAO.getImagenPrincipal(this.getId());
        String url = "http://ujiapps.uji.es/ocw/img/uji.png";

        if (listaImagenes.size() == 1)
        {
            CursoImagen cursoImagen = listaImagenes.get(0);
            url = MessageFormat.format("/ocw/rest/curso/{0,number,#}/imagen/{1,number,#}",
                    cursoImagen.getCurso().getId(), cursoImagen.getId());
        }

        return url;
    }

    public List<Categoria> getCategorias()
    {
        return cursoDAO.getCategorias(this.id);
    }

    public List<CursoIdioma> getTodosCursosIdiomas()
    {
        return cursoDAO.getTodosCursosIdiomas(this.id);
    }

    public static List<CursoCategoria> getCursosCategoriasByCursosIds(List<Long> listaCursosIds)
    {
        return cursoDAO.getCursosCategoriasByCursosIds(listaCursosIds);
    }

    public static List<CursoTag> getEtiquetasFromCursosIds(List listaCursosIds)
    {
        return cursoDAO.getEtiquetasFromCursosIds(listaCursosIds);
    }

    public static List<CursoImagen> getImagenFromCursosIds(List<Long> listaCursosIds)
    {
        return cursoDAO.getImagenFromCursosIds(listaCursosIds);
    }

    public static List<Curso> getUltimosCincoCursos()
    {
        return cursoDAO.getUltimosCursos(NUM_MAX_CURSOS_A_VISUALIZAR);
    }

    public static List<Curso> getCincoCursosMasVisitados()
    {
        return cursoDAO.getCursosMasVisitados(NUM_MAX_CURSOS_A_VISUALIZAR);
    }

    public static List<Curso> getCursosPaginadosByCategoriaId(Long categoriaId, Integer pagina)

    {
        return cursoDAO.getCursosPaginadosByCategoriaId(categoriaId, pagina, NUM_CURSOS_POR_PAGINA);
    }

    public static Integer getNumeroTotalPaginas()
    {
        return cursoDAO.getNumeroTotalPaginas(NUM_CURSOS_POR_PAGINA);
    }

    public static Integer getNumeroTotalPaginasByCategoriaId(Long categoriaId)
    {
        return cursoDAO.getNumeroTotalPaginasByCategoriaId(categoriaId, NUM_CURSOS_POR_PAGINA);
    }

    public static List<Curso> getTodosLosCursosPaginados(int pagina)
    {
        return cursoDAO.getTodosLosCursosPaginados(pagina, NUM_CURSOS_POR_PAGINA);
    }

    public static List<Curso> getCursosByMaterialId(Long materialId)
    {
        return cursoDAO.getCursosByMaterialId(materialId);
    }

    public static Integer getNumeroTotalPaginasBusquedaSimple(String cadena)
    {
        return cursoDAO.getNumeroTotalPaginasBusquedaSimple(cadena, NUM_CURSOS_POR_PAGINA);
    }

    public static List<Curso> getCursosPaginadosBusquedaSimple(String cadena, int pagina)
    {
        return cursoDAO.getCursosPaginadosBusquedaSimple(cadena, pagina, NUM_CURSOS_POR_PAGINA);
    }

    public static List<Curso> getCursosPaginadosByTagId(Long tagId, Integer pagina)
    {
        return cursoDAO.getCursosPaginadosByTagId(tagId, pagina, NUM_CURSOS_POR_PAGINA);
    }

    public static Integer getNumeroTotalPaginasByTagId(Long tagId)
    {
        return cursoDAO.getNumeroTotalPaginasByTagId(tagId, NUM_CURSOS_POR_PAGINA);
    }

}