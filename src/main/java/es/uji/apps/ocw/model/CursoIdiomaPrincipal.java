package es.uji.apps.ocw.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the OCW_VW_CURSOS database table.
 * 
 */
@Entity
@SuppressWarnings("serial")
@Table(name = "OCW_VW_CURSOS")
public class CursoIdiomaPrincipal implements Serializable
{

    private String estado;

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_BAJA")
    private Date fechaBaja;

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_CREACION")
    private Date fechaCreacion;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String nombre;
    
    private String descripcion;
    
    @Column(name = "PERSONA_ID")
    private Long personaId;

    private String visible;

    public CursoIdiomaPrincipal()
    {
    }

    public String getEstado()
    {
        return this.estado;
    }

    public void setEstado(String estado)
    {
        this.estado = estado;
    }

    public Date getFechaBaja()
    {
        return this.fechaBaja;
    }

    public void setFechaBaja(Date fechaBaja)
    {
        this.fechaBaja = fechaBaja;
    }

    public Date getFechaCreacion()
    {
        return this.fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion)
    {
        this.fechaCreacion = fechaCreacion;
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return this.nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public Long getPersonaId()
    {
        return this.personaId;
    }

    public void setPersonaId(Long personaId)
    {
        this.personaId = personaId;
    }

    public String getVisible()
    {
        return this.visible;
    }

    public void setVisible(String visible)
    {
        this.visible = visible;
    }

    public String getDescripcion()
    {
        return descripcion;
    }

    public void setDescripcion(String descripcion)
    {
        this.descripcion = descripcion;
    }
}