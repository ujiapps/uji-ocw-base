package es.uji.apps.ocw.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import es.uji.apps.ocw.dao.CursoMaterialDAO;
import es.uji.apps.ocw.dao.CursoMaterialGrupoDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.commons.db.BaseDAO;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

/**
 * The persistent class for the OCW_CURSOS_MATERIALES database table.
 * 
 */
@Entity
@SuppressWarnings("serial")
@Table(name = "OCW_CURSOS_MATERIALES")
@Component
public class CursoMaterial implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    // bi-directional many-to-one association to Curso
    @ManyToOne
    @JoinColumn(name = "CURSO_ID")
    private Curso curso;

    // bi-directional many-to-one association to Material
    @ManyToOne
    @JoinColumn(name = "MATERIAL_ID")
    private Material material;

    // bi-directional many-to-one association to Grupo
    @ManyToOne
    @JoinColumn(name = "GRUPO_ID")
    private Grupo grupo;

    private Long orden;

    private String descripcion;

    public static final int INCREMENTO_ORDEN = 10;

    private static CursoMaterialDAO cursoMaterialDAO;

    @Autowired
    public void setCursoMaterialDAO(CursoMaterialDAO cursoMaterialDAO)
    {
        CursoMaterial.cursoMaterialDAO = cursoMaterialDAO;
    }

    public CursoMaterial()
    {
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Grupo getGrupo()
    {
        return this.grupo;
    }

    public void setGrupo(Grupo grupo)
    {
        this.grupo = grupo;
    }

    public Curso getCurso()
    {
        return this.curso;
    }

    public void setCurso(Curso curso)
    {
        this.curso = curso;
    }

    public String getDescripcion()
    {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion)
    {
        this.descripcion = descripcion;
    }

    public Long getOrden()
    {
        return this.orden;
    }

    public void setOrden(Long orden)
    {
        this.orden = orden;
    }

    public Material getMaterial()
    {
        return this.material;
    }

    public void setMaterial(Material material)
    {
        this.material = material;
    }

    public void moverMaterial(Long anteriorId, Long grupoPadreId, String tipoAnterior)
            throws RegistroNoEncontradoException
    {

        Long ordenAnterior = new Long(0);

        if (grupoPadreId != -1)
        {
            Grupo grupo = Grupo.getGrupoById(grupoPadreId);

            if (anteriorId != -1)
            {
                CursoMaterial anterior = CursoMaterial.getCursoMaterialById(anteriorId);
                ordenAnterior = anterior.getOrden();

            }
            grupo.incrementaOrdenMateriales(ordenAnterior);
            
            this.setGrupo(grupo);
            this.setOrden(new Long(INCREMENTO_ORDEN) + ordenAnterior);
            this.update();

            List<CursoMaterial> listaMateriales = grupo.getMateriales();
            listaMateriales.add(this);
            grupo.setCursoMateriales(new HashSet<CursoMaterial>(listaMateriales));

        }
        else
        {
            if (anteriorId != -1)
            {
                if (tipoAnterior.equals("MAT"))
                {
                    CursoMaterial anterior = CursoMaterial.getCursoMaterialById(anteriorId);
                    ordenAnterior = anterior.getOrden();
                }
                else
                {
                    Grupo anterior = Grupo.getGrupoById(anteriorId);
                    ordenAnterior = anterior.getOrden();
                }
                this.setGrupo(null);

            }
            Grupo.incrementaOrdenGrupoRaiz(this.getCurso().getId(), ordenAnterior);
            this.setOrden(new Long(INCREMENTO_ORDEN) + ordenAnterior);
            this.setGrupo(null);
            this.update();

        }
    }

    public static CursoMaterial getCursoMaterialById(Long cursoMaterialId)
            throws RegistroNoEncontradoException
    {
        List<CursoMaterial> cursoMaterial = cursoMaterialDAO.get(CursoMaterial.class, cursoMaterialId);

        if (cursoMaterial.size() > 0)
        {
            return cursoMaterial.get(0);
        }
        else
        {
            throw new RegistroNoEncontradoException("El registre no s'ha encontrat.");
        }
    }

    public void update()
    {
        cursoMaterialDAO.update(this);
    }

    public static void delete(Long cursoMaterialId)
    {
        cursoMaterialDAO.delete(CursoMaterial.class, cursoMaterialId);
    }

    public static Long getSiquienteOrdenByGrupo(Long grupoId)
    {
        Long maxOrder =  cursoMaterialDAO.getMaxOrdenCursoMaterialByGrupo(grupoId);
        return maxOrder == null ? 10 : maxOrder + 10;
    }
}