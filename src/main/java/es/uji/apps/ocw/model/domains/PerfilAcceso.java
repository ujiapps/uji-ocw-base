package es.uji.apps.ocw.model.domains;

public enum PerfilAcceso
{
    ADMIN, USUARIO;
}
