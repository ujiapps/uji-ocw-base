package es.uji.apps.ocw.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.apps.ocw.dao.CursoMultiIdiomaDAO;

/**
 * The persistent class for the OCW_VW_CURSOS_DETALLE database table.
 * 
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "OCW_VW_CURSOS_DETALLE")
@Component
public class CursoMultiIdioma implements Serializable
{

    private String descripcion;

    @Lob()
    @Column(name = "GUIA_APRENDIZAJE")
    private String guiaAprendizaje;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Id
    private String iso;

    @Column(name = "ID_IMAGEN")
    private Long imageId;

    @Column(name = "CONTENT_TYPE")
    private String contenTypeImagen;

    @Id
    @Column(name = "TAG_ID")
    private Long tagId;

    @Column(name = "TAG")
    private String tagNombre;

    @Id
    @Column(name = "CATEGORIA_ID")
    private Long categoriaId;

    @Column(name = "CATEGORIA")
    private String categoriaNombre;

    private String nombre;

    private String periodo;

    @Column(name = "NOMBRE_CREADOR")
    private String creador;

    @Lob()
    private String programa;

    private String estado;

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_BAJA")
    private Date fechaBaja;

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_CREACION")
    private Date fechaCreacion;

    private String visible;

    @Column(name = "NUMERO_ACCESOS")
    private Long numeroAccesos;

    protected static CursoMultiIdiomaDAO cursoMultiIdiomaDAO;

    @Autowired
    protected void setCursoMultiIdiomaDAO(CursoMultiIdiomaDAO cursoMultiIdiomaDAO)
    {
        CursoMultiIdioma.cursoMultiIdiomaDAO = cursoMultiIdiomaDAO;
    }

    public CursoMultiIdioma()
    {
    }

    public String getDescripcion()
    {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion)
    {
        this.descripcion = descripcion;
    }

    public String getGuiaAprendizaje()
    {
        return this.guiaAprendizaje;
    }

    public void setGuiaAprendizaje(String guiaAprendizaje)
    {
        this.guiaAprendizaje = guiaAprendizaje;
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getIso()
    {
        return this.iso;
    }

    public void setIso(String iso)
    {
        this.iso = iso;
    }

    public String getNombre()
    {
        return this.nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getPeriodo()
    {
        return this.periodo;
    }

    public void setPeriodo(String periodo)
    {
        this.periodo = periodo;
    }

    public String getPrograma()
    {
        return this.programa;
    }

    public void setPrograma(String programa)
    {
        this.programa = programa;
    }

    public String getEstado()
    {
        return estado;
    }

    public void setEstado(String estado)
    {
        this.estado = estado;
    }

    public Date getFechaBaja()
    {
        return fechaBaja;
    }

    public void setFechaBaja(Date fechaBaja)
    {
        this.fechaBaja = fechaBaja;
    }

    public String getVisible()
    {
        return visible;
    }

    public void setVisible(String visible)
    {
        this.visible = visible;
    }

    public Date getFechaCreacion()
    {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion)
    {
        this.fechaCreacion = fechaCreacion;
    }

    public String getCreador()
    {
        return creador;
    }

    public void setCreador(String creador)
    {
        this.creador = creador;
    }

    public Long getImageId()
    {
        return imageId;
    }

    public void setImageId(Long imageId)
    {
        this.imageId = imageId;
    }

    public Long getTagId()
    {
        return tagId;
    }

    public void setTagId(Long tagId)
    {
        this.tagId = tagId;
    }

    public String getCategoriaNombre()
    {
        return categoriaNombre;
    }

    public void setCategoriaNombre(String categoriaNombre)
    {
        this.categoriaNombre = categoriaNombre;
    }

    public Long getCategoriaId()
    {
        return categoriaId;
    }

    public void setCategoriaId(Long categoriaId)
    {
        this.categoriaId = categoriaId;
    }

    public String getTagNombre()
    {
        return tagNombre;
    }

    public void setTagNombre(String tagNombre)
    {
        this.tagNombre = tagNombre;
    }

    public Long getNumeroAccesos()
    {
        return numeroAccesos;
    }

    public void setNumeroAccesos(Long numeroAccesos)
    {
        this.numeroAccesos = numeroAccesos;
    }

    public static List<CursoMultiIdioma> getCursosByIdsAndIdioma(List<Long> listaCursosIds,
            String idioma)
    {
        return cursoMultiIdiomaDAO.getCursosByIdsAndIdioma(listaCursosIds, idioma);
    }

    public static List<CursoMultiIdioma> getCursosByIdsAndIdiomaVistaPrevia(List<Long> listaCursosIds,
                                                                 String idioma)
    {
        return cursoMultiIdiomaDAO.getCursosByIdsAndIdiomaVistaPrevia(listaCursosIds, idioma);
    }

    public String getContenTypeImagen()
    {
        return contenTypeImagen;
    }

    public void setContenTypeImagen(String contenTypeImagen)
    {
        this.contenTypeImagen = contenTypeImagen;
    }

}