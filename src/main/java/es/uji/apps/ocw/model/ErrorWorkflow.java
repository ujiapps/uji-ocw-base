package es.uji.apps.ocw.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * The persistent class for the OCW_ERRORES_WORKFLOW database table.
 * 
 */
@Entity
@Table(name = "OCW_ERRORES_WORKFLOW")
public class ErrorWorkflow implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String error;

    private String fecha;

    @Column(name = "ID_ACTIVIDAD")
    private String idActividad;

    @Column(name = "ID_PROCESO")
    private String idProceso;

    public ErrorWorkflow()
    {
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getError()
    {
        return this.error;
    }

    public void setError(String error)
    {
        this.error = error;
    }

    public String getFecha()
    {
        return this.fecha;
    }

    public void setFecha(String fecha)
    {
        this.fecha = fecha;
    }

    public String getIdActividad()
    {
        return this.idActividad;
    }

    public void setIdActividad(String idActividad)
    {
        this.idActividad = idActividad;
    }

    public String getIdProceso()
    {
        return this.idProceso;
    }

    public void setIdProceso(String idProceso)
    {
        this.idProceso = idProceso;
    }

}