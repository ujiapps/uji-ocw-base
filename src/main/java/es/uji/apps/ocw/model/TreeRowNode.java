package es.uji.apps.ocw.model;

import java.util.Comparator;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import es.uji.commons.rest.model.tree.TreeRow;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
public class TreeRowNode extends TreeRow
{
    @XmlAttribute
    protected String orden;
    
    @XmlAttribute
    protected String tipo;

    @XmlAttribute
    protected String descripcion;

    public void setOrden(String orden)
    {
        this.orden = orden;
    }

    public String getOrden()
    {
        return orden;
    }
    
    public void setTipo(String tipo)
    {
        this.tipo = tipo;
    }

    public String getTipo()
    {
        return tipo;
    }

    public String getDescripcion()
    {
        return descripcion;
    }
    
    public void setDescripcion(String descripcion)
    {
        this.descripcion = descripcion;
    }
}
